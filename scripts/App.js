var app = (function() {
	var api = {
		views: {},
		models: {},
		collections: {},
		content: null,
		router: null,
		init: function() {
			this.content = $("#content");
			ViewsFactory.menu();
			ViewsFactory.footer();
//			this.myteam = new api.models.Player();

			Backbone.history.start();
			return this;
		},
		changeContent: function(el) {
			this.content.empty().append(el);
			
			return this;
		},
		title: function(str) {
			$("h1").text(str);
			return this;
		}
	};


	var ViewsFactory = {
		menu: function() {
			if(!this.menuView) {
				var headerInfo = new HeaderInfo();
				this.menuView = new api.views.menu({
					el: $("#menu"),
					model: headerInfo
				});
			}
			return this.menuView;
		},
		/*
			matchClockView: function() {
			if(!this.matchClockView) {
				var matchClock = new MatchClock();
				this.matchClockView = new api.views.matchClockView({
					el: $("#gameClock"),
					model: matchClock
				});
			}
			return this.matchClockView;
		},
		*/
		home: function() {
			if(!this.homeView) {
				this.homeView = new api.views.home({
					el: $(".maincontainer")
			//		model: api.myteam
				});
			}	
			return this.homeView;
			
		},
		play: function() {
			if(!this.playView) {
				var fixtureInfo = new HeaderInfo();
				setTimeout(function(){
				this.playView = new api.views.play({
					el: $(".maincontainer"),
					model: fixtureInfo
				});
				playerFetch();
				},100);
					
			}	
			return this.playView;
		},
		punditpicks: function() {
			console.log('hello1');
			if(!this.punditpickView) {
				this.punditpickView = new api.views.punditpick({
					el: $(".maincontainer")
			//		model: api.myteam
				});
			}	
			return this.punditpickView;
		},
		footer: function() {
			if(!this.footerView) {
				this.footerView = new api.views.footer({ 
					el: $("footer") 
				});
			}
			return this.footerView;
		}
	};

	var Router = Backbone.Router.extend({
		routes: {
			"": "home",
			"play": "play",
		},
		home: function() {
			var view = ViewsFactory.home();
			api
				.title('Test')
		  	.changeContent(view.$el);
			view.render();
			$('.homebadgecontainer,.awaybadgecontainer').show();
		},
		play: function() {
			var view = ViewsFactory.play();
			api
				.title("Team Select")
		//		.changeContent(view.$el);
		//	view.render()
		}
	});
	api.router = new Router();

	return api;

})();

