var removefromleague = 0,
		TeamChanged = 0,
		isDeposit = false,
		isUpgradeAccount = false;

(function(){

    // Localize jQuery variable
    var jQuery,
        gfmUtils = {};

    /******** Load jQuery if not present *********/
    if (window.jQuery === undefined) {
        //alert('jQuery is required');
    } else {
        // The jQuery version on the window is the one we want to use
        jQuery = window.jQuery;
        main();
    }

    gfmUtils.textMaxLength = function (element) {
        var maxLength = parseInt(element.getAttribute('maxlength'), 10);
        if (element.value.length >= maxLength){
            element.value = element.value.substr(0, maxLength-1);
        }
    }

    function main() {

        jQuery(document).ready(function($) {

            /*+++++++++++++++++++ GET SITE INFORMATION +++++++++++++++++*/
            $('#sba-logout').hide();

            var ssoDomain       = '';
            var cookieSession   = '';
            var cookieDetail    = '';
            var sTokenStore     = '';

						/* Check for cookies */
						function checkCookie(){
    					var cookieEnabled=(navigator.cookieEnabled)? true : false   
    					if (typeof navigator.cookieEnabled=="undefined" && !cookieEnabled){ 
        				document.cookie="testcookie";
        				cookieEnabled=(document.cookie.indexOf("testcookie")!=-1)? true : false;
    						}
    					return (cookieEnabled)?true:showCookieFail();
						}

						function showCookieFail(){
  						alertV2('to login, please enable cookies');
						}
						checkCookie();

            /* Get The base url */
            if(window.skyBaseUrl != undefined){
                ssoDomain = window.skyBaseUrl;
            }

            if(window.sessCookie != undefined){
                cookieSession = window.sessCookie;
            }

            if(window.detailCookie != undefined){
                cookieDetail = window.detailCookie;
            }

            var cookieValue = $.cookie(window.sessCookie);
            if(!cookieValue){
                window.whenLoggedIn = reloadPage;
            }

            var sPitchDisp = $.cookie(window.pitchCookie);
            if(sPitchDisp){
                oSwitchPitch(sPitchDisp);
            }

            if(cookieValue){
                $('#sba-login').hide();
                $('#sba-join').hide();
                $('#sba-logout').css('display','inline-block');
            }else{
                $('#sba-login').css('display','inline-block');
                $('#sba-join').css('display','inline-block');
                $('#sba-logout').hide();
            }

            var sba = window.SkyBetAccount;
            if (!sba) {
                //throw new Error('Could not find SkyBetAccount integration object');
            }else{

                function logoutUser(){
                    window.location.href = '/logout2/';
                    return;
                }
                
                function loginUser(sToken){
                    $('#sba-login').hide();
                    $('#sba-join').hide();
                    $('#sba-forgot').hide();
                    $('#sba-logout').hide();
                    loadJson('/json/login/?token='+sToken,loginSuccess);         
                    return;
                }

                function loginSuccess(data){
                	
                	  if (data && data.redirect) {               	  	
                	    window.location.href = data.redirect;
                	    return;
                	  }
                	
                    if(typeof window.whenLoggedIn == 'function'){
                        window.whenLoggedIn()
                    }
                    $('#sba-login').hide();
                    $('#sba-join').hide();
                    $('#sba-forgot').hide();
                    var Manager = $.parseJSON(decodeURI($.cookie(cookieDetail)));
                    $('#sba-logout').html(Manager.fname+'<span class="sprite icoLoggedin"></span>');
                    $('#sba-logout').css('display','inline-block');
                    if(typeof window.whenLoggedIn == 'function'){
                        window.whenLoggedIn()
                    }
                }

                if(cookieValue){
                    loginSuccess();
                }

                sba.bind('loggedIn', function(data) {
                    sTokenStore = data.user.ssoToken;
                    if(!cookieValue){
                        loginUser(data.user.ssoToken);
                    }else{
                        loginSuccess();
                    }
                });

                var iChanged = 0;
                sba.bind('userDataChange', function(data) {
                    //loginUser(sTokenStore);
                    iChanged = 1;
                });

                sba.bind('close', function(data) {
                    if(iChanged == 1 && (isDeposit === true || isUpgradeAccount === true)){
                    	isDeposit = false;
                    	isUpgradeAccount = false;
                    	window.whenLoggedIn = reloadPage;
                    	window.whenLoggedIn();
                    }
                    else if(iChanged == 1){
                        window.whenLoggedIn = reloadPage;
                        loginUser(sTokenStore);
                    }
                });

                sba.bind('loggedOut', function(data) {
                    if(cookieValue){
                        logoutUser();
                    }
                });

                sba.setup({
                    consumer: window.skyConsumer,
                    consumerUrl: 'https://'+window.location.host,
                    host:'www'+window.skyBaseUrl+'.skybet.com',
                    window:window
                });
                //    hasCookies:true

                // Get anchor URLs and target attribute values
                var loginUrl    = sba.getLoginUrl();
                var registerUrl = sba.getRegistrationUrl();
                var accountUrl  = sba.getAccountUrl();
                var loginTarget = sba.getLoginTarget();

                // Set up login button and register click event
                $('#sba-login, #CaroLogin').click(function(event){
					      //if(sba.requiresFullScreenRedirect && sba.requiresFullScreenRedirect() === false){
                        sba.openLogin();
                        return false;
                    //}
                    //var redirect = 'https://www'+window.skyBaseUrl+'.skybet.com/identity/redirect/index.php?consumer='+window.skyConsumer+'&returnUrl='+encodeURIComponent(window.location.href+"?open=login");
                    //window.location.href = redirect;
                    //return false;
                });

                $('#sba-logout').click(function(event){
                    //if(!sba.requiresFullScreenRedirect()){
                        sba.openMyAccount();
                        return false;
                    //}
                    //var redirect = 'https://www'+window.skyBaseUrl+'.skybet.com/identity/redirect/index.php?consumer='+window.skyConsumer+'&returnUrl='+encodeURIComponent(window.location.href+"?open=account");
                    //window.location.href = redirect;
                    //return false;
                });

                $('#sba-join').click(function(event){
                    //if(!sba.requiresFullScreenRedirect()){
                        sba.openRegistration();
                        return false;
                    //}
                    //var redirect = 'https://www'+window.skyBaseUrl+'.skybet.com/identity/redirect/index.php?consumer='+window.skyConsumer+'&returnUrl='+encodeURIComponent(window.location.href+"?open=register");
                    //window.location.href = redirect;
                    //return false;
                });

                $('.openUpgrade').click(function(){
                    //if(!sba.requiresFullScreenRedirect()){
                    		isUpgradeAccount = true;
                        sba.openUpgradeAccount();
                        return false;
                    //}
                    //var redirect = 'https://www'+window.skyBaseUrl+'.skybet.com/identity/redirect/index.php?consumer='+window.skyConsumer+'&returnUrl='+encodeURIComponent(window.location.href+"?open=upgrade");
                    //window.location.href = redirect;
                    //return false;
                });

                $('.openDeposit').click(function(){
                    //if(!sba.requiresFullScreenRedirect()){
                        isDeposit = true;
                        sba.openDeposit();
                        return false;
                    //}
                    //var redirect = 'https://www'+window.skyBaseUrl+'.skybet.com/identity/redirect/index.php?consumer='+window.skyConsumer+'&returnUrl='+encodeURIComponent(window.location.href+"?open=deposit");
                    //window.location.href = redirect;
                    //return false;
                });

                //var params = getParams();
                //if (params != undefined && params.open) {
                //    switch (params.open) {
                //        case "register":
                //            sba.openRegistration();
                //            break;
                //        case "login":
                //            sba.openLogin();
                //            break;
                //        case "account":
                //            sba.openMyAccount();
                //            break;
                //        case "upgrade":
                //            sba.openUpgradeAccount();
                //            break;
                //    }
                //}


                if($('#topPlayerArea').length){
                    $('#topPlayerArea .playerNav').click(function(event){
                        var pos = $(this).attr('href').match(/\d/g);
                        loadJson('/json/topplayers/'+pos,drawTopPlayers);
                    });

                    function drawTopPlayers(pageContent){
                        $('#topPlayerArea').html(pageContent.fixtures);
                        $('#topPlayerArea .playerNav').click(function(event){
                            var pos = $(this).attr('href').match(/\d/g);
                            loadJson('/json/topplayers/'+pos,drawTopPlayers);
                        });
                        $('#topPlayerArea a.icoStat').click(function(){
                            $.openPlayerStats($(this).attr('href'));
                            return false;
                        });
                    }
                }

                if($('#assitantArea').length){

                    $('#assitantArea .playerNav').click(function(event){
                        var iTipTeamPin = $('#tipTeamPin').val();
                        var pos = (TeamChanged) ? 1 : $(this).attr('href').match(/\d/g);
			                  var OnTransferPage=0;
			                  TeamChanged=0;
                	if($('#pageTransfer').length){OnTransferPage=1;}
                        loadJson('/json/toptips/'+iTipTeamPin+'/'+pos+'/'+OnTransferPage,drawTopTips);
                    });

                    function drawTopTips(pageContent){
                        $('#assitantArea').html(pageContent.tips);
                        $('#assitantArea .playerNav').click(function(event){
                            var iTipTeamPin = $('#tipTeamPin').val();
                            var pos = (TeamChanged) ? 1 : $(this).attr('href').match(/\d/g);
			                      var OnTransferPage=0;
			                      TeamChanged=0;
                	    if($('#pageTransfer').length){OnTransferPage=1;}
                            loadJson('/json/toptips/'+iTipTeamPin+'/'+pos+'/'+OnTransferPage,drawTopTips);
                        });
                        $('#assitantArea a.icoStat').click(function(event){
                            $.openPlayerStats($(this).attr('href'));
                            return false;
                        });

                    }
                }
            }

            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '142785632466600', // App ID
                    channelUrl : '//'+window.location.host+'/channel.html', // Channel File
                    status     : true, // check login status
                    cookie     : true, // enable cookies to allow the server to access the session
                    xfbml      : true  // parse XFBML
                });

                // calling the API ...
                var obj = {
                    method: 'feed',
                    picture: 'http://content.fantasyfootball.skysports.com/stff/images/generic/fbicon.jpg',
                    name: 'Sky Fantasy Football'
                };

                function callback(response) {
                    //alert("Post ID: " + response['post_id']);
                }

                $('.FBBtn').on('click', function(event){
                    obj.caption = $(this).attr('title');
                    obj.link    = $(this).attr('href');
                    FB.ui(obj, callback);
                    return false;
                });

            };

            // Load the SDK Asynchronously
            /*(function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/all.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));*/

            $('.TWBtn').on('click',function(event){
                window.open($(this).attr('href'));
                return false;
            });

            $('.bxslider').bxSlider({
                mode: 'fade',
                captions: false,
                responsive: false,
                controls: true,
                infiniteLoop: true,
                auto: true
            });

            if($("#countdown").length){

                var nextTime = $("#countdown").attr('data-time');
                if(nextTime == undefined){
                    nextTime = "2013/08/25 16:00:00";
                }
                
                // timezone was hard coded to 0 and needed manually changing for BST 
                // timezone needed to be set to a value of 1 for BST and 0 GMT
                // In order to make this automatic, we use the same timezone
                // calculation in jquery.countdown.js which returns -60 for BST
                // and convert it back to 1 for BST or 0 for GMT
                var timeOffset = new Date().getTimezoneOffset();
                timeOffset = timeOffset == 0 ? timeOffset : -(timeOffset / 60);
 
                $("#countdown").jCountdown({
        			timeText:nextTime,
        			timeZone: timeOffset, 
        			style:"slide",
        			color:"black",
        			width:190,
        			textGroupSpace:14,
        			textSpace:0,
        			reflection:false,
        			reflectionOpacity:10,
        			reflectionBlur:0,
        			dayTextNumber:2,
        			displayDay:true,
        			displayHour:true,
        			displayMinute:true,
        			displaySecond:false,
        			displayLabel:true,
        			onFinish:function(){
        				//alert("finish");
        			}
        		});
            }

            $('.pitch-switcher').click(function(){
                var iHeight = $(this).height();
                if(iHeight == 45){
                    $(this).animate({height:'21'},'fast',function(){
                        $('.pitch-switcher .btn-group').hide();
                    });
                }else{
                    $(this).animate({height:'45'},'fast');
                    $('.pitch-switcher .btn-group').show();
                }

            });
            $('.pitch-switcher .btn-group li').click(function(){
                $(".pitch-switcher .btn-group li").removeClass("selected");
                $(this).addClass("selected");
                var sOption = $('a',this).attr('href').substring(1);
                oSwitchPitch(sOption);
                return false;
            });

            $('select.selNav').change(function(){
                document.location = $('option:selected',this).attr('value');
            });

            $(".remove-me").click(function(){
                returnval=false;
                if (removefromleague != 1) {
                    removefromleague = -1;
                    //confirmV2("<p>Are you sure you want to remove this team from this league?</p>",".remove-me");
                    confirmV2("<p>Are you sure you want to remove this team from this league?</p>",$(this).attr("id"));
                } else {
                    window.location.href = $(this).attr("href");
                    removefromleague = 0;
                    returnval=true;
                }
                return returnval;
            });

            $('[placeholder]').focus(function() {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                    input.removeClass('placeholder');
                }
            }).blur(function() {
                var input = $(this);
                if (input.val() == '' || input.val() == input.attr('placeholder')) {
                    input.addClass('placeholder');
                    input.val(input.attr('placeholder'));
                }
            }).blur().parents('form').submit(function() {
                $(this).find('[placeholder]').each(function() {
                    var input = $(this);
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                })
            });

            if($('.statsPopup').length){

                var urlArr = new Array;
                var sUrl = parent.document.location.href.toUpperCase();
                urlArr = sUrl.split('/');

                if(urlArr[4] == 'TRANSFER' || urlArr[4] == 'NEW' || urlArr[4] == 'SELECT'){
                    $('#tranShow').hide();
                    $('#captShow').hide();
                };

                $('#tranShow').click(function(){
                    parent.document.location = $(this).attr('href');
                    return false;
                });
            }

            if($(".awardShelf").length){
                $('.awardShelf li img').click(function(event){
                    showModal($(this).attr('data-status'));
                });

                $('.awardShelf li span').click(function(event){
                    showModal($(this).attr('data-status'));
                });

                function showModal(sTitle){
                    var aText = sTitle.split("|");
                    $('.modal h2').text(aText[0]);
                    $('.modal p').text(aText[1]);
                    if(aText[2] == "Y"){
                        $('#badegeaward').removeClass('hide');
                    }else{
                        $('#badegeaward').addClass('hide');
                    }

                    var sSiteUrl      = "https://"+document.domain; 
                    var message       = "I've just unlocked '" + aText[0] + "' award on Sky Sports Fantasy Football";
                    var sTwitterText  = encodeURIComponent(message +" #SkySportsFF");
                    var sTwitterUrl   = "https://twitter.com/share?original_referer="+encodeURIComponent(sSiteUrl)+"&text="+sTwitterText+"&url="+encodeURIComponent(sSiteUrl);
                    var sFaceBookUrl  = 'https://www.facebook.com/dialog/feed?app_id=635591246508993&name=' + encodeURIComponent("Sky Fantasy Football") + '&redirect_uri=https%3A%2F%2Fm.fantasyfootball.skysports.com' + '&picture=' + encodeURIComponent("https://content.fantasyfootball.skysports.com/social/stff-facebook_large2.jpg") + "&description=" + encodeURIComponent(message);
                    var sEmailUrl     =  'mailto:?subject=Sky Fantasy Football&body=' + message;

                    var sTwitterUrl = "https://twitter.com/share?original_referer="+encodeURIComponent(sSiteUrl)+"&text="+sTwitterText+"&url="+encodeURIComponent(sSiteUrl);
                    $('.modal .TWBtn').attr('title',sTwitterText);
                    $('.modal .TWBtn').attr('href',sTwitterUrl);
                    $('.modal .FBBtn').attr('title',message);
                    $('.modal .FBBtn').attr('href',sFaceBookUrl);
                    $('.modal .EMBtn').attr('title',message);
                    $('.modal .EMBtn').attr('href',sEmailUrl);

                    $('.modal').removeClass('hide');

                }

                $('.modal a.icoLoggedClose').on('click', function(event){
                    $('.modal').addClass('hide');
                    return false;
                });


            };

            /*+++++++++++++++++++ GAMEWEEK CONTROL +++++++++++++++++++++*/

            if($("#STFFFixtCont").length){
                $("#fixtprev").click(function(){
                    var iPrevWeek = getViewWeekNo() - 1;
                    loadJson('/json/fixtures/'+iPrevWeek,drawFixtures);
                });

                $("#fixtnext").click(function(){
                    var iNextWeek = getViewWeekNo() + 1;
                    loadJson('/json/fixtures/'+iNextWeek,drawFixtures);
                });

                function getViewWeekNo(){
                    var txt = $('#STFFFixtCont .STFFsectHead').text();
                    var numbs = txt.match(/\d/g);
                    return parseInt(numbs.join(""));
                }

                function drawFixtures(pageContent){
                    $('#STFFFixtCont .STFFsectCont').html(pageContent.fixtures);
                    var txt = $('#fixttitle').text('Gameweek '+pageContent.weekno);
                }
            }

            /*+++++++++++++++++++ FACEBOOK LIKE ++++++++++++++++++++++++*/

            function alertV2(sMessage){
                $('#alertbox').html('<h2>Alert</h2><p>'+sMessage+'</p>');
                $.colorbox({width:"370", inline:true, href:"#alertbox", scrolling:false});
            }

            function confirmV2(sMessage,sTrigger){
                $('#alertbox').html('<h2>Confirm</h2><p>'+sMessage+'</p><p><span class="confirms" id="confirm-yes">Yes</span><span class="confirms" id="confirm-no">No</span></p>');
                $.colorbox({width:"370", inline:true, href:"#alertbox", scrolling:false});
                $("#confirm-yes").off("click").click(function(){
                    removefromleague = 1;
                    $("#cboxClose").trigger("click");
                    $("#"+sTrigger).trigger("click");
                });
                $("#confirm-no").off("click").click(function(){
                    $("#cboxClose").trigger("click");
                });
            }

            /*++++++++++++++++++++ LOGIN WIDGET ++++++++++++++++++++++++*/

            /* Set up tabs */
            $('div.sky-tab').tabs();

            /* Set up as an accordion */
            $('ul.sky-accordion').accordion();

            /* Set Up Drop Downs */
            $('.stff-dropdown').hover(
                function () {
                    //show its submenu
                    $('ul', this).slideDown(100);
                },
                function () {
                    //hide its submenu
                    $('ul', this).slideUp(100);
                }
            )

            //On Hover Out
            function HoverOut(){
                $(this).find(".sub").hide();  //after fading, hide it
                $('#topnav > li').removeClass('hover');
                $('#topnav').removeClass('hover');
            }

            function hoverOver(){
                $(this).find(".sub").show();
                $(this).addClass('hover');
                $('#topnav').addClass('hover');
            }

            $('#addToTeam').click(function(){
                var iPlayerId = $(this).attr('data-pla-id');
                $.selectPlayer(iPlayerId);
                return false;
            });

            $('#amendShortList').click(function(){
                var iPlayerId = $(this).attr('data-pla-id');
                if($(this).hasClass('redBtn')){
                    amendShortlist(iPlayerId,"remove");
                    $(this).addClass('whiteBtn').removeClass('redBtn').text('Add to Shortlist');
                }else{
                    amendShortlist(iPlayerId,"add");
                    $(this).addClass('redBtn').removeClass('whiteBtn').text('Remove from Shortlist');
                }
                return false;
            });

            $("ul#topnav > li").hover(hoverOver,HoverOut);

            $('#mailMates').click(function(){

                $.colorbox({href:$(this).attr('href'),width:"520px",height:"630px",iframe:true,scrolling:false,initialWidth:300,initialHeight:100});
                return false;
            });

            $('a.stff-popup-text').click(function(){
                var url = $(this).attr('href')+'popup/';
                $.colorbox({href:url,width:"720px",height:"720px",iframe:true,scrolling:false,initialWidth:300,initialHeight:100});
                return false;
            });

            if($('#createLeagueBtn').length){

                var regLeaguePin    = /^[0-9]{1,8}$/;
                var leagueJoinValid = 0;
                var leagueInfoStore = new Object;

                $('.optSwitch li').removeClass().addClass('lgreygrad').click(function(){
                    $(this).siblings().removeClass().addClass('lgreygrad');
                    $(this).removeClass().addClass('greygrad2');
                });

                $('#joinLeagueTeamSwitch li').click(function(){
                    var iLeagueTeampin = $(this).attr('data-teampin');
                    $('#joinLeagueTeam').attr('value',iLeagueTeampin);
                });

                $('#createLeagueTeamSwitch li').click(function(){
                    var iLeagueTeampin = $(this).attr('data-teampin');
                    $('#createLeagueTeam').attr('value',iLeagueTeampin);
                });
                
                $('#restartPaidLeagueTeamSwitch li').click(function(){
                    var iLeagueTeampin = $(this).attr('data-teampin');
                    $('#restartPaidLeagueTeam').attr('value',iLeagueTeampin);
                });
                
                $('#restartFreeLeagueTeamSwitch li').click(function(){
                    var iLeagueTeampin = $(this).attr('data-teampin');
                    $('#restartFreeLeagueTeam').attr('value',iLeagueTeampin);
                });

                $('#createLeagueTypeSwitch li').click(function(){
                    var sLeagueType = $(this).attr('data-leaguetype');
                    $('#createLeagueType').attr('value',sLeagueType);
                    if(sLeagueType == 'PLE'){
                        $('#pleDetails').show();
                    }else{
                        $('#pleDetails').hide();
                    }
                    $.colorbox.resize({});
                });
                
                $('#joinPrivateLeague').click(function () {                	
                	 $('#joinLeagueType').val('PL');     
                   if(!$('#joinLeaguePin').length){
                     $('#privateLeagueContainer').append("<input type=\"text\" value=\"\" name=\"leaguepin\" pattern=\"\\d*\" placeholder=\"League PIN\" id=\"joinLeaguePin\">"); 
                   }
                   if(!$('#joinLeaguePassword').length){
                     $('#joinLeaguePassCont').append("<input type=\"password\" name=\"leaguepassword\" id=\"joinLeaguePassword\" maxlength=\"10\">"); 
                   }                  
                   $('#joinLeaguePassCont').hide();           
                   $('#joinPublicLeagueType').remove();                	 
                });
                
                $('#joinPublicLeague').click(function () {
                	 $('#joinLeagueType').val('PB');
                	 if(!$('#joinPublicLeagueType').length){
                     $('#publicLeagueContainer').append("<input type=\"hidden\" value=\"CLA\" name=\"publicleaguetype\" id=\"joinPublicLeagueType\">"); 
                   }
                   $('#joinLeaguePin').remove();
                   $('#joinLeaguePassword').remove();
                });
                               
                $('#joinPublicLeagueTypeSwitch li').click(function(){
                    var sLeagueType = $(this).attr('data-leaguetype');
                    $('#joinPublicLeagueType').attr('value',sLeagueType);
                });

                $('#joinLeaguePin').keyup(function(){                	  
                    var iLeaguePin = $(this).attr('value');
                    //alert(iLeaguePin.length);
                    if (iLeaguePin.length == 7){

                        if(!regLeaguePin.test(iLeaguePin)){
                            joinleagueError('Please enter a valid league pin');
                            return false;
                        }
                        if(leagueInfoStore[iLeaguePin] == undefined){
                            $.loadLeagueDetail(iLeaguePin,leagueDetailReturn);
                        }
                        hideleagueError();
                    }
                });

                $('#joinLeagueSubmit').click(function(){
                    return validateJoinLeague();
                });

                $('#createLeagueSubmit').click(function(){
					         return validateCreateLeague();
                });

                $('#createLeagueBtn').click(function(){
                    if($('#createLeague .errorMsg p').text() == ""){
                        $('#createLeague .errorMsg').hide();
                    }
                    var leagueType = $('#createLeagueType').val();
                    $.colorbox({width:"370", inline:true, href:"#createLeague", scrolling:false, transition:"none"});
                });

                $('.restartBtn').click(function(){
                	  var iLeaguepin      = $(this).attr('href');
                    var iLeaguepinArray = iLeaguepin.split('/');
                    var iLeaguepin      = iLeaguepinArray[iLeaguepinArray.length - 1];                    
                    var iLeagueName     = $(this).parent().parent().parent().find('>h4').text();        
                    var type = ($(this).attr('data-type')); 
                    if (type == "LTFREE") {
                    	$('#restartFreeLeagueName').val(iLeagueName);
                      $('#restartFreeLeaguePin').val(iLeaguepin);  
                      $.colorbox({width:"370", inline:true, href:"#restartFreeLeague", scrolling:false, transition:"none"});
                    } else if (type == "LTPAIDFR") {
                      $('#restartPaidLeagueName').val(iLeagueName);                
                      $('#restartPaidLeaguePin').val(iLeaguepin);      
                    	$.colorbox({width:"370", inline:true, href:"#restartPaidLeague", scrolling:false, transition:"none"});
                    }
                    return false;
                });

                $('#joinLeagueBtn').click(function(){
                    if($('#joinLeague .errorMsg p').text() == ""){
                        $('#joinLeague .errorMsg').hide();
                    }
                    $.colorbox({height:"690px", width:"370", inline:true, href:"#joinLeague", scrolling:false, transition:"none"});
                });

                if($('#joinLeague .errorMsg p').text() != ""){
                    $('#joinLeagueBtn').trigger('click');
                }

                if($('#createLeague .errorMsg p').text() != ""){
                    $('#createLeagueBtn').trigger('click');
                }
                
                if($('#createLeague .alertMsg p').text() != ""){
                    $('#createLeagueBtn').trigger('click');
                }

                if($('#createLeagueType').attr('value') == 'PLE'){
                    $('#createLeagueTypeSwitch li:last').trigger('click');
                }else{
                    $('#createLeagueTypeSwitch li:first').trigger('click');
                }

                if($('#joinLeagueTeam').attr('value')){
                    var iCurrTeamPin = $('#joinLeagueTeam').attr('value');
                    $('#joinLeagueTeamSwitch li').each(function(){
                        if($(this).attr('data-teampin') == iCurrTeamPin){
                            $(this).trigger('click');
                        }
                    });
                }else{
                    $('#joinLeagueTeamSwitch li:first').trigger('click');
                }

                if($('#createLeagueTeam').attr('value')){
                	var iCurrTeamPin = $('#createLeagueTeam').attr('value');
                    $('#createLeagueTeamSwitch li').each(function(i){
                        if($(this).attr('data-teampin') == iCurrTeamPin){
                            $(this).trigger('click');
                        }
                    });
                }else{
                    $('#createLeagueTeamSwitch li:first').trigger('click');
                }
               
                if($('#restartPaidLeagueTeam').attr('value')){
                	var iCurrTeamPin = $('#restartPaidLeagueTeam').attr('value');
                    $('#restartPaidLeagueTeamSwitch li').each(function(i){
                        if($(this).attr('data-teampin') == iCurrTeamPin){
                            $(this).trigger('click');
                        }
                    });
                }else{
                	  $('#restartPaidLeagueTeamSwitch li:first').trigger('click');
                }
                
               if($('#restartFreeLeagueTeam').attr('value')){
                	var iCurrTeamPin = $('#restartFreeLeagueTeam').attr('value');
                    $('#restartFreeLeagueTeamSwitch li').each(function(i){
                        if($(this).attr('data-teampin') == iCurrTeamPin){
                            $(this).trigger('click');
                        }
                    });
                }else{
                    $('#restartFreeLeagueTeamSwitch li:first').trigger('click');
                }
                
                if($('#joinLeagueTypeSwitch').attr('value')){
                	var iCurrTeamPin = $('#restartPaidLeagueTeam').attr('value');
                    $('#joinLeagueTypeSwitch li').each(function(i){
                        if($(this).attr('data-teampin') == iCurrTeamPin){
                            $(this).trigger('click');
                        }
                    });
                }else{
                	  $('#joinLeagueTypeSwitch li:first').trigger('click');
                }               
                
                if($('#joinPublicLeagueType')){
                	$('#joinPublicLeagueTypeSwitch li:first').trigger('click');
                }

                function joinleagueError(sMessage){
                    $('#joinLeague .errorMsg p').text(sMessage);
                    $('#joinLeague .errorMsg').show();
                    $.colorbox.resize({});
                }

                function hideleagueError(){
                    $('#joinLeague .errorMsg').hide();
                    $('#createLeague .errorMsg').hide();
                    $.colorbox.resize({});
                }

                function createleagueError(sMessage){
                    $('#createLeague .errorMsg p').text(sMessage);
                    $('#createLeague .errorMsg').show();
                    $.colorbox.resize({});
                }

                function validateJoinLeague(){
                	  if ($('#joinLeagueType').attr('value') === 'PL') {
                      var iLeaguePin = $('#joinLeaguePin').attr('value');
                      if(!regLeaguePin.test(iLeaguePin)){
						             alert ('Invalid league pin');
                         return false;
                      }
                    }
                    return true;
                }

                function validateCreateLeague(){
					        if ($('#createLeagueName').val() === "" || $('#createLeagueName').val() === "League name" ) {
						        alert ('Please enter a league name');
                    return false;
					        } else if ($('#createLeagueName').attr('value').match(/^\s+$/)) {
					          alert ('Invalid league name');
                    return false;					        	
					        } else if ($('#createLeagueName').attr('value').match(/[^A-z0-9\ \-\!\']/)) {
					          alert ('Invalid league name');
                    return false;		
                  }
					        
					        if ($('#createLeagueType').val() === "PLE") {
						        if ($('#createLeaguePassword').val() === "") {
							        alert ('Please enter a league password');
							        return false;
						        }
					        }
                  return true;
                }

                function leagueDetailReturn(data){
                    if(data.error){
                        joinleagueError(data.error);
                        return;
                    }
                    leagueInfoStore[data.leaguepin] = data.paid;
                    if(data.paid == "Y"){
                        $('#joinLeaguePassCont').show();
                    }else{
                        $('#joinLeaguePassCont').hide();
                    }
                    $.colorbox.resize({});
                }
            }

            $('a.icoStat').click(function(){
                $.openPlayerStats($(this).attr('href'));
                return false;
            });

            $('a.teamPopup').click(function(){
                var url = $(this).attr('href');
                $.colorbox({href:url,width:"720px",height:"520px", iframe:true, scrolling:false,initialWidth:300,initialHeight:100});
                return false;
            });

            $.openPlayerStats = function(plaId){
                var regPlayerId    = /^[0-9]{4}$/;
                if(!regPlayerId.test(plaId)){
                    var urlArr = plaId.split('/');
                    plaId = urlArr[urlArr.length-1];
                }

                var url = '/statistics/popup/player/'+plaId;
                $.colorbox({href:url,width:"650px",height:"570px", iframe:true, scrolling:false,initialWidth:300,initialHeight:100});
            }

            $.loadLeagueDetail = function(iLeaguePin,callBack){
                $.ajaxSetup({cache:true,timeout:10000});
                $.getJSON("/json/league/detail/"+iLeaguePin, function(data){
                    callBack(data);
                }).success(function() {
                }).error(function(xhr, ajaxOptions, thrownError) {
                }).complete(function() {
                });
            }

            $.loadPlayers = function(callBack){

                var oPlayers = new Object;
                /* We need to load the team */
                $.ajaxSetup({cache:true,timeout:10000});
                $.getJSON("/json/players/", function(data){

                    // Looks for the error message;
                    if(data.error){
                        alert(data.error);
                        return;
                    }

                    /* sets up or updates the player information */
                    json_players    = data.players;
                    json_team       = data.team;
                    json_teamname   = data.teamname;
                    json_group      = data.group;

                    var iGroupLen = json_group.length;
                    while(iGroupLen--) {
                        var sGroup = json_group[iGroupLen];
                        var plaGroup = json_players[iGroupLen];
                        var iPlaLen = plaGroup.length;
                        while(iPlaLen--) {

/*
0 $iPlaRefId,
1 "$rPlayer->{NAME}",
2 $rTeamMap->{$rPlayer->{TEAM}},
3 $sNewValue,
4 "$sStatus",
5 $rPlayer->{SCORE},
6 $rPlayer->{STARTINGXI},
7 $rPlayer->{SUB},
8 $rPlayer->{GOALS},
9 $rPlayer->{ASSISTS},
10 $rPlayer->{WINBONUS},
11 $rPlayer->{DRAWBONUS},
12 $rPlayer->{YELLOW},
13 $rPlayer->{RED},
14 $rPlayer->{CLEANSHEET},
15 $rPlayer->{CONCEED},
16 $rPlayer->{OWNGOAL},
17 $rPlayer->{SAVEDPEN},
18 $rPlayer->{MISSEDPEN},
19 $rPlayer->{MOTM},
20 $rPlayer->{TACKLEBONUS},
21 $rPlayer->{PASSINGBONUS},
22 $rPlayer->{PICKED},
23 "$rPlayer->{AVAILABILITY}"
24 PERMILL
25 SAVEDBONUS */


                            var currPlayer      = plaGroup[iPlaLen];
                            var newPlayer       = new oPlayer;
                            newPlayer.name      = currPlayer[1];
                            var nameArr = newPlayer.name.split(",");
                            newPlayer.surname   = nameArr[0];
                            newPlayer.team      = json_team[currPlayer[2]];
                            newPlayer.teamname  = json_teamname[currPlayer[2]];
                            newPlayer.value     = (currPlayer[3]/10).toFixed(1);
                            if(currPlayer[4] == "C"){
                                newPlayer.status = 'RSCURENT';
                            }else{
                                newPlayer.status = 'RSSUSPEN';
                            }
                            newPlayer.points    = currPlayer[5];
                            newPlayer.sxi       = currPlayer[6];
                            newPlayer.sxi       = currPlayer[7];
                            newPlayer.played    = currPlayer[6]+currPlayer[7];
                            newPlayer.goals     = currPlayer[8];
                            newPlayer.assists   = currPlayer[9] || 0;
                            newPlayer.yellow    = currPlayer[12];
                            newPlayer.red       = currPlayer[13];

                            newPlayer.cleansheet= currPlayer[14] || 0;
                            newPlayer.conceed   = currPlayer[15];
                            newPlayer.savedpen  = currPlayer[17] || 0;

                            newPlayer.motm      = currPlayer[19];
                            newPlayer.tackle    = currPlayer[20] || 0;
                            newPlayer.passing   = currPlayer[21] || 0;
                            newPlayer.picked    = currPlayer[22];
                            newPlayer.avail     = currPlayer[23];

                            newPlayer.permill   = currPlayer[24];
                            newPlayer.savebonus = currPlayer[25] || 0;

                            newPlayer.g = sGroup;
                            newPlayer.i = 'Y';

                            oPlayers[currPlayer[0]] = newPlayer;

                        }
                        //Do something
                    }
                    callBack(oPlayers);

                }).success(function() {
                }).error(function(xhr, ajaxOptions, thrownError) {
                }).complete(function() {
                });
            }

            function oSwitchPitch(sPitchId){
                $(".myteam-backgrounds div").hide();
                $("."+sPitchId).fadeIn(200);
                $.cookie(window.pitchCookie,sPitchId);
                if(sPitchId != 'pitch-theme1'){
                    $('#lterms,#autosel,#resetteam,#saveteam,#stff-agree-text,#teamViewKey td').addClass('lightText')
                }else{
                    $('#lterms,#autosel,#resetteam,#saveteam,#stff-agree-text,#teamViewKey td').removeClass('lightText')
                }
            }

            function oPlayer(){
                this.name       = '';
                this.team       = 'UKN';
                this.value      = 0;
                this.group      = 'G';
                this.points     = 0;
                this.pickedk    = 0;
                this.status     = 'RSSUSPEN';
                this.injured    = 'N';
                this.suspended  = 'N';
            }

            jQuery.fn.outerHTML = function(s) {
                return (s)
                ? this.before(s).remove()
                : $("<div>").append(this.eq(0).clone()).html();
            };

            /*+++++++++++++++++++ VIEW TEAM  +++++++++++++++*/

            $('a.view-team').click(function(){
                var url = $(this).attr('href');
                $.colorbox({href:url,width:"700px", height:"470px", iframe:true, scrolling:false,initialWidth:300,initialHeight:100});
                return false;
            });

            /*+++++++++++++++++++ PLAYER STATS ATTRIBUTE +++++++++++++++*/

            $('.stff-pla-stats, .stff-stat-top3-ico').click(function(){
                $.openPlayerStats($(this).attr('href'))
                return false;
            });

            /*+++++++++++++++++++ HOW TO PLAY ICON +++++++++++++++++++++*/

            $('#howtoplay').click(function(){
                var url = '/how-to-play/popup/';
                $.colorbox({href:url,width:"720px",height:"740px",iframe:true,scrolling:false,initialWidth:300,initialHeight:100});
                return false;
            });

            /*+++++++++++++++++++ MENU DROP DOWN CONTROL +++++++++++++++*/

            $('table.sortable').sortable({createClass: 'grey',callback:function(iColNo){

                $('a.icoStat').click(function(){
                    $.openPlayerStats($(this).attr('href'));
                    return false;
                });

            }});

            /*+++++++++++++++++++ SKY OMINTURE WRAPPER +++++++++++++++*/

            /**
             * Constructor
             *
             * @param {Object} options Sky basic basic visit parameters
             * @param {Function} logger Logging class
             */
            function Omniture(options, logger) {
                this.options = options;
                this.logger = logger;
                this.counters = {};
                this.registeredEvents = {};

                this.initOmniture();
            };

            /**
             * Log a message
             *
             * @param string level
             * @param string message
             */
            Omniture.prototype.log = function(level, message) {
                if (this.logger && this.logger[level]) {
                    this.logger[level]('[omniture] ' + message);
                }
            };

            /**
             * Initialise Omniture and fire basic page visit tracking
             */
            Omniture.prototype.initOmniture = function() {

                if (this.options == undefined){
                    return;
                }

                var account = this.options.account;
                if (!account) {
                    this.log('error', 'Could not initialise as account name was not provided');
                    return;
                }

                if ('undefined' == typeof s_gi) {
                    this.log('info', 'Could not initialise as Omniture factory is not available');
                    return;
                }

                this.omni = s_gi(account);
            };

            /**
             * Set an option
             *
             * @param {String}        key
             * @param {String|Number} value
             */
            Omniture.prototype.setOption = function(key, value) {
                if (!this.omni) {
                    return;
                }

                this.omni[key] = value;
                this.log('debug', 'Set option [' + key + '] to [' + value + ']');
                return this;
            };


            /**
             * Set a custom variable
             *
             * Variables are available between in the range 1 - 75
             *
             * @param {Integer} counterId
             */
            Omniture.prototype.setVariable = function(variableId, value) {
                if (this.counters[variableId]) {
                    this.log('error', 'Could not track variable at ' + variableId + ' - ID in use as counter');
                    return;
                }

                if (!this.omni) {
                    return;
                }

                var omni = this.omni;
                var name = 'eVar' + variableId;

                omni[name] = value;
                this.log('debug', 'Set variable at ID ' + variableId + ' to ' + omni[name]);

                return this;
            };

            /**
             * Register a mapping of event names to their event codes
             *
             * @param {Object} eventData
             */
            Omniture.prototype.registerEvents = function(eventData) {
                var self = this;
                $.each(eventData, function(name,eventId) {
                    self.registerEvent(name,eventId);
                });
                return this;
            };

            /**
             * Register a mapping of an event name to its event code
             *
             * The name can be used later when firing an event.
             *
             * @param {String} name
             * @param {String} eventId
             */
            Omniture.prototype.registerEvent = function(name, eventId) {
                if (undefined !== this.registeredEvents[name]) {
                    this.log(
                        'error',
                        'Event ID [' + name + '] has already been registered with value [' +
                            this.registeredEvents[name]
                    );
                    return this;
                }

                this.registeredEvents[name] = eventId;

                return this;
            };

            /**
             * Get an Event ID by the name it was registered against
             *
             * @param {String} name
             *
             * @return {String}
             */
            Omniture.prototype.getEvent = function(name) {
                if (undefined === this.registeredEvents[name]) {
                    this.log('error', 'Could not get event [' + name + '] as it has not been registered');
                    return false;
                }
                return this.registeredEvents[name];
            };

            /**
             * Fire an event(s) by a name or array of names
             *
             * @param {String|String[]} eventNames
             */
            Omniture.prototype.fireEvent = function(eventNames) {
                if (!this.omni) {
                    return;
                }
                eventNames = $.isArray(eventNames) ? eventNames : [eventNames];

                var eventIds = [];
                var self = this;
                $.each(eventNames, function(i,eventName) {
                    eventIds.push(self.getEvent(eventName));
                });

                var events = eventIds.join(',');

                this.log('debug', 'Send events: ' + eventNames.join(',') + ' (' + events + ')');

                this.omni.events = events;
                this.omni.t();

                return this;
            };

            // Pass to context
            window.Omniture = Omniture;

            var omniture = new window.Omniture(window.SKY_TRACKING);
            omniture.registerEvents({
                'submitTeam':           'event9',
                'startLeague':          'event10',
                'startFreeLeague':      'event11',
                'completeFreeLeague':   'event12',
                'startPaidLeague':      'event13',
                'completePaidLeague':   'event14',
                'startJoinLeague':      'event63',
                'completeJoinPaid':     'event64',
                'completeJoinFree':     'event65'
            });

            window.OmniObj = omniture;
            //omniture.fireEvent('betslipOpen');

        });

        function loadJson(sContentUrl,oSuccessFunc){
            /* Load Content and return */
            $.ajaxSetup({cache:false,timeout:10000});
            $.getJSON(sContentUrl, function(data){

                // Looks for the error message;
                if(data.error){
                    alert(data.error);
                    return;
                }

                oSuccessFunc(data)

            }).success(function() {

            }).error(function(xhr, ajaxOptions, thrownError) {

            }).complete(function() {

            });
        }

        function reloadPage(){
            location.reload();
        }

        function getParams() {
            var query = window.location.search.substring(1);
            var oRtnObj = new Object;
            var vars = query.split('&');
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split('=');
                oRtnObj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
            }
            return oRtnObj;
        }

        function amendShortlist(iPlayerId,sAction){

            /* We need to load the players */
            $.ajaxSetup({cache:true,timeout:10000});
            $.getJSON("/json/team/shortlist/"+sAction+"/"+iPlayerId, function(data){
                // Looks for the error message;
                if(data.error){
                    alert(data.error);
                    return;
                }
                //oOnComplete();
            }).success(function() {
            }).error(function(xhr, ajaxOptions, thrownError) {
            }).complete(function() {
            });
        }
    }
})();

// Load the Twitter SDK Asynchronously
(function(d){
    var js, id = 'twitter-jssdk'; if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//platform.twitter.com/anywhere.js?id=qAlPKUgkKOD6w0CLRTk9CA&v=1";
    d.getElementsByTagName('head')[0].appendChild(js);
}(document));

/*!
 * jQuery Cookie Plugin v1.3.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */

(function (factory) {
	if (typeof define === 'function' && define.amd && define.amd.jQuery) {
		// AMD. Register as anonymous module.
		define(['jquery'], factory);
	} else {
		// Browser globals.
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function raw(s) {
		return s;
	}

	function decoded(s) {
		return decodeURIComponent(s.replace(pluses, ' '));
	}

	function converted(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}
		try {
			return config.json ? JSON.parse(s) : s;
		} catch(er) {}
	}

	var config = $.cookie = function (key, value, options) {

		// write
		if (value !== undefined) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setDate(t.getDate() + days);
			}

			value = config.json ? JSON.stringify(value) : String(value);

			return (document.cookie = [
				encodeURIComponent(key), '=', config.raw ? value : encodeURIComponent(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '; path=/',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// read
		var decode = config.raw ? raw : decoded;
		var cookies = document.cookie.split('; ');
		var result = key ? undefined : {};
		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			var name = decode(parts.shift());
			var cookie = decode(parts.join('='));

			if (key && key === name) {
				result = converted(cookie);
				break;
			}

			if (!key) {
				result[name] = converted(cookie);
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		if ($.cookie(key) !== undefined) {
			$.cookie(key, '', $.extend(options, { expires: -1 }));
			return true;
		}
		return false;
	};

}));

/* accordion */

(function( $ ){
    var methods = {
		init : function(options) {

            var settings = $.extend( {
    			onClass: 'on',
                hoverClass: 'hover',
                defaultElement: 0,
                speed: 'fast'
            }, options);

            return this.each(function(){
                var $accoObj = $(this);
                $accoObj.data('gfmacco-options',settings);
                $accoObj.accordion("closeAll");

                $accoObj.accordion("openItem",settings.defaultElement);

                $accoObj.children('li').children('h4').each(function(intIndex){
                    $(this).bind("click",function(){
                        if($(this).parent().hasClass(settings.onClass)){
                            $accoObj.accordion('closeAll');
                            $(this).parent().addClass(settings.hoverClass);
                        }else{
                            $accoObj.accordion('toggleItem',intIndex);
                        }
                    });

                    $(this).bind("mouseover",function(){
                        if($(this).parent().hasClass(settings.onClass)){
                        }else{
                            $(this).parent().addClass(settings.hoverClass);
                        }
                    });

                    $(this).bind("mouseout",function(){
                        $(this).parent().removeClass(settings.hoverClass);
                    });

                });
            });
		},

		destroy: function() {
            return this.each(function(){
                $(window).unbind('.accordion');
            });
		},

        closeAll: function() {
            var $accoObj = $(this),
                options = $accoObj.data('gfmacco-options');

            /* slide all up */
            $accoObj.find('li>div').slideUp(options.speed,function(){});
            $accoObj.find('li.'+options.onClass).removeClass(options.onClass);
            return self;
        },

        openItem: function(iIndex){
            var $accoObj = $(this),
                options = $accoObj.data('gfmacco-options');

            /* look for any that may be on */
            $accoObj.children('li').eq(iIndex).addClass(options.onClass).children('div').slideDown(options.speed);
        },

        toggleItem: function(iIndex){
            var $accoObj = $(this),
                options = $accoObj.data('gfmacco-options');

            if($accoObj.find('li.on>div').length == 0){
                $accoObj.accordion('openItem',iIndex);
                return;
            }

            $accoObj.find('li.on').removeClass(options.onClass).children('div').slideUp(options.speed,function(){
                $accoObj.accordion('openItem',iIndex);
            });
        }
	};

     $.fn.accordion = function( method ) {

        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on GFM.accordion' );
        }
    };

})( jQuery );

/* Tabs */

(function( $ ){
    var methods = {
        init : function( options ) {

            var settings = $.extend( {
    			defaultClass: 'on',
                defaultElement: 0,
                changetab: undefined
            }, options);

            return this.each(function(){
                var $tabsObj = $(this);
                $tabsObj.data('gfmtabs-options',settings);

                var iCurrOpen = settings.defaultElement;

                $tabsObj.children('ul').find('li a').not('[target]').each(function(intIndex){
                    $(this).bind("click",function(){
                        $tabsObj.tabs('openItem',intIndex);
                        return false;
                    }).bind("mouseenter",function(){
                        //alert('over')
                    });

                    if($(this).parent().hasClass('on')){
                        iCurrOpen = intIndex;
                    }

                });
                $tabsObj.tabs('openItem',iCurrOpen);
            });

        },
        destroy : function( ) {

            return this.each(function(){
                $(window).unbind('.gfmtabs');
            });

        },
        openItem: function(iIndex){

            var $tabsObj = $(this),
                options = $tabsObj.data('gfmtabs-options');

            /* look for any that may be on */
            $tabsObj.children('ul').find('li').removeClass(options.defaultClass);
            $tabsObj.children('ul').find('li').eq(iIndex).addClass(options.defaultClass);

            $tabsObj.children('div').css('display','none');
            if($tabsObj.children('div').eq(iIndex).hasClass('gfmtab-nofade')){
                 $tabsObj.children('div').eq(iIndex).show();
            }else{
                $tabsObj.children('div').eq(iIndex).fadeIn().addClass('gfmtab-nofade');
            }

            // call back if there is one
            if(typeof options.changetab == 'function'){
                options.changetab(iIndex);
            }
        }
    };

    $.fn.tabs = function( method ) {

        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
        }
    };

})( jQuery );

/* Sortable tables */

(function( $ ){

   var oTimeOut = 0;

   var methods = {
        init: function(options) {

            var settings = $.extend( {
    			defaultClass: 'on',
                defaultElement: 0,
                zebraClass: '',
                speed: 'fast',
                callback: undefined,
                beforesort: undefined,
                startsort:1,
                startdesc:'ASC',
                columnDefaults:[0,0,1,1,1,1,1,1]
            }, options);

            return this.each(function(){
                var $sortObj = $(this);

                // Create click event to all the form th elements..
                $sortObj.find('th').each(function(intIndex){
                    $(this).bind("click", function(){
                        $sortObj.sortable("sort",intIndex);
                    });
                });

                if($sortObj.attr("data-direc")){
                    settings.startdesc = $sortObj.attr("data-direc");
                }else{
                    $sortObj.attr("data-direc",settings.startdesc);
                }

                if($sortObj.attr("data-lastclick")){
                    settings.startsort = $sortObj.attr("data-lastclick");
                }else{
                    $sortObj.attr("data-lastclick",settings.startsort);
                }

                $sortObj.data("sortable-options",settings);

                // Sort table
                $sortObj.sortable("sort",settings.startsort);

            });
        },

        destroy: function() {
            return this.each(function(){
                $(window).unbind('.sortable');
            });
        },

        sort: function(iColNo){

            var self = this,
                o = $(this).data('sortable-options'),
                el = $(this);

            // call back if there is one
            if(typeof o.beforesort == 'function'){
                o.beforesort(iColNo);
            }

        	var sortAscending 	= 0,
        	    sTableId 	    = 1;

        	var trEls = el.find('tbody tr').map(function(){return this}).get();

        	var originalCol = new Array();

        	// Now loop through all the data row elements
            var i = trEls.length;
        	while(i--){
        		// Text value of the selected column on this row
        		var valueCol = $(trEls[i].cells[iColNo]).text();
        		// Format text value for sorting depending on its type, ie Date, Currency, number, etc..
        		valueCol = el.sortable('formatForType',valueCol);

        		// Assign the column value AND the row number it was originally on in the table
                var sTmp = String(trEls[i].outerHTML);
                originalCol[i] = [valueCol,sTmp];
        	}
            var OrderMe = '';

            if(el.attr('data-lastclick')!=iColNo){
                el.attr('data-lastclick',iColNo)
                sortAscending = o.columnDefaults[iColNo];
                if(sortAscending == 1){
                    el.attr("data-direc","ASC");
                }else{
                    el.attr("data-direc","DESC");
                }
            }else{
                if(el.attr("data-direc") == "DESC"){
                    sortAscending = 1;
                    el.attr("data-direc","ASC");
                }else{
                    sortAscending = 0;
                    el.attr("data-direc","DESC");
                }
            }

            // Function used by the sort routine to compare the current value in the array with the next one
            function sortCompare(a,b) {
                // Since the elements of this array are actually arrays themselves, just sort
                // on the first element which contiains the value, not the second which contains
                // the original row position
                if ( a[0] == b[0] ) return 0;
                if ( a[0] < b[0] )  return -1;
                if ( a[0] > b[0] )  return 1;

            }

        	var sortCol = originalCol.sort(sortCompare);

        	if (sortAscending) { sortCol.reverse(); }

        	var tBody = el.find('tbody');

        	var i = trEls.length;
            var s=[];
        	while(i--){
        		 //var old_row = sortCol[i][1];
                 s.push(sortCol[i][1])
                 //sHTML += sortCol[i][1];
        	}
            var sHTML = s.join("");
            tBody.html(sHTML);

        	if(o.zebraClass) {
        	    tBody.find('tr:even').addClass(o.zebraClass);
        		tBody.find('tr:odd').removeClass(o.zebraClass);
        	}

            var oThead = $("tr th:nth-child("+(iColNo+1)+")",el);
            var sClass = "dn";
            if(sortAscending == 1){
                sClass = "up";
            }
            $("th",el).removeClass("up").removeClass("dn");
            oThead.addClass(sClass);

            // call back if there is one
            if(typeof o.callback == 'function'){
                // it slow is down proper so wait for screen to refresh then act
                if(oTimeOut != undefined){
                    clearTimeout(oTimeOut);
                }
                oTimeOut = setTimeout(function(){
                    o.callback(iColNo);
                },100);
            }

        },

        formatForType: function(itm) {

            var sortValue = itm.toLowerCase();

            // If the item matches a Percent patten (contains a percent sign)
            if (itm.match(/%/)) {
                // Replace anything that is not part of a number (decimal pt, neg sign, or 0 through 9) with an empty string.
                sortValue = itm.replace(/[^0-9.-]/g,'');
                sortValue = parseFloat(sortValue);
                return sortValue;
            }

            // If item starts with a "(" and ends with a ")" then remove them and put a negative sign in front
            if (itm.substr(0,1) == "(" & itm.substr(itm.length - 1,1) == ")") {
                itm = "-" + itm.substr(1,itm.length - 2);
            }

            // If the item matches a currency pattern (starts with a dollar or negative dollar sign)
            if (itm.match(/\u00a3/)) {
            //if (itm.match(/^[&]|(^-)/)) {

                // Replace anything that is not part of a number (decimal pt, neg sign, or 0 through 9) with an empty string.
                sortValue = itm.replace(/[^0-9.-]/g,'');
                if (isNaN(sortValue)) {
                    sortValue = 0;
                } else {
                    sortValue = parseFloat(sortValue);
                }
                return sortValue;
            }

            // If the item matches a numeric pattern
            if (itm.match(/(\d*,\d*$)|(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/)) {
                // Replace anything that is not part of a number (decimal pt, neg sign, or 0 through 9) with an empty string.
                sortValue = itm.replace(/[^0-9.-]/g,'');
                //  sortValue = sortValue.replace(/,/g,'');
                if (isNaN(sortValue)) {
                    sortValue = 0;
                } else {
                    sortValue = parseFloat(sortValue);
                }
                return sortValue;
            }

           return sortValue;

        }
    }

    $.fn.sortable = function( method ) {

        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
        }
    };

})( jQuery );

/*
 * jQuery dropdown: A simple dropdown plugin
 *
 * Inspired by Bootstrap: http://twitter.github.com/bootstrap/javascript.html#dropdowns
 *
 * Copyright 2013 Cory LaViska for A Beautiful Site, LLC. (http://abeautifulsite.net/)
 *
 * Dual licensed under the MIT / GPL Version 2 licenses
 *
*/
if(jQuery) (function($) {

	$.extend($.fn, {
		dropdown: function(method, data) {

			switch( method ) {
				case 'hide':
					hide();
					return $(this);
				case 'attach':
					return $(this).attr('data-dropdown', data);
				case 'detach':
					hide();
					return $(this).removeAttr('data-dropdown');
				case 'disable':
					return $(this).addClass('dropdown-disabled');
				case 'enable':
					hide();
					return $(this).removeClass('dropdown-disabled');
			}

		}
	});

	function show(event) {

		var trigger = $(this),
			dropdown = $(trigger.attr('data-dropdown')),
			isOpen = trigger.hasClass('dropdown-open');

		// In some cases we don't want to show it
		if( trigger !== event.target && $(event.target).hasClass('dropdown-ignore') ) return;

		event.preventDefault();
		event.stopPropagation();
		hide();

		if( isOpen || trigger.hasClass('dropdown-disabled') ) return;

		// Show it
		trigger.addClass('dropdown-open');
		dropdown
			.data('dropdown-trigger', trigger)
			.show();

		// Position it
		position();

		// Trigger the show callback
		dropdown
			.trigger('show', {
				dropdown: dropdown,
				trigger: trigger
			});

	}

	function hide(event) {

		// In some cases we don't hide them
		var targetGroup = event ? $(event.target).parents().addBack() : null;

		// Are we clicking anywhere in a dropdown?
		if( targetGroup && targetGroup.is('.dropdown') ) {
			// Is it a dropdown menu?
			if( targetGroup.is('.dropdown-menu') ) {
				// Did we click on an option? If so close it.
				if( !targetGroup.is('A') ) return;
			} else {
				// Nope, it's a panel. Leave it open.
				return;
			}
		}

		// Hide any dropdown that may be showing
		$(document).find('.dropdown:visible').each( function() {
			var dropdown = $(this);
			dropdown
				.hide()
				.removeData('dropdown-trigger')
				.trigger('hide', { dropdown: dropdown });
		});

		// Remove all dropdown-open classes
		$(document).find('.dropdown-open').removeClass('dropdown-open');

	}

	function position() {

		var dropdown = $('.dropdown:visible').eq(0),
			trigger = dropdown.data('dropdown-trigger'),
			hOffset = trigger ? parseInt(trigger.attr('data-horizontal-offset') || 0, 10) : null,
			vOffset = trigger ? parseInt(trigger.attr('data-vertical-offset') || 0, 10) : null;

		if( dropdown.length === 0 || !trigger ) return;

		// Position the dropdown
		dropdown
			.css({
				left: dropdown.hasClass('dropdown-anchor-right') ?
					trigger.offset().left - (dropdown.outerWidth() - trigger.outerWidth()) + hOffset : trigger.offset().left + hOffset,
				top: dropdown.hasClass('dropdown-anchor-top') ?
				    trigger.offset().top - (trigger.outerHeight() + dropdown.height()) + 15 : trigger.offset().top + trigger.outerHeight() + vOffset
			});

	}

	$(document).on('click.dropdown', '[data-dropdown]', show);
	$(document).on('click.dropdown', hide);
	$(window).on('resize', position);

})(jQuery);

/**
 * BxSlider v4.1.1 - Fully loaded, responsive content slider
 * http://bxslider.com
 *
 * Copyright 2012, Steven Wanderski - http://stevenwanderski.com - http://bxcreative.com
 * Written while drinking Belgian ales and listening to jazz
 *
 * Released under the WTFPL license - http://sam.zoy.org/wtfpl/
 */

;(function($){

	var plugin = {};

	var defaults = {

		// GENERAL
		mode: 'horizontal',
		slideSelector: '',
		infiniteLoop: true,
		hideControlOnEnd: false,
		speed: 500,
		easing: null,
		slideMargin: 0,
		startSlide: 0,
		randomStart: false,
		captions: false,
		ticker: false,
		tickerHover: false,
		adaptiveHeight: false,
		adaptiveHeightSpeed: 500,
		video: false,
		useCSS: true,
		preloadImages: 'visible',
		responsive: true,

		// TOUCH
		touchEnabled: true,
		swipeThreshold: 50,
		oneToOneTouch: true,
		preventDefaultSwipeX: true,
		preventDefaultSwipeY: false,

		// PAGER
		pager: true,
		pagerType: 'full',
		pagerShortSeparator: ' / ',
		pagerSelector: null,
		buildPager: null,
		pagerCustom: null,

		// CONTROLS
		controls: true,
		nextText: 'Next',
		prevText: 'Prev',
		nextSelector: null,
		prevSelector: null,
		autoControls: false,
		startText: 'Start',
		stopText: 'Stop',
		autoControlsCombine: false,
		autoControlsSelector: null,

		// AUTO
		auto: false,
		pause: 4000,
		autoStart: true,
		autoDirection: 'next',
		autoHover: false,
		autoDelay: 0,

		// CAROUSEL
		minSlides: 1,
		maxSlides: 1,
		moveSlides: 0,
		slideWidth: 0,

		// CALLBACKS
		onSliderLoad: function() {},
		onSlideBefore: function() {},
		onSlideAfter: function() {},
		onSlideNext: function() {},
		onSlidePrev: function() {}
	}

	$.fn.bxSlider = function(options){

		if(this.length == 0) return this;

		// support mutltiple elements
		if(this.length > 1){
			this.each(function(){$(this).bxSlider(options)});
			return this;
		}

		// create a namespace to be used throughout the plugin
		var slider = {};
		// set a reference to our slider element
		var el = this;
		plugin.el = this;

		/**
		 * Makes slideshow responsive
		 */
		// first get the original window dimens (thanks alot IE)
		var windowWidth = $(window).width();
		var windowHeight = $(window).height();



		/**
		 * ===================================================================================
		 * = PRIVATE FUNCTIONS
		 * ===================================================================================
		 */

		/**
		 * Initializes namespace settings to be used throughout plugin
		 */
		var init = function(){
			// merge user-supplied options with the defaults
			slider.settings = $.extend({}, defaults, options);
			// parse slideWidth setting
			slider.settings.slideWidth = parseInt(slider.settings.slideWidth);
			// store the original children
			slider.children = el.children(slider.settings.slideSelector);
			// check if actual number of slides is less than minSlides / maxSlides
			if(slider.children.length < slider.settings.minSlides) slider.settings.minSlides = slider.children.length;
			if(slider.children.length < slider.settings.maxSlides) slider.settings.maxSlides = slider.children.length;
			// if random start, set the startSlide setting to random number
			if(slider.settings.randomStart) slider.settings.startSlide = Math.floor(Math.random() * slider.children.length);
			// store active slide information
			slider.active = { index: slider.settings.startSlide }
			// store if the slider is in carousel mode (displaying / moving multiple slides)
			slider.carousel = slider.settings.minSlides > 1 || slider.settings.maxSlides > 1;
			// if carousel, force preloadImages = 'all'
			if(slider.carousel) slider.settings.preloadImages = 'all';
			// calculate the min / max width thresholds based on min / max number of slides
			// used to setup and update carousel slides dimensions
			slider.minThreshold = (slider.settings.minSlides * slider.settings.slideWidth) + ((slider.settings.minSlides - 1) * slider.settings.slideMargin);
			slider.maxThreshold = (slider.settings.maxSlides * slider.settings.slideWidth) + ((slider.settings.maxSlides - 1) * slider.settings.slideMargin);
			// store the current state of the slider (if currently animating, working is true)
			slider.working = false;
			// initialize the controls object
			slider.controls = {};
			// initialize an auto interval
			slider.interval = null;
			// determine which property to use for transitions
			slider.animProp = slider.settings.mode == 'vertical' ? 'top' : 'left';
			// determine if hardware acceleration can be used
			slider.usingCSS = slider.settings.useCSS && slider.settings.mode != 'fade' && (function(){
				// create our test div element
				var div = document.createElement('div');
				// css transition properties
				var props = ['WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective'];
				// test for each property
				for(var i in props){
					if(div.style[props[i]] !== undefined){
						slider.cssPrefix = props[i].replace('Perspective', '').toLowerCase();
						slider.animProp = '-' + slider.cssPrefix + '-transform';
						return true;
					}
				}
				return false;
			}());
			// if vertical mode always make maxSlides and minSlides equal
			if(slider.settings.mode == 'vertical') slider.settings.maxSlides = slider.settings.minSlides;
			// save original style data
			el.data("origStyle", el.attr("style"));
			el.children(slider.settings.slideSelector).each(function() {
			  $(this).data("origStyle", $(this).attr("style"));
			});
			// perform all DOM / CSS modifications
			setup();
		}

		/**
		 * Performs all DOM and CSS modifications
		 */
		var setup = function(){
			// wrap el in a wrapper
			el.wrap('<div class="bx-wrapper"><div class="bx-viewport"></div></div>');
			// store a namspace reference to .bx-viewport
			slider.viewport = el.parent();
			// add a loading div to display while images are loading
			slider.loader = $('<div class="bx-loading" />');
			slider.viewport.prepend(slider.loader);
			// set el to a massive width, to hold any needed slides
			// also strip any margin and padding from el
			el.css({
				width: slider.settings.mode == 'horizontal' ? (slider.children.length * 100 + 215) + '%' : 'auto',
				position: 'relative'
			});
			// if using CSS, add the easing property
			if(slider.usingCSS && slider.settings.easing){
				el.css('-' + slider.cssPrefix + '-transition-timing-function', slider.settings.easing);
			// if not using CSS and no easing value was supplied, use the default JS animation easing (swing)
			}else if(!slider.settings.easing){
				slider.settings.easing = 'swing';
			}
			var slidesShowing = getNumberSlidesShowing();
			// make modifications to the viewport (.bx-viewport)
			slider.viewport.css({
				width: '100%',
				overflow: 'hidden',
				position: 'relative'
			});
			slider.viewport.parent().css({
				maxWidth: getViewportMaxWidth()
			});
			// make modification to the wrapper (.bx-wrapper)
			if(!slider.settings.pager) {
				slider.viewport.parent().css({
				margin: '0 auto 0px'
				});
			}
			// apply css to all slider children
			slider.children.css({
				'float': slider.settings.mode == 'horizontal' ? 'left' : 'none',
				listStyle: 'none',
				position: 'relative'
			});
			// apply the calculated width after the float is applied to prevent scrollbar interference
			slider.children.css('width', getSlideWidth());
			// if slideMargin is supplied, add the css
			if(slider.settings.mode == 'horizontal' && slider.settings.slideMargin > 0) slider.children.css('marginRight', slider.settings.slideMargin);
			if(slider.settings.mode == 'vertical' && slider.settings.slideMargin > 0) slider.children.css('marginBottom', slider.settings.slideMargin);
			// if "fade" mode, add positioning and z-index CSS
			if(slider.settings.mode == 'fade'){
				slider.children.css({
					position: 'absolute',
					zIndex: 0,
					display: 'none'
				});
				// prepare the z-index on the showing element
				slider.children.eq(slider.settings.startSlide).css({zIndex: 50, display: 'block'});
			}
			// create an element to contain all slider controls (pager, start / stop, etc)
			slider.controls.el = $('<div class="bx-controls" />');
			// if captions are requested, add them
			if(slider.settings.captions) appendCaptions();
			// check if startSlide is last slide
			slider.active.last = slider.settings.startSlide == getPagerQty() - 1;
			// if video is true, set up the fitVids plugin
			if(slider.settings.video) el.fitVids();
			// set the default preload selector (visible)
			var preloadSelector = slider.children.eq(slider.settings.startSlide);
			if (slider.settings.preloadImages == "all") preloadSelector = slider.children;
			// only check for control addition if not in "ticker" mode
			if(!slider.settings.ticker){
				// if pager is requested, add it
				if(slider.settings.pager) appendPager();
				// if controls are requested, add them
				if(slider.settings.controls) appendControls();
				// if auto is true, and auto controls are requested, add them
				if(slider.settings.auto && slider.settings.autoControls) appendControlsAuto();
				// if any control option is requested, add the controls wrapper
				if(slider.settings.controls || slider.settings.autoControls || slider.settings.pager) slider.viewport.after(slider.controls.el);
			// if ticker mode, do not allow a pager
			}else{
				slider.settings.pager = false;
			}
			// preload all images, then perform final DOM / CSS modifications that depend on images being loaded
			loadElements(preloadSelector, start);
		}

		var loadElements = function(selector, callback){
			var total = selector.find('img, iframe').length;
			if (total == 0){
				callback();
				return;
			}
			var count = 0;
			selector.find('img, iframe').each(function(){
				if($(this).is('img')) $(this).attr('src', $(this).attr('src') + '?timestamp=' + new Date().getTime());
				$(this).load(function(){
					setTimeout(function(){
						if(++count == total) callback();
					}, 0)
				});
			});
		}

		/**
		 * Start the slider
		 */
		var start = function(){
			// if infinite loop, prepare additional slides
			if(slider.settings.infiniteLoop && slider.settings.mode != 'fade' && !slider.settings.ticker){
				var slice = slider.settings.mode == 'vertical' ? slider.settings.minSlides : slider.settings.maxSlides;
				var sliceAppend = slider.children.slice(0, slice).clone().addClass('bx-clone');
				var slicePrepend = slider.children.slice(-slice).clone().addClass('bx-clone');
				el.append(sliceAppend).prepend(slicePrepend);
			}
			// remove the loading DOM element
			slider.loader.remove();
			// set the left / top position of "el"
			setSlidePosition();
			// if "vertical" mode, always use adaptiveHeight to prevent odd behavior
			if (slider.settings.mode == 'vertical') slider.settings.adaptiveHeight = true;
			// set the viewport height
			slider.viewport.height(getViewportHeight());
			// make sure everything is positioned just right (same as a window resize)
			el.redrawSlider();
			// onSliderLoad callback
			slider.settings.onSliderLoad(slider.active.index);
			// slider has been fully initialized
			slider.initialized = true;
			// bind the resize call to the window
			if (slider.settings.responsive) $(window).bind('resize', resizeWindow);
			// if auto is true, start the show
			if (slider.settings.auto && slider.settings.autoStart) initAuto();
			// if ticker is true, start the ticker
			if (slider.settings.ticker) initTicker();
			// if pager is requested, make the appropriate pager link active
			if (slider.settings.pager) updatePagerActive(slider.settings.startSlide);
			// check for any updates to the controls (like hideControlOnEnd updates)
			if (slider.settings.controls) updateDirectionControls();
			// if touchEnabled is true, setup the touch events
			if (slider.settings.touchEnabled && !slider.settings.ticker) initTouch();
		}

		/**
		 * Returns the calculated height of the viewport, used to determine either adaptiveHeight or the maxHeight value
		 */
		var getViewportHeight = function(){
			var height = 0;
			// first determine which children (slides) should be used in our height calculation
			var children = $();
			// if mode is not "vertical" and adaptiveHeight is false, include all children
			if(slider.settings.mode != 'vertical' && !slider.settings.adaptiveHeight){
				children = slider.children;
			}else{
				// if not carousel, return the single active child
				if(!slider.carousel){
					children = slider.children.eq(slider.active.index);
				// if carousel, return a slice of children
				}else{
					// get the individual slide index
					var currentIndex = slider.settings.moveSlides == 1 ? slider.active.index : slider.active.index * getMoveBy();
					// add the current slide to the children
					children = slider.children.eq(currentIndex);
					// cycle through the remaining "showing" slides
					for (i = 1; i <= slider.settings.maxSlides - 1; i++){
						// if looped back to the start
						if(currentIndex + i >= slider.children.length){
							children = children.add(slider.children.eq(i - 1));
						}else{
							children = children.add(slider.children.eq(currentIndex + i));
						}
					}
				}
			}
			// if "vertical" mode, calculate the sum of the heights of the children
			if(slider.settings.mode == 'vertical'){
				children.each(function(index) {
				  height += $(this).outerHeight();
				});
				// add user-supplied margins
				if(slider.settings.slideMargin > 0){
					height += slider.settings.slideMargin * (slider.settings.minSlides - 1);
				}
			// if not "vertical" mode, calculate the max height of the children
			}else{
				height = Math.max.apply(Math, children.map(function(){
					return $(this).outerHeight(false);
				}).get());
			}
			return height;
		}

		/**
		 * Returns the calculated width to be used for the outer wrapper / viewport
		 */
		var getViewportMaxWidth = function(){
			var width = '100%';
			if(slider.settings.slideWidth > 0){
				if(slider.settings.mode == 'horizontal'){
					width = (slider.settings.maxSlides * slider.settings.slideWidth) + ((slider.settings.maxSlides - 1) * slider.settings.slideMargin);
				}else{
					width = slider.settings.slideWidth;
				}
			}
			return width;
		}

		/**
		 * Returns the calculated width to be applied to each slide
		 */
		var getSlideWidth = function(){
			// start with any user-supplied slide width
			var newElWidth = slider.settings.slideWidth;
			// get the current viewport width
			var wrapWidth = slider.viewport.width();
			// if slide width was not supplied, or is larger than the viewport use the viewport width
			if(slider.settings.slideWidth == 0 ||
				(slider.settings.slideWidth > wrapWidth && !slider.carousel) ||
				slider.settings.mode == 'vertical'){
				newElWidth = wrapWidth;
			// if carousel, use the thresholds to determine the width
			}else if(slider.settings.maxSlides > 1 && slider.settings.mode == 'horizontal'){
				if(wrapWidth > slider.maxThreshold){
					// newElWidth = (wrapWidth - (slider.settings.slideMargin * (slider.settings.maxSlides - 1))) / slider.settings.maxSlides;
				}else if(wrapWidth < slider.minThreshold){
					newElWidth = (wrapWidth - (slider.settings.slideMargin * (slider.settings.minSlides - 1))) / slider.settings.minSlides;
				}
			}
			return newElWidth;
		}

		/**
		 * Returns the number of slides currently visible in the viewport (includes partially visible slides)
		 */
		var getNumberSlidesShowing = function(){
			var slidesShowing = 1;
			if(slider.settings.mode == 'horizontal' && slider.settings.slideWidth > 0){
				// if viewport is smaller than minThreshold, return minSlides
				if(slider.viewport.width() < slider.minThreshold){
					slidesShowing = slider.settings.minSlides;
				// if viewport is larger than minThreshold, return maxSlides
				}else if(slider.viewport.width() > slider.maxThreshold){
					slidesShowing = slider.settings.maxSlides;
				// if viewport is between min / max thresholds, divide viewport width by first child width
				}else{
					var childWidth = slider.children.first().width();
					slidesShowing = Math.floor(slider.viewport.width() / childWidth);
				}
			// if "vertical" mode, slides showing will always be minSlides
			}else if(slider.settings.mode == 'vertical'){
				slidesShowing = slider.settings.minSlides;
			}
			return slidesShowing;
		}

		/**
		 * Returns the number of pages (one full viewport of slides is one "page")
		 */
		var getPagerQty = function(){
			var pagerQty = 0;
			// if moveSlides is specified by the user
			if(slider.settings.moveSlides > 0){
				if(slider.settings.infiniteLoop){
					pagerQty = slider.children.length / getMoveBy();
				}else{
					// use a while loop to determine pages
					var breakPoint = 0;
					var counter = 0
					// when breakpoint goes above children length, counter is the number of pages
					while (breakPoint < slider.children.length){
						++pagerQty;
						breakPoint = counter + getNumberSlidesShowing();
						counter += slider.settings.moveSlides <= getNumberSlidesShowing() ? slider.settings.moveSlides : getNumberSlidesShowing();
					}
				}
			// if moveSlides is 0 (auto) divide children length by sides showing, then round up
			}else{
				pagerQty = Math.ceil(slider.children.length / getNumberSlidesShowing());
			}
			return pagerQty;
		}

		/**
		 * Returns the number of indivual slides by which to shift the slider
		 */
		var getMoveBy = function(){
			// if moveSlides was set by the user and moveSlides is less than number of slides showing
			if(slider.settings.moveSlides > 0 && slider.settings.moveSlides <= getNumberSlidesShowing()){
				return slider.settings.moveSlides;
			}
			// if moveSlides is 0 (auto)
			return getNumberSlidesShowing();
		}

		/**
		 * Sets the slider's (el) left or top position
		 */
		var setSlidePosition = function(){
			// if last slide, not infinite loop, and number of children is larger than specified maxSlides
			if(slider.children.length > slider.settings.maxSlides && slider.active.last && !slider.settings.infiniteLoop){
				if (slider.settings.mode == 'horizontal'){
					// get the last child's position
					var lastChild = slider.children.last();
					var position = lastChild.position();
					// set the left position
					setPositionProperty(-(position.left - (slider.viewport.width() - lastChild.width())), 'reset', 0);
				}else if(slider.settings.mode == 'vertical'){
					// get the last showing index's position
					var lastShowingIndex = slider.children.length - slider.settings.minSlides;
					var position = slider.children.eq(lastShowingIndex).position();
					// set the top position
					setPositionProperty(-position.top, 'reset', 0);
				}
			// if not last slide
			}else{
				// get the position of the first showing slide
				var position = slider.children.eq(slider.active.index * getMoveBy()).position();
				// check for last slide
				if (slider.active.index == getPagerQty() - 1) slider.active.last = true;
				// set the repective position
				if (position != undefined){
					if (slider.settings.mode == 'horizontal') setPositionProperty(-position.left, 'reset', 0);
					else if (slider.settings.mode == 'vertical') setPositionProperty(-position.top, 'reset', 0);
				}
			}
		}

		/**
		 * Sets the el's animating property position (which in turn will sometimes animate el).
		 * If using CSS, sets the transform property. If not using CSS, sets the top / left property.
		 *
		 * @param value (int)
		 *  - the animating property's value
		 *
		 * @param type (string) 'slider', 'reset', 'ticker'
		 *  - the type of instance for which the function is being
		 *
		 * @param duration (int)
		 *  - the amount of time (in ms) the transition should occupy
		 *
		 * @param params (array) optional
		 *  - an optional parameter containing any variables that need to be passed in
		 */
		var setPositionProperty = function(value, type, duration, params){
			// use CSS transform
			if(slider.usingCSS){
				// determine the translate3d value
				var propValue = slider.settings.mode == 'vertical' ? 'translate3d(0, ' + value + 'px, 0)' : 'translate3d(' + value + 'px, 0, 0)';
				// add the CSS transition-duration
				el.css('-' + slider.cssPrefix + '-transition-duration', duration / 1000 + 's');
				if(type == 'slide'){
					// set the property value
					el.css(slider.animProp, propValue);
					// bind a callback method - executes when CSS transition completes
					el.bind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function(){
						// unbind the callback
						el.unbind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd');
						updateAfterSlideTransition();
					});
				}else if(type == 'reset'){
					el.css(slider.animProp, propValue);
				}else if(type == 'ticker'){
					// make the transition use 'linear'
					el.css('-' + slider.cssPrefix + '-transition-timing-function', 'linear');
					el.css(slider.animProp, propValue);
					// bind a callback method - executes when CSS transition completes
					el.bind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function(){
						// unbind the callback
						el.unbind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd');
						// reset the position
						setPositionProperty(params['resetValue'], 'reset', 0);
						// start the loop again
						tickerLoop();
					});
				}
			// use JS animate
			}else{
				var animateObj = {};
				animateObj[slider.animProp] = value;
				if(type == 'slide'){
					el.animate(animateObj, duration, slider.settings.easing, function(){
						updateAfterSlideTransition();
					});
				}else if(type == 'reset'){
					el.css(slider.animProp, value)
				}else if(type == 'ticker'){
					el.animate(animateObj, speed, 'linear', function(){
						setPositionProperty(params['resetValue'], 'reset', 0);
						// run the recursive loop after animation
						tickerLoop();
					});
				}
			}
		}

		/**
		 * Populates the pager with proper amount of pages
		 */
		var populatePager = function(){
			var pagerHtml = '';
			var pagerQty = getPagerQty();
			// loop through each pager item
			for(var i=0; i < pagerQty; i++){
				var linkContent = '';
				// if a buildPager function is supplied, use it to get pager link value, else use index + 1
				if(slider.settings.buildPager && $.isFunction(slider.settings.buildPager)){
					linkContent = slider.settings.buildPager(i);
					slider.pagerEl.addClass('bx-custom-pager');
				}else{
					linkContent = i + 1;
					slider.pagerEl.addClass('bx-default-pager');
				}
				// var linkContent = slider.settings.buildPager && $.isFunction(slider.settings.buildPager) ? slider.settings.buildPager(i) : i + 1;
				// add the markup to the string
				pagerHtml += '<div class="bx-pager-item"><a href="" data-slide-index="' + i + '" class="bx-pager-link">' + linkContent + '</a></div>';
			};
			// populate the pager element with pager links
			slider.pagerEl.html(pagerHtml);
		}

		/**
		 * Appends the pager to the controls element
		 */
		var appendPager = function(){
			if(!slider.settings.pagerCustom){
				// create the pager DOM element
				slider.pagerEl = $('<div class="bx-pager" />');
				// if a pager selector was supplied, populate it with the pager
				if(slider.settings.pagerSelector){
					$(slider.settings.pagerSelector).html(slider.pagerEl);
				// if no pager selector was supplied, add it after the wrapper
				}else{
					slider.controls.el.addClass('bx-has-pager').append(slider.pagerEl);
				}
				// populate the pager
				populatePager();
			}else{
				slider.pagerEl = $(slider.settings.pagerCustom);
			}
			// assign the pager click binding
			slider.pagerEl.delegate('a', 'click', clickPagerBind);
		}

		/**
		 * Appends prev / next controls to the controls element
		 */
		var appendControls = function(){
			slider.controls.next = $('<a class="bx-next" href="">' + slider.settings.nextText + '</a>');
			slider.controls.prev = $('<a class="bx-prev" href="">' + slider.settings.prevText + '</a>');
			// bind click actions to the controls
			slider.controls.next.bind('click', clickNextBind);
			slider.controls.prev.bind('click', clickPrevBind);
			// if nextSlector was supplied, populate it
			if(slider.settings.nextSelector){
				$(slider.settings.nextSelector).append(slider.controls.next);
			}
			// if prevSlector was supplied, populate it
			if(slider.settings.prevSelector){
				$(slider.settings.prevSelector).append(slider.controls.prev);
			}
			// if no custom selectors were supplied
			if(!slider.settings.nextSelector && !slider.settings.prevSelector){
				// add the controls to the DOM
				slider.controls.directionEl = $('<div class="bx-controls-direction" />');
				// add the control elements to the directionEl
				slider.controls.directionEl.append(slider.controls.prev).append(slider.controls.next);
				// slider.viewport.append(slider.controls.directionEl);
				slider.controls.el.addClass('bx-has-controls-direction').append(slider.controls.directionEl);
			}
		}

		/**
		 * Appends start / stop auto controls to the controls element
		 */
		var appendControlsAuto = function(){
			slider.controls.start = $('<div class="bx-controls-auto-item"><a class="bx-start" href="">' + slider.settings.startText + '</a></div>');
			slider.controls.stop = $('<div class="bx-controls-auto-item"><a class="bx-stop" href="">' + slider.settings.stopText + '</a></div>');
			// add the controls to the DOM
			slider.controls.autoEl = $('<div class="bx-controls-auto" />');
			// bind click actions to the controls
			slider.controls.autoEl.delegate('.bx-start', 'click', clickStartBind);
			slider.controls.autoEl.delegate('.bx-stop', 'click', clickStopBind);
			// if autoControlsCombine, insert only the "start" control
			if(slider.settings.autoControlsCombine){
				slider.controls.autoEl.append(slider.controls.start);
			// if autoControlsCombine is false, insert both controls
			}else{
				slider.controls.autoEl.append(slider.controls.start).append(slider.controls.stop);
			}
			// if auto controls selector was supplied, populate it with the controls
			if(slider.settings.autoControlsSelector){
				$(slider.settings.autoControlsSelector).html(slider.controls.autoEl);
			// if auto controls selector was not supplied, add it after the wrapper
			}else{
				slider.controls.el.addClass('bx-has-controls-auto').append(slider.controls.autoEl);
			}
			// update the auto controls
			updateAutoControls(slider.settings.autoStart ? 'stop' : 'start');
		}

		/**
		 * Appends image captions to the DOM
		 */
		var appendCaptions = function(){
			// cycle through each child
			slider.children.each(function(index){
				// get the image title attribute
				var title = $(this).find('img:first').attr('title');
				// append the caption
				if (title != undefined && ('' + title).length) {
                    $(this).append('<div class="bx-caption"><span>' + title + '</span></div>');
                }
			});
		}

		/**
		 * Click next binding
		 *
		 * @param e (event)
		 *  - DOM event object
		 */
		var clickNextBind = function(e){
			// if auto show is running, stop it
			if (slider.settings.auto) el.stopAuto();
			el.goToNextSlide();
			e.preventDefault();
		}

		/**
		 * Click prev binding
		 *
		 * @param e (event)
		 *  - DOM event object
		 */
		var clickPrevBind = function(e){
			// if auto show is running, stop it
			if (slider.settings.auto) el.stopAuto();
			el.goToPrevSlide();
			e.preventDefault();
		}

		/**
		 * Click start binding
		 *
		 * @param e (event)
		 *  - DOM event object
		 */
		var clickStartBind = function(e){
			el.startAuto();
			e.preventDefault();
		}

		/**
		 * Click stop binding
		 *
		 * @param e (event)
		 *  - DOM event object
		 */
		var clickStopBind = function(e){
			el.stopAuto();
			e.preventDefault();
		}

		/**
		 * Click pager binding
		 *
		 * @param e (event)
		 *  - DOM event object
		 */
		var clickPagerBind = function(e){
			// if auto show is running, stop it
			if (slider.settings.auto) el.stopAuto();
			var pagerLink = $(e.currentTarget);
			var pagerIndex = parseInt(pagerLink.attr('data-slide-index'));
			// if clicked pager link is not active, continue with the goToSlide call
			if(pagerIndex != slider.active.index) el.goToSlide(pagerIndex);
			e.preventDefault();
		}

		/**
		 * Updates the pager links with an active class
		 *
		 * @param slideIndex (int)
		 *  - index of slide to make active
		 */
		var updatePagerActive = function(slideIndex){
			// if "short" pager type
			var len = slider.children.length; // nb of children
			if(slider.settings.pagerType == 'short'){
				if(slider.settings.maxSlides > 1) {
					len = Math.ceil(slider.children.length/slider.settings.maxSlides);
				}
				slider.pagerEl.html( (slideIndex + 1) + slider.settings.pagerShortSeparator + len);
				return;
			}
			// remove all pager active classes
			slider.pagerEl.find('a').removeClass('active');
			// apply the active class for all pagers
			slider.pagerEl.each(function(i, el) { $(el).find('a').eq(slideIndex).addClass('active'); });
		}

		/**
		 * Performs needed actions after a slide transition
		 */
		var updateAfterSlideTransition = function(){
			// if infinte loop is true
			if(slider.settings.infiniteLoop){
				var position = '';
				// first slide
				if(slider.active.index == 0){
					// set the new position
					position = slider.children.eq(0).position();
				// carousel, last slide
				}else if(slider.active.index == getPagerQty() - 1 && slider.carousel){
					position = slider.children.eq((getPagerQty() - 1) * getMoveBy()).position();
				// last slide
				}else if(slider.active.index == slider.children.length - 1){
					position = slider.children.eq(slider.children.length - 1).position();
				}
				if (slider.settings.mode == 'horizontal') { setPositionProperty(-position.left, 'reset', 0);; }
				else if (slider.settings.mode == 'vertical') { setPositionProperty(-position.top, 'reset', 0);; }
			}
			// declare that the transition is complete
			slider.working = false;
			// onSlideAfter callback
			slider.settings.onSlideAfter(slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index);
		}

		/**
		 * Updates the auto controls state (either active, or combined switch)
		 *
		 * @param state (string) "start", "stop"
		 *  - the new state of the auto show
		 */
		var updateAutoControls = function(state){
			// if autoControlsCombine is true, replace the current control with the new state
			if(slider.settings.autoControlsCombine){
				slider.controls.autoEl.html(slider.controls[state]);
			// if autoControlsCombine is false, apply the "active" class to the appropriate control
			}else{
				slider.controls.autoEl.find('a').removeClass('active');
				slider.controls.autoEl.find('a:not(.bx-' + state + ')').addClass('active');
			}
		}

		/**
		 * Updates the direction controls (checks if either should be hidden)
		 */
		var updateDirectionControls = function(){
			if(getPagerQty() == 1){
				slider.controls.prev.addClass('disabled');
				slider.controls.next.addClass('disabled');
			}else if(!slider.settings.infiniteLoop && slider.settings.hideControlOnEnd){
				// if first slide
				if (slider.active.index == 0){
					slider.controls.prev.addClass('disabled');
					slider.controls.next.removeClass('disabled');
				// if last slide
				}else if(slider.active.index == getPagerQty() - 1){
					slider.controls.next.addClass('disabled');
					slider.controls.prev.removeClass('disabled');
				// if any slide in the middle
				}else{
					slider.controls.prev.removeClass('disabled');
					slider.controls.next.removeClass('disabled');
				}
			}
		}

		/**
		 * Initialzes the auto process
		 */
		var initAuto = function(){
			// if autoDelay was supplied, launch the auto show using a setTimeout() call
			if(slider.settings.autoDelay > 0){
				var timeout = setTimeout(el.startAuto, slider.settings.autoDelay);
			// if autoDelay was not supplied, start the auto show normally
			}else{
				el.startAuto();
			}
			// if autoHover is requested
			if(slider.settings.autoHover){
				// on el hover
				el.hover(function(){
					// if the auto show is currently playing (has an active interval)
					if(slider.interval){
						// stop the auto show and pass true agument which will prevent control update
						el.stopAuto(true);
						// create a new autoPaused value which will be used by the relative "mouseout" event
						slider.autoPaused = true;
					}
				}, function(){
					// if the autoPaused value was created be the prior "mouseover" event
					if(slider.autoPaused){
						// start the auto show and pass true agument which will prevent control update
						el.startAuto(true);
						// reset the autoPaused value
						slider.autoPaused = null;
					}
				});
			}
		}

		/**
		 * Initialzes the ticker process
		 */
		var initTicker = function(){
			var startPosition = 0;
			// if autoDirection is "next", append a clone of the entire slider
			if(slider.settings.autoDirection == 'next'){
				el.append(slider.children.clone().addClass('bx-clone'));
			// if autoDirection is "prev", prepend a clone of the entire slider, and set the left position
			}else{
				el.prepend(slider.children.clone().addClass('bx-clone'));
				var position = slider.children.first().position();
				startPosition = slider.settings.mode == 'horizontal' ? -position.left : -position.top;
			}
			setPositionProperty(startPosition, 'reset', 0);
			// do not allow controls in ticker mode
			slider.settings.pager = false;
			slider.settings.controls = false;
			slider.settings.autoControls = false;
			// if autoHover is requested
			if(slider.settings.tickerHover && !slider.usingCSS){
				// on el hover
				slider.viewport.hover(function(){
					el.stop();
				}, function(){
					// calculate the total width of children (used to calculate the speed ratio)
					var totalDimens = 0;
					slider.children.each(function(index){
					  totalDimens += slider.settings.mode == 'horizontal' ? $(this).outerWidth(true) : $(this).outerHeight(true);
					});
					// calculate the speed ratio (used to determine the new speed to finish the paused animation)
					var ratio = slider.settings.speed / totalDimens;
					// determine which property to use
					var property = slider.settings.mode == 'horizontal' ? 'left' : 'top';
					// calculate the new speed
					var newSpeed = ratio * (totalDimens - (Math.abs(parseInt(el.css(property)))));
					tickerLoop(newSpeed);
				});
			}
			// start the ticker loop
			tickerLoop();
		}

		/**
		 * Runs a continuous loop, news ticker-style
		 */
		var tickerLoop = function(resumeSpeed){
			speed = resumeSpeed ? resumeSpeed : slider.settings.speed;
			var position = {left: 0, top: 0};
			var reset = {left: 0, top: 0};
			// if "next" animate left position to last child, then reset left to 0
			if(slider.settings.autoDirection == 'next'){
				position = el.find('.bx-clone').first().position();
			// if "prev" animate left position to 0, then reset left to first non-clone child
			}else{
				reset = slider.children.first().position();
			}
			var animateProperty = slider.settings.mode == 'horizontal' ? -position.left : -position.top;
			var resetValue = slider.settings.mode == 'horizontal' ? -reset.left : -reset.top;
			var params = {resetValue: resetValue};
			setPositionProperty(animateProperty, 'ticker', speed, params);
		}

		/**
		 * Initializes touch events
		 */
		var initTouch = function(){
			// initialize object to contain all touch values
			slider.touch = {
				start: {x: 0, y: 0},
				end: {x: 0, y: 0}
			}
			slider.viewport.bind('touchstart', onTouchStart);
		}

		/**
		 * Event handler for "touchstart"
		 *
		 * @param e (event)
		 *  - DOM event object
		 */
		var onTouchStart = function(e){
			if(slider.working){
				e.preventDefault();
			}else{
				// record the original position when touch starts
				slider.touch.originalPos = el.position();
				var orig = e.originalEvent;
				// record the starting touch x, y coordinates
				slider.touch.start.x = orig.changedTouches[0].pageX;
				slider.touch.start.y = orig.changedTouches[0].pageY;
				// bind a "touchmove" event to the viewport
				slider.viewport.bind('touchmove', onTouchMove);
				// bind a "touchend" event to the viewport
				slider.viewport.bind('touchend', onTouchEnd);
			}
		}

		/**
		 * Event handler for "touchmove"
		 *
		 * @param e (event)
		 *  - DOM event object
		 */
		var onTouchMove = function(e){
			var orig = e.originalEvent;
			// if scrolling on y axis, do not prevent default
			var xMovement = Math.abs(orig.changedTouches[0].pageX - slider.touch.start.x);
			var yMovement = Math.abs(orig.changedTouches[0].pageY - slider.touch.start.y);
			// x axis swipe
			if((xMovement * 3) > yMovement && slider.settings.preventDefaultSwipeX){
				e.preventDefault();
			// y axis swipe
			}else if((yMovement * 3) > xMovement && slider.settings.preventDefaultSwipeY){
				e.preventDefault();
			}
			if(slider.settings.mode != 'fade' && slider.settings.oneToOneTouch){
				var value = 0;
				// if horizontal, drag along x axis
				if(slider.settings.mode == 'horizontal'){
					var change = orig.changedTouches[0].pageX - slider.touch.start.x;
					value = slider.touch.originalPos.left + change;
				// if vertical, drag along y axis
				}else{
					var change = orig.changedTouches[0].pageY - slider.touch.start.y;
					value = slider.touch.originalPos.top + change;
				}
				setPositionProperty(value, 'reset', 0);
			}
		}

		/**
		 * Event handler for "touchend"
		 *
		 * @param e (event)
		 *  - DOM event object
		 */
		var onTouchEnd = function(e){
			slider.viewport.unbind('touchmove', onTouchMove);
			var orig = e.originalEvent;
			var value = 0;
			// record end x, y positions
			slider.touch.end.x = orig.changedTouches[0].pageX;
			slider.touch.end.y = orig.changedTouches[0].pageY;
			// if fade mode, check if absolute x distance clears the threshold
			if(slider.settings.mode == 'fade'){
				var distance = Math.abs(slider.touch.start.x - slider.touch.end.x);
				if(distance >= slider.settings.swipeThreshold){
					slider.touch.start.x > slider.touch.end.x ? el.goToNextSlide() : el.goToPrevSlide();
					el.stopAuto();
				}
			// not fade mode
			}else{
				var distance = 0;
				// calculate distance and el's animate property
				if(slider.settings.mode == 'horizontal'){
					distance = slider.touch.end.x - slider.touch.start.x;
					value = slider.touch.originalPos.left;
				}else{
					distance = slider.touch.end.y - slider.touch.start.y;
					value = slider.touch.originalPos.top;
				}
				// if not infinite loop and first / last slide, do not attempt a slide transition
				if(!slider.settings.infiniteLoop && ((slider.active.index == 0 && distance > 0) || (slider.active.last && distance < 0))){
					setPositionProperty(value, 'reset', 200);
				}else{
					// check if distance clears threshold
					if(Math.abs(distance) >= slider.settings.swipeThreshold){
						distance < 0 ? el.goToNextSlide() : el.goToPrevSlide();
						el.stopAuto();
					}else{
						// el.animate(property, 200);
						setPositionProperty(value, 'reset', 200);
					}
				}
			}
			slider.viewport.unbind('touchend', onTouchEnd);
		}

		/**
		 * Window resize event callback
		 */
		var resizeWindow = function(e){
			// get the new window dimens (again, thank you IE)
			var windowWidthNew = $(window).width();
			var windowHeightNew = $(window).height();
			// make sure that it is a true window resize
			// *we must check this because our dinosaur friend IE fires a window resize event when certain DOM elements
			// are resized. Can you just die already?*
			if(windowWidth != windowWidthNew || windowHeight != windowHeightNew){
				// set the new window dimens
				windowWidth = windowWidthNew;
				windowHeight = windowHeightNew;
				// update all dynamic elements
				el.redrawSlider();
			}
		}

		/**
		 * ===================================================================================
		 * = PUBLIC FUNCTIONS
		 * ===================================================================================
		 */

		/**
		 * Performs slide transition to the specified slide
		 *
		 * @param slideIndex (int)
		 *  - the destination slide's index (zero-based)
		 *
		 * @param direction (string)
		 *  - INTERNAL USE ONLY - the direction of travel ("prev" / "next")
		 */
		el.goToSlide = function(slideIndex, direction){
			// if plugin is currently in motion, ignore request
			if(slider.working || slider.active.index == slideIndex) return;
			// declare that plugin is in motion
			slider.working = true;
			// store the old index
			slider.oldIndex = slider.active.index;
			// if slideIndex is less than zero, set active index to last child (this happens during infinite loop)
			if(slideIndex < 0){
				slider.active.index = getPagerQty() - 1;
			// if slideIndex is greater than children length, set active index to 0 (this happens during infinite loop)
			}else if(slideIndex >= getPagerQty()){
				slider.active.index = 0;
			// set active index to requested slide
			}else{
				slider.active.index = slideIndex;
			}
			// onSlideBefore, onSlideNext, onSlidePrev callbacks
			slider.settings.onSlideBefore(slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index);
			if(direction == 'next'){
				slider.settings.onSlideNext(slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index);
			}else if(direction == 'prev'){
				slider.settings.onSlidePrev(slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index);
			}
			// check if last slide
			slider.active.last = slider.active.index >= getPagerQty() - 1;
			// update the pager with active class
			if(slider.settings.pager) updatePagerActive(slider.active.index);
			// // check for direction control update
			if(slider.settings.controls) updateDirectionControls();
			// if slider is set to mode: "fade"
			if(slider.settings.mode == 'fade'){
				// if adaptiveHeight is true and next height is different from current height, animate to the new height
				if(slider.settings.adaptiveHeight && slider.viewport.height() != getViewportHeight()){
					slider.viewport.animate({height: getViewportHeight()}, slider.settings.adaptiveHeightSpeed);
				}
				// fade out the visible child and reset its z-index value
				slider.children.filter(':visible').fadeOut(slider.settings.speed).css({zIndex: 0});
				// fade in the newly requested slide
				slider.children.eq(slider.active.index).css('zIndex', 51).fadeIn(slider.settings.speed, function(){
					$(this).css('zIndex', 50);
					updateAfterSlideTransition();
				});
			// slider mode is not "fade"
			}else{
				// if adaptiveHeight is true and next height is different from current height, animate to the new height
				if(slider.settings.adaptiveHeight && slider.viewport.height() != getViewportHeight()){
					slider.viewport.animate({height: getViewportHeight()}, slider.settings.adaptiveHeightSpeed);
				}
				var moveBy = 0;
				var position = {left: 0, top: 0};
				// if carousel and not infinite loop
				if(!slider.settings.infiniteLoop && slider.carousel && slider.active.last){
					if(slider.settings.mode == 'horizontal'){
						// get the last child position
						var lastChild = slider.children.eq(slider.children.length - 1);
						position = lastChild.position();
						// calculate the position of the last slide
						moveBy = slider.viewport.width() - lastChild.outerWidth();
					}else{
						// get last showing index position
						var lastShowingIndex = slider.children.length - slider.settings.minSlides;
						position = slider.children.eq(lastShowingIndex).position();
					}
					// horizontal carousel, going previous while on first slide (infiniteLoop mode)
				}else if(slider.carousel && slider.active.last && direction == 'prev'){
					// get the last child position
					var eq = slider.settings.moveSlides == 1 ? slider.settings.maxSlides - getMoveBy() : ((getPagerQty() - 1) * getMoveBy()) - (slider.children.length - slider.settings.maxSlides);
					var lastChild = el.children('.bx-clone').eq(eq);
					position = lastChild.position();
				// if infinite loop and "Next" is clicked on the last slide
				}else if(direction == 'next' && slider.active.index == 0){
					// get the last clone position
					position = el.find('> .bx-clone').eq(slider.settings.maxSlides).position();
					slider.active.last = false;
				// normal non-zero requests
				}else if(slideIndex >= 0){
					var requestEl = slideIndex * getMoveBy();
					position = slider.children.eq(requestEl).position();
				}

				/* If the position doesn't exist
				 * (e.g. if you destroy the slider on a next click),
				 * it doesn't throw an error.
				 */
				if ("undefined" !== typeof(position)) {
					var value = slider.settings.mode == 'horizontal' ? -(position.left - moveBy) : -position.top;
					// plugin values to be animated
					setPositionProperty(value, 'slide', slider.settings.speed);
				}
			}
		}

		/**
		 * Transitions to the next slide in the show
		 */
		el.goToNextSlide = function(){
			// if infiniteLoop is false and last page is showing, disregard call
			if (!slider.settings.infiniteLoop && slider.active.last) return;
			var pagerIndex = parseInt(slider.active.index) + 1;
			el.goToSlide(pagerIndex, 'next');
		}

		/**
		 * Transitions to the prev slide in the show
		 */
		el.goToPrevSlide = function(){
			// if infiniteLoop is false and last page is showing, disregard call
			if (!slider.settings.infiniteLoop && slider.active.index == 0) return;
			var pagerIndex = parseInt(slider.active.index) - 1;
			el.goToSlide(pagerIndex, 'prev');
		}

		/**
		 * Starts the auto show
		 *
		 * @param preventControlUpdate (boolean)
		 *  - if true, auto controls state will not be updated
		 */
		el.startAuto = function(preventControlUpdate){
			// if an interval already exists, disregard call
			if(slider.interval) return;
			// create an interval
			slider.interval = setInterval(function(){
				slider.settings.autoDirection == 'next' ? el.goToNextSlide() : el.goToPrevSlide();
			}, slider.settings.pause);
			// if auto controls are displayed and preventControlUpdate is not true
			if (slider.settings.autoControls && preventControlUpdate != true) updateAutoControls('stop');
		}

		/**
		 * Stops the auto show
		 *
		 * @param preventControlUpdate (boolean)
		 *  - if true, auto controls state will not be updated
		 */
		el.stopAuto = function(preventControlUpdate){
			// if no interval exists, disregard call
			if(!slider.interval) return;
			// clear the interval
			clearInterval(slider.interval);
			slider.interval = null;
			// if auto controls are displayed and preventControlUpdate is not true
			if (slider.settings.autoControls && preventControlUpdate != true) updateAutoControls('start');
		}

		/**
		 * Returns current slide index (zero-based)
		 */
		el.getCurrentSlide = function(){
			return slider.active.index;
		}

		/**
		 * Returns number of slides in show
		 */
		el.getSlideCount = function(){
			return slider.children.length;
		}

		/**
		 * Update all dynamic slider elements
		 */
		el.redrawSlider = function(){
			// resize all children in ratio to new screen size
			slider.children.add(el.find('.bx-clone')).outerWidth(getSlideWidth());
			// adjust the height
			slider.viewport.css('height', getViewportHeight());
			// update the slide position
			if(!slider.settings.ticker) setSlidePosition();
			// if active.last was true before the screen resize, we want
			// to keep it last no matter what screen size we end on
			if (slider.active.last) slider.active.index = getPagerQty() - 1;
			// if the active index (page) no longer exists due to the resize, simply set the index as last
			if (slider.active.index >= getPagerQty()) slider.active.last = true;
			// if a pager is being displayed and a custom pager is not being used, update it
			if(slider.settings.pager && !slider.settings.pagerCustom){
				populatePager();
				updatePagerActive(slider.active.index);
			}
		}

		/**
		 * Destroy the current instance of the slider (revert everything back to original state)
		 */
		el.destroySlider = function(){
			// don't do anything if slider has already been destroyed
			if(!slider.initialized) return;
			slider.initialized = false;
			$('.bx-clone', this).remove();
			slider.children.each(function() {
				$(this).data("origStyle") != undefined ? $(this).attr("style", $(this).data("origStyle")) : $(this).removeAttr('style');
			});
			$(this).data("origStyle") != undefined ? this.attr("style", $(this).data("origStyle")) : $(this).removeAttr('style');
			$(this).unwrap().unwrap();
			if(slider.controls.el) slider.controls.el.remove();
			if(slider.controls.next) slider.controls.next.remove();
			if(slider.controls.prev) slider.controls.prev.remove();
			if(slider.pagerEl) slider.pagerEl.remove();
			$('.bx-caption', this).remove();
			if(slider.controls.autoEl) slider.controls.autoEl.remove();
			clearInterval(slider.interval);
			if(slider.settings.responsive) $(window).unbind('resize', resizeWindow);
		}

		/**
		 * Reload the slider (revert all DOM changes, and re-initialize)
		 */
		el.reloadSlider = function(settings){
			if (settings != undefined) options = settings;
			el.destroySlider();
			init();
		}

		init();

		// returns the current jQuery object
		return this;
	}

})(jQuery);
