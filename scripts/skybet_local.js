var SkyBetAccount;
(function(e) {
    typeof module != "undefined" ? module.exports = e : typeof define == "function" ? define(e) : SkyBetAccount = e
})(function() {
    function n() {}
    function r(e, t, n, r) {
        this.window = e, this.bus = t, this.getUrl = n, this.sba = r, this.overlay = new o(this), this.loadingOverlay = new o(this, !0), this.loadingBox = new u(this), this.isOpen=!1, this.isPreloading=!1, this.onPreloadCallbacks = [], this.loginTokenTimeout = 6e5;
        var i = this;
        this.window.addEventListener("resize", function() {
            i.resize()
        }, !1)
    }
    function s(e, t, n) {
        this.ui = e, this.bus = n, this.container = t, this.isVisible=!1, this.initContainer(), this.initSlider(), this.initIframe()
    }
    function o(e, t) {
        this.config = {
            dialog: {
                title: "Are you sure?",
                description: "Closing the menu will lose any information provided.",
                buttons: [{
                    label: "No, keep menu"
                }, {
                    label: "Yes, close menu",
                    aux: "close"
                }
                ],
                level: "info",
                doneCallback: this.closeMyAccountDialogHandler.bind(this),
                isConsumerDialog: !0
            }
        }, this.ui = e, this.loadingOverlay = t?!0 : !1, this.initOverlay()
    }
    function u(e) {
        this.ui = e, this.cachedIsIOS43Device = null, this.spinner = new a(e);
        var t = this.ui.createElement("div");
        t.style.cssText = r.Styles.loadingBox, t.style.display = "none";
        var n = this.ui.createElement("p");
        n.innerHTML = "Loading&hellip;", t.appendChild(n), this.loadingParagraph = n, this.spinner.attach(t);
        var i = this.createSecureInfo();
        i.style.display = "none", t.appendChild(i), this.secureInfo = i, this.ui.getElement("body").appendChild(t), this.loadingBox = t
    }
    function a(e) {
        this.ui = e;
        var t = this.ui.createElement("div");
        t.style.cssText = r.Styles.spinner, t.style.backgroundImage = "url(data:image/png;base64," + g() + ")", this.spinner = t, this.bgPosition =- 280
    }
    function f(e) {
        this.config = e;
        var t = this;
        e.window.addEventListener("message", function() {
            return t.receiveMessage.apply(t, arguments)
        }, !1)
    }
    function l() {
        this.hasBoundEvent = function(e) {
            return undefined !== this.callback && undefined !== this.callback[e] && this.callback[e].length?!0 : !1
        }, this.bind = function(e, t) {
            return undefined === this.callback && (this.callback = {}), undefined === this.callback[e] && (this.callback[e] = []), this.callback[e].push(t), this
        }, this.unbind = function(e, t) {
            undefined === this.callback && (this.callback = {});
            if (!t)
                delete this.callback[e];
            else 
                for (var n = 0, r = this.callback[e].length; n < r; n++)
                    this.callback[e][n] === t && delete this.callback[e][n];
            return this
        }, this.unbindLast = function(e) {
            return undefined === this.callback && (this.callback = {}), this.callback[e] && this.callback[e].pop(), this
        }, this.fire = function(e, t) {
            undefined === this.callback && (this.callback = {});
            if (undefined === this.callback[e])
                return;
            for (var n in this.callback[e])
                this.callback[e].hasOwnProperty(n) && this.callback[e][n](t);
            return this
        }
    }
    function h(e, t) {
        this.config = e, this.sba = t
    }
    function p(e, t, n) {
        this.queryString = e, this.document = t, this.config = n, this.queryVars = null, this.affCookie = "AFF_ID", this.btagCookie = "IBSAFFIA", this.assetIdCookie = "IBSAFFASSET", this.affQueryVars = ["a", "aff", "affid", "aff_id"], this.btagQueryVar = "btag", this.assetIdQueryVar = "asset_id"
    }
    function v(e) {
        this.sba = e, this.config = {
            fetchEnabled: !0,
            iframeEventsEnabled: !0
        }, this.events = new l, this.eventsStore = [], this.bind("configureAppBridge", this.setConfig.bind(this)), this.bindAppBridgeEvents(e)
    }
    function m(e) {
        this.sba = e
    }
    function g() {
        return "iVBORw0KGgoAAAANSUhEUgAAAFAAAAKACAYAAAALsP+rAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAMh9JREFUeNrsnWuMVdeV5zfXOBDauIhAGCNQyiiCagFyTUiah1qi1CSxNQIHtS0IaEZdiWyT9IcOHvqTiZpiFPtTysbzJTZYcVmagTbCLeJCauI07qLTA3jclS6LQlNVSkP1QDD2wDSvYDDu7tn/e9emdp177nmttc89+1Qtaauoupx9z/3dtZ9n7fWfoppoGzduXKt/dOjSSqVdl1l47fDhw1Os/zdb/1hDv96i8qku13S5rv/vlWZ9hqk5A3sUPwhaR8ZqZlCBLaR68eOyLpdQNNBPSwNQf7gvE7Tt5GWubA6VZfo94ZnnUTTMz70ESOC6dOlsQstqoQKYADnsyiunlgxcmKGZL3QFcqoguBYCt10V0wzIYf3zrFTTrgjB+7b+MVpgeLYt0WUtjezNBQiv0+UNzDrM9MMTwyi+Rt/70qY1YZqS9NDczVdbRJ54ImuTrjDg9XkOzx6xv6E/04N5NuEez5psnN2vy9I8AWIVMVAigJh4f5AbQN1fXCsRxGvO+0DdP6zW5V27nygJxDp4+jN+QZf1Sac5lQTw0Df8DMsiXQ6VCGIoPP1jvS7z8ZN+zw6QYL1MI5VyDLHPWgJ2hH1YXQaxiqCdF1fw5tCfptHvkTYlBiDgbQ55CR/kKX0D1wNLubRTm57ad3D45xmmUpjDzqPysDA82/r1/+1PDVBX+pj+8UbEjWSFeFWXPSjkvRJLyS/SWncRTUmk4Bnr1dd8lBggNdH3raYrBbFLElwDr1xCIKXgwW7o6w6kAbhb/3gm4X0ngYj+sVP/nw/zGB1oBG1XYzvXHHiRTXlKyJsv0D/+V8p7joI4oP/+3byHWPJGDHoPCsCD3dHlbV3PzTiAh9TYAxwWxCJaRnjGsCF7vOE0BhPmjPBCpzglgwdbout4IGoeuIl5j1WIJYVnbEUoQOr7Ngvc68kiAtRN7zP946ZAVa32CsX2QAl4g/pGdxW4C8SgdoNZB1YorWEANwnc4PeKPICQF/YJVLVsHEDaMFjIrLRb3+CFou8g0IpimFnNHDOYVIS8DxPVfR7txJykeR2rL7QBrmZW9nrR538hTXmEWc38KkCaty2bQN5n7LSUB3KfjR71yfssL8SUZpS75q4wVh73ACp/bZR5/WyuB17T3+QvJjpAztr1jMfwzGBymQuQ04RPKv+NEx78BW501mAJAHLWx3O4AK+XAOBFzsXYtX1yovaB1tJur5q0/A2PLSqTGCatqTZFu+E/MK7fFvXU3pNmiDVtZ9bruU34gRI4USvnYi7AJSUAyIm0vQSAnCa4ogQA5zGuvQ2AnIcsiz3v/6YzAVY9kLMzO5OOrPpqbczr2U0Y1jHRAXKfUHUEwx08ab6zuAD1FO5Shba2ORBn6rLVQ+/jHhIasqcx3Ga8xScvpMFjFbOaURvgEWZlvnnh47pMF/NA3YwxEl9kVvis/mYXe+B9rQLNF3kZrgZXIkcE7m+XB013o0BVp8KWcr0CFSMAcVfBmy73kORt03zHAaSdWQmIiwvsffMEqhrSrG432kzgNmNMh7YVESB96B5Vyy3DsT77l0rgTfoZU5oqvGAUe8kgDpjBw1jYkf9uXfZz4ekm83v6xwu6nNN/f6UJTRanlxC2h7jHbpPuBBD1a4DYmbJJ31YhwZn31TXwoaErbW1tmBQvF4CHE0OLdH0rdRnWdV/NCR764T9Ttf1KnFdZqt//A/3+n9Nn/Fz/jmfaX1HJN4X/Xn/GoeAfG51UeoAGlJlMeEHDcal39P//nUOv26DLupCXz9ueaA0sSTzxqr5uT9gLUYcN11JzloJn7CZ9OWIg6WjXagI3I+K/ZoXYo68ZTQWQKt9F36gUvKAdw6RU1/N+Rm9rt0pSSwuxT//fvkaVxQFEU35NjX/2IQUvaIgYRb/0sS6f6PoHA7AwGOAsyxz6yXkekxQilmyvRlVUiRn2AQknN284hqdo0NqiaumjXgy8Bng7VO0syzrFf5hVrY++mEZTHPO7ygzQ2mj4c8fwmmFREEep37sdV8kURsftFJ6++ScC05IdjkDWNec0VikivGZ7onOAJYJnQ/xBngBfVzInH4tiyAp8MDeANMXYqWo5XHw39IHPZz3nlzk2Rr/hOYJ4zGN4x/Tn+DEnryorhyotxV7RHTC2uH+o/InWwtGGN2mKxjKRCFVaiiFNyjsewMM6/McS8NgeGOKNr2tvBMSnFf+5q7Qh99YR6bTx4nmk9Q1+gqWYBjmXlmbrmjy6DrgA5wxgACT6R+wBrtQFK4uHchxZ4XEnXefVd55Ln0Ci3+nVMB+hpr1MJd/xTmrD5G0f5qnukKuaA019zllLQgNyLpVHEozkGEGvUIGnXZAaEAoPMMTwLBqDD3aRH1TjpS5sw3ZavwXwMwJ4Q/HTmPgDkBbsJmli1gxC5rr5Vr34gdieUZQ8H63moScylRbrKC0O32o+FaR4h5eiWY/QmWD/AJK3LVH8fDRZvRRlhb4PeGW/K6+cWjJwQZtG97KEZDDEQU4VbqqN0m8WwQxIDEanpZq2lJ4IBoVvKD82WXE46El9zw83HSC8jvJufV1FZ88tmiHiYgMlnGxOE6a+7uuOR1bXtpw88UjWJp31oRImvWs9h2eP2FuySgRlbcLtnjXZJKP16jwBYqfjWokAYuL9LrWuFucAKSdzWSBetvvAtFnWEw0ilgBVh3kDQNR/B8Q1HveFl4MDCAUZfUfVstJdYntgQICqz3Zxzz2xEbxOVUsD0Em/ZwdIsOB5s6zBwxVEfCA8Zx6k+mzD1lUv/f204uuJRMEz4W3m90iLiw98o0ElA3ZztpZyaZszNkTRTC6n1TSiXM6tVpGEZxsCQI+mBkhyj4cjbiQrxLvkaWL6lhQIirXuMpqSSMEzli7El5roqIo/FpUWoqgwaAOvXK3CAzCzwoOlCzInKaCkQqNJIOK1gbxyrdLyrEONnTLgwDMWGisdJofxZZU+NWYURGig5676ZXnjbAF4MESrvho8qRQG8G9VtkQSdRCLaBnh3fuMUNBqOI2hOV9Hxnurm+KUDF71M1KyiobzQK7AcruSSfZfRHjGOkIBUt/XKXCvhVQ5pIh7ibN6bfYKxfZAEXjNEKBKw1EAIuC1uQLYqYpMr+aFhwWqWjUOIA0ercxKu/LSjWNCHBXoZuaZwcR4IDeThZF79MWO0ryO1RdKAtxT9PlfSFM+xaym2mIrNG9rn0DeJzVbaDUe2M7/Qg97t6FKS7IhzmiMgIKK4uf/O6z8tSHm9fPYHphFXLlEAGcBICcVUp/H8MxgwknE08r1QK8BkrEyGXE9cLQEAK9yPVBNcICszzCVOQoPlKQJ96hJm7RJm4gGPRFO8tkn9VzKa2k0bkoV7ij8YAmciJWylAtwWQkAzmZcOwyAJxgVrC4BwIVcD+SEWyz1mZyVFS6rjQAgR1yvRd/EYx4z5O6FXuE2YdjjExjgea4HVgHSuRHfmu9sxd8LvVChkDOOSiueqTzjofdxB8ABexrDnQw/7ZMX0uDBTccybAM8yKzMNy9EYt0ZzDo+vAdQN2P0g+eZFSKJ4QIPvG+BgPedN6lV7JXIQYH7+5kHTbdToKpjYUu5twQqXqZvcneBGW5S/FQEJp3UeICUgFAC4uoCe59EHocBO51UcDOB24wxHXqqiADpQ3cL9PXjNFfGqTkMDQ1daGtrW53xm6rCy+soQxYjFYcPaA2fJZb7RHD/M+y0JvLn/5ILz0ojj1QjLzWhySJVM/IYIh/XTiN8AE/Ur8ETd2RwlDrFnzA9kf+rvyUM9csE4OHE0HJd31pdTuq6b+YED9CeV7UMHV/S5av6/X+l3/8uwxN7wwJI7wv7n/iw+sd/VvHidXHwjCEvwVZdryJhls9ceZ2u/09U7bTBl6yXuBCrOVeNoIttUYcNsU31hgC8oCHbGpIy7pfKIkTN9QlaYUSlzztrN2drdI5rzt2NUuzFHXfFmbnNgvCCIPtU7Qza8YzQ0M2sSrmySAsRqZIbzk7iAGKD4FCgP5SAF2ZGUQxPCT8ihTEbFvo1JGlcRP/mZMBMChFLth9HVRSnJwJIz6mx0+iu4Cnq8J/VpYvqsw3AXqS+7QnFTx+KL+EF+mIazRNvqWhJpHiA1kbD9xzDa4ZFQRxWCSUyOHoiTuHpm/+a9V7LVL3KjZTVNec0VikivGZ7onOAJYJnQ3w+T4AvqSZnzxU2zEdfzw0gTTG2KZ64fVEMfeB2ynGdmwcatS9A7PUYHtQVt1O2dZUrQIJ4U5fdNAH1qUlD/A8qNq9zK5LSEzlO69D9HsA7QE12UKIyST0RdMQvkXpDF60simR4EHSA01ydArRAIj/+Ng0SANercHFT78E5AxgYqfs1yL0EETDn5ziyAtx7rrSMnQMMeCQg7qV45A5VS2ArPRFHWjwcoh7MOiUpJMCQqc8IwTQZ19DUHybvBOA4VW30tQD0CZXTUgNC4QEGBx06IXCHoM2mnzOtNXcLHeYGtF/Tn3GyyJyyvKqarLCYt54IvK6VyvwEkMNOwpvMQ61Wvfgxqmrnf4eCCcK8BkhZ1BZTmePwrcwXg4BPeCfCLwaSaAQXEiB524om7drASxF63KHvA17Z58orp5YMXNDwWLYqYq/va8AFyKnCTXV5AVcgxgCyjXRAT0k1bSk9EUxDniwwPNsjMQ/9vr7n1kIAJE2ODQnmb0UypDlAou2OpjVh6uu+5XhkdW0YZJADqydrk876UGk2NVmf4dkj9naSNcqtCaPZTlPlsekq44mrrAChvXG5RAAx8f7L3ABSTuYjJYF4idMHJtUTQZAR4ug+MGncAVH/HRDXe9wX1sGjIKMf6HKQAu95HkgVriFIayhDeRk8sRG8HbSK2kG/ZwdIsGzNuBaHEHE9NkVx/LY37MOqWurOU4qZ7yoGnglvm6ESJKOIiw9Esw1TK8Q20wlblYGWcmmb86gak7RNFfZLuZzbrCIJzzbERh9JDZDmemsibiQrRGygYgdZTN+SMupirbtKRcd1p4VnLF2ILzXRtSr+RGNaiGii/a40f8krO5SV51kAXrV70dftDHuhUZT+76taOG2SCehc/f9/OzQ09G/4g/75r/r3f9L/XGB9Aejf/hrfIl53NSogil6X3+j3R7fQankjB161P8QJA133SCxAqjjNrkocRITMvpenbLd+/6v6/bH/ZyL2OfCMLdR1/oOu+9PIJqwrb1fZjnrVNeciWkZ4xvD53mw4jWGeaKyb4pQMnqLPNztqHsjdhm+JGbmbbRx4xtaHAhQ8T3u9wADPC9TRbq9QKoF9Ma5dU7wUKq7toMCKCTOL1WEAJfTRB4o8iNC5jzcFqlozDiDttnDTgAwX+bC1BXFE8dNdLTSDSUWo+Rq5R18MTfkWs45HJQGeLfr8L6QpH+MOJlWANG/jasH55H3GuOmulhgP5MI775P3WV6IzEOsROIIGAXA2cx7uaT8NW4m9gUV7uibRIe8xADnAOAXGRV4/VSOBpPzXA/k9IFXlP82wlmVAOD9jApulQAgpxUt5EZnfVoCgBc4F09lLmuulQAg+sBuNWmTNmkT0aAn8u+M6zuypG0qknFTqnBH4dYSONEjnIsB8OoEB/gQ49qPK8z1YEcJAHJEZT7heqDXAClbEedZ0DmuB+Imvu0xw5XM66tNuI/7RXoMcBXzer4H1pxwY4unzZcFECflK3SomQMRwY3bPfS+J5jXn7bngVyZ7+0+eSF5HzcdyylJgL554RYVne03ib1/DyAlmB5lVtilv9lHPfC+RwSa71mTyMdeyvUI3F+PB033hwJVheqJSHx4hH69UWCGTyuZIKr36gBql/xnKYhl3Tgw3menkwruxuxhVj5Q8OUdjipww1AO2L9UAhPDDxkrkyq8BslyCmHkORyIx4JZ4MICwjszjMh18Kw08mf0359rwoCBaQpiopHwZ5s5ZgGI+jVAfCFDf3gg+IcwPZFrbW1ts1Isc6LgYatoma7vMV1+Da2SnOAB2k/U2CnT1fr93zUyHJDEgDSG/udX1XjZjEh4+jO+H/xjo6NeLeSFs5jwgobHh/tcRbKS121VtZz8QRu2PdGa1iTxRORc3R6WizDqsOG3Y1YoaeEZw/9/XRKkBQ4rjKj0K1khPt8oxV7ccdc3VLiQXVZ4QZA4/3tU1/OLjODW0qifZl2bFuI7Udl+4wC20KjcLgwvzBAhgahRRAqct9WzrGSNJlHjCsXLkpQUIpZskWv8OD2Ra+SBVx3DU9Th76C56NuB1wAP+fu7qH/jppiq1kdfTKMpDuDGPu5MoifyITUTl/CaYVEQsde3M0nmX46eiFN4+ubnW++1QtWr3EhZXXNOY5Uiwmu2JzoHWCJ4NsTuPAHuUuWIDTR2I1eANMWAiutgCeChD9zaKCuHKw80al+A+JbH8PbTAPJR1gpYx/ON7pzugI/S/M2XJ3NIAL7bFv/LalJ6IliKIUxinydet1UCHtsDQ7xxl/bGfeSNRcudgHxceznN1SlACySODTxFebfwEGdzGcE5AxgYZJ4jNWlA3KRkklok7eOwe9znCpxzgAGPBMRu8krkKn3MwUQc05HjBG0kL/fOW08EXnmGYJqsmOgrF5B3JlGbxqR3hLwM3oXBYDjP1FJNAxgcdDREAPifqvboAGkH/kaFpx+4QaBg2C/EGT14Ns653WgWvNwBUp4/kzCxNWM1C631q6nXNGFsuX1Ip9HLAZBy+rVTmefwrZZQ2azfE15a3eFOohFcSIDkbR2qOaEeC2nkB0yAPOLKK6eWDFyYYZBa4wrkVOGmukoVNzbGgMTE+phU05bSE8GA8H3lx7kRPAL9EUUvNB+gvhFMjDtVfBRDkQzhHki0valpTZj6uu84Hlld2zryxJ9m7RuzPlSaR03WZ3j2iI0mvSDPJoxmO12Vx2bQZodbgNZZEGhvXCoRQEy8f+ocoIlKoJzMPSWBWM3akXVaU0noebN1WU9p3lWJINbBQ5CRLi/QeRI+QHpib8SVywQxFJ6qRWhBYBAQ57IAEixIn02z5k+uII6q2vmzo6r+uIVJjoNHqMcUP51xFDwT3gbHeT6uorj4wNX0bQTtMq0rPwss5TpTTm0gHIotqNG0mkaUy7ndKpLwbMsWYElyj1GRn1kh3iZPE9O3pIy6+LLXqeh8iGnhGUsX4ktNFMJ7cXKPaSECXJ8rzV/yyg0EUgoerGGQeSM9kT9QCXU2dFkA6QujEwJND/07vq2vqLEjpegf/7u+gQG87mpUID2RMySFASgtAvBMf3hX1z0YC5BG3W+knMVHQfyNLofylO3W739dv/8H+p8P0p848Iwt13Ue03X/LrIJU+R7FlWHuuZcRMsIzxj2EV9pOI2xouGzbhGNm+KUDJ7ZvZkbNQ/kRr/PUQG9jYIZB56xLaEAyXMkdNGLnJj2nEAd68iT6zxQYosb/eDJAgPEhFgibf0fuQJ4vMiDCM3jXpHwwnEAaSbPVWg9nWdEAAMimvE7zGoWmcHEeODDzAoh99iv/DGEvnHjaVbaAFuZlQ0Wff4X0pR7mdWssgHO5zZf5Z9xm3F1l+o+2nXhTF8Qm+edIAsd+8eu84KsdejrT0voiYwqf+0U8/pHJAB+5DHA95nXPwSAMxkVXPRp8GgwmJzleiAHYBn0RDjn/eZyAd4oAcCPuU1YTXAPZG0wTGVOKMsC8Hk1aZM2aRPRoCfCSQd8woctrCjjplThjsJfLIETsTaSAfAu4/qWEgDk7IVeBEBO+pIHSwCQsxP1EQByDpzM8ZkcPQdfwfXAW8yb8DlSn/scfKQisJrwGWAHG6DANGQhyYz72HxZig5InWKmMVyd4EUeet9W5vX99jyQ64WLfPJC8r4tzGr6bIDcAPH7PfNCeN9MMQ+krENcseUllImj6N6HlcezzGqGTWoVeykn8Wiy3QPv2yVQx5GwtbDEYZkW/Q23F9j7dimZEL7eOoACivc+LO8kItB67Tw1wd2YYWblWFefKDDAbQKfca/9S1BPhOOFVXi6js+LSo88hwOxN5jMLGw/cFgCHs4W6/KPzdRY0u/9si7v2rMDJsS9wT80Oqm0NMW8LhSeGp+DHwdfOikreh7gcP8vq7EMcXh4/pStHkGT6ddSDCr79fUvBf/YaEca385dIXhmejOAUdClAiI8TRfk4/+lGp9eD/8+xPDEi2He19AD6Wawy/J1AXhBw4klpAjdI6W/RGCeUbWMmVFfUFZP3NFISz7uuCtALBSEF7SemjMc/nlGcEjkiAQYadKMpoUY2nSTAsQGwZrAtyoFL2xxjjKqaueHjwc8bCl9mSirFS/JbVKIsUIFUxJ8y3jytpY2DFzBC5tyTLHeA8DeFu4y4yAicGprXA7WJHoin9LkODd4OVnUwNKvEmY45+iJOIWXgwc29MQ0VikivGZ7onOAJYJnQ/xZngA71ZhQVRkM/fuu3ADSkqxd8cTti2LoA79JOa5z80CjR9yhCq5qHWP7aAC5kLUCrp4IXP+7ugM+TCB9yWKJLbu/yKqoKOKBAZBYirUqvsBzXl73TQl4bA8M8UaoNwBilwrX5GymIe9WN6e5OgUY6BvRrLsKAtIJOGcAG4BEsvvtin8uOU0fh6Z60JWWsXOAAZDIU/CKhvkowexQ8jmnsV5H6ryTWackhQQYMn9E2U1LQqMP3EqlPcFIjr72DHnZBdrgaFqmkPua2Tm1tbVNoxXNvxAQgPnf2A3Rr40MDQ1Vg570zwv6d8C+SK//H11+q2p5uT7Wr3/SrM+Qt54ITocvJk/LGiHwEBXYOqoXP5B2AAeoBykzRzkA0oas2UF2KUq1nAreEycwkRPhvbCcf14ApFw061Vz9OXgoXjI9Iy+D+RcPZBEbLkQAJsMLszQzNe5AjlVuKniZjeoYpoBiaQ770g1bSk9EQwMPyowPNsQ2rtH3/OyQgAkTQ5EA/h06AZ95Iv63p9uWhOmvu4HKj+5Rxf2BHnii1n7xqwPlRZQk/UZnrFF1KQfybMJo9nOUOUxPFB/Ok+AP1Uy4cBFMQTYv5gbQIpW6C4JRMDbmXVak1RPZJ4unZTmvUwQ6+AhPkaX15LKpiXRE8H2UqeqbTeVCWIoPFULLqrmUaDUgGwPhPSZgTbPIUSEkmG59VaD+pAcZx9tEpx2CM/sEuEo2E/iKoqLD0Tw4qqQl3Aop8dWZaCl3I6UUxvsIuPh/EhaTSPK5byM7m+dMDzbsgVYktxjZ8SNZIV4izxNTN+SYD5BS8kHBOEZQ6hbf2KA1EQhvBe3vZ4WIsD1utL8JZBbCKYUPBh2wreGRao20hP5Q1VToU4yAf0KpC+MTghJYUCKwtZLR3/23/DswrGeCDQ/fq3f/xRB+ZIAPNMffqbr7o8FaGlmppnFR0HEc47X9c3/v7yGV/3+V/X7/8oCyIFnbIWus1fXfTOyCW+sPWDIEvtX15yLaBnhGUP3s7vhNIa8L2vgZN0Up2TwYBuCc8PgPLCDeY/zVPFiYmzjwDP2bChA8hyJsN0iqxyOCNSxgTy5zgOl4B0tMMBuxT8vrJT16EIa4OEiDyI0j9stUNWWcQDpYCE3hRME9wovUkrZNvYzq5lvdmuk5DCM3KMvhqOr3BzYK2yAbczKThV9/hfSlA8wq+mQ9ECfvO/eLouEB95Huy6cAWSgkfJpkU0vyT7TSzOWloq+vpq9jTt4DCl/rY95/WIJgKMeA+yXAMg5HDPq0+DRYDDhQJzPBXhJ+W+clcnDXIBlOLE5wvVANcE9kKUJNUVNcGNqKt87vj9pGa0yiYBnk014LKlPJpuqK+AkZO1Nklul4AZ4bzerCc8ugRMu5VwMgJx9sZklAMgJUz7PBTjRPZANEFvbX/B8AFnDBcjNo/+wx963mnn9GQmArR4DfJwNUGAassTHZkzNdzOnDkSbmWnMReb9LPfQ+55hXn/CngeOMitb5pMXkvdxz8kdtQFymzFyH6zwzPu4mygn7wEkXSXug+bldACx6N6Hed8OZjWDJrWKvZQ7LXB/a4vclKnpvixQ1cGwtbBE6NccgbmVS0Ng0TInALVLfqZkQr+K3IyXCtTxlp1OKrgbw31OCnm1IwUG+JSqZazkWLf9S1BP5CbDC6vwyJMLaeQ5HIhvBbPAhR35hxcu4cKz0shfp1ErV6EWetaDXIaI++kwwgeAqF8DxEMZ+sPu4B8qId9S2qf1UfDwIbDftiZPyTTKEtenagHvANhnPzzL6ImhOQgb7UifTjgvjINnrIWmOEtdKiCSig6mKQOqXs+EA9HkI6yzqMOG2KbaIAAvaBB6wdGrs1LNmsBspxIVaTFgN2drbhjXnJ9slGIv7rjr6gYbBVnhhX2ziG64nBYmQetQtYSOnSkuTQtxn/6/DZN0J5HD+GM1PqmOFLywL8VIVF4PyvZSskY0xVYzMDDeKylEDH7fiqooyVO5d3W54xieWcUsog8Qts3eR6PqdsU/UZWkTwTc78VVlERP5CZNji86hNcMi4KIvb5EGc45eiJO4emb7w283787AlnXnNNYpYjwmu2JzgGqmlxamaK6APFwngDPqGTCfb7YVRqc8gFIne1xGql8t+qqJatsZeZlFWXe+LuUOpxFM6grPsepgB1gSc8GTii+RnueNkoj73PciqT0RK5Qkx72AF4XNdnjEpVJ6olgLTuimzTWt0tU8bJb9gAeiSOImQs9EfSNkMIdLghIJ+CcAQwBOUgQMdDklTZ0gMD1SEnw5g4w0LQhDnCOdjzw1G6ekk+b3EeT4b68lLRzARgyf7xOQE0q5QdpVfNF8tA4Lx21CjxtQGpAKDzAEMOuNzZs51JBKmKTk6XXWnsj7sY8p8GjBgQBYHcI3cTNZn6AvPVEAAgJE1cyJt8IbF+hrGAm0hPpp9JHmTnKAVB/uLkEDDn9HnL4Vgbqs/o94Z0HCOZHXgIkcEhQs64JLWu+qkVg7dD3ga5gryuQUx2Be1qF515thuHJ4gZXICX1RKLSbxYJ5H4CKTL4SOmJoI/bV2B4tm3FCE9P+ZrrgeR1Tzepn+MYRvJuCW+sMOBhSvKCh/CC3pgoY7koQAveIuW/YcNjf9Lc+VIe+EMVnfBaedikd+UJcKeqBQiVxbD1ti03gJSTuSwQq/CyDiRJ9UQW6PIjSvNeJoh18LDlpssheljGB0gCVCY3/o4SQQyFp2oRWoi6OESfPdLui4EHWH+qxjY/sW+3FGnerbTvdynt+lfVWOr1LIao2PdUTbDgHVvyVtePLayTtNsyYq13XcAz4W1IC71av/fP9b3caVRRXIDln6jwUDM8OOq2VRloUp12agNYSB/6fsbplBF43iAMz7ZsAZYUqP2nETeSFSJuHAt7MX1LSt+0ldbiMwXhGfuuvuYXiQFS04VMWNz2elqIosKgESCfFYRnPuc3wwTvG+mJ/LFKdlYkaZ+IQQaPFn+F112NCsiLCs0P/f59BGWOADzzOafpuvtiPZAe9KQVqYvyxHP67680Y5jV94C+azETnm1/EIxanZJi4EgNsYjGgAd7KxhPUwnxvqx5VOrmiSWDB9scnBsGJ9Lrmfe4UPFPg7s0DjxjO0IBkudI6KIXWeXwjEAdm+1zf7YHSpw0P6/7iDcLDBCDikTW9U1hACV2lnuKPIDQPO45cYDUMXKDfXqTHEwpAEQ0425mNcvMYGI8cDGzQiP36IvhCSI37O1xGyBXDuhY0ed/IU35dUmAXKkwn7zP9kKOVWcslaxPoyw74ZP3BbzwLebEfDU8cAHzXgaUv8aVcFtaUcwg8DzDaR3YSe7KCwA5mYaGPYZnmvEJrgdyAF5Q/tsZrgdyJtDnSwDwPBcgx65McA+ctEmbNL9tCikbZrVLPsui0WqihbMXgBDfTsb79yi/hfkUwevLejF3FJ5VglbI2okCQI423LwSAOR0YaOTAHl5uNgAW30mxx1AtA0A4CXmTbT5zJB5/SgboOLLi/sMcAB6ItxpSLv2wumeNl8WQJyUl5LDWOWh921nXt9nzwO5Mt+rfPJCK2ktywFtgFwPnK74aTnztC6BRcAYQN2WLwkMJvDCeR5436MC3jdgEvnYS7kBmfsrblOmptsjUFVP2FpYAiA88PECO+AexY/CCAdI21JSEEu5cWDg2emkKmFDM8MuqWKHuHUIOEmX/UtQT+Qq4w0u0bdT2A1W8hwOxJ5gFripDSaI7Vx4JE6FmGs8uTuZt1ALpWpGPDTCcU+YXP2AqF/ryPg5u4J/qDtoMzQ0dLutrS3NTksUvDlUFug6P9F1f5oTPEBDqua5NEedq9//t/r9/40+4x39+1/SgJe0z8ZBoZ8H/9joqBfe9PsJJptx8IKGE5n9rryRvA6hemHHzK7ZnmhNa5J4IhYa7WG5CKMOG8IDOwXhGcPRUQR6n5YCSScMTJLH+yP+a1aIHY1S7MUdd328wUZBVnhh32y1pIVJ3jaPSpq0JWkhRqZKjgM4nbxwngN4QYPUBvJa4XD1FTvHFcFqoQEBJ0hnK14q+qQQsWT7D1EVVWKGfUBCZ3vbMTxF1yMZI0JngweoW+jvy6iZcvP4V+uz9Z1CpjiY0sXuFybRE7mqxp7/uoLXDIuC2Ef9XmzmX46eiFN4+ub3Wu/FOQSZujmnsUpKaC0l8LxYT3QG0JoHfask8GyIX3cO0LKTakyoqgyGNARncgNI4gNv09TDd0OrOh6WUCKJcfREkIPgryLE+3yws0bqO6tJ6ImgOfcqvkZ7nnaLRl52fLSUnshH1KT7PYA3TE1WJEBeUk8Ea9l+ksFYofgHGKUNxxmGpc/1udATQd94XIPsLwhIJ+CcAQwBiT5yMZW85o7XCNx518raeeiJoGlj/2+Qclu1Upkv/FaXabPjUp7Hb/PWE7lpYNKSEPt4WOfOtH7OTDCCViV0VQMp3TytaXoi5I3zqZ9cTOAWWwD3Wv8df3/Nap5nqInisOMJxT+26gdAOh3foctaxuDSEtiZ2UF6IvBq5Pg7KjG/KwxAS6t9vYN+z7ZlVHaQNBtyIhzMukRrOkCS8Vmv0qXnlDI8YPqvKPo+kBeh21VOm6mOPK5LWZI9TbbNqpbvyglIST0RDApIvbm1oEs4A3IfgRRp2lJ6ImtpQ6Go8Gx7Rpdf0i5ScwHC6yjNZneC+VuRDH3k2/redzcNIE1JXmvSICHmjfpzvJskY7koQBphXyvgjkvW6c8vk+bOl/LA/+JZk00yOd+dJ0BobwyXCCBWMd/LDSBtCpQFIuA9lXVak1RPBBkb91By7TJBrIOH4AFd/pbOk/ABkgDV86oW1PNCiSCGwlMUF4Of+vcvswASLFuAyiVEPALYS8vAoL4RdleeVLUTRphznnAIz4S3ITr3cFxFcfGBgBeW3RfiAjttVQZayqWZ2tygG+7LKrCs3/MxVYtz3iwMz7ZsAZYk97gz4kayQgQ4yGLsl9K3pKByLNGeVtGxg2nhGUsX4ktNdJ+K145LC3GvJLgIkDsE4cFGVYMg80Z6IlB0SBKuAb2Qr0I/xOiEQNND//6uqmVGt/U8/kzfwLt43dWogOMLupzU74/UntAzmSsAz/SHt3Xdx2M9kPSB06YIjvLEEf333SpnI2/E+y5lwrOtNRi1GgbwBZUtWKgOYhGNAQ+GEOfvNpzGYMKsskda1U1xSgYP1hmcGwbngVxBgirEAjsgB56xrlCA1PdJKDqcKzBAifPQnSZWPOiBEvDONkuAKqFtl4IYBvCPBCouMjwTJN8pDpA2DB5iVnpA32CRm6+BiMzre5jVtJvBxHggN/MQVhbveLQTg4HgKrOOjTbAlczKeos+/wtpyntEANK8bdEE8r57uyzM6zuMBz7CrOh9n7wv4IWHWS64cePaiuKf8Til/LXDzOvb2R6YVVy5JABbAZCzdj2tfKZXa8YDzfTAQeW/9XE98AFGBR+XAOAoFyDHPikBwAE1aZM2aRPVpjCVDc/7qGoYWE2YpD6ZDBdzJL0RZjHiuRMFD+6kMu4oPLsErfBBzsUAeGuCA5zBuPYaAHKE6RaXACDHCe4CIOeoqNdB5twBRNt1LkCVNJKzoMZN2XyrIjCKtk9ggNe5fSBsDaXg9K354p4f5tSBk/IVmghzIa7z0PsWMq+/bM8DuTsS63zyQho8FjGruSQJcIZnXogotPvFANIhZG4mtg2cQ3s5et9sgeZ7zewB2Eu5YwL311nkpkxNV2LWcD5sLSyROgTf7KYCO2A7c+lWXX2EAiSXPCEEsYzr3nt9n51OKrgbc0TAtbsLDBAOco1Zx7gTWeOOOUBtoa2tLWsnW4VX5A1WqDlA1UGNqTyk/ozBrB9hWTvghWu48Kw08hjuj+Yt1EKpmlfTbssRk6sfzU+/doI+Y5qNhLsq5DxgmJ4IvHBGiolmFDwjFvAVXecFXffNnOBhifYfVS1TEj4L9Ez+Sb//vzI88Tf6M34cCxCmK8eZj68l6HTj4BnDw/uv6XqnE8jPXXmdrv8PybumBQYPDkRsOg8YQRfbog4bYrN0hwC8oKEpI6LrlFSzplNRS2iFMS1m/XrElt6guWFccz7RKMVe3HHXTQ2WaFnh1fUYpqSFSX1cq1XSbAKkgRiZKnlKghv9UWBUloIXtrYcVbXY5Uu2bC/Bmq3GkjSij+NqlySBiCXb30VVlOSp3E/V2IMnV/AUXY9g98dV/VEEgNtAH3C5ktEuWU9fjFlIfB6YJ95NsrBIoidyhSbHww7hNcOiIF5WCSUyOHoiTuHpm+8KTEtcpZiqa85prFJEeM32ROcAtX2nJPBsiN/KE+BRNSZUVQaDNsrJ3ACSIvariq+KXQRDH3ggay5qjp4I5muvRoj3+WCnSc4js0noiaA59yj+4b08DblrernwRAASxFFq0n0e9HVIMfW2rZzIMUk9EQwqSNiFR6QdqnghH1gI9Esn/XGhJ4KmfFiD7CsAyDu0vu53lS3JpZ6IAXmUILbnOHfEyIqgqRHXytp56Inc2//TMJFCqY1Kq/BbXVRjUrs383LxvPVErhqYtCRsJa+cZf2cZS0ZW0ISfsG74FVXVIiUbt7WND0RMmwSYIsKISHYc0SeK7Mf12XBs/VERq0yQGViAKRczZh4I3HisozVtAabP+mJYNDC+d8+ysxRDoAUcIQMk5uU26iFDiqKplKY3PeE5fzzAiAl+9+k0qXnlDKM+EgqgczDPdQd/LMXAMnj9ih3gspprVPVosacgJTUE8GJHzwGfaagyzgDskvVEsuKNG0pPREMCu8XGJ5tADhAGijNBQiv0+Vl/c83FO/ASt7WSuv2l5sGkKYkh5o0SEjZdv05/jFJxnJRgDTCHmLM5Ypk7dSkH83TA3d71mTjbJbKmEsrK8CnVDlyxhjDxHtjbgApJ3NZIFY3gLNOa5LqiazQ5X9QGFmZINbBQ5AR+nia1/IBUpzgT1Qt/u61EkEMhUcrKDxoX0O/ZwdIsKAbZwSoXEJEUA+CmJBp98mwD0uriT2K//AqCp4ZHO9PshyNC7AEvLCgHjyg2Wbv/JLLp5na4Oax3Y8A9F9kmEq1ENSNKl1m3iTwbMsWYElLnagzH1kh4sYhdrBPSt+SYG6nMksQ3r3WkSrEl5por4rXjksLsVsSXAOQXQRSCh4MAabHw+IFG/WBO1Qy4b2kfSL+/U39WrdLwWTAIfmedjX+CC8HHqzhsY8pDUbd/SnvPcoTz0RpErky8sY9BJMDz7a/CZ7ECgOIhzdZxJXrIBbRGPBgOOo10LAJk+hoVmXquuZcMniwhcHz0ME+cD3zHqsQSwrP/oz1AAUDufuLCJBGUIkBbJ69QrE9UALesL7RlwrcBQ4q/nlhrFAWhgHkNl+EWfx5kQcQ8kKJpLPjAdLUZT6z0gPNjFFJARHN+CyzmhYzmFQEvW+/8scw5brL7QttgCsEvO+mL/SoKXO9sJp3sGKdtZ0o3ndvUsy8/mHjgdwkin0+eZ/lhViSsfpsLFcrAs23T/lr3INCswFwMfObPD6BAc4AwJmMCvo9hmcGE87Emt2EvQZIxlne3c+NzhouAUBOHu0WLsCbJQB4RU3apE3aRLUpJAue1c75qGoYWE2YpD6ZDDurLzLe/3nlf4SWSeqTybij8NwStELWPigX4EMlAMh5ingZADnyjmWIkeboiXwGgJxBYLnP5GgA4SQyuwKA55g3sdJjhq3cVQy3CcNWTXSA55iVrCSZcd+a7wNMgHeMngj6QM4DFtzIEx56H7f//siexnBVqjf45IU0eHAVyS7aALk66b55IY6qTWPWMXoPoG7G6Ae5Ystb9Df7iAfeh8eR3CeRl82TSHsl8p7A/f3Qg6bbIVDVYNhSTkKQZZG+ySJDBLyZzDpMOqnxALVLfiIE8ZECe59E9OyonU4quJnwDrNyTId2FhEgfWgoVXC1o8Y9iawE3uQcY2VShVfkDVYBiMPBMJaww3SvqNpJIhY8ip/DeRNEtr/ZhCaLVM04BoaTSz0mVz8g6tcAcX3KjYQ7KuQ5eJieyO/a2tp+L8VQHwUPkZwLdX3tkNjQdV/PCR6WaP9J1XJyod+DnsmgkeGAJAakMej1pDpLH4blnGmkJ4IH5shx9QUmPGOIjF+r60Xd5x3qiUCvZD3d+/TARJ8DESF8fUaLxLaow4YrYwaEpPCCdotG+2NS+kuUl7CddoaixFUu2c3ZGp3jmnNvo/DluOOumNOtE4QXNJwRRtD3SFqY1Me1WSWppYUYmSo5DiD6whfU+IN2UvDqRjhdjETlBV3/SACW0WeaRdtQXO2SJBCxZPurqIoqMcP+72hUvukYnqJBC96+WdVLEQFWJ/Vtq5SMdkknfTGNpjh3VAKdvSR6IueoL3QJrxkWBRFbVYkkMjh6Ik7h6ZvfFpiWdDoCWdec01iliPCa6Yl0ztgtwBLBsyF+hzz/Wh4AexQvsrNohuZ7NLcmTAKdP1b8wypFMPSBe0gjJR+ABNGofR3zGB7UFV/lKCyycqjS6uGg7ngxCd6k+HpvednV2u2Pif9lNSk9kQ+pSRfdG6uSHbq8KgGP7YENvPEYLYnWFAwe1tx9lM9fzFzoiaBvfNPatFzTZI8bcgHOGcAQkAdV7UH2mhznjhhRESww5FpZOw89ETRtPHN+T8NEMCOSvbYr/sPtoMHTRglabgJZeeuJXDEwadmE+JQFNHqbneGFCbzrNv0EqNGsczgJ+/8CDAAEOw8uj2QYigAAAABJRU5ErkJggg=="
    }
    function y() {
        return "iVBORw0KGgoAAAANSUhEUgAAAG4AAAB8CAYAAACIRYVrAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYxIDY0LjE0MDk0OSwgMjAxMC8xMi8wNy0xMDo1NzowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowNzgwMTE3NDA3MjA2ODExOTEwOTlBMjY0RjdDODlBMyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpDQjA5OEJBREUwODYxMUUxQjY5QkEwRDk4NDExRkJCQSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpDQjA5OEJBQ0UwODYxMUUxQjY5QkEwRDk4NDExRkJCQSIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1LjEgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RTI3MERFRjExNzIwNjgxMTkxMDlDMDc3MUZCMzAxREQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDc4MDExNzQwNzIwNjgxMTkxMDk5QTI2NEY3Qzg5QTMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7ff/ymAAAVoElEQVR42uyda2wcx33A/7O7934fnyJFihRF0ZIsia4rI4jjmG5TFEWdxPlQtOgDsfupKFokTlLLsQPEBmIXDWrYKRoULVrY/ZD2Q5JWbVrUReNarmNFbmqLjh6WZVEiRVJ8He/Fe9/tbv8zO7v34B15fN8dd6jR7O3u7e7Nb/+PmZ35L1FVFczUfEkwq6A5k7STByeE7PgPeOFb3xrFwq9lMlpjt3EANUoXnv3GN87v5PXslgYjO3mi7Qb3ogZprLPd+3Bbm3vU7vAOHOztBJvdC20BF7hcbhAtDtBOS/9TIZ9NQCyegPhKBhIrUQgtR2FxYW4yGk+Nx+PpD3Cn889sI0wTHE9/+sILozab5Yud7b7H+vt7BoaPHgOvLwBOd7BsP7mQAQVz5TlFiwuIsFqxZNMrsLQ4B9euXYGbN6eisXj6HH7177/+7LPnTXBbAPdnL7445nDYvnlkqH/s1OlT0NHVx9YnE8tQyGUgkwrjxctQyCdAEgQQRYIZS4EgKMz4R3+boqgg0ywrSNEGgmADyYaSKTnA4Q4ASi3ksmmYuTMBFy5chJnZxUmBkOfPPvPMaya4jQEbsEjSq8MjA2MPPPAJcLm9EFqcgXh0AeR8moFhkPSSw6JZ4ADpaRk4oOAAs8LhUYgKL7XPomRHgEHo6B4Em80BH390Bd5++x2IRFbQLsITCHDcBLc+tMcDAe/Lj/zSI367wwZzM7chn0sVQbEMBiwB10eiKQRGWI7FM1CgkmVYOPRYvHaQJAFUXE3397hsDJqiwzOAArg8QTjYP8KA/8/5N+DGjSncJlPpe84EVxvaq51d7Y9/+uGHYH5uGtVXikESODAVJSUWTwE6FZBIZhkwKlqEZ7aMxykUCkzC2PlRAi2ShYHAGjUq1WGTwOO2QcDnBB9ml9NmSCH9qr+tG4aG74WLF96Cyz+/Dul05jWE94QJrgq0QND3+NGRIwwYcuLAAFZWUnB3PgzL4QSzXZQBSgFKjIxlgdkuuqxyMOxPhbJl4MuSxcJUqNVmRSmUwGq1ss82BNnV4YOeAwE8t8gAWm1OuOf4fXD16jhceu9y3fD2DTiE9mV/wPfywb4ehAUcGkAmnYWpO/PoxqfYceSCrEkThaTVENoniUmVhFLFPEhRBEHSPUjtdykINpfLsgql3y/k85DFz/pxKDyHwwFOzNRG9nQHoLe3g0meIEpw/MQvwLvvXoAb1yfoDbMuvH0BDqGNWqyWS4ePDGLlixo0zOFwFKZnFrg0KQwavU56OBGlBiVn0mq1nccVH+A5xjFPfvlLfzS51rle+YvvDuBBBlSWldOZTGYsl82MJhIJlPIsg+71esFuR2cF7es9I/0IEq9JtMDIPSfhv9/4MczfXaCH+gLCO7ffwV060NszihLHoFBokUgUpqbuGlJG7RU9imS1Ri0223dw/Wtf++qTk9txfS+99AqF+FgqlfpSPB4byGbSCM0Jfr8fJdAGw0cP4XWIEAi2M6m+iM2FVDJFe2AGEV50X4KjHqTFYnl18OgRBozumkomYeLmJLM7VNJoG0xAScT9XkGV+PxTT30tulPX+u1v//lzsVj0m7FoFBxOBwT8AXC7nOikHGIe6cDgEFy+fBnu3J6iu9f0NHcL3F52Mn8x2NGBv5RwBwJg+s4sc+QpMLqOQbNanjj79FNP7iQ0mvD4z3m9vvsCwWAUJRCoCk0kkrC8HGW28M70HRg8PERvInbte93JvCfgaCOb9jm6PF7gzJhdy+Xyxl1LHQXJIj1x9uxTr+3WdZ09+yfjbpf7Cy6nC52iOLYJZViYX2I3Eb02Cs3l8dBdB/A3PLbvwGF6zIZOAEHjr3mIBOLxFa3ZzDUNStv5p58++9puXxhK93mfz/8KXWaOCwKjkkcvaym0DD0HD+q7PrwfwT1stTm4USCsUlbiccOLp9KG9vHJvaoUURKfp02EZCrJHKNEIsWuk6pQr8+v7za2H8GNSlbao6FBy6Qz5U6NIEw+/fWz43tVKSjpUVSL56hHm8d2XzKZ0lQ6wqNSJ2DTgf6G/QhuwJA4hCfz/kUoOmTn9tr4O50u+qwOMtjG4+19bo9pT4u91FbvK3CsVwK4xFU60IJAYnsNThKl8xowhTVTVKY09Vy8AfcdOOB3L1SpEJUNNdj7RHtR9A7rUoWgAtnza9tDcJXAyuQuCg2Scvl82U3WCNCYRtjb0xPDblSIYgMmUqIhYD9LXKXkNVpSy26gGjZuP4OrUWkNdwWNdYM1CDjS4PeO2nDX2DASV1otjTEsXntq3qAG1xyCXhNbg8+pEBpO3BrQrSQmuNpwCB+lpd3tDSFyDY1ujyVOLfHXGqtyyrCRxtMGQmNUkVpxU6sNhI6U3FSqKXGVdUBK7uxGU0zadTWW3d1bG7fKmJGGsSmNcyXV0571VapqCTi12h2+11JWxerqo5oaQPr2rpOZDQtX2JCAyooQGoDcqqGF/EZTG0Rl7qHEsflPdJxCQzZ2SbXr1dW7uq8ljg4rl7UHO6oCDZ8YLKVhbrIdH8l85UdfeRkqBtYsJy1jdLIFHZtPE51smMvm2H2uogT6nGTcahGjdAowHcJHh4HTEtiywNcJfIqVwKdbCYZxZNapxIM31Bub0aMYpaqXisw1gMyXZcjmZH8omhmlN5XFIrKn4doNpkCeThrBa/bY5XGrqJQ+9P3OiUdfOtf04K7+21fHViLxNz0Bb/UbmFU2AiESaJAs+FFCoFKxZMsig0yBaUP3RDZLR4cHbOqwoHEjlf5gKTTQ7KqilkBTGCgKgkJTKDg6k0eh8+wKuFwsVSXP1oNaYBqDgtRPl8/m6YiwyTO/9VeDLaEq45EV/EEyBDsD3LgTDZZo4aAsKEhWtixIFjaISBD1UuIzZqRycHqulLq1XFJVNaZnlUublkvBKQwWL1nOa2Uhz+Apcg655TnIPOQyGQjNLYHX7x5oKRuXjCchk8lDoD0AdpeLA7MyYILEM8ISWSkxgKIOjuVKiROKs1AJ7zAjsE7LSy1KOqhGc0QHWA6uCE3mwNhyIadBLFgw51gQgWgkBvFQCK9rd/3NXXNOqCqZn10GuzMNnkAQfG1uBGRj0ESeGTCeKTCxROqIqAMTNhnNgZS0EUmZYLK5CkwKMdNZQqJsQBNoieeXER49t4w5n8vD8nwI4stLTPr2ohdj173KdCoL6fQihBYj4PEHwBsMMpAiV3m6GtQljH0WdUnbmeEDurqlI5UVHq+BqHhOLo30GjKpFMTDyxCen4N0coU5MsC84n3WHKDz32LhKMSjcay0O+BwuxhICtHl84PD5WYVqrKssLJSUrbb22dqk9u8eHgJErEIShVKVjiMdhrtmqJqTsl+boBXJnpHZ9NpWF6Y0yY6ov7xIkS7wwk2pwtc3gBYrDZcdoPV4eYqc/O9idRepVciDNZKZJFBiS8vokZIsmtRePNSUep/Gk72I7hqKRENQyoeLpvUr5cULI2iQKMD0QgNmroDI3wGXda7FvUIDLl0Aj3AJJs4qXAoFFDlcjMkCZo4UW8vHV/iEqoBE4y4JyXQeFZaKDSnOVjIBGcmE5yZWtvGlaZUOg+zc1GYnotAOpM3oufRfwd7sHlht8DwYKcJrlHSjVuL8OaFG/DB1dm69j94wM/yyWO9MHSoE2w2iwluV3tgUML+9h/egY9vL22oFTUzF2P54vss0Ay8/PxvmOB2TS2iKvzLV88z1WjauCZKf/ePF2B2PrblUUW93X4T3G6l2YUYTEwta8/gKtLoiV4YOdwB/b0BowEeCidZnr4bQcclBsuRpLH/rz1y3AS3W+nKR/PaMIaSRD3GP/jdT8DIUMeqnpOjh8t7TpKpHEKMsuiwfq+zaXtTmg7crekID7VRTL/84DACaq/r+04HbRa0G/2TpsTtVo+BKK4CN1wnNLPnZC8vWJCMJ+Z6nrgTM8E1eurt9hnDG/T8k0vz+w5e06nKw30B+NnVpbJ1BRngb75/DXo6XfDpX+yBoX4fBH12E1wjpXsGA9Db5YOFcHrVtsVIHn7wX1qPSE+HEyF74fhhPwz2uk1wjZA+/8gh+N7rtyGbq+0WLkbzmJfh4uVlsFtFBm9kwAunj/pNcHuVutoc8Hu/fhj++c1ZiCcL6+4vY1vt5kyK5R//7yJ8arQd7r8nYILbi9QZtMPvf24Q3v8oBu9dj0E2X1+jjEJ861IEbs2m4dFPdYNFEkxwu51sVgEePB2EMyf8MIHSNMGkKl3Xd+fDBTj31gJ87qGupoTXEg9S7RYB7h3ywMkjXtblNbOYYQCnFzKwFM3X/F40qcKV20m4b9hjgmuE1NflgL5uB+urjCUKMDGbgg8+TkI8tXow6/WpdFOCa/kxJ16XCKPDbvjNz3TAkV4He7tjaVYUgk2LvAmuYe2hhcAnT3pAEoVVOZaQm+73SLCPktspsnfSVaaCrJrgGjkl0caJVR6a+z2iCa6R05WbCRBhdXvPZTebAzue8gUFxrHBPb+UBZ9HAr/XAgGafRboaq/esRyJ5+HyR3GYxeZBpWw57CL43FLTPVRtOnDXPo7C4mKKeVUr8QLmDEyXbLdim87vsxpvJV4MZdb0xPq7nWbPyW6kTCoLglrbfS/kAEJL2brcZq/HCkcPe5tyCEPzDV1AaKKS2/JxgkEn3Hffgaa1100H7shQG8zfjUAknIRMeuMAHU4rDA13QU+v35jMaILbhRQIuKCtzcVmpGYzOUiuZCCxkoLwcoLZtXg8BYV8sUEdbPcwY0fLji4feLwOY0ZqM6embg44HFZwuWzQfcBX94zURo9uXrfJADOZ4MxkgjPTejaOrB0Ei1Qsk4rlNfO///UTrmpGdCEUh0xOCzKqRxPK5gqwsLxihLtgITAEbrNoSaMN0WWRwOJyGnIFhQdkox3HuE0UtH14Jnx/PSKRcVxe2rChHvRKPMQX4QHawAgiaoQexkz3pRlULdIe9WwkQQFHla4yPHYAivF/N5IBVscNrmaQVd05IVVArQdJqFHqyyxfuDQVGun1xa9+eNd7ezZa3IWUl8WAasXgakaUvJLlUjCCuDVlkcsr7DmcHtuy+INpOCi9pLH+tPiWhMW5lLGU2TKBAlsWADNRwO+zQzie/wF+0c0rVykplZLPldvUim1rwTTezkRK1CWpUvllICqyyLO+TCo+s/KBU33+z44dO6aoKkE3nIbHIrKiCHQZS5F9lrUS98Ht2j58GetIFegyeoQs2BarQ+Alrtekg71AXKvnNRJhgkVUwgUMbwqFRZzS1xGionArAl0v0G30M98HS31ZFAWZlYIgo2ZQsFSWY6mV737vwnUOQS7JSkWpVnyWK+BWglaqgF0XnFADolgFnlANXBXAa22vdhxSsb2WpK/3Zr5qd7RSQzqqVbBSsb0WmHq2KzW2V5PSmuA2YseEGupSqLGuFnxSBcxaKlioch1Q5aYDWC9gZbnqUUpKtQ5VppQALQWrrlPptfZRa6jQde0fqeGI1KoMUmdeSyKEOvdbL9eyyeulysqATTgS9Vb6ejZsLUjVnBVj244H0770wz8eTiXSzvK3+uqBr4vOiL6/HldZD69clqusA2MdMax2aSDSYkRYMML7ljiMxRezqzUyj5VSGlEWjMjt3HkBahpV4gt6Pzzx6Etp2IW0o11eV370le4rP7/9O5eu3oV0tgDzoaThTUbjaYglMlxcBL6ax1YuDZjN3HqBh/UVjaCjgl7S5gCb7Fj8TD1O0fBA6XaBNS/ocVjJmxmC0TzgUfnYTaDfWgq/zRTmRbLP3Ku0W7A5YQXDq3Q5RLBIBHr7swMnAP6pFfoqbQG3NTF25pCssOir1HkUVS3yuUgbZRQM0dpyWrMgmy/AIrbziAC8HQZGe46FPdQrnm/XmglgtNeKIRAJjy5U1DxcWNQKqWLeP/Xw6WelZLvWfhO1fk4WgFTl70tQeIRYbBIAgycJgmpzuynO1uhkpsZ7hZeVTkI1mwp2mwUGDraxF4Dw2JSkVrxKbZnUCnuo1upkrhKvklSLXckDjarrOD3AnSv6dDfVKuB0z6tQj0nchWspPY+6zefNV9ygTQ2OWu5sifu8GVCkBoB6j6Nu83nUGppF5fBaBlxuk+BInSCM/WiE9Uw6b3iZVC1Kksgenm6jxFU7v94eLLQSuHQNFbIlFZVArzQaSUA0nGBDGFbiKbKWSvR4nRBoc0MP2k+3x74ZiYN1bFyBa5eWk7htsXG3JhZh+k6obHiC/qBjrQoPR3OYwzAxEYZgmwvuPdVXGfJwK+AE/ltbSlVmNgmuqipbjmQhWxC19/NsUuUthfPw9jtT8MCZPkAXfjOqsXK7wKG1jMSpXOKULYArvxOIRGTBumWnQ8a29MX/m4eHH+xjDfQ1bCDUCU5tJRsnc4nbNnCdnR4SiimrttPeC5dTgs52e/GBFe6RSBUIHc2cTMurJDQr05HRMTipTeTfyo2gd5jn9rPErVmBg31u1i1Gp0ZR75HOHehqt7H54DVGebH+2Ou3EvD+tdiq498N5cnJjdu0ajZObDWvMlNnpdT9lvShfjaukvCeE9bDst5xRwZdrPfx/WsrZZKXLQAJRfMQ9Fo245SUghNayTnR9b66SRW0mfZdTYwjA0742bXkqvXJjILg1j3OWsdXOLiW6jnZbr2/Wbedfa9k8qkBIplWNtsMKD220EqqUq3Sa7JnL/meC+VArmJtHdUnNm60p6elJG4jqnKjErehY84itP98N1p1vrffvS1TiVte4nYNHI3zRQPVfDiZhFCNQDVubEYEPFuekarPo1RMcJsAR6PpzSxlYWYxy8JD1ROg7cwx/3ZdE2klcLqDom63k1H6vY8R0rXbico4Xmsdn207dsgFAwfspQ9LN/T9Kl5lS0ncdoMzvj+9kIbXf7qo1pCsNSt+7P52GB326RMbtwpObTVVCWv8mC2pzys3Y+p/XJhf76ZZlU4hrAdPt6kep2U9u6ZuYpvaShJX74+se1zkR1Nx+Ne3putqkNPosMcO+2Cgx0VGDnlZ1xgfc6LW8/06JJDU8VubUuLU7dwvk5PhX968DYWcXPN7NKj2iSMBOD4UgAPtDn2wkFpjRupWr2+7x640Hbi60vVbYUgmM6vW220iPHR/D5w52QVtNAJ6yVTiXUwtJXH1/si67tirN5fUQr6sF430dLrhD3/7NHv9ilAcxbxRVbcdqnJfSlxd+6VSWSgUyhrT6q98sg8cdmmzxzdV5W4kRS6w939XqskGSLvqnDTdHHAaMFQpFMryxffvwH5LzfeKlv4gXL5e/iLbn743CQ6Uus9+5ji4nLWH78/QtxbfjUEokmTvjztzqt8Et1vq496j3XDu9fFV69/4yQ2W6Rsb24JuaA+60B5qL/lLpbGs8j7VGdz2+V891ZRe5f8LMAABIAgsmcTU6AAAAABJRU5ErkJggg=="
    }
    function b() {
        return "iVBORw0KGgoAAAANSUhEUgAAAJQAAAAsCAYAAACDpZHMAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYxIDY0LjE0MDk0OSwgMjAxMC8xMi8wNy0xMDo1NzowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowNzgwMTE3NDA3MjA2ODExOTEwOTlBMjY0RjdDODlBMyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpFNTlDRTUxNEUwODcxMUUxQjY5QkEwRDk4NDExRkJCQSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpFNTlDRTUxM0UwODcxMUUxQjY5QkEwRDk4NDExRkJCQSIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1LjEgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6N0NDMDY0RTYyMDIwNjgxMTkxMDlDMDc3MUZCMzAxREQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDc4MDExNzQwNzIwNjgxMTkxMDk5QTI2NEY3Qzg5QTMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7XivEXAAAKBElEQVR42uxcW2xURRierQU0oaXqC5cCTRRCi7dAFPBBqoGqgYRb1RJjaCuYiAm0oPAidxMDCi0+QEIRlhhCwXJLIIFiQssDF5PWIggJ1bDIzZhooCUIBFjnm85s/zM757a7lC2cL9me3T33mW++//v/M9tQNBplAQKkChlBEwQICBUgfZGOIW9D/9ei/HU46J3ux6XMdLqg5wbnvcIXVX/dvRvuG81sDLqo+yEzTYhUKN+CUIWnM/6r7HsvqyXonoBQiUKFt4Y/LkRCC3nIw3v+ejPooseIUNzn5PFFnv79J1d+bvChTIcX3xvGroZuhWsyImvlKhDpWtA9jzihOIFy+GIyf02SS7vtsGiRKrOFE6xFI9JSvojIF7vJ7rVE2E117BavhAyQfgjBmYdCIS9EquKv0gTPA0It40TZIwkVld9tkcet5K8Z8FAIeUG3dN8sz7UOxclUwRfnncj07PChrN+YEaxndpbdJjDbu1EKQJiUpNkLMsn3INXegEyPsEI5qVLeO2PZ4LfHChJlDewXt+/VY80scqCRXTjYyNovXrWs25Z5jf0ZusOeYhmRAdEeDb+Hbpc+H+0VHn7/ycZh93tFgi5JG0R4RIn4VSgjoSSZDktliWHo+xPYiHmzjCSyw7kd+1nzmhpBrJU9/o593yf6BBsU7cFOZdxi0+8+Ld4HSEs0yGgS5gS7liihfqFkQkgbW7VILBPB2TNn2GfvTmFfb9/CdtXtZDvr6izrt9ZuY6NGjw66Lk3wz2/n2J22G2Kpoo1EGH7XjlhGD8XJtJmSCWFtYt26hMkEtLW1iTBnwuYNNQGZ0gzKE78ws4SN/34Vm3H2JzZy/ix4ZNif85wjkz15KLnhbnrgqfU/JHVxXy1fwU4cPy5USgdCXUFBPptevzXoxW4A2JbGyuVCtWTWvtQ25EnfhGwOS+GTQCaHzM0VINLCz79gly5dEp/zCwrYZf4eijUo2pMTSpxKsH/EvJndqnHhDc/t2BcbeGOWVdpuKztAbjvEtk0RYo4tqYp9xjGTiQwPCiAV7p+jmpOqkhKKFjYrFJmAsVWLkyJTB6FOxMgEfLl4Efuueq0g2rxX34o19KmaWi6vH8SdDxeNi1eA/CLDNI2c2tGdKgypdurgVODGpasWopjQvGajuLc7be1xIQXXiCSHAr6FHhOf0xGKG6c31lZwITrJSRVW66iHmkvLAoihyXimES+9zMlTbfn+w5LpLDc3V5hw2uFocMl4rTzxhoVkrT/uN54P5QkKdFY6jOKm1TVxZFJKhPX7ij/ttuEP/ScHd5V8BCeQSbxTTqo6BCoEUumYVlzMphZPi5lwkFaNSBBKPy/IBFIpsiHbQAfpSsZHCqNJhJ+yRqIY8t6E2KDrmd07jjB0gOB6euf2i2VOnWWYiZpyDeEJ0HrL53QGMv8rR5tzeJ8s4R/LYoTCOt3hUxyqr2fhTZuFusypmMsG8KWTbwpv2mRcR8mkGlQRqiNVjScLSEY7J3LgiCVUYD9aPNU7SZEVpFOdiQ7GPWKU6efTfUzRplWsvnyBuE7q9fDZ6qGGkmO0WshEvSj2g3L16pMVF/KwDdZZPVSWMZReOdoUazucH9eFz+oe0Q70+Lgnug5t3bR6Y6zNsa1fm4D9XpxVgmsu5aK0DN2jCGUpE8TXkc4KopyQ5AKpSsvLbdXJBPgnmHI9pDWSe+Bsj/NIaCy8VGOAGLShqDqphrExkBbP1X5xvyAnOpsqmu5jFJn8eCiso+ei99VRhlnvycCbPJTpftA2hz5eIO5fhdj+r4+M26Zz8LZaQjHeox2xhD/yA3hfOQgQ5aqVhyp0ItT4ovEsOzs75o9QCkD2pmNXXZ0gno45FRX8GEWxY1AC0Czm3zOtNhddYqtIIEWnOk2IG8m08fVnjmhAavrdOtgr4hSedzaSBpyLFAl9o0MV98cNODUgTH7NBLvtcGz9UZkXlZKDZZJuygUgxTqgLCu//cbyHardOqns1MkpTOr+wwQQxWTO0QC0cXQPhgyLHgNqBHVAoZZ2EvU1JqDDEO68JirYTldadBSuF+Takj9OkN0vdDXGveCeSo7v8aUsal9VsLRTVz+FUBXlMtxGV6dKFQml0UmlSIRQSEsECm5VcKpQTh1L1Ud5F5rd6WZceTIFPMwGefBCKKHbXjh4xPH60GnwKH4yXxhWWV02KgTCBPVqXguLCvAu9HrQPl5rVthWKTVCVrJ4pkAkDznUlHsClEZ4KRLWUBrIL8gXpt2EVZqyJQqojxqhaFiEDho+dDOu+w8oQyIAkRKpx2Ef7KvMsmn2Be4H9+U1K3ULaV7UXo9CydYa9eNl+PUMIIjuhRD67LyTU0aoq5LTCFOZWac5XeFoxk1Kg/1NL6T0djAVUr12viKPyigRmvRMyk+IodcJlaYEo6a7q0EHSZxC3b7uPApAEGR4tGhpqjmBdKXlZR4a3ns1mJYZaGOayKQkXW2HbajHQiOgM5Mp4DqRaV/xbHEOEIheXzLno/ePY28bNUUc+2GSiQyKCFWoBj9ZDYiS66I8IJ2uZKaGpwrl1ti6OXcrxKIsQeswKuXG+11FH4lKtalCnyxAJuXhcE6YcJyr4zVby3KH+CCU1SepdP9hkqmj3NOERQslVEunQXVPa0EU+Cmn9V7Uiab8XivDuho5Vcb1h6vq2aCqueiZU6oQ/4yunSQE7RZj7dfDIEM1KTIG1YNQWy9qLAndSAnVSEOBF8ZPLS62VSkv6kSzNeVxvDSurkamyrg1PV5n66/wPS0hpAq4RvglO0XFAECan8gMi9vXb4hZs7Mun5AlkPXiPc5J+83JF6YSpzduV2/D+EOnr2DqSp6Xai4tZC4wFDibfz3pSijcPH04igZ2M9amsOx1VHaE11axL/ZxmkaSaqgZkCoTS2ZKCtoM94C2yhrY31Jzo+oHQj/oZ5oQH1gHfl5MDy7Tp6/gJ01LVIchzXXLcMaJ6vcKiynHA2Av6kRrMOohsFckIu04h8rquhqpmtNEo4eT9/NTikgGsA+cTJgOXGkqG1Qrp67Scre6h8lL4QGwG1AlpmZ85PyZXaYW3R12YVQNGvjGBz0XTJFJkruMzjF3nQIMj+HW2ZjeC5UCwfQHwPG+yTppLhXTjB9X0FDalcpLHlCX0cl1xl+9cFIhFlX4JZUX6GRSpjkdp7kGMBMYkUtGFwuZbAklSYXf5BXqWUmio0DNvdFTdLspvQHSC/Bu+G2lVKWIJFODvl2X/9BTP5ZTyh/g4QJPTDCdCMmZVCT4JMwEqHb6XV7CP0VXGZMerqBGmFAGw2b6KXqAbgUQB0qEXw7vSfiXwxqxKmQ5IScFF4iLWxb0U/ojkX+p5IlQmlpNTpBYln/nE+DRhGdCacRS/3Cs0IVctv9wLEBAKDuC5bEk/iVigEeUUAECpAr/CzAAH1K+cqlN9hgAAAAASUVORK5CYII="
    }
    function w(e) {
        return "object" == typeof e && "number" == typeof e.length
    }
    var e = {}, t;
    return e = new n, l.call(n.prototype), n.prototype.AppBridgeClass = v, n.prototype.POLL_PERIOD_COOKIE_NAME = "__SBA_POLLTIME", n.prototype.Timeout = {
        GET_OXI_TOKEN: 1e4,
        GET_SSO_TRANSFER_TOKEN: 1e4,
        BG_VERIFICATION: 1e4,
        REDEEM_CODE: 1e4
    }, n.prototype.APP_USER_AGENT_EXTENSIONS = ["SkyVegas", "SkyBet", "SkySuper6", "SkyFantasyFootball", "SkyBingo", "SkyPoker", "SkyCasino"], n.prototype.setup = function(e) {
        this.eventsBind = this.bind;
        var n = this;
        this.bind = function(e, t) {
            this.eventsBind(e, t), e === "appBridgeReady" && n.fire("appBridgeReady", {
                appBridge: n.getAppBridge()
            })
        };
        if (!e.consumer)
            throw new Error("SkyBetAccount.setup expects options.consumer");
        if (!e.consumerUrl)
            throw new Error("SkyBetAccount.setup expects options.consumerUrl");
        this.config = {
            consumer: null,
            consumerUrl: null,
            host: "www.skybet.com",
            scheme: "https",
            window: typeof window != "undefined" ? window: null,
            hasCookies: !1,
            pollPeriod: 300,
            deeplinkParam: "open",
            appBridge: !1,
            allowsRefresh: !1
        }, this.eventStack = [];
        for (var i in e)
            i in this.config && e.hasOwnProperty(i) && (this.config[i] = e[i]);
        this.config.consumerUrl = this.config.consumerUrl.replace(/\/$/, ""), this.window = this.config.window, function(e, t) {
            function n(e) {
                var n = t[e];
                t[e] = function(e) {
                    return s(n(e))
                }
            }
            function r(t, n, r) {
                return (r = this).attachEvent("on" + t, function(t) {
                    var t = t || e.event;
                    t.preventDefault = t.preventDefault || function() {
                        t.returnValue=!1
                    }, t.stopPropagation = t.stopPropagation || function() {
                        t.cancelBubble=!0
                    };
                    try {
                        n.call(r, t)
                    } catch (t) {}
                })
            }
            function i(t, n, r) {
                return (r = this).detachEvent("on" + t, function(t) {
                    var t = t || e.event;
                    t.preventDefault = t.preventDefault || function() {
                        t.returnValue=!1
                    }, t.stopPropagation = t.stopPropagation || function() {
                        t.cancelBubble=!0
                    };
                    try {
                        n.call(r, t)
                    } catch (t) {}
                })
            }
            function s(e, t) {
                if (t = e.length)
                    while (t--)
                        e[t].addEventListener = r, e[t].removeEventListener = i;
                else 
                    e.addEventListener = r, e.removeEventListener = i;
                return e
            }
            if (e.addEventListener)
                return;
            s([t, e]), "Element"in e ? (e.Element.prototype.addEventListener = r, e.Element.prototype.removeEventListener = i) : (t.attachEvent("onreadystatechange", function() {
                s(t.all)
            }), n("getElementsByTagName"), n("getElementById"), n("createElement"), s(t.all))
        }(this.window, this.window.document), this.log = new m(this), this.affiliates = new p(this.window.document.location.search, this.window.document, this.config);
        var s = this.affiliates.getQueryVar("sba_promo");
        null !== s && this.affiliates.addCookie("PROMO_CODE", s);
        var o = this.affiliates.getQueryVar("sba_transferToken");
        o === null ? this.ssoTransferToken = "" : this.ssoTransferToken = o, this.initialLoader = new h(this.config, this);
        var u = this;
        this.bus = t = new f(this.config), this.ui = new r(this.window, this.bus, function() {
            return u.getUrl.apply(u, arguments)
        }, this), this.bindConsumerEvents(), this.bindInternalEvents(), this.window.SkyBetAccountJsonp = function(e) {
            switch (e.method) {
            case"get-oxi-token":
                u.bus.fire(f.Events.OXI_TOKEN, e);
                break;
            case"verify-user-and-get-oxi-token":
                u.bus.fire(f.Events.BG_VERIFICATION, e);
                break;
            case"redeem-code":
                u.bus.fire(f.Events.REDEEM_CODE, e);
                break;
            case"get-sso-transfer-token":
                u.bus.fire(f.Events.SSO_TRANSFER_TOKEN, e)
            }
        }, this.init()
    }, n.prototype.init = function() {
        var e = this.getPollPeriodCookie(), t = 0;
        if (e && e.timestamp && e.hasMyAccountCookies === "true") {
            var n = (new Date).getTime();
            t = n - e.timestamp
        }
        var r = this.affiliates.getQueryVar("sba_skipThrottle");
        return t !== 0 && t <= this.config.pollPeriod * 1e3 && this.ssoTransferToken === "" && r !== "true" ? (this.config.hasCookies = e.hasMyAccountCookies, this.fire("hasCookies", this.config.hasCookies), this.initialLoader.checkDeeplink(), this) : (this.affiliates.setAffilliateCookies(), this.ui.getMenu(), this)
    }, n.prototype.getAppBridge = function(e) {
        if (!this.config)
            throw new Error("SkyBetAccount.getAppBridge requires SkyBetAccount.setup to have been called");
        return this.appBridge || (this.appBridge = new v(this), this.appBridge.setConfig(e)), this.appBridge
    }, n.prototype.getAppName = function() {
        var e = this.window.navigator.userAgent;
        for (i in this.APP_USER_AGENT_EXTENSIONS)
            if (e.indexOf(this.APP_USER_AGENT_EXTENSIONS[i])!==-1)
                return this.APP_USER_AGENT_EXTENSIONS[i];
        return null
    }, n.prototype.getPollPeriodCookie = function() {
        var e = this.window.document.cookie.match(new RegExp("(?:^|;)\\s*" + this.POLL_PERIOD_COOKIE_NAME + "=([^;\\s]*)"));
        if (!(e instanceof Array && e.length))
            return null;
        e = e[1];
        var t = typeof e == "string" ? unescape(e).split("|"): null;
        return t ? {
            timestamp: t[0],
            loginState: t[1],
            hasMyAccountCookies: t[2] || "false",
            notificationToken: t[3] || null
        } : null
    }, n.prototype.updatePollPeriodCookie = function(e) {
        var t = "loggedOut";
        this.lastLoginState && this.lastLoginState.name && (t = this.lastLoginState.name);
        var n = (new Date).getTime(), r = this.POLL_PERIOD_COOKIE_NAME + "=" + n + "|" + t + "|" + this.config.hasCookies;
        if (e)
            r = r + "|" + e;
        else if (e === undefined) {
            var i = this.getPollPeriodCookie();
            i && i.notificationToken && (r = r + "|" + i.notificationToken)
        }
        return this.window.document.cookie = r + ";Path=/;", this
    }, n.prototype.clearPollPeriodCookie = function() {
        return this.window.document.cookie = this.POLL_PERIOD_COOKIE_NAME + "=;Path=/;Expires=Thu, 01 Jan 1970 00:00:01 GMT;", this
    }, n.prototype.bindConsumerEvents = function() {
        var e = this;
        this.bus.bind(f.Events.LOGGED_IN, function(t) {
            e.lastLoginState = t, e.fire("loggedIn", t), e.updatePollPeriodCookie()
        }), this.bus.bind(f.Events.QUEUE_UP_EVENT, function(t) {
            t.data.name === "loggedIn" && e.clearPollPeriodCookie(), e.eventStack[t.event] = t.data
        }), this.bus.bind(f.Events.LOGGED_OUT, function(t) {
            e.lastLoginState = t, t.userInteraction && (e.ui.getMenu().url = null), e.fire("loggedOut", t), e.updatePollPeriodCookie()
        }), this.bus.bind(f.Events.USER_DATA_CHANGE, function(t) {
            e.fire("userDataChange", t)
        }), this.bus.bind(f.Events.FULL_REGISTRATION, function(t) {
            e.fire("fullRegistrationComplete", t)
        }), this.bus.bind(f.Events.DEPOSIT_COMPLETE, function(t) {
            e.fire("depositComplete", t)
        }), this.bus.bind(f.Events.WITHDRAWAL_COMPLETE, function(t) {
            e.fire("withdrawalComplete", t)
        }), this.bus.bind(f.Events.REDEEM_PROMO_CODE, function(t) {
            e.fire("redeemPromoCode", t)
        }), this.bus.bind(f.Events.SELF_EXCLUSIONS_UPDATE, function(t) {
            e.fire("selfExclusionsUpdate", t)
        }), this.bus.bind(f.Events.COOL_OFF_UPDATE, function(t) {
            e.fire("coolOffUpdate", t)
        }), this.bus.bind(f.Events.PREFERENCES_UPDATE, function(t) {
            e.fire("preferencesUpdate", t)
        }), this.bus.bind(f.Events.AGE_VERIFICATION, function(t) {
            e.fire("ageVerificationComplete", t)
        }), this.bus.bind(f.Events.REDEEM_CODE, function(t) {
            e.fire("redeemCode", t)
        }), this.bus.bind(f.Events.OPTIN_TRIGGER, function(t) {
            e.fire("optinTrigger", t)
        }), this.bus.bind(f.Events.NAVIGATE, function(t) {
            e.fire("navigate", t)
        });
        var t = function(t) {
            e.fire("showModalDialog", t)
        };
        this.bus.bind(f.Events.SHOW_MODAL_DIALOG, t), this.ui.bind(f.Events.SHOW_MODAL_DIALOG, t), this.bus.bind(f.Events.MYACCOUNT_PAGE_READY, function(t) {
            var n = {};
            n[f.Events.SHOW_MODAL_DIALOG] = e.hasBoundEvent(f.Events.SHOW_MODAL_DIALOG), e.bus.postMessage(e.ui.getMenu().iframe.contentWindow, {
                type: "Event",
                event: {
                    name: "consumerAvailable",
                    data: n
                }
            })
        }), this.bus.bind(f.Events.HEALTH_CHECK_OK, function(t) {
            e.fire("healthCheckOk", t), e.ui.getMenu().redirect("loginState", null, !0)
        }), this.bus.bind(f.Events.GET_DATA_FOR_DEPOSIT, function() {
            var t = {
                currentConsumerUrl: e.getCurrentConsumerUrl(),
                deeplinkParam: e.config.deeplinkParam
            };
            e.bus.postMessage(e.ui.menu.iframe.contentWindow, {
                type: "Event",
                event: {
                    name: "dataForDeposit",
                    data: t
                }
            })
        }), this.bus.bind(f.Events.GET_SSO_TRANSFER_TOKEN, function() {
            e.getSsoTransferToken(function(t, n) {
                n && e.bus.postMessage(e.ui.menu.iframe.contentWindow, {
                    type: "Event",
                    event: {
                        name: "getSsoTransferToken",
                        data: {
                            token: n
                        }
                    }
                })
            })
        }), this.bus.bind(f.Events.APPBRIDGE_TRIGGER, function(t) {
            var n = e.getAppBridge();
            n.trigger(t.event, t.data)
        }), this.ui.bind(r.Events.CLOSE, function() {
            e.bus.postMessage(e.ui.getMenu().iframe.contentWindow, {
                type: "Event",
                event: {
                    name: "disableInteraction"
                }
            }), e.fire("close")
        }), this.ui.bind(r.Events.OPEN, function() {
            e.fire("open", {
                height: e.ui.height
            })
        }), this.ui.bind(r.Events.RESIZED, function() {
            e.fire("resized", {
                height: e.ui.height
            })
        }), this.ui.bind(r.Events.RENDERED, function() {
            e.bus.postMessage(e.ui.getMenu().iframe.contentWindow, {
                type: "Event",
                event: {
                    name: "enableInteraction"
                }
            })
        })
    }, n.prototype.bindInternalEvents = function() {
        var e = this;
        this.bus.bind(f.Events.PAGE_READY, function(t) {
            e.ui.getMenu().url = t.url, t.url === e.ui.getUrl("login") && (e.ui.loginToken = (new Date).getTime());
            if (e.ui.isPreloading) {
                e.ui.applyOnPreloadCallbacks();
                return 
            }
            e.ui.isOpen ? e.ui.ready() : e.ui.getMenu().redirect("loginState", null, !0), e.isExternalUrl = function(t) {
                if ("undefined" == typeof t)
                    return !1;
                var n = t.replace("http://", "").replace("https://", "").split("/")[0];
                return e.config.host != n
            }(t.url)
        }), this.bus.bind(f.Events.RESIZE_IFRAME, function(t) {
            e.ui.resize(t.height)
        }), this.bus.bind(f.Events.ALLOW_REFRESH, function() {
            e.config.allowsRefresh=!0
        }), this.bus.bind(f.Events.OPEN_MY_ACCOUNT, function() {
            e.fire("openMyAccount")
        }), this.bus.bind(f.Events.APP_LOGIN_ATTEMPTED, function(t) {
            e.getAppBridge().trigger("login-attempted", t), t.success && e.getAppBridge().trigger("quickpinShowObModal", {
                autoEnabledQuickPIN: t.rememberMe == 1
            })
        }), this.bus.bind(f.Events.CLOSE_MY_ACCOUNT, function() {
            e.isExternalUrl && e.ui.menu.redirect("loginState", null, !0);
            var t = e.eventStack;
            for (event in t)
                t.hasOwnProperty(event) && (e.fire(event, t[event]), delete t[event]);
            e.ui.close()
        }), this.bus.bind(f.Events.SCROLL_TO, function(t) {
            e.ui.scrollTo(t.value)
        }), this.bus.bind(f.Events.INITIAL_PAGE, function() {
            if (e.initialPageLoaded)
                return e.ui.close();
            e.initialPageLoaded=!0, e.initialLoader.checkDeeplink()
        }), this.bus.bind(f.Events.SET_LOADING_OVERLAY, function(t) {
            t.value&&!e.ui.isPreloading ? e.ui.showLoadingOverlay(t.secure) : e.ui.hideLoadingOverlay()
        }), this.bus.bind(f.Events.HAS_COOKIES, function(t) {
            if (!("hasCookies"in t))
                return;
            e.config.hasCookies = t.hasCookies, e.updatePollPeriodCookie();
            var n;
            "ssoTransferToken"in t && (n = t.ssoTransferToken);
            var r = e.affiliates.getQueryVar("sba_transferToken"), i = e.affiliates.getQueryVar("sba_transferTokenRedirect");
            if (!e.config.hasCookies && r && r !== "" && i === null) {
                var s = e.getCurrentConsumerUrl();
                typeof n != "undefined" && (s = s.replace(/sba_transferToken=[a-z0-9]{32}/g, "sba_transferToken=" + n)), s += s.indexOf("?") ? "&" : "?", s += "sba_transferTokenRedirect=1";
                var o = e.getUrl("fullScreenRedirect", "static") + "returnUrl=" + encodeURIComponent(s);
                return e.window.top.location.replace(o)
            }
            e.fire("hasCookies", t.hasCookies)
        })
    }, n.prototype.processAppBridgeNotificationToken = function(e) {
        this.updatePollPeriodCookie(e.token);
        var t = "app=" + encodeURIComponent(this.config.consumer) + "&deviceName=" + encodeURIComponent(e.deviceName) + "&deviceVersion=" + encodeURIComponent(e.deviceVersion) + "&appVersion=" + encodeURIComponent(e.appVersion) + "&token=" + encodeURIComponent(e.token);
        this.sendNotificationTokenRequest(t, function(e) {}), this.getLastLoginState() == "loggedIn" && this.processNotificationTokenOnLogin(this.lastLoginState)
    }, n.prototype.processNotificationTokenOnLogin = function(e) {
        var t = this.getPollPeriodCookie();
        if (t && t.notificationToken) {
            var n = "app=" + encodeURIComponent(this.config.consumer) + "&customerId=" + encodeURIComponent(e.user.customerId) + "&token=" + encodeURIComponent(t.notificationToken);
            this.sendNotificationTokenRequest(n, function(e) {
                e.status == 200 && this.updatePollPeriodCookie(null)
            }.bind(this))
        }
    }, n.prototype.sendNotificationTokenRequest = function(e, t) {
        var n = this.config.scheme + "://" + this.config.host + "/secure/identity/app/notification-token", r = new XMLHttpRequest;
        r.open("POST", n, !0), r.setRequestHeader("Content-type", "application/x-www-form-urlencoded"), r.onreadystatechange = function() {
            if (r.readyState != 4)
                return;
            t(r)
        }, r.send(e)
    }, n.prototype.remoteNotificationActioned = function(e) {
        var t = e.userInfo || {}, n = [{
            type: "business",
            name: this.config.consumer + ".app.remoteNotificationActioned",
            data: {
                deviceName: e.deviceName || "",
                deviceVersion: e.deviceVersion || "",
                appVersion: e.appVersion || "",
                token: e.token || "",
                customerid: t.user || "",
                campaign: t.campaign || ""
            }
        }
        ];
        this.log.sendLogEvents(n)
    }, n.prototype.isInitialPageLoaded = function(e, t, n, r) {
        return this.initialPageLoaded?!0 : !1
    }, n.prototype.getUrl = function(e, t, n, r) {
        t = t || "mobile", r = r ||!1;
        var i = {
            mobile: "https://{host}/secure/identity/",
            web: "https://{host}/secure/",
            "static": "https://{host}/"
        }, s = {
            account: "m/myaccount/{consumer}?urlconsumer={consumerUrl}&dl=1",
            addCard: "m/add-card/{consumer}",
            applyPromotion: "m/redeem-code/{consumer}",
            optinTrigger: "m/optin-trigger/{consumer}",
            completeDeposit: "complete-deposit-handler/{consumer}?returnUrl=/secure/identity/m/deposit/completed/{consumer}",
            bankingHistory: "m/history/financial",
            bettingHistory: "m/history/betting",
            bgverification: "m/verify-user-and-get-oxi-token/{consumer}",
            deposit: "m/choose-deposit-method/{consumer}?urlconsumer={consumerUrl}&dl=1",
            faq: "m/faq/{consumer}",
            fullScreenRedirect: "secure/identity/redirector/{consumer}?",
            gamingHistory: "m/history/gaming",
            getSsoTransferToken: "m/get-sso-transfer-token/{consumer}",
            healthCheck: "m/health-check/{consumer}?urlconsumer={consumerUrl}&dl=1",
            loginState: "m/login-state/{consumer}?urlconsumer={consumerUrl}&dl=1&ssoTransferToken={ssoTransferToken}",
            login: "m/login/{consumer}?urlconsumer={consumerUrl}&dl=1",
            logout: "logout?consumer={consumer}&urlconsumer={consumerUrl}&mobile=1&dl=1",
            managePaymentMethods: "m/manage-payment-methods/{consumer}",
            oxiToken: "m/get-oxi-token/{consumer}",
            promotion: "m/promotions/redeem-code/{consumer}",
            quickDeposit: "m/deposit/{consumer}?urlconsumer={consumerUrl}&dl=1",
            recoverMenu: "m/recover/{consumer}",
            recoverPin: "m/recover/pin/{consumer}",
            recoverUser: "m/recover/user?consumer={consumer}",
            registration: "m/register/step-1/{consumer}?dl=1&urlconsumer={consumerUrl}",
            resetTempPin: "m/reset-temp-pin?consumer={consumer}&urlconsumer={consumerUrl}",
            transactionsUnsettled: "m/history/betting?settled=N",
            unlockAccount: "m/unlock-account?consumer={consumer}&urlconsumer={consumerUrl}",
            upgradeReg: "m/upgrade/step-1/{consumer}",
            verification: "m/verification/choose-document/{consumer}",
            withdrawal: "m/choose-withdrawal-method/{consumer}?urlconsumer={consumerUrl}&dl=1",
            selfExclude: "m/self-exclude/{consumer}",
            changePin: "m/edit-pin/{consumer}"
        };
        if (!s[e])
            return !1;
        var o = i[t] + s[e], u = {
            host: this.config.host,
            consumer: this.config.consumer,
            consumerUrl: this.config.consumerUrl,
            ssoTransferToken: this.ssoTransferToken
        };
        r && (o = o.slice(i["static"].length - 1));
        for (var a in u)
            u.hasOwnProperty(a) && (o = o.replace(new RegExp("{" + a + "}", "g"), u[a]));
        if (typeof n == "object" && n !== null) {
            var f = "?";
            o.indexOf("?")!==-1 && (f = "&");
            for (e in n) {
                var l = n[e];
                typeof l == "object" && (l = JSON.stringify(l)), f += e + "=" + l + "&"
            }
            return o + f
        }
        return o
    }, n.prototype.getRegistrationUrl = function(e, t) {
        return this.getUrl("registration", e, t)
    }, n.prototype.getLoginUrl = function() {
        return this.getUrl("login")
    }, n.prototype.getAccountUrl = function() {
        return this.getUrl("account")
    }, n.prototype.getLoginTarget = function() {
        return "SkyBetAccount"
    }, n.prototype.openRegistration = function(e, t, n) {
        return this.ui.open("registration", e, null, t, n), this
    }, n.prototype.openLogin = function(e) {
        if (typeof document != "undefined") {
            var t = document.cookie.match(/sbgCAtrial01=([^;$]+)/);
            if (t !== null) {
                t = t[1].split(",");
                for (i in t)
                    if (t[i] == "lifdelay") {
                        var n = this.ui;
                        return n.overlay.show(), n.showLoadingOverlay(), window.setTimeout(function() {
                            n.open("login", e)
                        }, 500), this
                    }
            }
        }
        return this.ui.open("login", e), this
    }, n.prototype.openExternalUrl = function(e, t) {
        var n = function() {
            this.ui.hideLoadingOverlay(), this.ui.getElement("body").style.marginLeft = "-320px", this.ui.getElement("body").style.marginRight = "320px", this.ui.getMenu().container.style.zIndex = "20", this.ui.getMenu().slider.style.left = "10px", this.ui.scrollTo(0)
        }.bind(this);
        return this.ui.open(e, n, !0, null, null, t), this
    }, n.prototype.requiresFullScreenRedirect = function(e) {
        return !1
    }, n.prototype.openMyAccount = function(e) {
        return this.ui.open("account", e), this
    }, n.prototype.openDeposit = function(e) {
        return this.ui.open("deposit", e), this
    }, n.prototype.openQuickDeposit = function(e) {
        return this.ui.open("quickDeposit", e), this
    }, n.prototype.openCompleteDeposit = function(e, t, n) {
        return this.ui.open("completeDeposit", e, t, n), this
    }, n.prototype.openVerification = function(e) {
        return this.ui.open("verification", e), this
    }, n.prototype.openUpgradeReg = function(e) {
        var t;
        return this.lastLoginState && "name"in this.lastLoginState ? t = this.lastLoginState.name : this.getPollPeriodCookie() && "loginState"in this.getPollPeriodCookie() && (t = this.getPollPeriodCookie().loginState), t === "loggedIn" ? this.ui.open("upgradeReg", e) : this.ui.open("login", e), this
    }, n.prototype.openPromotion = function(e) {
        return this.ui.open("promotion", e), this
    }, n.prototype.openRecoverPin = function(e) {
        return this.ui.open("recoverPin", e), this
    }, n.prototype.openAddCard = function(e) {
        return this.ui.open("addCard", e), this
    }, n.prototype.openManagePaymentMethods = function(e) {
        return this.ui.open("managePaymentMethods", e), this
    }, n.prototype.openWithdrawal = function(e) {
        return this.ui.open("withdrawal", e), this
    }, n.prototype.openBettingHistory = function(e) {
        return this.ui.open("bettingHistory", e), this
    }, n.prototype.openGamingHistory = function(e) {
        return this.ui.open("gamingHistory", e), this
    }, n.prototype.openBankingHistory = function(e) {
        return this.ui.open("bankingHistory", e), this
    }, n.prototype.openRecoverMenu = function(e) {
        return this.ui.open("recoverMenu", e), this
    }, n.prototype.openRecoverUser = function(e) {
        return this.ui.open("recoverUser", e), this
    }, n.prototype.openUnlockAccount = function(e) {
        return this.ui.open("unlockAccount", e), this
    }, n.prototype.openResetTempPin = function(e) {
        return this.ui.open("resetTempPin", e), this
    }, n.prototype.openFaq = function(e) {
        return this.ui.open("faq", e), this
    }, n.prototype.openSelfExclude = function(e) {
        return this.ui.open("selfExclude", e), this
    }, n.prototype.openChangePin = function(e) {
        return this.ui.open("changePin", e), this
    }, n.prototype.modalDialogDismissed = function(e) {
        return this.bus.postMessage(this.ui.getMenu().iframe.contentWindow, {
            type: "Event",
            event: {
                name: "modalDialogDismissed",
                data: e
            }
        }), this
    }, n.prototype.openOverlay = function(e) {
        return this.ui.open(null, e), this
    }, n.prototype.openTransactionHistoryUnsettled = function(e) {
        return this.ui.open("transactionsUnsettled", e), this
    }, n.prototype.getOxiToken = function(e) {
        var t = this, n = setTimeout(function() {
            t.bus.unbind(f.Events.OXI_TOKEN), e({
                code: 8,
                message: "Request timed out"
            })
        }, this.Timeout.GET_OXI_TOKEN), r = function(r) {
            clearTimeout(n), r.code == 0 ? e(null, r.data.token) : e({
                code: r.code,
                message: r.message
            }), t.bus.unbind(f.Events.OXI_TOKEN)
        };
        this.bus.bind(f.Events.OXI_TOKEN, r);
        var i = this.ui.createElement("script");
        return i.src = this.getUrl("oxiToken"), this.ui.getElement("head").appendChild(i), this
    }, n.prototype.getSsoTransferToken = function(e) {
        var t = this, n = setTimeout(function() {
            t.bus.unbind(f.Events.SSO_TRANSFER_TOKEN), e({
                code: 8,
                message: "Request timed out"
            })
        }, this.Timeout.GET_SSO_TRANSFER_TOKEN), r = function(r) {
            clearTimeout(n), r.code == 0 ? e(null, r.data.ssoTransferToken) : e({
                code: r.code,
                message: r.message
            }), t.bus.unbind(f.Events.SSO_TRANSFER_TOKEN)
        };
        this.bus.bind(f.Events.SSO_TRANSFER_TOKEN, r);
        var i = this.ui.createElement("script");
        return i.src = this.getUrl("getSsoTransferToken"), this.ui.getElement("head").appendChild(i), this
    }, n.prototype.verifyUserAndGetOxiToken = function(e) {
        var t = this;
        if (this.lastLoginState && this.lastLoginState.user.ageverified)
            return this.getOxiToken(e);
        var n = setTimeout(function() {
            t.bus.unbind(f.Events.BG_VERIFICATION), e({
                code: 8,
                message: "Request timed out"
            })
        }, this.Timeout.BG_VERIFICATION), r = function(r) {
            clearTimeout(n), r.code == 0 ? (t.lastLoginState && (t.lastLoginState.user.ageverified=!0), e(null, r.data.token)) : e({
                code: r.code,
                message: r.message
            }), t.bus.unbind(f.Events.BG_VERIFICATION)
        };
        this.bus.bind(f.Events.BG_VERIFICATION, r);
        var i = this.ui.createElement("script");
        return i.src = this.getUrl("bgverification"), this.ui.getElement("head").appendChild(i), this
    }, n.prototype.redeemCode = function(e, t) {
        var n = this, r = setTimeout(function() {
            n.bus.unbind(f.Events.REDEEM_CODE), t({
                code: 8,
                message: "Request timed out"
            })
        }, this.Timeout.REDEEM_CODE), i = function(e) {
            clearTimeout(r), e.code == 0 ? t(null, !0) : t({
                code: e.code,
                message: e.message
            }), n.bus.unbind(f.Events.REDEEM_CODE)
        };
        this.bus.bind(f.Events.REDEEM_CODE, i);
        var s = this.ui.createElement("script");
        return s.src = this.getUrl("applyPromotion", null, {
            code: e
        }), this.ui.getElement("head").appendChild(s), this
    }, n.prototype.optinTrigger = function(e, t, n, r) {
        var i = this, s = setTimeout(function() {
            i.bus.unbind(f.Events.OPTIN_TRIGGER), r({
                code: 8,
                message: "Request timed out"
            })
        }, this.Timeout.OPTIN_TRIGGER), o = function(e) {
            clearTimeout(s), e.code == 0 ? r(null, !0) : r({
                code: e.code,
                message: e.message
            }), i.bus.unbind(f.Events.OPTIN_TRIGGER)
        };
        this.bus.bind(f.Events.OPTIN_TRIGGER, o);
        var u = this.ui.createElement("script");
        return u.src = this.getUrl("optinTrigger", null, {
            triggerValue: e,
            monetaryValue: t,
            channel: n
        }), this.ui.getElement("head").appendChild(u), this
    }, n.prototype.getLoginState = function(e) {
        return this.lastLoginState ? e(null, this.lastLoginState) : e({
            code: 8,
            message: "Login state unknown"
        })
    }, n.prototype.fireLoginState = function() {
        this.lastLoginState && (this.lastLoginState.name === "loggedIn" ? this.fire("loggedIn", this.lastLoginState) : this.fire("loggedOut", this.lastLoginState))
    }, n.prototype.logout = function(e) {
        var t = this;
        return this.ui.open("logout", function() {
            t.ui.close(), t.ui.hideLoadingOverlay(), e && e()
        }), this
    }, n.prototype.openUpgradeAccount = function(e) {
        return this.openUpgradeReg(e), this
    }, n.prototype.gotoLogin = function(e) {
        this.ui.getMenu().redirect("login", null, e)
    }, n.prototype.healthCheck = function(e) {
        return this.ui.getMenu().redirect("healthCheck", e), this
    }, n.prototype.preloadMyAccount = function(e) {
        return this.ui.preload("account", e), this
    }, n.prototype.getCurrentConsumerUrl = function() {
        return this.window.location.href
    }, n.prototype.getLastLoginState = function() {
        if (this.lastLoginState)
            return this.lastLoginState.name || null;
        var e = this.getPollPeriodCookie();
        return e ? e.loginState || null : null
    }, l.call(r.prototype), r.prototype.getMenu = function() {
        return this.menu || (this.menu = new s(this, null, this.bus)), this.menu
    }, r.prototype.getElement = function(e) {
        return e === "body" ? this.window.document.body : e === "head" ? this.window.document.head : this.window.document.getElementById(e)
    }, r.prototype.createElement = function(e) {
        return this.window.document.createElement(e)
    }, r.prototype.createTextNode = function(e) {
        return this.window.document.createTextNode(e)
    }, r.Events = {
        OPEN: "UI.Open",
        CLOSE: "UI.Close",
        RESIZED: "UI.Resized",
        RENDERED: "UI.Rendered"
    }, r.Styles = {
        iframe: "border: 0; display: block; height: 1px; width: 320px",
        menu: "height: 1px; overflow-x: hidden; position: absolute; right: 0;            top: 0; width: 330px; color: #fff; -webkit-box-sizing:            border-box; -moz-box-sizing: border-box; box-sizing: border-box;            z-index: -1",
        menuSlider: "left: 340px; position: absolute; top: 0; width: 320px;            -webkit-box-shadow: 0 0 10px rgba(50, 50, 50, 1);            -moz-box-shadow: 0 0 10px rgba(50, 50, 50, 1);            box-shadow: 0 0 10px rgba(50, 50, 50, 1); z-index: 21",
        overlay: "background-color: transparent; bottom: 0; left: 0;            position: absolute; right: 0; top: 0; z-index: -1;            -webkit-tap-highlight-color: rgba(0, 0, 0, 0)",
        loadingBox: 'border: 1px solid #cccccc; background: #fff; color: #505050; width: 290px;            left: 50%; height: auto; margin-left: -150px;            position: absolute; text-align: center; z-index: 50;            -webkit-border-radius: 8px; -mox-border-radius: 8px; border-radius: 8px;            font-size: 14px; font-family: "Helvetica Neue", Arial, sans-serif;            font-weight: bold; padding-left: 5px; padding-top: 32px; padding-right: 5px;            -webkit-box-shadow: -2px 0px 16px rgba(50, 50, 50, 0.75);            -moz-box-shadow: -2px 0px 16px rgba(50, 50, 50, 0.75);            box-shadow: -2px 0px 16px rgba(50, 50, 50, 0.75)',
        loadingBoxPadlockIcon: "display: inline-block; background-size: 55px 62px;            vertical-align: middle; width: 55px; height: 62px;",
        loadingBoxSkySecureText: "font-family: SkyMedium, Arial, sans-serif!important;            font-weight: normal; margin-bottom: 0; margin-top: 10px; font-size: 26px;",
        loadingBoxVerisignLogo: "background-size: 74px 22px; vertical-align: middle;            width: 74px; height: 22px; display: inline-block;",
        loadingBoxStrapline: 'font-size: 10px; margin-top: -5px; margin-bottom: 0;            font-weight: normal; font-family: "Helvetica Neue", Arial, sans-serif;',
        spinner: " height: 40px;            left:50%;            margin-left: -20px;            position: relative;            width: 40px;            background-size: 40px;            top: 0;            transform: translate3d(0,0,0)"
    }, r.prototype.getViewportHeight = function() {
        var e = this.window.innerHeight;
        return this.window.top && (e = this.window.top.innerHeight), e === undefined && (e = window.document.body.clientHeight), e
    }, r.prototype.getViewportYOffset = function() {
        var e = this.window.pageYOffset;
        return e === undefined && (e = window.document.body.scrollTop), e
    }, r.prototype.getPageHeight = function() {
        var e = Math.max(this.window.document.documentElement.offsetHeight, this.getViewportHeight());
        return isNaN(e) && (e = this.window.document.documentElement.clientHeight), e
    }, r.prototype.open = function(e, t, n, i, s, o) {
        o!==!0 && (this.overlay.show(), this.showLoadingOverlay());
        if (this.isPreloading) {
            this.onPreload(function() {
                this.open(e, t, n, i, s, o)
            }.bind(this));
            return 
        }
        if (e === "login" && this.getMenu().url === this.getUrl(e) && this.isLoginTokenExpired() || e === "logout")
            n=!0;
        s===!0 && (n=!0), this.sba.config.allowsRefresh===!0 && (n=!0, this.sba.config.allowsRefresh=!1);
        if (!this.sba.config.hasCookies) {
            var u = this.sba.window.top.location, a = u.protocol + "//" + u.host + u.pathname + u.search;
            u.search.length > 0 ? a += "&" : a += "?", a.search(this.sba.config.deeplinkParam + "=")==-1 && (a += this.sba.config.deeplinkParam + "=" + e), a += u.hash;
            var f = this.sba.getUrl("fullScreenRedirect", "static") + "returnUrl=" + escape(a);
            this.sba.window.top.location.replace(f)
        }
        if (e) {
            var l = this, c = function() {
                l.fire(r.Events.RENDERED), t && t()
            };
            this.getMenu().redirect(e, c, n, i)
        }
        this.fire(r.Events.OPEN), this.isOpen=!0
    }, r.prototype.preload = function(e, t) {
        e&&!this.isOpen && (typeof t == "function" && this.onPreload(t), this.isPreloading=!0, this.getMenu().redirect(e, null, !0))
    }, r.prototype.onPreload = function(e) {
        this.onPreloadCallbacks.push(e)
    }, r.prototype.applyOnPreloadCallbacks = function() {
        this.isPreloading=!1;
        for (i in this.onPreloadCallbacks)
            this.onPreloadCallbacks[i]();
        this.onPreloadCallbacks = []
    }, r.prototype.close = function() {
        this.getElement("body").style.marginLeft = "0", this.getElement("body").style.marginRight = "0", this.overlay.hide(), this.getMenu().hide(), this.fire(r.Events.CLOSE), this.isOpen=!1
    }, r.prototype.showLoadingOverlay = function(e) {
        this.loadingOverlay.show(), this.loadingBox.show(e)
    }, r.prototype.hideLoadingOverlay = function() {
        this.loadingOverlay.hide(), this.loadingBox.hide()
    }, r.prototype.ready = function() {
        this.getElement("body").style.marginLeft = "0", this.getElement("body").style.marginRight = "0", this.hideLoadingOverlay(), this.getMenu().show()
    }, r.prototype.resize = function(e) {
        var t = Math.max(this.getPageHeight(), e || 0);
        isNaN(t) && (t = document.documentElement.clientHeight);
        if (e === undefined && t < this.height)
            return;
        this.height = t, this.getMenu().resize(this.height), this.overlay.resize(this.height), this.loadingOverlay.resize(this.height), this.loadingBox.resize(), this.fire(r.Events.RESIZED)
    }, r.prototype.scrollTo = function(e) {
        typeof this.window.top != "undefined" && typeof this.window.top.scroll == "function" ? this.window.top.scroll(0, e) : typeof this.window.scroll == "function" && this.window.scroll(0, e), this.loadingOverlay.resize()
    }, r.prototype.isLoginTokenExpired = function() {
        if (!this.loginToken)
            return !1;
        var e = (new Date).getTime() - this.loginToken;
        return e > this.loginTokenTimeout
    }, s.prototype.initContainer = function() {
        this.container || (this.container = this.ui.createElement("div"), this.ui.getElement("body").appendChild(this.container)), this.container.style.cssText = r.Styles.menu
    }, s.prototype.initSlider = function() {
        var e = this.ui.createElement("div");
        e.style.cssText = r.Styles.menuSlider, this.container.appendChild(e), this.slider = e
    }, s.prototype.initIframe = function() {
        var e = this.ui.createElement("iframe");
        e.name = "SkyBetAccount", e.id = "SkyBetAccount", e.src = this.ui.getUrl("loginState"), e.style.cssText = r.Styles.iframe, e.style.backgroundColor = "white", e.scrolling = "no", e.addEventListener("mousedown", function(e) {
            e.preventBubble()
        }, !0), this.iframe = e, this.slider.appendChild(this.iframe)
    }, s.prototype.redirect = function(e, t, n, r) {
        var i = this, s = e.match(/^(http|https):\/\//), o = e;
        s || (o = this.ui.getUrl(e, null, r));
        if (t) {
            var u = function() {
                t(null, o), i.iframe.removeEventListener("load", u, !1)
            };
            this.iframe.addEventListener("load", u, !1)
        }
        if (!n && this.url && this.isSameUrl(this.url, o, !0)) {
            this.ui.ready(), t && t(null, o);
            return 
        }
        n ||!this.url || this.url === this.ui.getUrl("loginState") ? (n ? this.iframe.contentWindow.location.replace(o) : this.iframe.src = o, this.url = o) : (this.bus.postMessage(this.iframe.contentWindow, {
            type: "Event",
            event: {
                name: "clientSideRedirect",
                data: this.ui.getUrl(e, null, r, !0)
            }
        }), this.bus.bind(f.Events.PAGE_READY, function() {
            t && t(null, o), i.bus.unbindLast(f.Events.PAGE_READY)
        }))
    }, s.prototype.resize = function(e) {
        if (!this.isVisible)
            return;
        this.iframe.style.height = e + "px", this.container.style.height = e + "px"
    }, s.prototype.hide = function() {
        this.isVisible=!1, this.container.style.height = "1px", this.container.style.zIndex = "-1", this.iframe.style.height = "1px", this.slider.style.display = "none", this.slider.style.left = "340px", this.ui.scrollTo(0)
    }, s.prototype.show = function() {
        this.isVisible=!0, this.ui.getElement("body").style.marginLeft = "-320px", this.ui.getElement("body").style.marginRight = "320px";
        var e = this.ui.getPageHeight();
        this.resize(e), this.container.style.zIndex = "20", this.slider.style.display = "block", this.slider.style.left = "10px", this.ui.scrollTo(0), this.bus.fire(f.Events.OPEN_MY_ACCOUNT)
    }, s.prototype.isSameUrl = function(e, t, n) {
        return n ? (c = e.indexOf("?")===-1 ? e : e.substring(0, e.indexOf("?")), d = t.indexOf("?")===-1 ? t : t.substring(0, t.indexOf("?")), c === d) : e === t
    }, o.prototype.updateConfig = function(e) {
        this.config = $.extend(!0, this.config, e)
    }, o.prototype.initOverlay = function() {
        var e = this.ui.createElement("div");
        this.loadingOverlay && (e.id = "loading-overlay"), e.style.cssText = r.Styles.overlay;
        if (!this.loadingOverlay) {
            var t = this;
            e.addEventListener("mousedown", function(e) {
                t.mouseDownHandler(e)
            }, !1)
        }
        this.ui.getElement("body").appendChild(e), this.overlay = e, this.hide()
    }, o.prototype.mouseDownHandler = function(e) {
        var n = this.ui.getMenu().url, r = ["/secure/identity/m/withdrawal", "/secure/identity/m/quick-deposit", "/secure/identity/m/verification", "/secure/identity/m/register", "/secure/identity/m/recover/pin", "/secure/identity/m/recover/user", "/secure/identity/m/reset-temp-pin", "/secure/identity/m/unlock-account"], i = r.some(function(e, t) {
            return n.indexOf(e)!==-1?!0 : !1
        });
        if (!1 === i) {
            t.fire(f.Events.CLOSE_MY_ACCOUNT);
            return 
        }
        this.ui.fire(f.Events.SHOW_MODAL_DIALOG, this.config.dialog)
    }, o.prototype.closeMyAccountDialogHandler = function(e) {
        e.aux && e.aux === "close" && this.ui.close()
    }, o.prototype.hide = function() {
        this.overlay.style.height = "0", this.overlay.style.display = "none", this.overlay.style.zIndex = "-1000"
    }, o.prototype.show = function() {
        this.overlay.style.height = this.ui.getPageHeight() + "px", this.overlay.style.display = "block", this.overlay.style.zIndex = this.loadingOverlay ? "50" : "10"
    }, o.prototype.resize = function(e) {
        e !== undefined && (this.overlay.style.height = e + "px")
    }, o.prototype.getElement = function() {
        return this.overlay
    }, u.prototype.createSecureInfo = function() {
        var e = this.ui.createElement("div"), t = this.ui.createElement("span");
        t.style.cssText = r.Styles.loadingBoxPadlockIcon, t.style.backgroundImage = "url(data:image/png;base64," + y() + ")";
        var n = this.ui.createElement("p");
        n.style.cssText = r.Styles.loadingBoxSkySecureText, n.appendChild(t), n.appendChild(this.ui.createTextNode("Sky Secure"));
        var i = this.ui.createElement("span");
        i.style.cssText = r.Styles.loadingBoxVerisignLogo, i.style.backgroundImage = "url(data:image/png;base64," + b() + ")";
        var s = this.ui.createElement("p");
        return s.style.cssText = r.Styles.loadingBoxStrapline, s.appendChild(this.ui.createTextNode("Safeguarding your personal data with ")), s.appendChild(i), e.appendChild(n), e.appendChild(s), e
    }, u.prototype.show = function(e) {
        this.resize(), this.spinner.start(), e != undefined && e == 1 ? (this.loadingParagraph.innerHTML = "Processing your request&hellip;", this.secureInfo.style.display = "block", this.loadingBox.style.marginTop = "-106px", this.loadingBox.style.paddingBottom = "16px") : (this.loadingParagraph.innerHTML = "Loading&hellip;", this.secureInfo.style.display = "none", this.loadingBox.style.marginTop = "-73px", this.loadingBox.style.paddingBottom = "32px"), this.startCenteringLoadingBoxOnScreen(), this.loadingBox.style.display = "block"
    }, u.prototype.startCenteringLoadingBoxOnScreen = function() {
        try {
            this.centerLoadingBoxOnScreenHandler = this.resize.bind(this), this.ui.window.document.addEventListener("scroll", this.centerLoadingBoxOnScreenHandler, !1)
        } catch (e) {}
    }, u.prototype.stopCenteringLoadingBoxOnScreen = function() {
        this.centerLoadingBoxOnScreenHandler && (this.ui.window.document.removeEventListener("scroll", this.centerLoadingBoxOnScreenHandler), this.centerLoadingBoxOnScreenHandler = null)
    }, u.prototype.hide = function() {
        this.loadingBox.style.display = "none", this.spinner.stop(), this.stopCenteringLoadingBoxOnScreen
        ()
    }, u.prototype.resize = function() {
        var e = this.ui.getViewportHeight(), t = this.ui.getViewportYOffset(), n = e / 2 + t;
        this.loadingBox.style.top = n + "px"
    }, a.prototype.attach = function(e) {
        e.appendChild(this.spinner)
    }, a.prototype.start = function() {
        clearTimeout(this.timeoutCode), this.rotate()
    }, a.prototype.stop = function() {
        clearTimeout(this.timeoutCode)
    }, a.prototype.rotate = function() {
        var e = this.spinner, t = this.bgPosition;
        e.style.backgroundPosition = "0 " + t + "px";
        var n = this;
        this.timeoutCode = setTimeout(function() {
            n.bgPosition += 40, n.bgPosition > 0 && (n.bgPosition =- 280), n.rotate()
        }, 100)
    }, l.call(f.prototype), f.Events = {
        OPEN_MY_ACCOUNT: "openMyAccount",
        CLOSE_MY_ACCOUNT: "closeMyAccount",
        APP_LOGIN_ATTEMPTED: "appLoginAttempted",
        APP_LOGIN_READY: "appLoginReady",
        LOGGED_IN: "loggedIn",
        LOGGED_OUT: "loggedOut",
        QUEUE_UP_EVENT: "queueUpEvent",
        PAGE_READY: "pageReady",
        RESIZE_IFRAME: "resizeFromIframe",
        SCROLL_TO: "scrollTo",
        OXI_TOKEN: "oxiToken",
        SSO_TRANSFER_TOKEN: "ssoTransferToken",
        BG_VERIFICATION: "bgverification",
        INITIAL_PAGE: "initialPage",
        USER_DATA_CHANGE: "userDataChange",
        NAVIGATE: "navigate",
        SHOW_MODAL_DIALOG: "showModalDialog",
        SET_LOADING_OVERLAY: "setLoadingOverlay",
        MYACCOUNT_PAGE_READY: "myaccountPageReady",
        HEALTH_CHECK_OK: "healthCheckOk",
        HAS_COOKIES: "hasCookies",
        PAGE_RENDERED: "pageRendered",
        REDEEM_CODE: "redeemCode",
        GET_DATA_FOR_DEPOSIT: "getDataForDeposit",
        GET_SSO_TRANSFER_TOKEN: "getSsoTransferToken",
        APPBRIDGE_TRIGGER: "appbridgeTrigger",
        ALLOW_REFRESH: "allowRefresh",
        OPTIN_TRIGGER: "optinTrigger",
        FULL_REGISTRATION: "fullRegistrationComplete",
        DEPOSIT_COMPLETE: "depositComplete",
        WITHDRAWAL_COMPLETE: "withdrawalComplete",
        REDEEM_PROMO_CODE: "redeemPromoCode",
        SELF_EXCLUSIONS_UPDATE: "selfExclusionsUpdate",
        COOL_OFF_UPDATE: "coolOffUpdate",
        PREFERENCES_UPDATE: "preferencesUpdate",
        AGE_VERIFICATION: "ageVerificationComplete"
    }, f.prototype.receiveMessage = function(e) {
        var t = [f.Events.LOGGED_OUT, f.Events.OPEN_MY_ACCUONT, f.Events.CLOSE_MY_ACCOUNT, f.Events.HEALTH_CHECK_OK];
        if (e.data.substring(0, 9) !== "myaccount")
            return;
        var n = e.data.slice(9), r;
        try {
            r = JSON.parse(n)
        } catch (i) {
            throw new Error("Invalid JSON received on message bus")
        }
        var s = t.some(function(e) {
            return e === r.event.name
        }), o = e.origin.match(/https:\/\/(.*)/);
        if (!o&!s)
            return !1;
        this.sendMessage(r)
    }, f.prototype.sendMessage = function(e) {
        if ("Event" === e.type)
            return this.fire(e.event.name, e.event.data);
        if ("UserEvent" === e.type)
            return this.fire(e.event.name, {
                name: e.event.name,
                user: e.event.data.user,
                userInteraction: e.event.data.userInteraction
            });
        throw new Error("Unknown message type: " + e.type)
    }, f.prototype.postMessage = function(e, t) {
        var n = this.config.scheme + "://" + this.config.host;
        e.postMessage("myaccount" + JSON.stringify(t), n)
    }, h.prototype.checkDeeplink = function(e) {
        var t = e || this.sba.affiliates.getQueryVar(this.config.deeplinkParam);
        "string" == typeof t && (t = t.toLowerCase());
        switch (t) {
        case"account":
            this.sba.openMyAccount(this.complete());
            break;
        case"addcard":
            this.sba.openAddCard(this.complete());
            break;
        case"applypromotion":
        case"promotion":
            this.sba.openPromotion(this.complete());
            break;
        case"bankinghistory":
            this.sba.openBankingHistory(this.complete());
            break;
        case"bettinghistory":
            this.sba.openBettingHistory(this.complete());
            break;
        case"deposit":
            this.sba.openDeposit(this.complete());
            break;
        case"quickdeposit":
            this.sba.openQuickDeposit(this.complete());
            break;
        case"completedeposit":
            this.sba.getLastLoginState() == "loggedIn" && this.sba.openCompleteDeposit(this.complete());
            break;
        case"gaminghistory":
            this.sba.openGamingHistory(this.complete());
            break;
        case"login":
            this.sba.getLastLoginState() == "loggedIn" ? this.complete() : this.sba.openLogin(this.complete());
            break;
        case"managepaymentmethods":
        case"paymentmethods":
            this.sba.openManagePaymentMethods(this.complete());
            break;
        case"recovermenu":
        case"recover":
            this.sba.getLastLoginState() == "loggedIn" ? this.complete() : this.sba.openRecoverMenu(this.complete());
            break;
        case"recoverpin":
            this.sba.getLastLoginState() == "loggedIn" ? this.complete() : this.sba.openRecoverPin(this.complete());
            break;
        case"recoveruser":
            this.sba.getLastLoginState() == "loggedIn" ? this.complete() : this.sba.openRecoverUser(this.complete());
            break;
        case"registration":
        case"register":
            this.sba.getLastLoginState() == "loggedIn" ? this.complete() : this.sba.openRegistration(this.complete(), t, !0);
            break;
        case"resettemppin":
            this.sba.getLastLoginState() == "loggedIn" ? this.complete() : this.sba.openResetTempPin(this.complete());
            break;
        case"unlockaccount":
            this.sba.getLastLoginState() == "loggedIn" ? this.complete() : this.sba.openUnlockAccount(this.complete());
            break;
        case"upgradereg":
            this.sba.openUpgradeReg(this.complete());
            break;
        case"verification":
            this.sba.openVerification(this.complete());
            break;
        case"withdrawal":
            this.sba.openWithdrawal(this.complete());
            break;
        case"faq":
            this.sba.openFaq(this.complete());
            break;
        case"changePin":
            this.sba.openChangePin(this.complete());
            break;
        default:
            this.complete()
        }
    }, h.prototype.complete = function() {
        this.sba.fire(f.Events.INITIAL_PAGE)
    }, p.prototype.setAffilliateCookies = function() {
        for (var e in this.affQueryVars)
            if (this.affQueryVars.hasOwnProperty(e) && this.getQueryVar(this.affQueryVars[e]) !== null) {
                var t = this.getQueryVar(this.affQueryVars[e]);
                break
            }
        var n = this.getQueryVar(this.btagQueryVar), r = this.getQueryVar(this.assetIdQueryVar);
        t !== null && /^\d+$/.test(t) && this.addCookie(this.affCookie, t), n !== null && /^\S+$/.test(n) && this.addCookie(this.btagCookie, n), r !== null && /^\d+$/.test(r) && this.addCookie(this.assetIdCookie, r)
    }, p.prototype.addCookie = function(e, t) {
        var n = document.getElementsByTagName("head")[0], r = document.createElement("link");
        r.rel = "stylesheet", r.type = "text/css", r.media = "all", r.href = "https://" + this.config.host + "/secure/identity/set-cookie?name=" + e + "&value=" + t, n.appendChild(r)
    }, p.prototype.getQueryVars = function() {
        if (this.queryVars === null) {
            var e = this.queryString.substring(1), t = e.split("&"), n = {};
            for (var r = 0; r < t.length; r++) {
                var i = t[r].split("=");
                n[i[0].toLowerCase()] = i[1]
            }
            this.queryVars = n
        }
        return this.queryVars
    }, p.prototype.getQueryVar = function(e) {
        e = e.toLowerCase();
        var t = this.getQueryVars(), n = t[e];
        return n === undefined && (n = null), n
    }, v.prototype.setConfig = function(e) {
        if (typeof e != "object")
            return;
        for (var t in e)
            this.config[t] = e[t]
    }, v.prototype.bindAppBridgeEvents = function(e) {
        this.bind("notificationToken", e.processAppBridgeNotificationToken.bind(e)), this.bind("applicationInfo", e.log.logAppInfo.bind(e.log)), this.bind("OpenedExternalURL", e.log.logOpenedExternalUrl.bind(e.log)), e.bind("loggedIn", e.processNotificationTokenOnLogin.bind(e)), this.bind("remoteNotificationActioned", e.remoteNotificationActioned.bind(e)), this.bind("feedback", e.log.logAppFeedback.bind(e.log)), this.bind("getSsoTransferToken", function() {
            e.getSsoTransferToken(function(e, t) {
                var n = {};
                e || (n.ssoTransferToken = t), this.trigger("ssoTransferToken", n)
            }.bind(this))
        }.bind(this)), this.bindNativeLoginEvents()
    }, v.prototype.bindNativeLoginEvents = function(e) {
        var t = this, n = function(e) {
            var n = "readyForNativeLogin";
            return e !== undefined && t.sba.window.sessionStorage.setItem(n, e), t.sba.window.sessionStorage.getItem(n) == "true"
        };
        this.bind("login", function(e) {
            var r = function() {
                var n = function(r) {
                    t.sba.bus.unbind(f.Events.APP_LOGIN_READY, n), t.sba.bus.postMessage(t.sba.ui.getMenu().iframe.contentWindow, {
                        type: "Event",
                        event: {
                            name: "submitLogin",
                            data: e
                        }
                    })
                };
                t.sba.bus.bind(f.Events.APP_LOGIN_READY, n), t.sba.ui.open("login", null, !0, {}, !0, !0)
            }, i = 0, s = function() {
                var e = n();
                if (e == 1) {
                    r();
                    return 
                }
                i++, i < 60 && setTimeout(s, 250)
            };
            s()
        }), this.sba.bind(f.Events.LOGGED_IN, function(e) {
            e.userInteraction == 0 && n(!0), t.trigger(f.Events.LOGGED_IN, e)
        }), this.sba.bind(f.Events.LOGGED_OUT, function(e) {
            e.userInteraction == 0 && n(!0), t.trigger(f.Events.LOGGED_OUT, e)
        })
    }, v.prototype.consumerReady = function(e) {
        e = e===!1?!1 : !0, e && this.attach(), this.trigger("consumerReady", {
            ready: e
        })
    }, v.prototype.myaccountReady = function(e) {
        e = e===!1?!1 : !0, this.trigger("myaccountReady", {
            ready: e
        })
    }, v.prototype.bind = function(e, t) {
        this.events.bind(e, t)
    }, v.prototype.trigger = function(e, t) {
        this.config.fetchEnabled && this.eventsStore.push({
            name: e,
            data: t || {}
        });
        if (this.config.iframeEventsEnabled) {
            var n = "js-frame:" + e + ";" + JSON.stringify(t || {});
            this.loadLocationInTemporaryIframe(n);
            var n = "sbappbridge-2:" + encodeURIComponent(escape(e)) + ";" + this.encode({
                name: e,
                data: t || {}
            });
            this.loadLocationInTemporaryIframe(n)
        }
        return this
    }, v.prototype.send = function(e, t) {
        return this.trigger(e, t), this
    }, v.prototype.loadLocationInTemporaryIframe = function(e) {
        if (!this.sba.getAppName())
            return;
        var t = this.sba.window.document.createElement("IFRAME");
        t.style.display = "none", t.setAttribute("src", e), this.sba.window.document.documentElement.appendChild(t), t.parentNode.removeChild(t), t = null
    }, v.prototype.fetch = function() {
        if (!this.config.fetchEnabled)
            return "";
        var e = this.eventsStore.shift();
        if (!e)
            return "";
        if (!e.data)
            return e.name;
        var t = JSON.stringify(e.data);
        return e.name + ":" + t
    }, v.prototype.fire = function(e, t) {
        return this.events.fire(e, t), "received:" + e
    }, v.prototype.attach = function() {
        var e = this.sba.window.top != this.sba.window.self ? this.sba.window.top: this.sba.window;
        e.appBridge = this
    }, v.prototype.encode = function(e) {
        var t = JSON.stringify(e), n = {
            _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
            encode: function(e) {
                var t = "", r, i, s, o, u, a, f, l = 0;
                e = n._utf8_encode(e);
                while (l < e.length)
                    r = e.charCodeAt(l++), i = e.charCodeAt(l++), s = e.charCodeAt(l++), o = r>>2, u = (r & 3)<<4 | i>>4, a = (i & 15)<<2 | s>>6, f = s & 63, isNaN(i) ? a = f = 64 : isNaN(s) && (f = 64), t = t + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a) + this._keyStr.charAt(f);
                return t
            },
            decode: function(e) {
                var t = "", r, i, s, o, u, a, f, l = 0;
                e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
                while (l < e.length)
                    o = this._keyStr.indexOf(e.charAt(l++)), u = this._keyStr.indexOf(e.charAt(l++)), a = this._keyStr.indexOf(e.charAt(l++)), f = this._keyStr.indexOf(e.charAt(l++)), r = o<<2 | u>>4, i = (u & 15)<<4 | a>>2, s = (a & 3)<<6 | f, t += String.fromCharCode(r), a != 64 && (t += String.fromCharCode(i)), f != 64 && (t += String.fromCharCode(s));
                return t = n._utf8_decode(t), t
            },
            _utf8_encode: function(e) {
                e = e.replace(/\r\n/g, "\n");
                var t = "";
                for (var n = 0; n < e.length; n++) {
                    var r = e.charCodeAt(n);
                    r < 128 ? t += String.fromCharCode(r) : r > 127 && r < 2048 ? (t += String.fromCharCode(r>>6 | 192), t += String.fromCharCode(r & 63 | 128)) : (t += String.fromCharCode(r>>12 | 224), t += String.fromCharCode(r>>6 & 63 | 128), t += String.fromCharCode(r & 63 | 128))
                }
                return t
            },
            _utf8_decode: function(e) {
                var t = "", n = 0, r = c1 = c2 = 0;
                while (n < e.length)
                    r = e.charCodeAt(n), r < 128 ? (t += String.fromCharCode(r), n++) : r > 191 && r < 224 ? (c2 = e.charCodeAt(n + 1), t += String.fromCharCode((r & 31)<<6 | c2 & 63), n += 2) : (c2 = e.charCodeAt(n + 1), c3 = e.charCodeAt(n + 2), t += String.fromCharCode((r & 15)<<12 | (c2 & 63)<<6 | c3 & 63), n += 3);
                return t
            }
        };
        return n.encode(t)
    }, m.prototype.logAppInfo = function(e) {
        var t = this.sba.getAppName();
        if (!t)
            return;
        var n = {
            appName: t,
            networkType: e.NetworkType || null,
            appVersion: e.appVersion || null,
            applicationInstallToken: e.applicationInstallToken || null,
            deviceName: e.deviceName || null,
            deviceVersion: e.deviceVersion || null
        };
        e.timeDeltas && typeof e.timeDeltas == "object" && (n.locationCheckTime = e.timeDeltas.LocationCheck || null, n.networkCheckTime = e.timeDeltas.NetworkCheck || null, n.siteLoadTime = e.timeDeltas.SiteLoad || null, n.applicationLoadTime = e.timeDeltas.applicationLoadTime || null), e.InstalledApps && typeof e.InstalledApps == "object" && (n.installedApps = e.InstalledApps.toString());
        var r = [{
            type: "business",
            name: "app.info." + t.toLowerCase(),
            data: n
        }
        ];
        this.sendLogEvents(r)
    }, m.prototype.logAppFeedback = function(e) {
        var t = this.sba.getAppName();
        if (!t)
            return;
        this.sba.getLoginState(function(t, n) {
            if (t)
                return;
            userInfo = n.user, userInfo && userInfo.customerId && (e.customerId = userInfo.customerId)
        });
        var n = [{
            type: "business",
            name: "app.feedback." + t.toLowerCase(),
            data: e
        }
        ];
        this.sendLogEvents(n)
    }, m.prototype.logOpenedExternalUrl = function(e) {
        var t = this.sba.getAppName();
        if (!t)
            return;
        var n = {
            appName: t,
            appVersion: e.appVersion || null,
            applicationInstallToken: e.applicationInstallToken || null,
            deviceName: e.deviceName || null,
            deviceVersion: e.deviceVersion || null,
            externalUrl: e.externalUrl || null,
            urlOpened: e.urlOpened || null
        }, r = [{
            type: "business",
            name: "app.externalUrl." + t.toLowerCase(),
            data: n
        }
        ];
        this.sendLogEvents(r)
    }, m.prototype.sendLogEvents = function(e, t) {
        if (typeof e != "object")
            return;
        var n = this.sba.config.scheme + "://" + this.sba.config.host + "/secure/identity/app/log-events", r = this.createRequest(n, t);
        r.send("events=" + encodeURIComponent(JSON.stringify(e)))
    }, m.prototype.createRequest = function(e, t) {
        var n = new XMLHttpRequest;
        return n.open("POST", e, !0), n.setRequestHeader("Content-type", "application/x-www-form-urlencoded"), n.onreadystatechange = function() {
            if (n.readyState !== 4)
                return;
            typeof t == "function" && t(n)
        }, n
    }, Array.prototype.some || (Array.prototype.some = function(e) {
        "use strict";
        if (this == null)
            throw new TypeError;
        var t = Object(this), n = t.length>>>0;
        if (typeof e != "function")
            throw new TypeError;
        var r = arguments[1];
        for (var i = 0; i < n; i++)
            if (i in t && e.call(r, t[i], i, t))
                return !0;
        return !1
    }), e
}());

