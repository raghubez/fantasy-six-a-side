! function(a) {
    "use strict";
    a(document).ready(function() {
        function n() {
            a("body").addClass("offcanvas-open"),
            a("body").animate({scrollTop: a(".headerbar").offset().top}, 700)
        }

        function s() {
            a("body").removeClass("offcanvas-open"), a(".offcanvas-sidebar ul ul").removeClass("offcanvas-child-visible")
        }

        function c() {
            a(".offcanvas-sidebar").addClass("offcanvas-child-open")
        }

        function f() {
            a(".offcanvas-sidebar").removeClass("offcanvas-child-open")
        }
        a(".offcanvas-nav").click(function() {
            a("body").hasClass("offcanvas-open") ? s() : n()
        }), a(".offcanvas-overlay").click(function() {
            s(), f()
        }), a(window).resize(function() {
            s(), f()
        }), a(".offcanvas-sidebar ul ul").parent().append('<span class="offcanvas-child-link">&raquo;</span>'), a(".offcanvas-sidebar ul ul").prepend('<span class="offcanvas-child-back-link">&laquo; Back</span>'), a(".offcanvas-child-link").click(function() {
            a(this).siblings("ul").addClass("offcanvas-child-visible"), c()
        }), a(".offcanvas-child-back-link").click(function() {
            a(this).parents("ul").removeClass("offcanvas-child-visible"), f()
        })
    })
}(jQuery);

