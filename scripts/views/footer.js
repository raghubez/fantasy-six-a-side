app.views.footer = Backbone.View.extend({
		template: _.template($("#tpl-footer").html()),
    initialize: function() {    		
        this.render();
    },
    render: function(){
        this.$el.html(this.template({}));
    }
});