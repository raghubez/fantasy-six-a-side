app.views.home = Backbone.View.extend({
	template: _.template($("#tpl-home").html()),
    initialize: function() {
        this.render();
    },
    render: function(){
        this.$el.html(this.template({}));
    }
});