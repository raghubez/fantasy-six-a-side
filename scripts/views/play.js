/* Elite players subview */
var hometeam,awayteam;

/*Team Select view */
app.views.play = Backbone.View.extend({
	template: _.template($("#tpl-myteam").html()),
    initialize: function() {
    	this.model.bind("change",this.render,this);
        this.model.fetch();
    },
    render: function(){
    	hometeam = this.model.toJSON()['HOMEID'];
		awayteam = this.model.toJSON()['AWAYID'];
        $(this.$el).html(this.template(this.model.toJSON()));
    
        
    }
});

/*Fixture Players Collections */
var playerCollections= new Object();
var Player = Backbone.Model.extend({});
		        var PlayerCollections = {
					ElitePlayersHome: Backbone.Collection.extend(),
					ElitePlayersAway: Backbone.Collection.extend(),
					NormalPlayersHome: Backbone.Collection.extend(),
					NormalPlayersAway: Backbone.Collection.extend(),
				};
var NormalPlayerSelect = Backbone.Model.extend({});
var addNormalPlayer = new NormalPlayerSelect();
var NormalPlayerSelections = Backbone.Collection.extend();
function playerFetch(){
	$.ajax({
		    url: 'json/playerselect2',
		    cache: false,
		    dataType: 'json',
		    success: function(playerList) {
		        console.log('success', playerList);
		        console.log(playerList["ELITE"][hometeam]["ID"]);

		        function createPlayerJson(category,team){
			        var playerLists = [];
			        for (var id in playerList[category][team]["ID"]) {
			        //	console.log(id);
			        	var player = '{"id":"'+id+'",';
			        	var counter = 0;
			        	for (var stats in playerList[category][team]["ID"][id]['STATS']){
			        		player = player + '"stats'+counter+'":"'+playerList[category][team]["ID"][id]['STATS'][stats]+'",'
			        //		console.log(playerList[category][team]["ID"][id]['STATS'][stats]);
			        		counter++;
			        	}
			        	player = player.substring(0, player.length - 1);
			        	player = player + '}'
			        	playerLists.push(JSON.parse(player));
			        };
			        return playerLists
		    	};
		      
	        	playerCollections["ElitePlayersHome"] = new PlayerCollections["ElitePlayersHome"](createPlayerJson("ELITE",hometeam));
		        playerCollections["ElitePlayersAway"] = new PlayerCollections["ElitePlayersAway"](createPlayerJson("ELITE",awayteam));
    			playerCollections["NormalPlayersHome"] = new PlayerCollections["NormalPlayersHome"](createPlayerJson("NORMAL",hometeam));
		        playerCollections["NormalPlayersAway"] = new PlayerCollections["NormalPlayersAway"](createPlayerJson("NORMAL",awayteam));
    			var normalPlayerSelections = new NormalPlayerSelections();
    			var RemoveElitePlayerView = Backbone.View.extend({
    					el: $('.eliteplayer'),
    					template: _.template($("#tpl-removeeliteplayer").html()),
                        render: function(e){
                        	$(this.$el).html(this.template({}));
						    var playerid = $(this.$el).attr('data-selectedeliteid');
						    $(this.$el).removeAttr('data-selectedeliteid');
						 //   console.log($("[data-playerid='" + playerid + "']").html());
						     $("[data-playerid='" + playerid + "']").removeClass('no-action');
					    	$('.eliteplayer').removeClass('populated innershadow').addClass('blackgrad-flat offcanvas-nav');
					    	$("[data-playerid='" + playerid + "']").find('.fa-minus').addClass('fa-plus eliteadd').removeClass('fa-minus eliteremove');
					   
						     this.delegateEvents();
						     return this;
                        }
    			});
		        var AddElitePlayerView = Backbone.View.extend({
		        		el: $('.eliteplayer'),
						template: _.template($("#tpl-addeliteplayer").html()),
						events:{
							'click .fa-minus':'removeElitePlayer'
						},
					    initialize: function () {},
					    render: function(){

						        $(this.$el).html(this.template({addplayer:this.model.toJSON()}));
						        $(this.$el).attr('data-selectedeliteid',this.model.get('id'));
						        console.log(this.model.get('id'));
						        this.delegateEvents();
						        return this;
						    
					    },
					    removeElitePlayer: function(){
					    //	var thisid = $(e.currentTarget).parent().data("id");
					    
					    //	console.log(this.model.toJSON());
					    //	this.model.destroy();
					    //	console.log(this.model.toJSON());
					    	this.removeElitePlayerView = new RemoveElitePlayerView({});
					    	this.removeElitePlayerView.render();
					    

					    }
					});
		        var AddNormalPlayerView = Backbone.View.extend({
		        		el: '#normalplayercontainer',
						template: _.template($("#tpl-addnormalplayer").html()),
						events:{
							'click .fa-minus':'removeNormalPlayer'
						},
					    initialize: function () {
					    	$('.fa-minus').on('click',function(){
					    		console.log('hello')
					    	});
					    },
					    render: function(){
					    		console.log('test');
					    		var that = this;
					    		this.collection.each(function(addnormalplayer,index){
					    			console.log(index);
					    			console.log(addnormalplayer.toJSON());
					    			console.log($(that.$el).find('.playercontainer:eq('+index+')'));
						        $(that.$el).find('.playercontainer:eq('+index+')').replaceWith(that.template({addnormalplayer:addnormalplayer.toJSON()}));
						    	that.delegateEvents();
						    	});
						        
						        return that;
						     	
					    },
					    removeNormalPlayer: function(e){
					    	var thisid = $(e.currentTarget).parent().data("id");
					    	console.log(thisid);
					    }
					});
				var elitePlayerSelect = Backbone.View.extend({
						template: _.template($("#tpl-eliteplayer").html()),
						events:{
					        'click .eliteadd':'addElitePlayer'
					    },
					    initialize: function () {

					    },
					    render: function(){
					        $(this.$el).html(this.template({persons:this.collection.toJSON()}));
					        return this;
					    },
					    addElitePlayer: function(e){
					   // 	e.preventDefault();
					   		console.log(document.querySelector('[data-selectedeliteid]'));
					   		if (!document.querySelector('[data-selectedeliteid]')) {
						    	var id = $(e.currentTarget).parent().data("playerid");
						    	var selectedPlayerId = this.collection.get(id);
						    	console.log(selectedPlayerId.toJSON());
						    	this.addElitePlayerView = new AddElitePlayerView({model:selectedPlayerId});
						    	this.addElitePlayerView.render({});
						    	$(e.currentTarget).parent().addClass('no-action');
						    	$('.eliteplayer').addClass('populated innershadow').removeClass('blackgrad-flat offcanvas-nav');
						    	$(e.currentTarget).parent().find('.fa-plus').addClass('fa-minus eliteremove').removeClass('fa-plus eliteadd');
						    } else {
						    	alert('eliteplayer selected');
						    }
					    }
					});
				var normalPlayerSelect = Backbone.View.extend({
						template: _.template($("#tpl-normalplayer").html()),
						events:{
					        'click .normaladd':'addNormalPlayer'
					    },
					    initialize: function () {
					    	
					    },
					    render: function(){
					        $(this.$el).html(this.template({persons:this.collection.toJSON()}));
					        return this;
					    },
					    addNormalPlayer: function(e){
					   // 	e.preventDefault();
					   		
					   		console.log('hello');
					    	var id = $(e.currentTarget).parent().data("playerid");
					    	var selectedNormalPlayerId = this.collection.get(id);
					    	console.log(selectedNormalPlayerId.toJSON());
					    	if (normalPlayerSelections.length < 5) {
					    		console.log(normalPlayerSelections.length);
					    		normalPlayerSelections.add(selectedNormalPlayerId);
					    		this.addNormalPlayerView = new AddNormalPlayerView({collection:normalPlayerSelections});
						    	this.addNormalPlayerView.render({});
						    	$(e.currentTarget).parent().addClass('no-action');
						    	$('.normalplayer').addClass('populated innershadow').removeClass('blackgrad-flat offcanvas-nav');
						    	$(e.currentTarget).parent().find('.fa-plus').addClass('fa-minus normalremove').removeClass('fa-plus normaladd');
						 
					    	} else {
						     	alert('selected all normal players');
						    };
					    	
   						}


					});
		    	this.eliteHomePlayerSelectView = new elitePlayerSelect({el:'#panel1-1',collection:playerCollections["ElitePlayersHome"]});
        		this.eliteHomePlayerSelectView.render({});
        		this.eliteAwayPlayerSelectView = new elitePlayerSelect({el:'#panel1-2',collection:playerCollections["ElitePlayersAway"]});
        		this.eliteAwayPlayerSelectView.render({});
        		this.eliteHomePlayerSelectView = new normalPlayerSelect({el:'#panel2-1',collection:playerCollections["NormalPlayersHome"]});
        		this.eliteHomePlayerSelectView.render({});
        		this.eliteAwayPlayerSelectView = new normalPlayerSelect({el:'#panel2-2',collection:playerCollections["NormalPlayersAway"]});
        		this.eliteAwayPlayerSelectView.render({});
        		accordionClick();
        		saveTeam();
        		
        		if (localStorage.getItem('ELITE')) {
        			var normalplayermodel = new Player();
        			var elitePlayerID = localStorage.getItem('ELITE');
        			var savedEliteModel = playerCollections["ElitePlayersAway"].get(elitePlayerID);
        			if (typeof savedEliteModel === 'undefined'){
        				var savedEliteModel = playerCollections["ElitePlayersHome"].get(elitePlayerID);
        			};
        			this.savedEliteView = new AddElitePlayerView({model:savedEliteModel});
        			this.savedEliteView.render();
        			$("[data-playerid='" + elitePlayerID + "']").addClass('no-action');
					$('.eliteplayer').addClass('populated innershadow').removeClass('blackgrad-flat offcanvas-nav');
					$("[data-playerid='" + elitePlayerID + "']").find('.fa-plus').addClass('fa-minus eliteremove').removeClass('fa-plus eliteadd');
        			
        			var savedNormalCollection = new NormalPlayerSelections();
        			for (var i=0;i<5;i++){
        				var normalPlayerID = localStorage.getItem('NORMAL'+i);
        				var savedNormalModel = playerCollections["NormalPlayersAway"].get(normalPlayerID);
        				if (typeof savedNormalModel === 'undefined'){
	        				var savedNormalModel = playerCollections["NormalPlayersHome"].get(normalPlayerID);
	        			};
	        			savedNormalCollection.add(savedNormalModel);
	        			$("[data-playerid='" + normalPlayerID + "']").addClass('no-action');
						
						$("[data-playerid='" + normalPlayerID + "']").find('.fa-plus').addClass('fa-minus normalremove').removeClass('fa-plus normaladd');

        			};
        			$('.normalplayer').addClass('populated innershadow').removeClass('blackgrad-flat offcanvas-nav');
        			this.savedNormalView = new AddNormalPlayerView({collection:savedNormalCollection});
        			this.savedNormalView.render();

        		}

		    },
		    error: function (request, status, error) {
		        console.log('error', error);
		    }
		});	

function saveTeam(){
	$('#saveTeam').on('click',function(){
		if (!$('.eliteplayer').attr('data-selectedeliteid')){
			alert('Elite player not selected. Team not complete');
			return false;
		};
		 if ($('.normalplayer').length < 5){
		 	alert('All normal players not selected. Team not complete');
			return false;
		 };
		 localStorage.setItem('ELITE',$('.eliteplayer').attr('data-selectedeliteid'));
		 for (var i =0;i<$('.normalplayer').length;i++){
		 	localStorage.setItem('NORMAL'+i,$('.normalplayer:eq('+i+')').attr('data-selectednormalid'));
		 }
		 alert('your team is saved');
	});
}

function accordionClick(){
	$('.elitepanel').click(function(){
	        
	            		$('#panel1a').addClass('active');
	            		$('#panel2a').removeClass('active');
	            	});
	            	$('.normalpanel').click(function(){
	            		$('#panel2a').addClass('active');
	            		$('#panel1a').removeClass('active');
	            	});
	            	$('.elitehometeam').click(function(){
	            		$('#panel1-1').addClass('active');
	            		$('#panel1-2').removeClass('active');
	            	});
	            	$('.eliteawayteam').click(function(){
	            		$('#panel1-2').addClass('active');
	            		$('#panel1-1').removeClass('active');
	            	});
	            	$('.normalhometeam').click(function(){
	            		$('#panel2-1').addClass('active');
	            		$('#panel2-2').removeClass('active');
	            	});
	            	$('.normalawayteam').click(function(){
	            		$('#panel2-2').addClass('active');
	            		$('#panel2-1').removeClass('active');
	            	});
};

	
}


