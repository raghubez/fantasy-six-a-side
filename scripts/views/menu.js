
/*
var MatchClock = Backbone.Model.extend({
   defaults: {
   lDay:"2",
   rDay:"3",
   lHour:"",
   rHour:"",
   lMin:"",
   rMin:""
    
  }
});
*/
//var matchClock = new MatchClock();

var HeaderInfo = Backbone.Model.extend({
  urlRoot:"json/fixture",
  parse: function(data){
  	return data.FIXTURE;
  }
});

/*
var MatchClockView = Backbone.View.extend({
	
	initialize: function() {
    	
    		
    		this.model = new MatchClock();
    		
    		showJSON(this.model);
    		
    		this.model.bind("change",this.render,this);
        
    },
   render: function(){
      // Compile the template using underscore
      
      var strClock = '<p>Game kicks off in:</p>' +
                      '<div class="row">' +
                          '<div class="days">' +
                              '<span><%= lDay %></span>' +                              
                              '<span>0</span>' +
                              '<p>Days</p>'+
                          '</div>'+
                          '<div class="days">'+
                              '<span>2</span>'+
                              '<span>2</span>'+
                              '<p>Hours</p>'+
                          '</div>'+
                          '<div class="days">'+
                              '<span>2</span>'+
                              '<span>6</span>'+
                              '<p>Min</p>'+
                          '</div>'+
                      '</div>';
      
      var template = _.template( strClock, {} );
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      // Load the compiled HTML into the Backbone "el"
      this.$el.html( template );
    }    
	
});
*/
app.views.menu = Backbone.View.extend({
		header_template: _.template($("#tpl-menu").html()),
    initialize: function() {
    	//	Backbone.Subviews.add (this );
    		
    		this.model.bind("change",this.render,this);
        this.model.fetch(
        
        {
    success: function (data) {
    	 // showJSON(user);
        
        var matchDate = data.get('DATE').split("/");

			 //showJSON(matchDate);
        
      
       
       var xdate = new XDate();
       
       xdate.setFullYear(matchDate[2]);
       xdate.setMonth(matchDate[1] - 1);
       xdate.setDate(matchDate[0]);
        
      
       
       var dayOfWeekToString = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
       var twoLetterSufffix = ["st","nd","rd","th","th","th","th","th","th","th","th","th","th","th","th","th","th","th","th","th","st","nd","rd","th","th","th","th","th","th","th","st"];
       var toMonth = ["January","February","March","April","May","June","July","August","September","October","November","December"];
       var date = dayOfWeekToString[xdate.getDay()] +" " +xdate.getDate() + twoLetterSufffix[xdate.getDate()]+" " +toMonth[xdate.getMonth()]+" "+xdate.getFullYear();
        
       data.set('DATE',date)
       
      
     // alert(data.get('CURRENTEPOCH'));
       var t = Number(data.get('CURRENTEPOCH'));
       //var currEpoch = new XDate(t*1000);
       
       
       
       var s = Number(data.get('MATCHEPOCH'));
       
       
       s = t + 300000;
      
  			matchTime(60);
       
      //
       var timer = setInterval(function() {adjustTimeToMatch() }, 60000) 
        
       
        function adjustTimeToMatch(){
        
        
        var mt = matchTime(0);
        
        
       
      }
      	function matchTime(diff){
      		
      		 	var currEpoch = new XDate(t*1000);
      		 
       			var matchEpoch = new XDate(s*1000);
       
			 			var minsDiff = currEpoch.diffMinutes(matchEpoch);
				
			 			var daysToMatch = Math.floor(minsDiff/(60*24));  /*its maths you USA *********/       
       
       			var hoursToMatch = Math.floor((minsDiff - daysToMatch*24*60)/60);
       
       			var minsToMatch = Math.floor(minsDiff-(hoursToMatch * 60 + daysToMatch*24*60));
       
       			//alert(minsToMatch);
       
      			//alert ("[Mins diff ="+minsDiff+"]  " + daysToMatch + "  " + hoursToMatch + "  " + minsToMatch);
      	
      			t = t + 60-diff;
      			
      			var lDay = Math.floor(daysToMatch/10);
      			var rDay = daysToMatch % 10;
      			
      			var lHour = Math.floor(hoursToMatch/10);
      			var rHour = hoursToMatch % 10;
      			
      			var lMin = Math.floor(minsToMatch/10);
      			var rMin = minsToMatch % 10;
      			
      			$('#lDay').text(lDay); 
			      $('#rDay').text(rDay); 
			      $('#lHr').text(lHour); 
			      $('#rHr').text(rHour); 
			      $('#lMin').text(lMin); 
			      $('#rMin').text(rMin); 
      			
      			
      			
 
      		
      	}
    },
    fail:function(){alert("problem");}
    
    
    
}
        
        
        	
        	
        
        
        
        
        
        
        
        );
    },
    /*
    subviewCreators:{
    	"matchClockView": function(){
    		var options = {};
    		return new MatchClockView( options );
    	}
    },
    */
    render: function(){
   // 	var that = this;
    	var modeldata = this.model.toJSON()
    	console.log(modeldata);
 /*   	$.get('/scripts/templates/menu.html', function (data) {
            template = _.template(data, modeldata);//Option to pass any dynamic values to template
            $(this.$el).html(template);//adding the template content to the main template.
        }, 'html');*/
        $(this.$el).html(this.header_template(this.model.toJSON()));
    }
});

/* code position ????? */