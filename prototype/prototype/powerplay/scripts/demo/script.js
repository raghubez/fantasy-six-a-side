(function(){

    // Localize jQuery variable
    var jQuery;
    /******** Load jQuery if not present *********/
    if (window.jQuery === undefined) {
        alert('jQuery is required');
    } else {
        // The jQuery version on the window is the one we want to use
        jQuery = window.jQuery;
        main();
    }

    function main() {
        jQuery(document).ready(function($) {
	        	var aFormations     = ["1442", "1433"];
	        	var sContentUrl     = '';
	        	var iRetry          = 0;
        	  var iTeamSize       = 11;
        	  var iRetry          = 0;
        	  var iMaxValue       = 50;
        	  var iMostExpensive  = 11; 
            var aPlaTables      = new Array('gt','dt','mt','st','at');
            var aGroupCat       = {'G':1,'D':2,'M':3,'S':4};
            var aCatGroup       = new Array('G','D','M','S');
            var aGroupName      = {'G':'Goalkeeper','D':'Defender','M':'Midfielder','S':'Striker'}
            var aGroup3Char     = {'G':'GK','D':'DEF','M':'MID','S':'STR'};
            var oPlayers        = new Object;
            var aTeamNames      = new Array();
            var oTeams          = new Object();
            var aMinForm        = new Array(1,3,3,1);
            var aMaxForm        = new Array(1,5,5,3);
            var aMaxPerTeam     = 15;
            var iCurrTotalVal   = 0;
            
            
            $('div.player-tab').gfmtabs();
            
            loadPlayers();
            loadTeamFromCookie();
            
            function oPlayer(){
                this.t          = 'UKN';
                this.v          = 0;
                this.g          = 'G';
                this.p          = 0;
                this.pk         = 0;
                this.s          = 'RSCURENT';
                this.i          = 'N';
                this.lyshots    = 0;
                this.lymins     = 0;
                this.lyred      = 0;
                this.lyassist   = 0;
                this.lyappear   = 0;
                this.lygoals    = 0;
                this.lyyellow   = 0;
            }
                      
            function loadPlayers(){

                /* We need to load all the players ready for selection */
                $.ajaxSetup({cache:true,timeout:10000});
                $.getJSON("players.json", function(data){

                    // Looks for the error message;
                    if(data.error){
                        alert(data.error);
                        return;
                    }

                    /* sets up or updates the player information */
                    json_players    = data.players;
                    json_team       = data.team;
                    json_teamname   = data.teamname;
                    json_group      = data.group; 

                    var iGroupLen = json_group.length;
                    while(iGroupLen--) {
                        var sGroup = json_group[iGroupLen];
                        var plaGroup = json_players[iGroupLen];
                        var iPlaLen = plaGroup.length;
                        while(iPlaLen--) {
                            var currPlayer = plaGroup[iPlaLen];
                            var newPlayer = new oPlayer;

                            newPlayer.n     = currPlayer[1];
                            newPlayer.t     = json_team[currPlayer[2]];
                            newPlayer.teamcode     = currPlayer[2];
                            newPlayer.v     = (currPlayer[3]/10).toFixed(1);
                            newPlayer.pk    = (currPlayer[4]/10).toFixed(1)+'%';
                            newPlayer.p     = currPlayer[5];
                            newPlayer.avail = currPlayer[6];
                            newPlayer.news 	= currPlayer[7];   
                            if(currPlayer[7] != 1){
                                newPlayer.s = 'RSCURENT';
                            }else{
                                newPlayer.s = 'RSSUSPEN';
                            }
                            newPlayer.shortname = currPlayer[8];

                            newPlayer.g = sGroup;
                            newPlayer.i = 'Y';

                            newPlayer.lyshots   = currPlayer[8];
                            newPlayer.lymins    = currPlayer[9];
                            newPlayer.lyred     = currPlayer[10];
                            newPlayer.lyassist  = currPlayer[11];
                            newPlayer.lyappear  = currPlayer[12];
                            newPlayer.lygoals   = currPlayer[13];
                            newPlayer.lyyellow  = currPlayer[14];
                            newPlayer.league    = currPlayer[31];

                            oPlayers[currPlayer[0]] = newPlayer;

                            aTeamNames[json_team[currPlayer[2]]] = json_teamname[currPlayer[2]];
                        }
                    }

                    initialise();

                }).success(function() {
                    //debug("second success");
                }).error(function(xhr, ajaxOptions, thrownError) {
                iRetry++;
                if(iRetry<=1){
                    loadPlayers();
                    return;
                }
                }).complete(function() {
                    //debug("complete");
                });
            }
            

            function getCookie(cname) {
		  				  var name = cname + "=";
						    var ca = document.cookie.split(';');
						    for(var i=0; i<ca.length; i++) {
						        var c = ca[i];
						        while (c.charAt(0)==' ') c = c.substring(1);
						        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
						    }
						    return "";
						}
		
            
						var popup = getCookie('MFDTSelectPopup');
						if (!popup) {
							$.colorbox({width:"300", height:"260", inline:true, scrolling:false,
	      				onComplete: function() { $('#cboxLoadedContent').html('<div class="colorboxContent"><h1>Start picking your team</h1><p>Tap Select Players or swipe to open the player list to get started.</p><button onclick="$.colorbox.close();" class="buttonL redBtn">Close</button></div>'); }
							});
							document.cookie = "MFDTSelectPopup=true;";
						}
						

            
            $('table.sortable th').on('click', function(){
							if ($(this).html() === '') {
								return;
							}
							var clicked = $(this);
							var table = $(this).closest('table.sortable');
							var index = $(this).index() + 1;
							var oPlayers = [];
							$(table).find('tbody tr').each(function() {
								var html = $(this)[0].outerHTML;
								var compared = $(this).find('td:nth-child('+index+')').html();
								if (compared === '' || compared === undefined) {
									return;
								}														
								if (compared === '-') {
									compared = 999;
								}
								if (!isNaN(compared)) {
									compared = parseFloat(compared);
								}
								else {
									compared = compared.toLowerCase();
								}
								$(this).remove();
								oPlayers.push({id:compared,player:html});
							});
							if (clicked.find('span').hasClass('') || clicked.find('span').hasClass('down')) {
								oPlayers = sortByKey(oPlayers, "id");
								$("th span").removeClass();
								$(this).find('span').addClass('up');
							}
							else {
								oPlayers = oPlayers.reverse();
								$("th span").removeClass();
								$(this).find('span').addClass('down');
							}
							for(var plaId in oPlayers) {
								$(table).find("tbody").append(oPlayers[plaId].player);
							}
							if($(table).attr('id') === 'gt' || $(table).attr('id') === 'dt' || $(table).attr('id') === 'mt' || $(table).attr('id') === 'st' || $(table).attr('id') === 'at'){
								assignEvents($(table).attr('id'));
							}
							oPlayers = []
						});
						
						function sortByKey(array, key) {
					    return array.sort(function(a, b) {
					        var x = a[key]; var y = b[key];
					        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
					    });
						}
            
            function filterPlayers(oBj){
                var re = new RegExp($(oBj).val().toLowerCase());
                var oTable = $(oBj).parent().parent();
                oTable.find(".filterable tr").each(function(){
                    var oCurrObj = $(this);
                    if($(this).attr('search').match(re)){
                        $(this).show();
                    }else{
                        $(this).hide();
                    }
                });
            }            
 
            function oDrawPlayerTable(table){
                if($('#'+table).length == 1){return;}

                var iNo = 1;
                var iGroup = 'G';

                if(table == 'gt'){
                    iNo = 1;
                    iGroup = 'G';
                }

                if(table == 'dt'){
                    iNo = 2;
                    iGroup = 'D';
                }

                if(table == 'mt'){
                    iNo = 3;
                    iGroup = 'M';
                }

                if(table == 'st'){
                    iNo = 4;
                    iGroup = 'S';
                }

                if(table == 'at'){
                    iNo = 5;
                    iGroup = 'A';
                }

                $('').appendTo('#pl'+iNo);

                var s=[];
                for(var k in oPlayers){
                    var v = oPlayers[k];

                    if(v.s != 'RSCURENT'){
                        continue;
                    }

                    if(iGroup != 'A' && v.g != iGroup){
                        continue;
                    }

                    if(v.p == undefined){v.p = '-'};
                    if(v.pk == undefined){v.pk = '-'};
                    if(v.cs == undefined){v.cs = '-'};
                    
                    
										var sAvail = getAvailability(v);
										if (sAvail === '') {
											sAvail = 'icoInfo'
										}
                    var sRow = '<tr id="p' + k + '" class="picked player-row" ><td class="lname">' + v.n + '</td><td class="lclub">' + v.t + '</td><td class="lpric">' + v.v + '</td><td class="lpoin">'+v.p+'</td><td class="lstat"><span plaId='+k+' class="pla-inf sprite ' +sAvail + '"></span></td></tr>';
                    s.push(sRow);
        						}			 
                
                var sHtml = '<table id="'+table+'" class="filterable"><tbody>'+s.join("")+'</tbody></table>';
                //$('#pl'+iNo).html(sHtml);
                if (s.length) {
		              $('#pl'+iNo+' table').attr('id', table)
									$('#pl'+iNo+' tbody').html(s.join(""));
								}
								assignEvents(table);
                
                oDrawPitch();
                
            }    
                
            
            function getAvailability(oPlayer) {
            	if (oPlayer.avail === 'D') {
            		return 'icoDou';
            	}
            	else if (oPlayer.avail === 'I') {
            		return 'icoInj';
            	}
            	else if (oPlayer.avail === 'S') {
            		return 'icoSus';
            	} 
            	else if (oPlayer.avail === 'U' || oPlayer.news !=0 ) {
            		return 'icoUna';
            	} 
            	else {
            		return '';
            	}
            }
            
                
                
		            function saveTeamToCookie() {
		            	var cookie = '';
		              for(var i=1;i<=iTeamSize;i++){  
		                var iCurrId = $('#p'+i).attr('value');
		                if(iCurrId) {
		               	  cookie = cookie + "%2C" + iCurrId;                   
		                } else {
		                	cookie = cookie + "%2C";                 
		                }
		              }
		              cookie = cookie + "%7C";
		              cookie = cookie + $('#teamname').val();
		                       
		              var date = new Date();
		              date.setTime(date.getTime() + (10000));
		              var expires = "; expires=" + date.toGMTString();
		              document.cookie = 'MFDTSelTeam' + "=" + cookie + expires + "; path=/";                    
		            }

		            function loadTeamFromCookie() {
		            	var team    = getCookie("MFDTSelTeam");
		            	if (!team) {
		            		return;
		            	}
		            	var parts    = team.split("%7C");           	
		            	var players  = parts[0].split("%2C");
		            	var teamName = parts[1];
		            	$('#teamname').val(decodeURIComponent(teamName).split('+').join(' '));   
		            	for (var i = 0; i < players.length; i++) {
		            		if (players[i]) {
		            		  oSelectPlayer(players[i]);    
		            		}        		
		            	} 
		        	
		            }		                     
                
                function initialise(){
                
                /* Draw table */
                oDrawPlayerTable('gt');

                $('.placols>li').click(function(){

                    var oTable = $(this).parent().parent().find('table');

                    if($(this).hasClass("lname")){
                        oTable.sortable('sort',0);
                    }else if ($(this).hasClass("lclub")){
                        oTable.sortable('sort',1);
                    }else if ($(this).hasClass("lpric")){
                        oTable.sortable('sort',2);
                    }else if ($(this).hasClass("lpick")){
                        oTable.sortable('sort',3);
                    }else if ($(this).hasClass("lpoin")){
                        oTable.sortable('sort',3);
                    }else if ($(this).hasClass("lcs")){
                        oTable.sortable('sort',4);
                    }
                });

                /* Functions for the filter options */
                $(".plasearch input").keypress(function(e){
                    if(e.which == 13){
                        e.preventDefault();
                        return false;
                    }
                }).focus(function(){
                    $(this).animate( { "opacity" : 1 }, 250 );
                }).blur(function(){
                    if($(this).hasClass('placeholder')){
                        $(this).animate( { "opacity" : .5 }, 250 );
                    }
                }).animate(
                    { "opacity" : .5 }
                    , 250
                ).keyup(function(e){
                    clearTimeout($.data(this, 'timer'));
                    var oObj = this;
                    var wait = setTimeout(function(){filterPlayers(oObj)}, 500);
                    $(this).data('timer', wait);
                });

                $('ul.gfm-tabs li>a').click(function(){
                    oDrawPlayerTable(aPlaTables[$(this).parent().index()]);
                });
                
                if($('.sidebarRight').length){
									var headerHeight = $("#page").offset().top;
                	$('.offcanvas .sidebarRight').css('top', headerHeight);
                }                

                /* These are new team options */
                if($('#teamname').length){

                    // get current number of characters
                    $('#teamname-counter').html("("+($('#teamname').attr('maxlength')-$('#teamname').val().length) + " char's)");

                    $('#teamname').keyup(function(){
                        // get new length of characters
                        $('#teamname-counter').html("("+($('#teamname').attr('maxlength')-$('#teamname').val().length) + " char's)");
                    }).blur(function(){
                        if($(this).val().length > 0){
                            $(this).addClass('reg-comp');
                        }else{
                            $(this).removeClass('reg-comp');
                        }
                        setSubmitState();
                    });

                    $('#email').blur(function(){
                        if($(this).val().length > 0 && validateEmail($(this).val())){
                            $(this).addClass('reg-comp');
                        }else{
                            $(this).removeClass('reg-comp');
                        }
                        setSubmitState();
                    });

                    $("#teamname").keyup(function(e){
                        clearTimeout($.data(this, 'timer'));
                        var oObj = this;
                        var wait = setTimeout(function(){setSubmitState()}, 500);
                        $(this).data('timer', wait);
                    }).blur(setSubmitState);

                    $('#termsok').click(
                        setSubmitState()
                    )

                    $('#autosel').click(function(){                   	                            
                         hideInstructions();
                         autoComplete();
                         saveTeamToCookie();
                         return false;
                    });

                    $('#lterms a').click(function(){
                        $.colorbox({href:'/terms-and-conditions/popup/',width:"737px",height:"570px",iframe:true,scrolling:false});
                        return false;
                    });

                    $('#save').click(function(){
                        oSave();
                        return false;
                    });

                }

                $('#teamSaved, #platPop, #goldPop').click(function(){
                    $(this).hide();
                });

                $('#headerBanner').click(function(e){
                    hideInstructions();
                    $('#platPop').hide();
                    $('#goldPop').hide();
                    var offset = $(this).offset();
                    //alert(e.clientX - offset.left);
                    //alert(e.clientY - offset.top);
                    if((e.clientX - offset.left) < 350){
                        $('#goldPop').show();
                    }else{
                        $('#platPop').show();
                    }
                });

                $('#footerBanner').click(function(e){
                    hideInstructions();
                    $('#platPop').hide();
                    $('#goldPop').hide();
                    var offset = $(this).offset();
                    if((e.clientX - offset.left) < 600){
                        if($('#sTStatus').text() == "Platinum"){
                            return;
                        }
                        //$('#goldPop').show();
                        $('#sTStatus').text('Gold');
                        $('#sTCredits').text('3');
                        $(this).attr('src',sContentUrl+'/images/team/footer/940x120-plat-upgrade.jpg')
                    }else{
                        //$('#platPop').show();
                        $('#sTStatus').text('Platinum');
                        $('#sTCredits').text('7');
                    }


                });
             

                $('#submit').click(function(){
                    hideInstructions();
                    return oConfirm();
                });

                $('#resetteam').click(function(){

                    if(!iTransfers){
                        //var agree = confirm("Are you sure you want to reset your selections?");
                        //if (!agree){
                        //    return false;
                        //}
                    }else{
                        iTransfersLeft = 3;
                        resetTeam();
                    }

                    for(var z=1;z<=iTeamSize;z++){
                        if(iTransfers){
                            $('#p'+z).attr('value',aOriginalTeam[(z-1)]);
                        }else{
                            $('#p'+z).attr('value','');
                        }
                    }
                                       
                    saveTeamToCookie('MFDTSelTeam');

                    oDrawPitch();
                    return false;
                });


            }

						function drawTableView(){
                var aRows=[];
            	for(var j=1;j<=iTeamSize;j++){
            		var sCurrPla = $('#p'+j).attr('value');

            		if(sCurrPla && oPlayers[sCurrPla]) {
                        var oPlayerData = oPlayers[sCurrPla];
			            			var sAvail = getAvailability(oPlayerData.avail); 
                        var playerUnavailable = '';
                        if (sAvail === 'icoUna' || oPlayerData.news === 1) {
                        	playerUnavailable = 'playerUnavailable';
                        	sAvail = 'icoUna';
                        }			       
                             			                       
                        var sRow = '<tr id="p' + sCurrPla + '" class="removePlayer '+playerUnavailable+'"><td><a href="/statistics/popup/player/'+sCurrPla+'/"class="pla-inf sprite '+sAvail+' iconInfo"></a>' + aGroup3Char[oPlayerData.g] + '</td><td class="tabName">' + oPlayerData.n + '</td><td>' + oPlayerData.t + '</td><td>' + oPlayerData.v + '</td><td>' + oPlayerData.pk + '</td><td class="points listViewRemoveIco"><div>' + oPlayerData.p + '</div><span></span</td></tr>';
                        aRows.push(sRow);
            		}

                    $('#listView table tbody').html(aRows.join(""));
                    
                    $('#listView').on('click', '#p'+sCurrPla+'.removePlayer', function(){
              				oRemovePlayer($(this).attr('id').substring(1,5));
                    	oDrawPitch();
              			});            			                    
            	}
            }            
            
            function oSelectFromList(iPlaId){
                for(var i=1;i<=iTeamSize;i++){
                    if($('#p'+i).attr('value') == iPlaId){
                        oRemovePlayer(iPlaId);
                        oDrawPitch();
                        return;
                    }
                }
                oSelectPlayer(iPlaId);
            }

            function oSelectPlayer(iPlaId){

                /* Hide on this action */
                $('#miff-firsttime').hide();

            	if(!iPlaId){alert('Unable to determine player ID');return;}

            	/* how many players for his player group */
                var oPlayer = oPlayers[iPlaId];

                if(oPlayer == undefined){
                    return;
                }

            	/* Count all existing players to see how many players in this position we have */
                var iPlaPosCount = 1;
                var iMaxInPosition = aMaxForm[(aGroupCat[oPlayer.g]-1)]; // add 1 to it for a sub of each type

                /* Load in existing Players */
                var aCurrPlayers = new Array;
                var aTotalPlayers = new Array;

                for(var i=1;i<=iTeamSize;i++){
                    var iCurrId = $('#p'+i).attr('value');
                    if(iCurrId){
                        aCurrPlayers.push(iCurrId);
                        aTotalPlayers.push(iCurrId);
                    }
                }

                var iTeamWatch = 1; /* Set to 1 as this is the player we are picking */
                for(var i=0;i<=(aTotalPlayers.length-1);i++){
                    var sCurrPla = aCurrPlayers[i];
                    if(sCurrPla){

                        /* Check they are not in team */
                        if(sCurrPla == iPlaId){
                            return;
                        }

                        /* Check Team retrictions */
                        if(oPlayers[sCurrPla].t == oPlayer.t){
                            iTeamWatch++;
                            if(iTeamWatch>aMaxPerTeam){
                                errorV2("You are unable to select any more players from '"+aTeamNames[oPlayer.t]+"'");
                				return;
                            }
                        }
                    }
                }

                /* Add in the new player and validate the formation */
                aCurrPlayers.push(iPlaId);
                var sFormation = getFormation(aCurrPlayers);
                if(!sFormation){
                	if(aGroupName[oPlayer.g] === 'Goalkeeper') {
                	errorV2("You can only choose one Goalkeeper!");
                }
                else {
                  errorV2("Invalid player formation, you can't pick any more "+aGroupName[oPlayer.g]+"s <br/><br/>The 11 players you select on the pitch must fit into one of the following two formations:<br/><br />4-4-2 or 4-3-3");
               	}
               	return;
                }else{
                  /* Set the player id as picked */
                	for(var i=1;i<=iTeamSize;i++){
                		if($('#p'+i).attr('value') == ""){
                			$('#p'+i).attr('value',iPlaId);
                            break;
                		}
                	}

									if ($('tr#p'+iPlaId).text() !== 'Player Added') {
									var playerRow = $('tr#p'+iPlaId).html();
									var index = $('tr#p'+iPlaId+' td').length;
									$('tr#p'+iPlaId).html('')
              		$("<td colspan="+index+" class='plaAdded'>Player Added</td>")
									    .appendTo($('tr#p'+iPlaId))
									    .timeout(1000, function(){ this.fadeOut('slow');})
									    .timeout(1500, function(){this.remove(); $('tr#p'+iPlaId).html(playerRow);})
									}
                	saveTeamToCookie();
                }
            }

            function oRemovePlayer(iPlaId){
            	for(var j=1;j<=iTeamSize;j++){
            		if($('#p'+j).attr('value') == iPlaId){
            			$('#p'+j).attr('value','')
                  /* move players list */
                  //$('div.player-tab').gfmtabs('openItem',(aGroupCat[oPlayers[iPlaId].g]-1));
                  oDrawPlayerTable(aPlaTables[aGroupCat[oPlayers[iPlaId].g]-1]);
                  saveTeamToCookie();
            			return;
            		}
            	}
            }

            function oDrawPitch(){
            	// this function puts all the players in place on the pitch
                var aCurrPlayers = new Array();
                for(var i=1;i<=iTeamSize;i++){
            		var iCurrId = $('#p'+i).attr('value');
                    if(iCurrId){aCurrPlayers.push(iCurrId)};
                }

                var sFormation = getFormation(aCurrPlayers);
                if (!sFormation){return}
                setFormation(sFormation);
                teamTidyUp(sFormation);

                /* Remove row highlight */
               	$('.plalist tr.unavail').removeClass('unavail');
                $('.plalist tr.picked').removeClass('picked');

                /* Remove also unbinds */
                $('#pitch .pla-icon').remove();

                /* Build team icons */
            	iRunningTotalValue = 0;
                var ClubWatch = new Array;
            	for(var j=1;j<=iTeamSize;j++){
            		var sCurrPla = $('#p'+j).attr('value');
            		if(sCurrPla && oPlayers[sCurrPla]) {
                	createIcon(j);

                	$('.plalist tr#p'+sCurrPla).addClass('picked');
            			iRunningTotalValue += parseFloat(oPlayers[sCurrPla].v);
            			iRunningTotalValue.toFixed(1);

                        /* count how many times its been used */
                        if(ClubWatch[oPlayers[sCurrPla].t] == undefined){
                            ClubWatch[oPlayers[sCurrPla].t] = 0;
                        }
                        ClubWatch[oPlayers[sCurrPla].t]++;
                        if(ClubWatch[oPlayers[sCurrPla].t] >= aMaxPerTeam){
                            $('.t'+oPlayers[sCurrPla].t+':not(.picked)').addClass('unavail');
                        }
            		}
            	}

            	iRunningTotalValue = parseFloat(iRunningTotalValue);
            	iRunningTotalValue = iRunningTotalValue.toFixed(1);
            	$('#budget').html("<span class='budgetPound'>&pound;<span><span class='budgetVal'>"+iRunningTotalValue+"</span>M");
                iRunningTotalValue = parseFloat(iMaxValue-iRunningTotalValue).toFixed(1);
                $('.budgetRemain').html("<span class='budgetPound'>&pound;</span><span class='budgetVal'>"+iRunningTotalValue+"</span>M");

                /* Values set here to save recalculation later */
                if(iRunningTotalValue < iMostExpensive){
                     iRunningTotalValue = parseInt(iRunningTotalValue*100);
                     /* Loop players to mark as unavailable price wise */
                     $('.plalist tr:not(.picked) td.lpric').each(function(){
                        var Val = $(this).text().replace(/[^0-9.-]/g,'');
                        Val = parseFloat(Val).toFixed(1);
                        Val = parseInt(Val*100);
                        if(Val > iRunningTotalValue){
                            $(this).parent().addClass('unavail');
                        }
                     });
                }

                drawTableView();

                //setSubmitState();

								$('#pitch .pla-icon').show();
            }

				
            function errorV2(sMessage){
                $.colorbox({width:"300", height:"240", inline:true, scrolling:false,
	      				onComplete: function() { $('#cboxLoadedContent').html('<div class="colorboxContent"><p>'+sMessage+'</p></div>'); }
							});
            }

            function createIcon(iIconNo){
                /* Function to create the player icon on the pitch */
                var sCurrPla = $('#p'+iIconNo).attr('value');
                var oPlayerData = oPlayers[sCurrPla];

                var sKeeper = (iIconNo == 1) ? '-keep' : '';

                var aName = oPlayerData.shortname.split(", ");
                var sName = "";
                if(aName.length == 2){
                    sName = aName[0];
                }else{
                    sName = oPlayerData.shortname;
                }

								var sAvail = getAvailability(oPlayerData);
								var plaUna = '';
								if (sAvail === 'icoUna') {
	              	plaUna = 'playerUnavailable';
	              }

                var sIcon = "<div id='icon"+iIconNo+"' class='pla-icon player-sel icon"+iIconNo+" "+plaUna+"' plaId='"+sCurrPla+"'>";
                sIcon += "<img class='pla-img' src='"+sContentUrl+"/sfdt/mobile/images/players/"+sCurrPla+".jpg' onerror=\"this.src='"+sContentUrl+"/sfdt/mobile/images/players/blank.jpg'\" />";
                sIcon += "<p class='pla-name pla-team "+oPlayerData.t.toLowerCase()+"'>"+sName+"</p>";
                sIcon += "<p class='pla-price'>&pound;"+oPlayerData.v+"m</p>";
                sIcon += "<a href='/statistics/popup/player/"+sCurrPla+"/'</a>";
                sIcon += "<div class='pla-avail'><span class='sprite "+sAvail+"'></span></div>";
                sIcon += "<p class='pla-rem'></p>";
                sIcon += "</div>";

                $('#pitch').append(sIcon);

								$('#icon'+iIconNo).mousedown(function() {
									dragging=0;
								    $(document).mousemove(function(){
								       dragging = 1;
								    });
								});
								
								$(document).mouseup(function(){
								    $(document).unbind('mousemove');
								});
                               
                /* Set click event */
                $('#icon'+iIconNo).click(function(){
                	if (dragging == 0){
                    openIconInfo($(this).attr('plaId'));
                    return false;
                  }
                  else return false;
                });
								$('#icon'+iIconNo).draggable({
										containment: ".draggableContainer",
                    helper: 'clone',
                    start: function(event, ui) {			
					            $('.droppableContainer').show();
						        },
						        stop: function(event, ui) {
				            	$('.droppableContainer').hide();
				        		} 
                });	
          	    /*$('#icon'+iIconNo).draggable({
                    containment: $('#siteContent'),
                    appendTo: 'body',
                    cursorAt: { top: 50, left: 45 },
                    helper: 'clone'
                });				*/	  

            }
            
            $(".droppableContainer").droppable({
            	drop: function( event, ui ) {
              	var plaId = ui.draggable.attr('plaid');
                	oRemovePlayer(plaId);
                  oDrawPitch();
              	}
            	});      
                            
            
            function openIconInfo(plaId){
            	var playerName = oPlayers[plaId].n;
            	removePlayer = false;
            	$.colorbox({width:"300", height:"260", inline:true, scrolling:false,
            		onClosed: function () {if(removePlayer !== false) { popupRemovePlayer(removePlayer);} }, 
	      				onComplete: function() { $('#cboxLoadedContent').html('<div class="colorboxContent"><h1 class="iconInfoName">'+playerName+'</h1><a href="/statistics/player/'+plaId+'/" class="buttonL greenBtn">View Stats</a><button onclick="$.colorbox.close();removePlayer='+plaId+';" class="buttonL redBtn">Remove Player</button></div>'); }
							});
            }    
            
            function popupRemovePlayer(removePlayerId) {	
							if (removePlayerId !== false) {
								oRemovePlayer(removePlayerId);
								oDrawPitch();
							}
						}  
											
            
            function getFormation(aTeam){

                var aFormCheck = new Array(0,0,0,0);
                var aCurrForm;
                for(var i=0; i<=aTeam.length;i++) {
                    var iPlaId = aTeam[i];
                    if(iPlaId){
                        if(oPlayers[iPlaId]){
                            aFormCheck[(aGroupCat[oPlayers[iPlaId].g]-1)]++;
                            if($('span#currFormVal').length) {
                            	$('span#currFormVal').html(aFormCheck.join("-"))
                            }
                        }else{
                            alert(iPlaId);
                        }
                    }
                }

                var aValidFormations = aFormations.slice();
                for( i = 0 ; i < aFormations.length ; i++ ) {
                    var aPositions = new Array;
                    aPositions = aFormations[i].split('');
                    for( j = 0 ; j < aPositions.length ; j++ ){
                        if(aFormCheck[j]>aPositions[j]){
                            aValidFormations[i]='';
                        }
                    }
                }

                /* Get first valid formation and say thats it */
                var sCurrFormation = '';
                var iFound = 0;
                for( i = 0 ; i < aValidFormations.length ; i++ ) {
                    if(aValidFormations[i]){
                        return aValidFormations[i];
                    }
                }

                return
            }
            
            function setFormation(sCurrFormation){
                /* Set the formation */
                $('#pitch').removeClass();
                $('#pitch').addClass("f"+sCurrFormation);
            }
            
            function teamTidyUp(sFormation) {

            	var iNumPlayersMoved;
            	Finished = 0;

                /* pull out all the data to find out what we have picked */
                var aPickedPlayers = new Array();
            	for(i=1;i<=iTeamSize;i++){
                    var sCurrPla = $('#p'+i).attr('value');
            		if(!sCurrPla){break};
                    aPickedPlayers.push(sCurrPla);
                }

                if(aPickedPlayers.length==iTeamSize){
                    /* We have a complete team so all we need to do is order it and it should be correct */
                    aPickedPlayers.sort();
                    for(i=1;i<=iTeamSize;i++){
                        var sCurrPla = $('#p'+i).attr('value',aPickedPlayers[i-1]);
                    }
                    return;
                }

            	while( Finished == 0 ) {
            		iNumPlayersMoved = 0;
            		for( i = 1 ; i <= iTeamSize ; i++ ) {
            			var sCurrPla = $('#p'+i).attr('value');
            			if(!sCurrPla){continue}

            			if(oGetGroupForPosition(i,sFormation) == oPlayers[sCurrPla].g){
            				continue;
            			}
            			newPosition = oGetEmptySlot(oPlayers[sCurrPla].g,sFormation);
            			if(newPosition == -1){   // No empty slot available, so just continue
            				continue;
            			}
                        $('#p'+newPosition).attr('value',sCurrPla);
                        $('#p'+i).attr('value','');
            			iNumPlayersMoved++;
            		}

            		if( iNumPlayersMoved == 0 ) {
            			Finished = 1;
            		}
            	}
            }
            
            
            function oGetGroupForPosition( position,sFormation) {
            	var iRunningTotal = 1;
            	for( var i = 0 ; i < sFormation.length ; i++ ) {
            		var iNumForCategory = parseInt(sFormation.substr(i,1));

            		for( var j = 0 ; j < iNumForCategory ; j++ ) {
            			if( position == j + iRunningTotal ) {
            				return(aCatGroup[i]);
            			}
            		}
            		iRunningTotal += iNumForCategory;
            	}
            }            
            
            function oGetEmptySlot(positionGroup,sFormation) {
            	iRunningTotal = 1;

            	for( var i = 0 ; i <sFormation.length ; i++ ) {
            		var iMaxForCategory = parseInt(sFormation.substr(i,1));
            		if( positionGroup != aCatGroup[i] ) {   // is this the right position
            			iRunningTotal += iMaxForCategory;
            			continue;
            		}
            		for( j = iRunningTotal ; j < iRunningTotal + iMaxForCategory ; j++ ) {
            			if(!$('#p'+j).attr('value')){return j;}
            		}
            		iRunningTotal += iMaxForCategory;
            	}
            	return -1;
            }                 
            
            function assignEvents(sTableId){
            	 $(".iconInfo, .pla-inf, .pla-icon").click(function () {
            	 	var plaId = $(this).attr("plaId");
                   	window.location = '/statistics/player/'+plaId+'';
                    return false;
                });
            	$('#'+sTableId+' tr.player-row').mousedown(function() {
									dragging=0;
								    $(document).mousemove(function(){
								    	
								       dragging = 1;
								    });
								});
								$(document).mouseup(function(){
								    $(document).unbind('mousemove');
								    dragging = 0;
								});
								
              $('#'+sTableId+' tr.player-row').click(function(){  
              	  if (dragging == 0){
                    oSelectFromList($(this).attr('id').substring(1,5));
                    oDrawPitch();
                  }
                  else return false;
              });
        		}       	
        });
    }


})();



(function ($) {
    var methods = {
        init: function (options) {

            var settings = $.extend({
                defaultClass: 'on',
                defaultElement: 0,
                changetab: undefined
            }, options);

            return this.each(function () {
                var $tabsObj = $(this);
                $tabsObj.data('gfmtabs-options', settings);

                var iCurrOpen = settings.defaultElement;

                $tabsObj.children('ul').find('li a').not('[target]').each(function (intIndex) {
                    $(this).bind("click", function () {
                        $tabsObj.gfmtabs('openItem', intIndex);
                        return false;
                    }).bind("mouseenter", function () {
                        //alert('over')
                    });

                    if ($(this).parent().hasClass('on')) {
                        iCurrOpen = intIndex;
                    }

                });
                $tabsObj.gfmtabs('openItem', iCurrOpen);
            });

        },
        destroy: function () {

            return this.each(function () {
                $(window).unbind('.gfmtabs');
            });

        },
        openItem: function (iIndex) {
            var $tabsObj = $(this),
                options = $tabsObj.data('gfmtabs-options');

            /* look for any that may be on */
            $tabsObj.children('ul').find('li').removeClass(options.defaultClass);
            $tabsObj.children('ul').find('li').eq(iIndex).addClass(options.defaultClass);

            $tabsObj.children('div').css('display', 'none');
            if ($tabsObj.children('div').eq(iIndex).hasClass('gfmtab-nofade')) {
                $tabsObj.children('div').eq(iIndex).show();
            } else {
                $tabsObj.children('div').eq(iIndex).fadeIn().addClass('gfmtab-nofade');
            }

            // call back if there is one
            if (typeof options.changetab == 'function') {
                options.changetab(iIndex);
            }
        }
    };

    $.fn.gfmtabs = function (method) {

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.tooltip');
        }
    };

})(jQuery);


(function($){ 
    $.fn.timeout = function(ms, callback)
    {
        var self = this;
        setTimeout(function(){ callback.call(self); }, ms);
        return this;
    }
})(jQuery);
