
(function () {

    // Localize jQuery variable
    var jQuery;
    var MAX_GOALKEEPERS  = 2;
    var MAX_DEFENDERS    = 5;
    var MIN_DEFENDERS    = 3;
    var MAX_MIDFIELDERS  = 5;
    var MIN_MIDFIELDERS  = 3;
    var MAX_STRIKERS     = 3;
    var MIN_STRIKERS     = 1;
    var MAX_BUDGET       = 75;
    
    function Player (playerId, points, name, group, availability) {
    	this.playerId = playerId;
      this.points   = points;
      this.name     = name;
      this.group    = group;
      this.availability = availability;
    }; 
    
    var players = [];
      
    /******** Load jQuery if not present *********/
    if (window.jQuery === undefined) {
        console.log('jQuery is required');
    } else {
        // The jQuery version on the window is the one we want to use
        jQuery = window.jQuery;
        main();
    }

   //=====================================
    function getUrlPrefix() {
        var splitUrl = window.location.href.split('/');
        splitUrl = splitUrl.slice(0, 4);
        var urlPrefix = '/' + splitUrl[3];
        return urlPrefix;
    }

    //=====================================
    function changeFormation(formation) {
     
        if (!formation) {
            return;
        }
        
        var gk  = Number(formation.charAt(1));
        var def = Number(formation.charAt(2));
        var mid = Number(formation.charAt(3));
        var str = Number(formation.charAt(4));

        if (gk + def + mid + str != 11) {
            return;
        }
               
        // build the arrays based on the player inputs
        var GK  = [];
        var DEF = [];
        var MID = [];
        var STR = []; 
        for (var i = 1; i <= 15; i++) { 
           var group    = $('#p' + i).attr('data-group');
           var playerId = $('#p' + i).attr('value');
           if (group === 'GK' && playerId && playerId != '') {
           	 GK.push(playerId);
           } else if (group === 'DEF' && playerId && playerId != '') {
           	 DEF.push(playerId);
           } else if (group === 'MID' && playerId && playerId != '') {
           	 MID.push(playerId);
           } else if (group === 'STR' && playerId && playerId != '') {
           	 STR.push(playerId);
           }
        }
        var captain = getCaptain();
               
        //console.log(GK);
        //console.log(DEF);
        //console.log(MID); 
        //console.log(STR); 
        
        for (var i =1; i <=15; i++) {
    	    removePlayer('',i);
      	} 
                 
        for (var i = 0; i < MAX_GOALKEEPERS; i++) {
        	 var playerId = GK && typeof(GK[i]) !=='undefined' ? GK[i] : '';         	       	
           if (i < gk) {                      	
             $('#p' + (i + 1).toString()).attr('value', playerId);  
             $('#p' + (i + 1).toString()).attr('data-group', 'GK');   
           } else {
             $('#p' + (11 + (MAX_GOALKEEPERS - i)).toString()).attr('value', playerId); 
             $('#p' + (11 + (MAX_GOALKEEPERS - i)).toString()).attr('data-group', 'GK');    
           } 
        }
        
        for (var i = 0; i < MAX_DEFENDERS; i++) {
       	  var playerId = DEF && typeof(DEF[i]) !=='undefined'  ? DEF[i] : '';   
          if (i < def) {
             $('#p' + (gk + (i + 1)).toString()).attr('value', playerId.toString()); 
          	 $('#p' + (gk + (i + 1)).toString()).attr('data-group', 'DEF');            
          } else {   
             $('#p' + (11 + (MAX_GOALKEEPERS - gk) + (MAX_DEFENDERS - i)).toString()).attr('value', playerId);
             $('#p' + (11 + (MAX_GOALKEEPERS - gk) + (MAX_DEFENDERS - i)).toString()).attr('data-group', 'DEF');    
          } 
        }
          
        for (var i = 0; i < MAX_MIDFIELDERS; i++) {
         	var playerId = MID && typeof(MID[i]) !=='undefined' ? MID[i] : '';        	    
          if (i < mid) { 
          	 $('#p' + (gk + def + (i + 1)).toString()).attr('value', playerId); 
          	 $('#p' + (gk + def + (i + 1)).toString()).attr('data-group', 'MID');                                   
          } else {  
             $('#p' + (11 + (MAX_GOALKEEPERS - gk) + (MAX_DEFENDERS - def) + (MAX_MIDFIELDERS - i)).toString()).attr('value', playerId);  
             $('#p' + (11 + (MAX_GOALKEEPERS - gk) + (MAX_DEFENDERS - def) + (MAX_MIDFIELDERS - i)).toString()).attr('data-group', 'MID');  
          }            
        }
            
        for (var i = 0; i < MAX_STRIKERS; i++) { 
        	var playerId = STR && typeof(STR[i]) !=='undefined' ? STR[i] : '';              	     
          if (i < str) {
         	  $('#p' + (gk + def + mid + (i + 1)).toString()).attr('value', playerId);
         	  $('#p' + (gk + def + mid + (i + 1)).toString()).attr('data-group', 'STR');                                    	
          } else {
            $('#p' + (11 + (MAX_GOALKEEPERS - gk) + (MAX_DEFENDERS - def) + (MAX_MIDFIELDERS - mid) + (MAX_STRIKERS - i)).toString()).attr('value', playerId);  
            $('#p' + (11 + (MAX_GOALKEEPERS - gk) + (MAX_DEFENDERS - def) + (MAX_MIDFIELDERS - mid) + (MAX_STRIKERS - i)).toString()).attr('data-group', 'STR');          	 
          }        
        }
                     
        for (var i = 1; i <= 15; i++) { 
          var playerId = $('#p' + i).attr('value');
          removePlayer(playerId);
          addPlayer(playerId); 
        }
        
        if (captain && captain.length > 0) {
        	changeCaptain(captain);
        }
                 
        $('#js-pitch-formation').attr('class', '');
        $('#js-pitch-formation').addClass(formation);
        $('.js-formation').removeClass('active');
        $('.formation-' + gk + def + mid + str).addClass('active');
        $('#f').val(gk.toString() + def.toString() + mid.toString() + str.toString());
        
    }

    //=====================================
    function reset() {
    	
    	for (var i =1; i <=15; i++) {
    	   removePlayer('',i);
    	} 
    	
      var formation = $('#js-pitch-formation').attr('class');
      changeCaptain('');
      changeFormation(formation);   
    }
    
    //=====================================
    function removePlayer (playerId, playerNumber) {
      if (!playerNumber && !playerId) {
      	return;
      }
      
      if (!playerId || playerId.length < 4) {
    		playerId = $('#p' + playerNumber.toString()).attr('value');  
      }
   
    	if (!playerNumber) {
    	  for (var i =1; i <=15; i++) {
    		  if ($('#p' + i).attr('value') === playerId) {
    		    playerNumber = i;
    		    break;
    		  }
    	  }
    	}
    	  
    	if ( playerNumber > 0) {	 
    		var position = $('#p' + playerNumber.toString()).attr('data-group'); 
    		var name = '';
    		switch(position) {
        	case 'GK':
        		name = lang && lang.GOALKEEPER ? lang.GOALKEEPER : '';
        	  break;
         	case 'DEF':
       		  name = lang && lang.DEFENDER ? lang.DEFENDER : '';
        		break;
        	case 'MID':
       		  name = lang && lang.MIDFIELDER ? lang.MIDFIELDER : '';
        	  break;
        	case 'STR':
     		    name = lang && lang.FORWARD ? lang.FORWARD : '';
        	  break;       		  
        }
    		    		
        if (playerNumber <= 11 && playerId && playerId.length > 0) {
          $("#js-capt option[value='" + playerId.toString() + "']").remove();
        }     
        $('#js-shirt-'           + playerNumber.toString()).attr('src', getUrlPrefix() + "/images/shirts/ic_shirt_default.png");
        $('#js-name-'            + playerNumber.toString()).html(name);
        $('#js-points-'          + playerNumber.toString()).html('');
        $('#p'                   + playerNumber.toString()).attr('value',''); 
      	$('#js-availability-'    + playerNumber).removeClass("pla-ava-doubtful");
       	$('#js-availability-'    + playerNumber).removeClass("pla-ava-unavailable");
      	$('#js-availability-'    + playerNumber).removeClass("pla-ava-injured");
      	$('#js-availability-'    + playerNumber).removeClass("pla-ava-suspended");
    
      }
      changeCaptain('');
    }
     
    //=====================================
    function addPlayer (playerId) {
 
      if (!playerId) {
      	return;
      }
      
      var playerNumber = 0;
      var group        = '';
      var points       = 0;
      var value        = 0
      var playerName   = '';
      var availability = '';
          
      // find the player    
      for (var i = 0; i < 15; i++) {
      	 if (players[i].playerId === playerId) {
      	 	 group        = players[i].group;
      	 	 points       = players[i].points;
      	 	 value        = players[i].value;
      	 	 playerName   = players[i].name;
      	 	 availability = players[i].availability;
      	 	 break;
      	 }
      }
      
      // find a slot to put him in
    	for (var i = 1; i <=15; i++) {  
    		if ($('#p' + i).attr('data-group').toString() === group.toString()){
    		  if (!$('#p' + i).attr('value') || $('#p' + i).attr('value') === '') { 		  	
    		    playerNumber = i;  		      
    		    break;
    		 }
    		}   		 
      }
          	   	  
    	if ( playerNumber > 0) {
    		$('#p'            + playerNumber.toString()).attr('value',playerId.toString());
    		$('#p'            + playerNumber.toString()).attr('data-group', group);
        $('#js-shirt-'    + playerNumber.toString()).attr('src', getUrlPrefix() + "/images/shirts/" + playerId + ".png");
        $('#js-shirt-'    + playerNumber.toString()).addClass('js-selected-player');
        $('#js-name-'     + playerNumber.toString()).html(playerName);
        $('#js-points-'   + playerNumber.toString()).html(points.toString());

        if (playerNumber <= 11) {
          $('#js-capt').append('<option id="js-capt-' + playerId + '" value="' + playerId + '">' + playerName + '</option>');
        }
        
       	$('#js-availability-' + playerNumber).removeClass("pla-ava-doubtful");
       	$('#js-availability-' + playerNumber).removeClass("pla-ava-unavailable");
      	$('#js-availability-' + playerNumber).removeClass("pla-ava-injured");
      	$('#js-availability-' + playerNumber).removeClass("pla-ava-suspended");
       	switch(availability) {
        	case 'I':
        		$('#js-availability-' + playerNumber).addClass("pla-ava-injured");
        	  break;
         	case 'S':
        		$('#js-availability-' + playerNumber).addClass("pla-ava-suspended");
        		break;
        	case 'D':
        		$('#js-availability-' + playerNumber).addClass("pla-ava-doubtful");
        	  break;
        	case 'U':
        		$('#js-availability-' + playerNumber).addClass("pla-ava-unavailable");
        	  break;       		  
        }        
      }          
    }
    
    //=====================================
    function swapPlayer (srcPlayerNumber, dstPlayerNumber) {
 
      if (!srcPlayerNumber || !dstPlayerNumber) {
      	return;
      }
      
      var changeFormationRequired = false;
      var formation               = $('#js-pitch-formation').attr('class');
        
      var srcPlayerGroup  = $('#p' + srcPlayerNumber).attr('data-group');  
      var dstPlayerGroup  = $('#p' + dstPlayerNumber).attr('data-group');   
      
      if (srcPlayerGroup === dstPlayerGroup) {
        // like for like replacement    
      	changeFormationRequired = false;     
      } else if (srcPlayerGroup !== 'GK' && dstPlayerGroup !== 'GK') { 
      	// we need to check if this swap is possible
      	// and if so it will require a change of formation to achieve it
      	changeFormationRequired = true;
            
      	var gk  = Number(formation.charAt(1));
        var def = Number(formation.charAt(2));
        var mid = Number(formation.charAt(3));
        var str = Number(formation.charAt(4));
                              
        var addGroup;
        var removeGroup;
        if (srcPlayerNumber > 11 && dstPlayerNumber <= 11) { 
          // replace player with sub
          addGroup    = srcPlayerGroup;
          removeGroup = dstPlayerGroup;
        } else if (srcPlayerNumber <= 11 && dstPlayerNumber > 11) {
        	// put player on the bench 
          addGroup    = dstPlayerGroup;
          removeGroup = srcPlayerGroup;     
        } else if ( srcPlayerNumber > 11 && dstPlayerNumber > 11) {
          // swapping subs on the bench
          changeFormationRequired = false;      
        } else if ( srcPlayerNumber <= 11 && dstPlayerNumber <= 11) {
        	return;
        }
           
        switch (addGroup) {
        	case 'DEF':        	 	 
        	  if (def + 1 > MAX_DEFENDERS) {
        	    return;
        	  } else {
        	   	def += 1;       	 	 
        	  }
        	  break;
          case 'MID':
        	  if (mid + 1 > MAX_MIDFIELDERS) {
        	  	return;
        	  } else {
        	 	  mid += 1;       	 	 
        	  }
        	  break;
          case 'STR':
        	  if (str + 1 > MAX_STRIKERS) {
        	  	return;
        	  } else {
        	 	  str += 1;       	 	 
        	  }
        	  break;
        }
        
        switch (removeGroup) {         
          case 'DEF':   
            def -= 1; 
            if (def <= 0) {
            	return;
            } 
            break;     	 	 
          case 'MID':   
            mid -= 1; 
            if (mid <= 0 ) {
            	return;
            }  
            break;    	 	 
          case 'STR':	   
            str -= 1;  
            if (str <= 0) {
            	return;
            }
            break; 
        }  
          
        // update to the new formation
        formation = 'f1' + def.toString() + mid.toString() + str.toString();       
      } else {
      	return;
      }        
                 
      var dstValue = $('#p' + dstPlayerNumber.toString()).attr('value');
      var srcValue = $('#p' + srcPlayerNumber.toString()).attr('value'); 
      $('#p' + srcPlayerNumber.toString()).attr('value',dstValue.toString());                	   	  
      $('#p' + dstPlayerNumber.toString()).attr('value',srcValue.toString());
        
      var dstValue = $('#p' + dstPlayerNumber.toString()).attr('data-group'); 
      var srcValue = $('#p' + srcPlayerNumber.toString()).attr('data-group'); 
      $('#p' + srcPlayerNumber.toString()).attr('data-group',dstValue.toString());                	   	  
      $('#p' + dstPlayerNumber.toString()).attr('data-group',srcValue.toString());
                
      dstValue = $('#js-shirt-' + dstPlayerNumber.toString()).attr('src');
      srcValue = $('#js-shirt-' + srcPlayerNumber.toString()).attr('src');
      $('#js-shirt-' + srcPlayerNumber.toString()).attr('src',dstValue);
      $('#js-shirt-' + dstPlayerNumber.toString()).attr('src',srcValue);
      dstValue = $('#js-name-' + dstPlayerNumber.toString()).html();
      srcValue = $('#js-name-' + srcPlayerNumber.toString()).html();
      $('#js-name-' + srcPlayerNumber.toString()).html(dstValue);
      $('#js-name-' + dstPlayerNumber.toString()).html(srcValue);
      dstValue = $('#js-points-' + dstPlayerNumber.toString()).html();
      srcValue = $('#js-points-' + srcPlayerNumber.toString()).html();
      $('#js-points-' + srcPlayerNumber.toString()).html(dstValue);
      $('#js-points-' + dstPlayerNumber.toString()).html(srcValue);
      dstValue = $('#js-availability-' + dstPlayerNumber.toString()).attr('class');
      srcValue = $('#js-availability-' + srcPlayerNumber.toString()).attr('class');
      $('#js-availability-' + srcPlayerNumber.toString()).attr('class',dstValue);
      $('#js-availability-' + dstPlayerNumber.toString()).attr('class',srcValue);
      // deal with the captain
      if (srcPlayerNumber > 11 && dstPlayerNumber <= 11) {
        // replace player with sub
        if ($('#js-captain-' + dstPlayerNumber.toString()).hasClass("pla-cpt")) {
        	changeCaptain('');
        }
      }	else if (srcPlayerNumber <= 11 && dstPlayerNumber > 11) {
        // put player on the bench
        if ($('#js-captain-' + srcPlayerNumber.toString()).hasClass("pla-cpt")) {
          changeCaptain('');
        }
      } else if ((srcPlayerNumber <= 11 && dstPlayerNumber <= 11)
      	 || (srcPlayerNumber > 12  && dstPlayerNumber <= 15)) {
      	// swap player within a group
      	if ($('#js-captain-' + dstPlayerNumber.toString()).hasClass("pla-cpt")) {
        	changeCaptain($('#p' + srcPlayerNumber).val());
        } else if ($('#js-captain-' + srcPlayerNumber.toString()).hasClass("pla-cpt")) {
        	changeCaptain($('#p' + dstPlayerNumber).val());
        }
      } 
        
      if (changeFormationRequired === true) {
        changeFormation(formation);
      } 
    }
    
          
    //=========================================== 
    function getCaptain () {
    	for (var i = 1; i <=11; i++) { 
        var captain =  $('#p' + i ).attr("data-capt");
        if (captain && captain.length > 0) {
        	return $('#p' + i ).attr("value");
        }
    	}
    	return '';
    }
    
    //=========================================== 
    function changeCaptain(captain_id) {
      // only three things should really change the captain; the page load, the auto select, the captain selector.
      $('#js-capt').val(captain_id);
     
      for (var i = 1; i <= 11; i++) { 
        var playerId = $('#p' + i).attr('value');
        if ( playerId && playerId.length > 0 && playerId == captain_id ) {
          $('#p' + i ).attr("data-capt","Y"); // redundent captain logic should operate on playerId not playerNumber
          $('#js-captain-' + i).addClass("pla-cpt");
        } else {
          $('#p' + i.toString()).removeAttr("data-capt"); // redundent captain logic should operate on playerId not playerNumber
          $('#js-captain-' + i).removeClass("pla-cpt");
        }
      }
    }
    
    //=========================================== 
    function changeTeam () {
      var queryParams = '';
           
      //var captain = getCaptain();
      //if (captain === '') {
      //	alert (lang.ERROR_SELECT_CAPTAIN);
      //	return;
      //}

      for (var i = 1; i <= 15; i++) {
        if (i == 1) {
          queryParams = queryParams + '?';
        } else {
          queryParams = queryParams + '&';
        }
        queryParams = queryParams + $('#p' + i).attr('name') + "=" + $('#p' + i).attr('value');
      }
      queryParams = queryParams + '&' + $('#js-capt').attr('name') + "=" + $('#js-capt').attr('value');
      queryParams = queryParams + '&' + $('#f').attr('name') + "=" + $('#f').attr('value')

      $.ajaxSetup({async:false, cache: false, timeout: 10000});
      $.getJSON(getUrlPrefix() + "/json/team/modify/" + queryParams, function (data) {
      }).success(function (data) {
        if (data.error) {
          alert(data.error);
        } else {
          alert(lang.TEAM_SAVED);
        }
      }).error(function (xhr, ajaxOptions, thrownError) {
        alert(lang.NETWORK_ERROR);
      }).complete(function () {
      });    	
    }
    
    //=========================================== 
    function main() {
        jQuery(document).ready(function () {


            
                           
            // change the pitch formation
            $('.js-formation').click(function (event) {
                $('.js-formation').removeClass('active');
                var formation = $(this).attr('data-formation');
                changeFormation(formation);
                $(this).addClass('active');
                event.preventDefault();
                return false;
            });

             
            // click on player icon
            $('.js-icon').click(function () {
  
            });
            
            $('#js-my-team-submit').click( function () {
            	changeTeam()
            	return false;
            });
          
            // change captain
            $('#js-capt').change( function () {
                var captain_id = $('#js-capt').val();
                changeCaptain(captain_id);
            })

            
            function initialize () {
            	 // create a snapshot of all players.
            	 // No real adds or remove takes place, just
            	 // a reshuffle of the current team positions 
               for (var i = 1; i <=15; i ++) {              	
               	  players.push(new Player(               	  
               	    $('#p'+ i).val(),
               	    $('#js-points-' + i).html(),
               	    $('#js-name-' + i).html(),
               	    $('#p' + i).attr('data-group'),
               	    $('#js-availability-' + i).attr('class')
               	  ));
               }               
               console.log(players);   
            }
    
            // initialize
            initialize();
            
                              
            $(".js-icon").draggable({
             	 helper: "clone",
             	 start: function (event, ui) {
             	 	
             	 	 var draggedId    = $(ui.helper).attr('data-player');
             	 	 var draggedGroup = $('#p' + draggedId).attr('data-group');
             	 	 
             	 	 
             	 	 if (draggedId > 11 && draggedGroup !== 'GK') {
             	 	   // drag a non GK sub
             	 	 	 for (var i = 2; i <= 11; i++) {       	 	 	  	             	 	 	
             	 	 	   $('#js-container-' + i).addClass('highlight');   	 	 	   
             	 	 	 }
              	 	 for (var i = 13; i <= 15; i++) {       	 	 	  	             	 	 	
             	 	 	   $('#js-container-' + i).addClass('highlight');   	 	 	   
             	 	 	 }
             	 	 } else if (draggedId > 11 && draggedGroup === 'GK') { 
             	 	   // drag a GK sub
             	 	 	 $('#js-container-1').addClass('highlight');   	
             	 	 } else if (draggedId <= 11 && draggedGroup !== 'GK') { 
             	 	   // drag a non GK player
             	 	 	 for (var i = 1; i <= 11; i++) {
             	 	 	   if ($('#p' + i).attr('data-group') === draggedGroup) { 
             	 	 	     $('#js-container-' + i).addClass('highlight');             	 	 	   	
             	 	 	   } 	            	 	 	 	
             	 	 	 }
             	 	 	 
             	 	 	 var formation = $('#js-pitch-formation').attr('class');             	 	 	 
                   var def       = Number(formation.charAt(2));
                   var mid       = Number(formation.charAt(3));
                   var str       = Number(formation.charAt(4));
                   
                   if (draggedGroup === 'DEF') {
                   	 if ((def - 1) < MIN_DEFENDERS) {
                   	 	 // we can only substitute a defender
                   	 	 for (var i = 12; i <= 15; i++) {              	 	 	 	
            	 	 	 	     if ($('#p' + i).attr('data-group') === 'DEF') {  	 	 	  	             	 	 	
             	 	 	         $('#js-container-' + i).addClass('highlight');  
             	 	 	       }
             	 	 	     } 	 	 	   
             	 	 	   } else {
             	 	 	   	 for (var i = 12; i <= 15; i++) {              	 	 	 	
            	 	 	 	     if ($('#p' + i).attr('data-group') !== 'GK') {  	 	 	  	             	 	 	
             	 	 	         $('#js-container-' + i).addClass('highlight');  
             	 	 	       }
             	 	 	     } 	 	 	   
             	 	 	   } 
                   } else if (draggedGroup === 'MID') {
                 	   if ((mid - 1) < MIN_MIDFIELDERS) {
                   	   // we can only substitute a midfielder
                   	   for (var i = 12; i <= 15; i++) {              	 	 	 	
            	 	 	 	     if ($('#p' + i).attr('data-group') === 'MID') {  	 	 	  	             	 	 	
             	 	 	         $('#js-container-' + i).addClass('highlight');  
             	 	 	       }
             	 	 	     } 	 	 	   
             	 	 	   } else {
             	 	 	   	 for (var i = 12; i <= 15; i++) {              	 	 	 	
            	 	 	 	     if ($('#p' + i).attr('data-group') !== 'GK') {  	 	 	  	             	 	 	
             	 	 	         $('#js-container-' + i).addClass('highlight');               	 	 	         
             	 	 	       }
             	 	 	     } 	 	 	   
             	 	 	   }                   	
                   }	else if (draggedGroup === 'STR') {
                 	   if ((str - 1) < MIN_STRIKERS) {
                   	   // we can only substitute a striker
                   	   for (var i = 12; i <= 15; i++) {              	 	 	 	
            	 	 	 	     if ($('#p' + i).attr('data-group') === 'STR') {  	 	 	  	             	 	 	
             	 	 	         $('#js-container-' + i).addClass('highlight'); 
             	 	 	       } 
             	 	 	     } 	 	 	   
             	 	 	   } else {
             	 	 	   	 for (var i = 12; i <= 15; i++) {              	 	 	 	
            	 	 	 	     if ($('#p' + i).attr('data-group') !== 'GK') {  	 	 	  	             	 	 	
             	 	 	         $('#js-container-' + i).addClass('highlight');  
             	 	 	       }
             	 	 	     } 	 	 	   
             	 	 	   }       
                   }     	 	 	 
             	 	 } else if (draggedId < 11 && draggedGroup === 'GK') {
             	 	 	 // drag a GK player
           	 	 	   for (var i = 12; i <= 15; i++) {   
            	 	 	 	 if ($('#p' + i).attr('data-group') === 'GK') {  	 	 	  	             	 	 	
             	 	 	      $('#js-container-' + i).addClass('highlight');  
             	 	 	   } 	 	 	   
             	 	 	 }                 	 	 	
             	 	 }             	 	 
             	 },
             	 stop : function (event, ui) {       
             	 	 for (var i = 1; i <= 15; i++) {       	 	 	  	             	 	 	
             	 	   $('#js-container-' + i).removeClass('highlight');   	 	 	   
             	 	 } 
             	 }            	 	             	      	
            });
            
            $(".js-icon").droppable({            	           	
               drop: function (event, ui) {                	
                 var draggedId    = $(ui.draggable).attr('data-player');               
                 var droppedId    = $(this).attr('data-player');
                 swapPlayer(draggedId,droppedId);
               }
            });
        })
    }
})();