
//uefa SSO message handler after user logs in
//this function is called after the user logs in with the new Uefa SSO
var debug        = true;
var xusername     = '';
var xpassword     = '';

//===================================================================
function setEnvironment () {
	 console.log(environment + "!!!!");
	 if (!environment) {
	 	 // should already have been set in the head template
	 	 environment = 'PRD';
	 }
	 
	 if (environment === 'DEV' || environment === 'TST' ) {
	 	 currentDomain = 'endava-mpw.uefa.com';

	   REMOTE = location.protocol + "//" + LANGUAGE_PREFIX + currentDomain;

	 }
}


//===================================================================
// successfull login
function uefa_login_success(xsuccess) {
	showAlert("uefa_login_success");
  // at this point uefa_login cookie has been created by UEFA
  $.ajaxSetup({cache: false, timeout: 10000});
  $.getJSON("/appgame/json/login/success/", function(data){
  }).success(function(data) {
  	
    if (data && data.redirect) {    	
    	window.location.href = data.redirect;
    } else if (data.error) {  	
      showAlert(data.error);
    	$('#logoutHL').trigger('click');  
    }
  }).error(function (xhr, ajaxOptions, thrownError) {
  	console.log(thrownError);
    alert(lang.NETWORK_ERROR);             
  });
}

//===================================================================
// login error
function uefa_login_error(xerror) {
  // this gets called in case credentials are invalid or if the call failed
//  showAlert("uefa_login_error");
  
	var queryParameters;
	var status=-1;
	var error =-1;
	
	if (xerror) {	
		console.log(xerror);
		if (xerror.loginStatus != null) {			
		  status = xerror.loginStatus;
		}
		
		if (xerror.data) {
		  if (xerror.data.errorCode) {
			  error = xerror.data.errorCode;
			}
		}
  }
  
  queryParameters = "?status=" + status.toString() + "&error=" + error.toString();
  // development only
  if (environment = 'DEV') {
  	queryParameters = queryParameters + '&username=' + xusername + '&password=' + xpassword;
  	console.log(queryParameters + "!!!!");
  }

  if (environment = 'TST') {
  	queryParameters = queryParameters + '&username=' + xusername + '&password=' + xpassword;
  	console.log(queryParameters + "!!!!");
  }
  		
  $.ajaxSetup({cache: false, timeout: 10000});  
  $.getJSON("/appgame/json/login/error/" + queryParameters, function(data){
      
  }).success(function(data) { 	
  	if (data) {
      if (data.redirect) {
    	  window.location.href = data.redirect;
      } else if (data.error) { 
      	$.removeCookie('uefa_login', { path: '/' });
  	    $.removeCookie('UEFASess', { path: '/' }); 	
    	  showAlert(data.error);
    	  location.reload();
      }
    }    
  }).error(function (xhr, ajaxOptions, thrownError) {
                
  });
	return true;
}

//===================================================================
function showAlert (message) {	
	if (debug) {
		alert(message);
	}	
}

//===================================================================
jQuery(document).ready(function($) { 
	
  // initialize the environment
	setEnvironment();
		
	
	
  // logout
  $('#logoutHL').click(function(event){

    // create a return URL which UEFA will call
    // on logout which should always be the home page
  	var homeUrl = window.location.href.split('/'),
  	oldPath = $(this).attr('href'),
  	fullPath;
  	
  	homeUrl = homeUrl.slice(0,4); 	
  	homeUrl = homeUrl.join('/');
   	  	
  	fullPath = oldPath.split('?returnurl=')[0] + '?returnurl=' + homeUrl; 	
  	$(this).attr('href', fullPath);
  	
  	$.removeCookie('uefa_login', { path: '/' });
  	$.removeCookie('UEFASess', { path: '/' });  	
  
    event.preventDefault();
  	window.location.href = fullPath;  	
  });
  
  
  // login
  $('#Email').blur(function(event){
  	xusername = $('#Email').val(); 
  	xpassword = $('#Password').val(); 
  	console.log(xpassword + "!!!!" + xusername + "!!!!");
 	
  });	
  
   $('#Password').blur(function(event){
  	xusername = $('#Email').val(); 
  	xpassword = $('#Password').val();  	
  	console.log(xpassword + "!!!!" + xusername + "!!!!");
  });	
  
});