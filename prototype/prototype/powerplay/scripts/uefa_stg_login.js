var script = document.createElement("script");
script.src = "//cdn.optimizely.com/js/2681140464.js";
document.getElementsByTagName("head")[0].appendChild(script);
var css_browser_selector = function() {

    var C = this;
    var B = navigator.userAgent.toLowerCase();
    var E = function(H) {
        return B.indexOf(H) != -1
    };
    var D = document.getElementsByTagName("html")[0];
    var A = (B.indexOf("opera/9") > -1) ? "opera opera9" : (B.indexOf("opera 8") > -1) ? "opera opera8" : (B.indexOf("opera 7") > -1) ? "opera opera7" : (B.indexOf("msie 10") > -1) ? "ie ie10" : (B.indexOf("msie 9") > -1) ? "ie ie9" : (B.indexOf("msie 8") > -1) ? "ie ie8" : (B.indexOf("msie 7") > -1) ? "ie ie7" : (B.indexOf("msie 6") > -1) ? "ie ie6" : (B.indexOf("msie 5.5") > -1) ? "ie ie55" : (B.indexOf("msie 5.23") > -1) ? "ie ie523" : (B.indexOf("msie 5.0") > -1) ? "ie ie5" : (B.indexOf("chrome") > -1) ? "safari chrome" : (B.indexOf("safari") > -1 && B.indexOf("version/4.") > -1 && B.indexOf("version/4.") < B.indexOf("safari")) ? "safari safari4 safari3" : (B.indexOf("safari") > -1 && B.indexOf("version/3.") > -1 && B.indexOf("version/3.") < B.indexOf("safari")) ? "safari safari3" : (B.indexOf("safari") > -1) ? "safari safari2" : (B.indexOf("seamonkey") > -1) ? "seamonkey gecko" : (B.indexOf("netscape") > -1) ? "netscape gecko" : (B.indexOf("firefox/1.") > -1) ? "firefox ff1 gecko" : (B.indexOf("firefox/2.") > -1) ? "firefox ff2 gecko" : (B.indexOf("firefox/3.") > -1) ? "firefox ff3 gecko" : (B.indexOf("firefox/4.") > -1) ? "firefox ff4 gecko" : (B.indexOf("firefox/5.") > -1) ? "firefox ff5 gecko" : (B.indexOf("firefox/6.") > -1) ? "firefox ff6 gecko" : (B.indexOf("firefox/7.") > -1) ? "firefox ff7 gecko" : (B.indexOf("firefox/8.") > -1) ? "firefox ff8 gecko" : (B.indexOf("firefox/9.") > -1) ? "firefox ff9 gecko" : (B.indexOf("firefox/10.") > -1) ? "firefox ff10 gecko" : (B.indexOf("firefox/11.") > -1) ? "firefox ff11 gecko" : (B.indexOf("firefox/12.") > -1) ? "firefox ff12 gecko" : (B.indexOf("gecko") > -1) ? "gecko" : "",
        F = (E("x11") || E("linux")) ? " linux" : E("mac") ? " mac" : E("win") ? " win" : "";
    var G = A + F + " js";
    D.className += D.className ? " " + G : G;
    C.ver = navigator.appVersion;
    C.agent = navigator.userAgent;
    C.mac = B.indexOf("mac") > -1;
    C.pc = B.indexOf("win") > -1;
    C.opera = B.indexOf("opera") > -1;
    C.opera9 = B.indexOf("opera/9") > -1;
    C.opera8 = B.indexOf("opera 8") > -1;
    C.opera7 = B.indexOf("opera 7") > -1;
    C.ie10 = (B.indexOf("msie 10") > -1 && !C.opera) ? 1 : 0;
    C.ie9 = (B.indexOf("msie 9") > -1 && !C.opera) ? 1 : 0;
    C.ie7 = (B.indexOf("msie 7") > -1 && !C.opera) ? 1 : 0;
    C.ie7 = C.ie8 = (B.indexOf("msie 8") > -1 && !C.opera) ? 1 : 0;
    C.ie6 = (B.indexOf("msie 6") > -1 && !C.opera) ? 1 : 0;
    C.ie55 = (B.indexOf("msie 5.5") > -1 && !C.opera) ? 1 : 0;
    C.ie523 = (B.indexOf("msie 5.23") > -1) ? 1 : 0;
    C.ie4 = (B.indexOf("msie 4") > -1) ? 1 : 0;
    C.ie3 = (B.indexOf("msie 3") > -1) ? 1 : 0;
    C.ie5 = (B.indexOf("msie 5.0") > -1 && !C.ie55 && !C.ie523) ? 1 : 0;
    C.ns71 = (B.indexOf("netscape/7.1") > -1) ? 1 : 0;
    C.safari4 = (B.indexOf("safari") > -1 && B.indexOf("version/4.") > -1 && B.indexOf("version/4.") < B.indexOf("safari")) ? 1 : 0;
    C.safari3 = (B.indexOf("safari") > -1 && B.indexOf("version/3.") > -1 && B.indexOf("version/3.") < B.indexOf("safari")) ? 1 : 0;
    C.safari2 = (B.indexOf("safari") > -1 && B.indexOf("version/") == -1) ? 1 : 0;
    C.chrome = (B.indexOf("chrome") > -1) ? 1 : 0;
    C.safari = (C.safari2 || C.safari3 || C.safari4);
    C.ie = (C.ie9 || C.ie8 || C.ie7 || C.ie55 || C.ie5 || C.ie6 || C.ie523 || C.ie4 || C.ie3);
    C.gecko = (B.indexOf("gecko") > -1 & !C.ie) ? 1 : 0;
    C.ns = (C.ns71);
    C.ieOld = (C.ie55 || C.ie5 || C.ie523 || C.ie4 || C.ie3);
    C.ajaxaware = (C.opera || C.ie8 || C.ie7 || C.ie6 || C.safari || C.gecko || C.chrome) ? 1 : 0
}(); /* * jQuery Cookie Plugin v1.4.0 * https://github.com/carhartl/jquery-cookie * * Copyright 2013 Klaus Hartl * Released under the MIT license */
(function(A) {
// Here
    if (typeof define === "function" && define.amd) {
        define(["jquery"], A)
    } else {
        A(jQuery)
    }
}(function(A) {
// Here
    var F = /\+/g;
    function D(I) {
// Not Here
        return B.raw ? I : encodeURIComponent(I)
    }

    function C(I) {
// not here
        return B.raw ? I : decodeURIComponent(I)
    }

    function H(I) {
// not here
        return D(B.json ? JSON.stringify(I) : String(I))
    }

    function E(J) {
// not here
        if (J.indexOf('"') === 0) {
            J = J.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, "\\")
        }
        try {
            J = decodeURIComponent(J.replace(F, " "));
            return B.json ? JSON.parse(J) : J
        } catch (I) {}
    }

    function G(J, I) {
// not here
        var K = B.raw ? J : E(J);
        return A.isFunction(I) ? I(K) : K
    }
    var B = A.cookie = function(M, T, P) {
// not here
        if (T !== undefined && !A.isFunction(T)) {
            P = A.extend({}, B.defaults, P);
            if (typeof P.expires === "number") {
                var K = P.expires,
                    S = P.expires = new Date();
                S.setDate(S.getDate() + K)
            }
            return (document.cookie = [D(M), "=", H(T), P.expires ? "; expires=" + P.expires.toUTCString() : "", P.path ? "; path=" + P.path : "", P.domain ? "; domain=" + P.domain : "", P.secure ? "; secure" : ""].join(""))
        }
        var R = M ? undefined : {};
        var J = document.cookie ? document.cookie.split("; ") : [];
        for (var L = 0, N = J.length; L < N; L++) {
            var Q = J[L].split("=");
            var O = C(Q.shift());
            var I = Q.join("=");
            if (M && M === O) {
                R = G(I, T);
                break
            }
            if (!M && (I = G(I)) !== undefined) {
                R[O] = I
            }
        }
        return R
    };
    B.defaults = {};
    A.removeCookie = function(I, J) {
// not here
        if (A.cookie(I) === undefined) {
            return false
        }
        A.cookie(I, "", A.extend({}, J, {
            expires: -1
        }));
        return !A.cookie(I)
    }
}));
var Cookie = {
    set: function(E, I, C, F, B, G) {
        var H = new Date();
// not here
        H.setTime(H.getTime());
        if (C) {
            C = C * 1000 * 60 * 60 * 24
        }
        var D = new Date(H.getTime() + (C));
        var A = (B !== undefined && B != null && B != "") ? ";domain=" + B : ((cookieDomain !== undefined && cookieDomain != null && cookieDomain != "") ? ";domain=" + cookieDomain : "");
        document.cookie = E + "=" + encodeURIComponent(I) + ((C) ? ";expires=" + D.toGMTString() : "") + ((F) ? ";path=" + F : ";path=/") + A + ((G) ? ";secure" : "")
    },
    get: function(C, B) {
// Here
        if (typeof B == "undefined") {
            B = true
        }
        var A = document.cookie.match(new RegExp("(^|;)\\s*" + encodeURIComponent(C) + "=([^;\\s]*)"));
        return (A ? (B ? decodeURIComponent(A[2]) : A[2]) : null)
    },
    erase: function(A) {
// Not Here
        Cookie.set(A, "", -1)
    },
    accept: function() {
// not here
        if (typeof navigator.cookieEnabled == "boolean") {
            return navigator.cookieEnabled
        }
        Cookie.set("_test", "1");
        return (Cookie.erase("_test") === "1")
    }
};
String.prototype.cookieStrDecode = function(F) {
// not here
    if (!F) {
        F = "&"
    }
    var B = {};
    var C = this;
    var D = C.split(F);
    for (var E = 0; E < D.length; E++) {
        var A = D[E].split("=");
        B[A[0]] = decodeURIComponent(A[1].toString())
    }
    return B
};
var LANGUAGE_PREFIX = "";
if (location.host.substr(2, 1) == ".") {
// Not Here
    LANGUAGE_PREFIX = location.host.substr(0, 3)
}
var cookieDomain = ".uefa.com";

//var thisDomain = LANGUAGE_PREFIX + "user.uefa.com";
var thisDomain = LANGUAGE_PREFIX + "endava-mpw.uefa.com";
// Here
var xhr = null;
var lazyLoader = (function() {
// here
    var B = function(D) {
// Not here
        var C = jQuery.extend({}, A, D);
        if (C.isInitialized()) {
            C.callback()
        } else {
            C.initialize(C.callback)
        }
    };
    var A = {
        isInitialized: null,
        initialize: null,
        callback: null
    };
    return {
        load: B
    }
}());
var easyXdmBootstrapper = (function() {
// Here
    var D = function(G, F) {
        options = {
            dataType: "script",
            cache: true,
            url: G,
            success: F
        };
// Not here
        return jQuery.ajax(options)
    };
    var E = function(F, G) {
        return !!(typeof(F[G]) == "object" && F[G])
    };
    var B = function(F) {
        lazyLoader.load({
            isInitialized: function() {
                return E(window, "JSON")
            },
            initialize: function(G) {
                D(REMOTE + "/easyxdm/json2.js", function() {
                    G()
                })
            },
            callback: F
        })
// Not here
    };
    var A = function(F) {
// Not Here
        B(function() {
            lazyLoader.load({
                isInitialized: function() {
                    if (typeof easyXDM === "undefined") {
                        return false
                    }
                    return true
                },
                initialize: function(G) {
                    D(REMOTE + "/easyxdm/easyxdm.js", function() {
                        G()
                    })
                },
                callback: F
            })
        })
    };
    var C = function(F) {
// Not here
        lazyLoader.load({
            isInitialized: function() {
                if (xhr) {
                    return true
                }
                return false
            },
            initialize: function(G) {
                xhr = new easyXDM.Rpc({
                    method: "POST",
                    local: "../easyxdm/name.html",
                    swf: REMOTE + "/easyxdm/easyxdm.swf",
                    remote: REMOTE + "/easyxdm/cors/",
                    remoteHelper: REMOTE + "/easyxdm/name.html"
                }, {
                    remote: {
                        request: {}
                    }
                });
                G()
            },
            callback: F
        })
    };
// Here
    return {
        afterXhrIsInitialized: C,
        afterEasyXdmIsLoaded: A
    }
}());

function User() {
// Here
    this.cookieName = "uefa_login";
    this.cookieNameUclf = "uclf";
    this.ckLogOrigin = "uefa__ckLogOrigin";
    this.ckRegOrigin = "uefa__ckRegOrigin";
    this.cookieDomain = ".uefa.com";
    this.Data = null;
    this.isLogged = function() {
// Here
        var B = Cookie.get(this.cookieName);
        if (B === undefined || B == null || B == "") {
            return false
        }
        return true
    };
    this.loadData = function() {
// Here
        if (this.isLogged()) {
            this.Data = Cookie.get(this.cookieName, true).cookieStrDecode()
        }
    };
    this.logout = function() {
        history.go(0)
    };
    this.logoutNoRedirect = function() {
// Not Here
        Cookie.set(this.cookieName, "", -1, "/", this.cookieDomain);
        var B = Cookie.get(this.cookieNameUclf);
        if ((B === undefined) || (B == null) || (B == "null")) {
            Cookie.set(this.cookieNameUclf, "", -1, "/", ".uclfantasy" + this.cookieDomain);
            Cookie.erase(this.cookieNameUclf)
        }
    };
    this.loadData()
}

function headerScript() {
// Here
    this.MAX_MAIL_LEN = 30;
    this.currentUser = null;
    this.setUserHeader = function() {
// Not here
        var R = this.currentUser;
        var N = (typeof R != "undefined") && R.isLogged();

        if (N) {
            document.getElementById("loggedH").style.display = "block";
            document.getElementById("registerH").style.display = "none";
            document.getElementById("loginH").style.display = "none";
            var L = R.Data.UserEMail;
            var O = jQuery(".loggedUserSpan");
            var Q = R.Data.ScreenName;

            if (Q === undefined) {
                Q = "Profile"
            } else {
                if (Q.length > 24) {
                    Q = Q.substring(0, 24) + "..."
                }
            }
            var M = location.hostname.split(".")[0];
            var P = thisDomain;
            L = this.trimString(L, this.MAX_MAIL_LEN, "..");
            var J = L.indexOf("@", 0);
            L = L.substring(0, J);
            jQuery(O).attr("href", "http://" + P + "/profile").attr("title", Q).attr("class", "hdr-lnk").text(Q).append("<span class='hdr-arrow hdr-arrow-white'>&nbsp;</span>");
            var K = document.getElementById("logoutHL");
            K.setAttribute("href", "http://" + thisDomain + "/account/logoff?returnurl=" + window.location.href);
            var V = document.getElementById("loggedAccount");
            V.setAttribute("href", "http://" + thisDomain + "/Profile/AccountSettings");
            var U = document.getElementById("loggedPref");
            U.setAttribute("href", "http://" + thisDomain + "/Profile/PreferencesSettings")
        } else {
            document.getElementById("loggedH").style.display = "none";
            document.getElementById("registerH").style.display = "block";
            document.getElementById("loginH").style.display = "block"
        }
    };
    
    this.myWriteConsole = function(B) {
        if (typeof window.console != "undefined") {
            window.console.log(B)
        }
// Not Here
    };
    this.trimString = function(G, E, H) {
// Not Here
        G = "" + G;
        var F = E - H.length;
        if (G.length > F) {
            G = G.substring(0, F) + H
        }
        return G
    };
    this.logout = function() {
// Not Here
        if (typeof this.currentUser != "undefined" && this.currentUser.isLogged()) {
            this.currentUser.logout();
            this.setUserHeader()
        }
    };
    this.myInit = function() {
// Here
        this.currentUser = new User();
        if (isdefined("loggedH") && isdefined("registerH") && isdefined("loginH")) {
            this.setUserHeader()
        }
    };

    this.myInit()
}

function initLogin() {
// Here
    if (typeof myHeader == "undefined") {
        myHeader = new headerScript()
    }
}
jQuery(document).ready(function() {
// Here
    jQuery("#loginPopup input").live("keydown", function(A) {
// Not Here
        if (A.keyCode == 13) {
            if (jQuery("#Email").val() != "" && jQuery("#Password").val() != "") {
                jQuery("#loginPopup .button").click()
            }
            return false
        }
    })
});

function loginSubmit() {
    var B = 0;
// Here
    if (!validateField("Email", "requiredEmail")) {
        B++
    }
    if (!validateField("Password", "required")) {
        B++
    }

    if (B > 0) {
        jQuery(".sso #error_title").slideDown();
        return false
    } else {
        jQuery(".sso #error_title").slideUp()
    }
    var C = jQuery("form#loginPopup input#chkRem").attr("checked");

    doLogin(jQuery("input#Email").val().replace(/\s/g, ""), jQuery("input#Password").val().replace(/\s/g, ""), (C == "checked" || C == true) ? true : false, getReturnUrl(window.location.href));
    return true
}

function uefa_login_error(B) {
// Here
    if (window.console && console.log) {
        console.log("xerror\ndata : " + B.data + "\nstatus:" + B.status + "\nmessage:" + B.message)
    }
}

function uefa_login_success(B) {
// Not Here
    window.location.reload()
}

function validateField(F, G) {
// Here -- how do we check that the correct value is being eval'd

    var E = jQuery("#" + F);
    switch (G) {
        case "required":
            if (E.val() != "") {
                E.parent().removeClass("error");
                return true
            } else {
                E.parent().addClass("error");
                return false
            }
            break;
        case "requiredEmail":
            var H = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

            if (H.test(E.val().replace(/\s/g, "")) == false) {
                E.parent().addClass("error");
                return false
            } else {
                E.parent().removeClass("error");
                return true
            }
            break;
        default:
            console.log("No type matched");
            break
    }
    return false
}
jQuery(document).ready(function() {
// Here
    var C = jQuery("#loginHL");
    var D = jQuery("#login2");
    C.click(function() {
// Not Here
        D.children(".login_mask").html("<span>" + C.html() + "</span>");
        var A = jQuery("form#loginPopup");
        A.get(0).reset();
        A.find(".field-validation-error").removeClass("field-validation-error").addClass("field-validation-valid");
        A.find(".input-validation-error").removeClass("input-validation-error").addClass("input-validation-valid");
        A.find(".validation-summary-errors").removeClass("validation-summary-errors").addClass("validation-summary-valid");
        A.find(".error").removeClass("error");
        D.show()
    });
    jQuery("#loginH").click(function(A) {
// Not Here
        A.stopPropagation()
    });
    jQuery("body").click(function() {
// Here
        D.hide()
    });
    jQuery(document).keyup(function(A) {
// Not Here
        if (A.keyCode == 27) {
            D.hide();
            jQuery(".sso_overlay, .sso_overlay_mask").fadeOut()
        }
    });
    initLogin()
});
if (User && User.prototype) {
// Here
    User.prototype.fillOmnitureProperties = function() {
// Not Here
        return;
        if (this.isLogged()) {
            s.prop25 = "Logged in";
            s.prop24 = this.Data.UserID;
            s.prop27 = this.Data.BirthDateStr.substr(this.Data.BirthDateStr.length - 4, 4);
            s.prop28 = this.Data.Gender;
            s.prop29 = this.Data.Lang;
            if (this.Data.Avatar) {
                s.prop31 = "Y"
            } else {
                s.prop31 = "N"
            }
            s.prop32 = this.Data.CountryCode;
            s.prop43 = "web";
            if ((this.Data.NewsLetter) && (this.Data.NewsLetter > 0)) {
                s.prop33 = "yes"
            } else {
                s.prop33 = "no"
            } if (this.Data.RefService) {
                s.prop26 = this.Data.RefService
            }
            if (this.Data.FavTeamsNames) {
                s.prop30 = this.Data.FavTeamsNames
            }
            if (this.Data.FavClubNames) {
                s.prop46 = this.Data.FavClubNames
            }
        } else {
            s.prop25 = "Not logged in"
        }
        var A = Cookie.get(this.ckLogOrigin);
        if (A) {
            s.eVar36 = A;
            if (s.events) {
                s.events += ","
            } else {
                s.events = ""
            }
            Cookie.erase(this.ckLogOrigin)
        }
    }
}(function(A, E, D) {
// Here
    E.Deltatre = E.Deltatre || {};
    var C = E.Deltatre;
    C.IS_DEBUG = (/isdebug/i).test(document.location.href);
    C.Trace = function() {
// Not Here
        if (window.console) {
            if (C.IS_DEBUG === true) {
                if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
                    console.log.apply(console, arguments)
                } else {
                    console.log(Array.prototype.slice.call(arguments))
                }
            }
        }
    };
    C.enableLog = function() {
// Not Here
        var F = new Date();
        F.setDate(F.getDate() + 1);
        document.cookie = "isdebug=1; expires=" + F.toUTCString();
        C.IS_DEBUG = true
    };
    var B = function(G, H, I) {
// Not Here
        var F = E.document.createElement("link");
        F.type = "text/css";
        F.rel = "stylesheet";
        F.media = I || "screen";
        F.href = G;
        if (H) {
            F.id = H
        }
        return F
    };
    C.util = {
        getPaddedNewsId: function(I) {
// Not Here
            I = I.toString();
            if (I.length % 2 !== 0) {
                I = "0" + I
            }
            var F = I.length,
                G = 0,
                H;
            var J = "";
            for (H = 0; H < F - 1; H = H + 2) {
                J += "n" + (++G) + "=" + I.substr(H, 2) + "/"
            }
            return J.substr(0, J.length - 1)
        },
        getUrlParameters: function() {
// Not Here
            var G = document.location.search;
            if (!G) {
                return {}
            }
            var F = G.substr(1);
            return C.util.splitQueryString(F)
        },
        getHashParameters: function() {
// Not Here
            var F = document.location.hash;
            if (!F) {
                return {}
            }
            var G = F.substr(1);
            return C.util.splitQueryString(G)
        },
        getQueryString: function(G) {
// Not Here
            var H = "";
            var I;
            var F;
            for (F in G) {
                if (G.hasOwnProperty(F)) {
                    I = G[F];
                    if (!(F === D || I === null)) {
                        H += "&" + F + "=" + encodeURIComponent(I)
                    }
                }
            }
            if (H.length > 0) {
                H = H.substr(1)
            }
            return H
        },
        splitQueryString: function(H) {
// Not Here
            var G = H.split("&");
            var I = {};
            var F = 0,
                J;
            for (F = 0; F < G.length; F++) {
                J = G[F].toString().split("=");
                I[J[0]] = decodeURIComponent(J[1])
            }
            return I
        },
        prependEventHandler: function(F, G, H) {
// Not Here
            var I = this;
            F.each(function(N, L) {
                var J = A(L);
                var K = I.replaceEventHandler(J, G, H);
                var O;
                var M;
                var P = K.length;
                for (O = 0; O < P; O++) {
                    M = K[O];
                    J.bind(G, M)
                }
            })
        },
        removeEventHandler: function(F, H, J) {
// Not Here
            var G = [];
            var L;
            var K;
            var N = F.data("events");
            if (N === D) {
                return G
            }
            var I = N[H];
            if (I === D) {
                return G
            }
            var M = I.length;
            for (L = 0; L < M; L++) {
                K = A.extend({}, I[L]);
                G.push(K);
                F.unbind(H)
            }
            return G
        },
        replaceEventHandler: function(F, H, I) {
// Not Here
            var G = C.util.removeEventHandler(F, H, I);
            F.bind(H, I);
            return G
        },
        appendScript: function(J, H, I) {
// Not Here
            var F = E.document;
            if (I && F.getElementById(I)) {
                return H ? H() : false
            }
            var G = F.createElement("script");
            G.type = "text/javascript";
            G.async = true;
            G.src = J;
            if (I) {
                G.id = I
            }
            if (H) {
                if (G.addEventListener) {
                    G.addEventListener("load", H, false)
                } else {
                    if (G.readyState) {
                        G.onreadystatechange = function() {
                            if (G.readyState === "loaded" || G.readyState === "complete") {
                                H()
                            }
                        }
                    }
                }
            }
            F.body.appendChild(G)
        },
        appendStyle: function(G, H, I) {
// Not Here
            if (H && E.document.getElementById(H)) {
                return
            }
            var F = B(G, H, I);
            E.document.getElementsByTagName("head")[0].appendChild(F)
        },
        prependStyle: function(I, J, K) {
// Not Here
            if (J && E.document.getElementById(J)) {
                return
            }
            var F = B(I, J, K);
            var G = E.document.getElementsByTagName("head")[0];
            var H = G.getElementsByTagName("link");
            if (H && H[0]) {
                G.insertBefore(F, H[0])
            } else {
                G.appendChild(F)
            }
        }
    };
    if ((/edt-filter-on=y/i).test(document.location.href.toLowerCase())) {
// Not Here
        A(document).ready(function() {
            try {
                A.ajax({
                    url: "/livecommon/editorial/_ask.json",
                    type: "GET",
                    dataType: "json",
                    success: function(H) {
                        var G = H.root.editorial.areas;
                        var I = H.root.editorial.kinds;
                        var J = H.root.editorial.sections;
                        A(".edt-filter-caption").mouseover(function() {
                            A(this).css("z-index", 3000);
                            A(this).css("background-color", "rgba(0, 0, 0, 1)")
                        }).mouseout(function() {
                            A(this).css("z-index", 2000);
                            A(this).css("background-color", "rgba(0, 0, 0, 0.7)")
                        });
                        A("a").attr("href", function(L, K) {
                            return K + (K.indexOf("?") != -1 ? "&edt-filter-on=y" : "?edt-filter-on=y")
                        });
                        A("body").addClass("edt-filter-on");
                        A(".edt-filter-caption").each(function() {
                            try {
                                var O = A(this);
                                var K = O.find(".edt-a span");
                                var L = "ID_" + K.html();
                                var M = (L in G) ? G[L].name : "";
                                if (M != "") {
                                    K.append("<span> - " + M + "</span>")
                                }
                                var T = O.find(".edt-s span");
                                var U = "ID_" + T.html();
                                var V = (U in J) ? J[U].name : "";
                                if (V != "") {
                                    T.append("<span> - " + V + "</span>")
                                }
                                var Q = O.find(".edt-k span");
                                var R = "ID_" + Q.html();
                                var S = (R in I) ? I[R].name : "";
                                if (S != "") {
                                    Q.append("<span> - " + S + "</span>")
                                }
                                var N = A(this).parents("ul").eq(0);
                                if (N.size() > 0) {
                                    A(this).insertBefore(A(N));
                                    A(N).addClass("edt-filter-cont")
                                }
                            } catch (P) {
                                console.log("edt-filter " + P.message)
                            }
                        })
                    },
                    error: function(H, I, G) {
                        console.log("edt-filter " + G.message)
                    }
                })
            } catch (F) {
                console.log("error in edt-filter" + F)
            }
        })
    }
}(jQuery, window));
(function(A, C, B) {
// Here
    C.d3UEFACOM = C.d3UEFACOM || {};
    C.calcAge = function() {};
    C.basicPaging = function() {};
    C.addADLinkScript = function(D) {
// Not Here
        document.write('<script src="' + D + '" type="text/javascript"></script>')
    };
    C.isdefined = function(E) {
// Here
        var D = document.getElementById(E) != null;
        return D
    };
    C.LangFromHost = function() {
// Here
        var D = location.host.split(".");
        var E = new RegExp("/|www|en|it|fr|de|es|pt|ru|pl|ua|jp|kr|cn|/");
        var G = D[0] && E.test(D[0]) ? D[0] : "www";
        var F = new RegExp("/|template.infra|template.stg.irt|template.trn.irt|static.stg.infra|static.trn.infra|/");
        return F.test(location.host) ? location.host.replace(".uefa.com", "") : G
    };
    C.keepValueFromClass = function(H, F) {
// Not Here
        var D = H.split(" ");
        var E;
        var G;
        var I;
        E = 0;
        G = "";
        do {
            if (D[E] !== "") {
                I = D[E].split("_");
                if (I[0] === F) {
                    G = I[1];
                    E = D.length
                }
            }
            E += 1
        } while (E < D.length);
        return G
    };
// Here
    C.LANG = LangFromHost();
    C.SEARCH_HOSTNAME = window.location.hostname.indexOf(LANG + ".uefa.org") > -1 || window.location.hostname.indexOf(LANG + ".uefa.com") > -1 ? window.location.hostname : LANG + ".uefa.com";
    C.Deltatre = C.Deltatre || {};
    C.Deltatre.Uefacom = C.Deltatre.Uefacom || {}
}(jQuery, window));
(function(A) {
// Here
    A.fn.hoverIntent = function(G, H) {
// Not Here
        var B = {
            sensitivity: 7,
            interval: 100,
            timeout: 0
        };
        B = A.extend(B, H ? {
            over: G,
            out: H
        } : G);
        var D, E, J, K;
        var L = function(M) {
            D = M.pageX;
            E = M.pageY
        };
        var C = function(M, N) {
            N.hoverIntent_t = clearTimeout(N.hoverIntent_t);
            if ((Math.abs(J - D) + Math.abs(K - E)) < B.sensitivity) {
                A(N).unbind("mousemove", L);
                N.hoverIntent_s = 1;
                return B.over.apply(N, [M])
            } else {
                J = D;
                K = E;
                N.hoverIntent_t = setTimeout(function() {
                    C(M, N)
                }, B.interval)
            }
        };
        var F = function(M, N) {
            N.hoverIntent_t = clearTimeout(N.hoverIntent_t);
            N.hoverIntent_s = 0;
            return B.out.apply(N, [M])
        };
        var I = function(M) {
            var N = jQuery.extend({}, M);
            var O = this;
            if (O.hoverIntent_t) {
                O.hoverIntent_t = clearTimeout(O.hoverIntent_t)
            }
            if (M.type == "mouseenter") {
                J = N.pageX;
                K = N.pageY;
                A(O).bind("mousemove", L);
                if (O.hoverIntent_s != 1) {
                    O.hoverIntent_t = setTimeout(function() {
                        C(N, O)
                    }, B.interval)
                }
            } else {
                A(O).unbind("mousemove", L);
                if (O.hoverIntent_s == 1) {
                    O.hoverIntent_t = setTimeout(function() {
                        F(N, O)
                    }, B.timeout)
                }
            }
        };
        return this.bind("mouseenter", I).bind("mouseleave", I)
    }
})(jQuery);
(function(A, G) {
// Here
    G.Deltatre = G.Deltatre || {};
    G.Deltatre.plugins = G.Deltatre.plugins || {};
    var C = G.Deltatre;
    var E = C.plugins;
    var F = C.Trace;
    var D = C.Legacy ? new C.Legacy() : false;
    A.fn.d3Plugin = function(H, J) {
// Not Here
        var I = H.options || {};
        if (C.IS_DEBUG === true && Object.prototype.toString.call(I) === "[object String]") {
            F("Options parsing error - " + J + " - ", I, this)
        }
        return this.each(function() {
            var K = A(this);
            if (!E[J] && E.generic && !E.generic.prototype.plugins[J]) {
                F(J + " does not exist")
            }
            try {
                var L = K.data(J);
                if (!L) {
                    var N;
                    if (E[J]) {
                        N = new E[J](K, I)
                    } else {
                        if (E.generic && E.generic.prototype.plugins[J]) {
                            N = new E.generic(J, K, I)
                        }
                    } if (H.api || N.api) {
                        K.data(J, N)
                    } else {
                        K.data(J, true)
                    }
                    N.init()
                } else {
                    F("plugin yet launched", K, I, J)
                }
            } catch (M) {
                F(M);
                F("plugin function error", K, I, J)
            }
        })
    };
    C.core = function() {
// Here
        var H = this;
        var I, J, L, K;
        this.launchPlugin = function(M) {
// Not Here
            var N = M.data("plugin");
            if (!N) {
                F("can not find data-plugin for element");
                F(M);
                return
            }
            L(M, N)
        };
        this.launchPlugins = function(N, S) {
// Here
            var R;
            if (!S) {
                S = ".d3-plugin"
            }
            if (D && N === document && S === ".d3-plugin") {
                D.parse(A(N))
            }
            N = A(N);
            if (N.is(S)) {
                N = N.parent()
            }
            var O = N.find(S);
            var P;
            var Q = O.length;
            var M;
            for (P = 0; P < Q; P++) {
                M = O.eq(P);
                R = M.data("plugin");
                if (!R) {
                    F("can not find data-plugin for element");
                    F(M);
                    continue
                }
                if (!I(R)) {
                    if (C.LazyScript) {
                        J(M, R)
                    } else {
                        F("d3.LazyScript missing - plugin not exist:", M, R)
                    }
                } else {
                    L(M, R)
                }
            }
            K(N)
        };
        this.parse = this.launchPlugins;
        this.parentPlugin = function(M, N) {
// Not Here
            return A(M).closest('[data-plugin="' + N + '"]').data(N)
        };
        L = function(N, P) {
// Not Here
            var M, O;
            N.each(function(R, Q) {
                M = A(Q);
                O = M.data();
                M.d3Plugin(O, P)
            })
        };
        J = function(M, P) {
// Not Here
            var N = M.data("script") || (E.jsInjectionTable && E.jsInjectionTable[P]);
            if (N) {
                var O = new C.LazyScript(P, N);
                O.whenReady(function() {
                    L(M, P)
                })
            } else {
                F("can not find plugin for element", P);
                F(M)
            }
        };
        I = function(M) {
// Not Here
            return !!E[M] || (E.generic && !! E.generic.prototype.plugins[M])
        };
        K = function(M) {
// Here
            A.each(H.parsers, function(O, P) {
// Not Here
                try {
                    P(M)
                } catch (N) {
                    F("error launching parser", O);
                    F(N);
                    return true
                }
            })
        }
    };
    A.extend(C.core.prototype, {
        parsers: {}
    });
    var B = new C.core();
    C.core.getInstance = function() {
// Here
        return B
    }
}(jQuery, window));
(function(A, F, E) {
// Here
    var B = F.Deltatre;
    var D = B.plugins;
    var C = {
        selector: ".js-target, .hover",
        method: "hover"
    };
    D.toggle = function(J, K) {
// Not Here
        var I = A.extend(true, {}, C, K || {});
        var H = J;
        var G = {};
        this.init = function() {
            G.target = H.find(I.selector);
            if (G.target.length === 0) {
                G.target = H.contents()
            }
            H[I.method](function() {
                G.target.show()
            }, function() {
                G.target.hide()
            }, I)
        }
    }
}(jQuery, window));
(function(A, G, F) {
// Here
    var C = G.Deltatre;
    var E = C.plugins;
    var B = C.core.getInstance();
    var D = {
        handler: false,
        event: "event",
        plugin: false,
        via: "lazybyevent"
    };
    E.lazybyevent = function(L, M) {
// Not Here
        var K = A.extend(true, {}, D, M || {});
        var I = L;
        var H = {};
        var J;
        this.init = function() {
            H.handler = K.handler ? I.find(K.handler) : I;
            if (K.event === "hoverIntent" && (A.fn.hoverIntent === F)) {
                K.event = "mouseenter"
            }
            if (K.event === "hoverIntent") {
                H.handler.hoverIntent({
                    over: function() {
                        J()
                    },
                    interval: 250
                })
            } else {
                H.handler.one(K.event, function() {
                    J()
                })
            }
        };
        J = function() {
            I.data("options", K).data("plugin", K.plugin);
            if (A.metadata) {
                I.metadata("data-options", K);
                I.attr("data-plugin", K.plugin)
            }
            B.parse(I)
        }
    }
}(jQuery, window));
(function(A, F, E) {
// Here
    var B = F.Deltatre;
    var C = B.plugins;
    var D = B.Trace;
    C.search = function(I, J) {
// Not Here
        var H = I;
        var G = {};
        this.init = function() {
            G.input = H.find(".js-input");
            G.seVal = H.find(".js-label");
            H.submit(function(K) {
                var L = G.input.val();
                if (L.toLowerCase() !== G.seVal.text() && L !== "") {
                    location.href = "http://" + SEARCH_HOSTNAME + "/search/index.html#" + L
                }
                K.preventDefault()
            });
            G.input.focus(function() {
                if (A(this).val().toLowerCase() === G.seVal.text().toLowerCase()) {
                    A(this).val("")
                }
            });
            G.input.blur(function() {
                if (A(this).val() === "") {
                    A(this).val(G.seVal.text()).css("text-transform", "capitalize")
                }
            })
        }
    };
    if (location.pathname === "/search/index.html" && location.search.length > 0) {
        location.href = location.href + "#" + location.search.replace("?q=", "")
// Not Here
    }
}(jQuery, window));
(function(A, E) {
// Here
    var B = E.Deltatre;
    var D = B.plugins;
    var C = {
        timeout: 60000,
        isthirdy: "Y",
        url: "/livecommon/_matchesminute_jsonp.json",
        domain: "http://www.uefa.com"
    };
    D.jsonplive = function(L, M) {
// Not Here
        var K = this;
        var J = A.extend(true, {}, C, M || {});
        var F = L;
        var H = [1, 3, 5, 9, 13, 14, 17, 18, 22, 23, 24, 25, 27, 28, 38, 39, 101, 2008, 1026, 1027, 1028, 1031, 1032, 1035, 1038, 1039, 1042, 1046, 1048, 1049, 1050, 1053, 1054, 1055, 1056, 1062, 1069];
        var I;
        var G;
        this.init = function() {
            B.Uefacom = B.Uefacom || {};
            B.Uefacom.Livepolling = B.Uefacom.Livepolling || {};
            B.Uefacom.Livepolling.checklive = K.checklive;
            if (J.isthirdy === "Y") {
                J.url = J.domain + J.url
            }
            G()
        };
        this.checklive = function(N) {
            I = false;
            A.each(N.m, function(O, P) {
                if ((A.inArray(P.cup, H) > -1 && (P.s === "3"))) {
                    I = true;
                    return false
                }
            });
            F.toggleClass("is-live", I);
            window.Deltatre.plugins.jsonplive.prototype.data = N;
            A(document).trigger("jsonplive.ondata")
        };
        G = function() {
            A.ajax({
                type: "GET",
                url: J.url,
                dataType: "jsonp"
            });
            E.setTimeout(G, J.timeout)
        }
    }
}(jQuery, window));
(function(A, G, F) {
// Here
    var B = G.Deltatre;
    var D = B.plugins;
    var E = B.Trace;
    var C = {
        url: F,
        thirdparty: false
    };
    D.dropdownhp = function(N, O) {
// Not Here
        var M = A.extend(true, {}, C, O || {});
        var I = N;
        var H = {};
        var K, L, J;
        this.init = function() {
            if (M.url) {
                H.target = I.find(".js-target");
                if (M.thirdparty) {
                    K()
                } else {
                    if (A.metadata) {
                        A.getJSON(M.url, K)
                    } else {
                        A.getJSON(M.url).done(K).fail(L)
                    }
                }
                J()
            }
        };
        J = function() {
            var P = A.fn.hoverIntent === F ? "hover" : "hoverIntent";
            if (P === "hover") {
                I[P](function() {
                    H.target.show()
                }, function() {
                    H.target.hide()
                })
            } else {
                I[P]({
                    over: function() {
                        H.target.show()
                    },
                    out: function() {
                        H.target.hide()
                    },
                    sensitivity: 7,
                    interval: 250,
                    timeout: 200
                })
            }
        };
        K = function(P) {
            H.target = H.target.show();
            if (P && P.html) {
                H.target.append(P.html)
            }
            H.target = H.target.children();
            if (M.via && M.via === "lazybyevent") {
                if (I.is(":hover")) {
                    H.target.show();
                    I.one("mouseleave", function() {
                        H.target.hide()
                    })
                }
            }
        };
        L = function(R, S, Q) {
            var P = S + ", " + Q;
            E("Request Failed: " + P)
        }
    };
    A(document).ready(function() {
        A(".d3cmsHeader .js-orig-hover").removeClass("js-orig-hover")
    })
}(jQuery, window));
(function(A, F) {
// Here
    var C = F.Deltatre;
    var D = C.plugins;
    var E = C.Trace;
    var B = C.core.getInstance();
    A(document).ready(function() {
        try {
// Here
            var G = document;
            B.launchPlugins(G, ".d3-plugin")
        } catch (H) {
            E(H);
            E("core function error")
        }
    })
}(jQuery, window));
(function(A, E) {
// Here
    var B = E.Deltatre;
    var D = B.plugins;
    var C = {
        randomize: true,
        geotargeting: true,
        trackatstart: false,
        items: [],
        extract: 2,
        force: undefined,
        weighted: undefined,
        datasponsor: undefined,
        datascope: undefined,
        datatrack: undefined,
        views: {
            random_list: '<div class="random_list clearfix" />'
        }
    };
    D.randomsponsors = function(T, V) {
// Not Here
        var Q = A.extend(true, {}, C, V || {});
        var G = T;
        var F = {};
        var U;
        var K = [];
        var J, I, H, L, S, M, N, R, O, P;
        this.init = function() {
            F.target = G.find(".js-target");
            if (!F.target.length) {
                F.target = G
            }
            if (Q.geotargeting) {
                U = new B.Geotargeting(Q);
                U.whenReady(J)
            } else {
                J()
            }
        };
        J = function() {
            if (Q.randomize) {
                I()
            } else {
                if (Q.geotargeting) {
                    H()
                } else {
                    L()
                }
            }
            S();
            if (Q.trackatstart) {
                R()
            }
        };
        H = function() {
            var W = U.getCountry();
            A.each(Q.items, function(X, Y) {
                if (K.length >= Q.extract) {
                    return false
                }
                if (Y.nocountry) {
                    if (Y.nocountry.toUpperCase().split(",").indexOf(W.toUpperCase()) === -1) {
                        K.push(Y)
                    }
                } else {
                    K.push(Y)
                }
            })
        };
        L = function() {
            K = Q.items.slice(0, Q.extract)
        };
        R = function() {
            var X = false,
                W = [];
            A.each(K, function(Y, Z) {
                if (Z.track === "Y") {
                    W.push(Z.title.toLowerCase());
                    X = true
                } else {
                    X = false
                }
            })
        };
        I = function() {
            var a, Y, W = 0,
                Z = Q.items.length,
                b = Q.extract,
                X = U ? U.getCountry() : false;
            while (b > 0) {
                if (W >= Z) {
                    break
                }
                if (Q.force !== undefined && Q.force.length > 0) {
                    a = Q.force.pop()
                } else {
                    if (Q.weighted !== undefined && Q.weighted.toLowerCase() === "y") {
                        a = O()
                    } else {
                        a = Math.floor(Math.random() * (Q.items.length))
                    }
                }
                Y = Q.items.splice(Q.items.indexOf(Q.items[a]), 1).pop();
                if (X) {
                    if (!(Y.nocountry && Y.nocountry.toUpperCase().split(",").indexOf(X.toUpperCase()) > -1)) {
                        K.push(Y);
                        b--
                    }
                } else {
                    K.push(Y);
                    b--
                }
                W++
            }
        };
        O = function() {
            var X;
            var W = [],
                Y = 0;
            A.each(Q.items, function(a, b) {
                Y += b.weight;
                W.push(Y)
            });
            var Z = Math.floor(Math.random() * 100) + 1;
            X = 0;
            A.each(W, function(a, b) {
                X = a;
                if (Z < b) {
                    return false
                }
            });
            return X
        };
        S = function() {
            var Z, a, W, Y, X;
            X = A("<ul />");
            if (Q.views[Q.view]) {
                Y = A(Q.views[Q.view]);
                X.appendTo(Y)
            } else {
                Y = X
            }
            A.each(K, function(b, c) {
                Z = c.url.split("|");
                a = c.title.split("|");
                W = c.width ? N(c, Z, a) : M(c, Z, a);
                X.append(W)
            });
            F.target.append(Y)
        };
        P = function() {
            var W = false;
            if (window.matchMedia) {
                W = (window.matchMedia("(-webkit-min-device-pixel-ratio: 2)").matches || window.matchMedia("(min-resolution: 192dpi)").matches)
            }
            return W
        };
        N = function(Y, X, a) {
            var W, Z;
            W = A("<a>").attr({
                title: Y.title || "",
                href: Y.url || "#",
                "data-track": Y.track || ""
            }).click(function(b) {
                if (Y.url) {
                    window.open(Y.url, "_blank")
                }
                b.preventDefault()
            });
            Z = (Y.retina && P()) ? Y.retina : Y.src;
            A("<img>").attr({
                width: Y.width,
                height: Y.height,
                src: Z,
                title: Y.title || ""
            }).appendTo(W);
            return A("<li>").attr({
                "data-sponsor": Y.datasponsor || "",
                "data-scope": Y.datascope || "",
                "data-track": Y.datatrack || ""
            }).append(W)
        };
        M = function(Z, Y, a) {
            var X = Y.length > 1 ? "half-link" : "single-link",
                W = [];
            W.push('<li data-sponsor="' + Z.datasponsor + '" data-scope="' + Z.datascope + '" data-track="' + Z.datatrack + '"><div class="promo" style="background-image:url(' + Z.src + ');">');
            A.each(Y, function(b, c) {
                W.push('<a class="' + X + '" href="' + c + '" title="' + a[b] + '" target="_blank">&nbsp;</a>')
            });
            W.push("</div></li>");
            return W.join("")
        }
    }
}(jQuery, window));;
