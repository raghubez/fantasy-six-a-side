var draggedItem = {};
(function () {

    // Localize jQuery variable
    var jQuery,
        team = {};
    team.GK = [];
    team.DEF = [];
    team.MID = [];
    team.FWD = [];
    var MAX_GOALKEEPERS = 2,
        MAX_DEFENDERS = 5,
        MAX_MIDFIELDERS = 5,
        MAX_STRIKERS = 3,
        Player = function () {
            this.playerId = 0;
            this.iconId = ''
            this.points = 0;
            this.value = 0;
            this.playerNumber = 0;
        }

    /******** Load jQuery if not present *********/
    if (window.jQuery === undefined) {
        console.log('jQuery is required');
    } else {
        // The jQuery version on the window is the one we want to use
        jQuery = window.jQuery;
        main();
    }

    function changeFormation(formation) {
        // changes the arrays of players based on their position
        // ans associates the appropriate query parameter.

        if (!formation) {
            return;
        }

        var gk = Number(formation.charAt(1));
        var def = Number(formation.charAt(2));
        var mid = Number(formation.charAt(3));
        var str = Number(formation.charAt(4));

        if (gk + def + mid + str != 11) {
            return;
        }

        team.GK.length = 0;
        while (team.GK.length < MAX_GOALKEEPERS) {
            team.GK.push(new Player());
            var playerNumber = 0;
            if (team.GK.length <= gk) {
                playerNumber = (team.GK.length).toString();
            } else {
                playerNumber = (11 + (team.GK.length - gk)).toString();
            }
            team.GK[team.GK.length - 1].iconId = '#p' + playerNumber;
            team.GK[team.GK.length - 1].playerNumber = playerNumber;
        }

        team.DEF.length = 0;
        while (team.DEF.length < MAX_DEFENDERS) {
            team.DEF.push(new Player());
            var playerNumber = 0;
            if (team.DEF.length <= def) {
                playerNumber = (gk + team.DEF.length).toString();
            } else {
                playerNumber = (11 + (MAX_GOALKEEPERS - gk) + (team.DEF.length - def)).toString();
            }
            team.DEF[team.DEF.length - 1].iconId = '#p' + playerNumber;
            team.DEF[team.DEF.length - 1].playerNumber = playerNumber;
        }

        team.MID.length = 0;
        while (team.MID.length < MAX_MIDFIELDERS) {
            team.MID.push(new Player());
            var playerNumber = 0;
            if (team.MID.length <= mid) {
                playerNumber = (gk + def + team.MID.length).toString();
            } else {
                playerNumber = (11 + (MAX_GOALKEEPERS - gk) + (MAX_DEFENDERS - def) + (team.MID.length - mid)).toString();
            }
            team.MID[team.MID.length - 1].iconId = '#p' + playerNumber;
            team.MID[team.MID.length - 1].playerNumber = playerNumber;
        }

        team.FWD = [];
        while (team.FWD.length < MAX_STRIKERS) {
            team.FWD.push(new Player());
            var playerNumber = 0;
            if (team.FWD.length <= str) {
                playerNumber = (gk + def + mid + team.FWD.length).toString();
            } else {
                playerNumber = (11 + (MAX_GOALKEEPERS - gk) + (MAX_DEFENDERS - def) + (MAX_MIDFIELDERS - mid) + (team.FWD.length - str)).toString();
            }
            team.FWD[team.FWD.length - 1].iconId = '#p' + playerNumber;
            team.FWD[team.FWD.length - 1].playerNumber = playerNumber;
        }

        $('#js-pitch-formation').attr('class', '');
        $('#js-pitch-formation').addClass(formation);
        $('#f').val(gk.toString() + def.toString() + mid.toString() + str.toString());

        //console.log(team);
    }

    function addPlayer(playerObject) {

        var playerId = playerObject.playerId,
            group = playerObject.group,
            value = playerObject.value,
            points = playerObject.points,
            playerName = playerObject.playerName;

				var optionalPlayerIndex = null;
        if (arguments.length > 1) {
            optionalPlayerIndex = arguments[1]; 
        }

        // check for dupes if so remove
        console.log(team, group);
        for (var i = 0; i < team[group].length; i++) {
            if (team[group][i].playerId === playerId) {
            		if(optionalPlayerIndex !== null){
            			team[group][i].playerNumber = optionalPlayerIndex;
            		}
                $("#js-capt option[value='" + team[group][i].playerId + "']").remove();
                $('#js-shirt-' + team[group][i].playerNumber.toString()).attr('src', "/championsleague/images/shirts/ic_shirt_default.png");
                $('#js-shirt-' + team[group][i].playerNumber.toString()).removeClass('js-selected-player');
                $('#js-add-icon-' + team[group][i].playerId.toString()).removeClass('fa-minus-square');
                $('#js-add-icon-' + team[group][i].playerId.toString()).addClass('fa-plus-square');
                team[group][i].playerId = 0;
                team[group][i].value = 0;
                team[group][i].points = 0;
                $(team[group][i].iconId).val('');
                return false;
            }
        }

        // check players
        for (var i = 0; i < team[group].length; i++) {
            if (team[group][i].playerId === 0) {
            	if(optionalPlayerIndex !== null && optionalPlayerIndex !== team[group][i].playerNumber){
            			continue;
            	}
                team[group][i].playerId = playerId;
                team[group][i].value = value;
                team[group][i].points = points;
                $(team[group][i].iconId).val(team[group][i].playerId);
                $('#js-shirt-' + team[group][i].playerNumber.toString()).attr('src', "/championsleague/images/shirts/" + team[group][i].playerId + ".png");
                $('#js-shirt-' + team[group][i].playerNumber.toString()).addClass('js-selected-player');
                $('#js-add-icon-' + team[group][i].playerId.toString()).removeClass('fa-plus-square');
                $('#js-add-icon-' + team[group][i].playerId.toString()).addClass('fa-minus-square');
                if (team[group][i].playerNumber <= 11) {
                    $('#js-capt').append('<option id="js-capt-' + playerId + '" value="' + playerId + '">' + playerName + '</option>');
                }
                break;
            }
        }
        
        $('.js-selected-player').draggable({
                helper: "clone",
                start: function (event, ui) {
                    draggedItem.tr = this;
                    draggedItem.helper = ui.helper;
                }
            });
    }

    function reset() {
        for (var i = 0; i < team.GK.length; i++) {
            $("#js-capt option[value='" + team.GK[i].playerId + "']").remove();
            $('#js-shirt-' + team.GK[i].playerNumber.toString()).attr('src', "/championsleague/images/shirts/ic_shirt_default.png");
            $('#js-add-icon-' + team.GK[i].playerId.toString()).removeClass('fa-minus-square');
            if (!$('#js-add-icon-' + team.GK[i].playerId.toString()).hasClass('fa-plus-square')) {
                $('#js-add-icon-' + team.GK[i].playerId.toString()).addClass('fa-plus-square');
            }
            team.GK[i].playerId = 0;
            team.GK[i].points = 0;
            team.GK[i].value = 0;
            $(team.GK[i].iconId).val('');
        }

        for (var i = 0; i < team.DEF.length; i++) {
            $("#js-capt option[value='" + team.DEF[i].playerId + "']").remove();
            $('#js-shirt-' + team.DEF[i].playerNumber.toString()).attr('src', "/championsleague/images/shirts/ic_shirt_default.png");
            $('#js-add-icon-' + team.DEF[i].playerId.toString()).removeClass('fa-minus-square');
            if (!$('#js-add-icon-' + team.DEF[i].playerId.toString()).hasClass('fa-plus-square')) {
                $('#js-add-icon-' + team.DEF[i].playerId.toString()).addClass('fa-plus-square');
            }
            team.DEF[i].playerId = 0;
            team.DEF[i].points = 0;
            team.DEF[i].value = 0;
            $(team.DEF[i].iconId).val('');
        }

        for (var i = 0; i < team.MID.length; i++) {
            $("#js-capt option[value='" + team.MID[i].playerId + "']").remove();
            $('#js-shirt-' + team.MID[i].playerNumber.toString()).attr('src', "/championsleague/images/shirts/ic_shirt_default.png");
            $('#js-add-icon-' + team.MID[i].playerId.toString()).removeClass('fa-minus-square');
            if (!$('#js-add-icon-' + team.MID[i].playerId.toString()).hasClass('fa-plus-square')) {
                $('#js-add-icon-' + team.MID[i].playerId.toString()).addClass('fa-plus-square');
            }
            team.MID[i].playerId = 0;
            team.MID[i].points = 0;
            team.MID[i].value = 0;
            $(team.MID[i].iconId).val('');
        }

        for (var i = 0; i < team.FWD.length; i++) {
            $("#js-capt option[value='" + team.FWD[i].playerId + "']").remove();
            $('#js-shirt-' + team.FWD[i].playerNumber.toString()).attr('src', "/championsleague/images/shirts/ic_shirt_default.png");
            $('#js-add-icon-' + team.FWD[i].playerId.toString()).removeClass('fa-minus-square');
            if (!$('#js-add-icon-' + team.FWD[i].playerId.toString()).hasClass('fa-plus-square')) {
                $('#js-add-icon-' + team.FWD[i].playerId.toString()).addClass('fa-plus-square');
            }
            team.FWD[i].playerId = 0;
            team.FWD[i].points = 0;
            team.FWD[i].value = 0;
            $(team.FWD[i].iconId).val('');
        }
    }


    function main() {
        jQuery(document).ready(function () {

            // change the pitch formation
            $('.js-formation').click(function (event) {
                $('.js-formation').removeClass('active');
                var formation = $(this).attr('data-formation');
                changeFormation(formation);
                $(this).addClass('active');
                event.preventDefault();
            });

            //select a player
            $('.js-add').click(function () {

                function getPlayerInfo(playerJQueryHTMLObject) {
                    var playerObject = {};
                    playerObject.playerId = playerJQueryHTMLObject.attr('data-id');
                    playerObject.group = playerJQueryHTMLObject.attr('data-pos');
                    playerObject.value = playerJQueryHTMLObject.attr('data-val');
                    playerObject.points = playerJQueryHTMLObject.attr('data-pts');
                    playerObject.playerName = playerJQueryHTMLObject.attr('data-name');
                    return playerObject;
                }

                addPlayer(getPlayerInfo($(this)));
                return false;
            });


            // reset
            $('#js-reset').click(function () {
                reset();
                return false;
            });

            // submit
            $('#js-team-select-1').click(function () {
                if ($('#js-name').val() === '') {
                    alert("Please enter a valid team name");
                    return false;
                }
                if ($('#js-team').val() === '') {
                    alert("Please select the team you support")
                    return false;
                }
                if ($('#js-ctry').val() === '') {
                    alert("Please select the country you support")
                    return false;
                }
                if ($('#js-ctry').val() === '') {
                    alert("Please select the country you support")
                    return false;
                }
                // dont need to check any other fields !          	 

            });

            $('#js-team-select-2').click(function () {
                for (var i = 1; i <= 15; i++) {
                    var player = $('#p' + i.toString()).val();
                    if (!player.length) {
                        alert(lang.ERROR_SELECT_PLAYERS);
                        return false;
                    }
                }

                if ($('js-capt').val() === '') {
                    alert("Please select your team captain");
                }

                return true;
            });

            $('#js-auto-select').click(function () {
                var queryParams = '';

                for (var i = 1; i <= 15; i++) {
                    if (i == 1) {
                        queryParams = queryParams + '?';
                    } else {
                        queryParams = queryParams + '&';
                    }
                    queryParams = queryParams + $('#p' + i).attr('name') + "=" + $('#p' + i).attr('value');
                }

                $.ajaxSetup({cache: true, timeout: 10000});
                $.getJSON("/championsleague/json/team/auto-select/" + queryParams, function (data) {

                }).success(function (data) {
                    if (data.error) {
                        alert(data.error);
                    } else {
                        console.log(data.formation);
                        changeFormation('f' + data.formation);
                        reset()
                        // set the players
                        for (var i = 0; i < data.players.length; i++) {
                            var playerObject = {};
                            playerObject.playerId = data.players[i];
                            playerObject.group = $('#js-add-' + playerId).attr('data-pos');
                            playerObject.value = $('#js-add-' + playerId).attr('data-val');
                            playerObject.points = $('#js-add-' + playerId).attr('data-pts');
                            playerObject.playerName = $('#js-add-' + playerId).attr('data-name');
                            addPlayer(playerObject);
                        }
                        // set the substitutes
                        for (var i = 0; i < data.substitutes.length; i++) {
                            var playerObject = {};
                            playerObject.playerId = data.substitutes[i];
                            playerObject.group = $('#js-add-' + playerId).attr('data-pos');
                            playerObject.value = $('#js-add-' + playerId).attr('data-val');
                            playerObject.points = $('#js-add-' + playerId).attr('data-pts');
                            playerObject.playerName = $('#js-add-' + playerId).attr('data-name');
                            addPlayer(playerObject);
                        }
                        // set the captain
                        $('#js-capt').val(data.captain);
                    }
                }).error(function (xhr, ajaxOptions, thrownError) {
                    alert("Network connection error");
                }).complete(function () {

                });
                return false;
            });

            $('#js-save').click(function () {
                var queryParams = '';

                for (var i = 1; i <= 15; i++) {
                    if (i == 1) {
                        queryParams = queryParams + '?';
                    } else {
                        queryParams = queryParams + '&';
                    }
                    queryParams = queryParams + $('#p' + i).attr('name') + "=" + $('#p' + i).attr('value');
                }
                queryParams = queryParams + '&' + $('#js-capt').attr('name') + "=" + $('#js-capt').attr('value');
                queryParams = queryParams + '&' + $('#f').attr('name') + "=" + $('#f').attr('value')

                $.ajaxSetup({cache: false, timeout: 10000});
                $.getJSON("/championsleague/json/team/save/" + queryParams, function (data) {

                }).success(function (data) {
                    if (data.error) {
                        alert(data.error);
                    } else {
                        alert("Saved ok");
                    }
                }).error(function (xhr, ajaxOptions, thrownError) {
                    alert("Network connection error");
                }).complete(function () {

                });
                return false;
            });


            // initialize
            var formation = $('#js-pitch-formation').attr("class");
            reset();
            changeFormation(formation);

            for (var i = 1; i <= 15; i++) {
                var player = $('#p' + i).attr('value');
                if (player !== '') {
                    var playerObject = {};
                    playerObject.playerId = player;
                    playerObject.group = $('#js-add-' + playerId).attr('data-pos');
                    playerObject.value = $('#js-add-' + playerId).attr('data-val');
                    playerObject.points = $('#js-add-' + playerId).attr('data-pts');
                    playerObject.playerName = $('#js-add-' + playerId).attr('data-name');
                    addPlayer(playerObject);
                }
                if ($('#p' + i).attr('data-capt')) {
                    $('#js-capt').val(player);
                }
            }

            function lookupPlayerFromPos(dropAreaIndex, team, draggedPlayer) {
                var i = 0,
                    player = {
                        position: draggedPlayer.find('.js-add').attr('data-pos')
                    };

                for (var position in team) {
                    if (team.hasOwnProperty(position)) {
                        var result = {
                            arrayPos: lookupPerPosition(),
                            playerType: position
                        };
                        if (result.arrayPos !== undefined && result.playerType === player.position) {
                            //obtain player information
                            var validPlayerObject = getPlayerInfo(draggedPlayer);
                            var targetPosition = result.arrayPos.playerNumber;
                            addPlayer(validPlayerObject, targetPosition);
                            //return result;
                        }
                    }
                }

                function lookupPerPosition() {
                    for (i; i < team[position].length; i++) {
                        if (team[position][i].playerNumber === dropAreaIndex) {
                            return team[position][i];
                        }
                    }
                    i = 0;
                    return undefined;
                }

                function getPlayerInfo(playerJQueryHTMLObject) {
                    var playerObject = {};
                    playerObject.playerId = playerJQueryHTMLObject.find('.js-add').attr('data-id');
                    playerObject.group = playerJQueryHTMLObject.find('.js-add').attr('data-pos');
                    playerObject.value = playerJQueryHTMLObject.find('.js-add').attr('data-val');
                    playerObject.points = playerJQueryHTMLObject.find('.js-add').attr('data-pts');
                    playerObject.playerName = playerJQueryHTMLObject.find('.js-add').attr('data-name');
                    return playerObject;
                }
            }

            $("tr[id^='js-tr-']").draggable({
                helper: "clone",
                start: function (event, ui) {
                    draggedItem.tr = this;
                    draggedItem.helper = ui.helper;
                }
            });
            $(".js-icon").droppable({
                drop: function (event, ui) {
                	var draggedID = $(ui.draggable).attr('id');
                	
                	if(draggedID.indexOf('js-shirt-') !== -1){
                		dropShirt(event, ui, this);
                	}
                	else if(draggedID.indexOf('js-tr-') !== -1){
                		dropTableRow(event, ui, this);
                	}
                	
                  function dropTableRow(event, ui, context){
                  	var selectedPlayerPos = $(context).attr('data-player'),
                        draggedPlayer = $(ui.draggable);
                    var data = lookupPlayerFromPos(selectedPlayerPos, team, draggedPlayer);
                    if (data !== undefined) {
                        //add Player
                        $(draggedItem.tr).remove();
                        $(draggedItem.helper).remove();
                    }
                  }
                  
                  function dropShirt(event, ui, context){
                  	console.log('event', event);
                  	console.log('ui', $(ui.draggable).parent().attr('data-player'));
                  	console.log('dropped on', $(context).attr('data-player'));
                  	console.log('team', team);
                  }
                }
            });
        })
    }
})();