(function(){

    // Localize jQuery variable
    var jQuery;
    /******** Load jQuery if not present *********/
    if (window.jQuery === undefined) {
        alert('jQuery is required');
    } else {
        // The jQuery version on the window is the one we want to use
        jQuery = window.jQuery;
        main();
    }

    function main() {
      	var aFormations     = ["1442", "1433"];
      	var sContentUrl     = '';
      	var iRetry          = 0;
    	  var iTeamSize       = 11;
    	  var iRetry          = 0;
    	  var iMaxValue       = 50;
    	  var iMostExpensive  = 11; 
        var aPlaTables      = new Array('gt','dt','mt','st','at');
        var aGroupCat       = {'G':1,'D':2,'M':3,'S':4};
        var aCatGroup       = new Array('G','D','M','S');
        var aGroupName      = {'G':'Goalkeeper','D':'Defender','M':'Midfielder','S':'Striker'}
        var aGroup3Char     = {'G':'GK','D':'DEF','M':'MID','S':'STR'};
        var oPlayers        = new Object;
        var aTeamNames      = new Array();
        var oTeams          = new Object();
        var aMinForm        = new Array(1,3,3,1);
        var aMaxForm        = new Array(1,5,5,3);
        var aMaxPerTeam     = 15;
        var iCurrTotalVal   = 0;
        var iTransfersLeft  = 99;
        var iTransfersLeftM = 99;
        var iTransfers      = 0; 
        var scrollTimeout;
           	
        jQuery(document).ready(function($) {            
            
            $('div.player-tab').gfmtabs();
            $('#js-plaCat').html($('ul.gfm-tabs li.on').attr('category'));
            
            $('ul.gfm-tabs li a').click(function() {
            	$('#plaSelContainer').scrollTop(0);
            	$('#js-plaCat').html($(this).parent().attr('category'));
            	drawCaterpillar();
            });
  
					  $('html').offcanvas();
						pageLoadAction();
						    var lastScrollTop = 0;
							$('#plaSelContainer').scroll(function(event){
						   	clearTimeout(scrollTimeout);
								var st = $(this).scrollTop();
								if (st === 0){
									$('.gfm-tabs, .plasearch').show();
								}
								else {
							  scrollTimeout = setTimeout(function() {
							  	if (lastScrollTop > 20) {
							    	$('.gfm-tabs, .plasearch ').fadeOut(500, function(){});
							  	}
							  }, 2000);		
								}
							  if (st > lastScrollTop ){
							    $('.gfm-tabs, .plasearch ').hide();
							    $('.plalist').css('top', '0px')
							    if ($('.filterContent').css('display','block')) {
							    	$('.filterContent').hide();
							    	$('button.filter').html('Show Filters')
							    }
							  } else {
							  	$('.gfm-tabs, .plasearch').show();
							    $('.plalist').css('top', '90px')
							    
								//	var offset = $('#plaSelContainer').offset().top;
								//	$('.gfm-tabs').css({'position' : 'fixed', 'display' : 'block'/*'width' : '100%', */})
							//		$('.plasearch').css({'position' : 'fixed','display' : 'block', 'top' : offset+$('.gfm-tabs').height()})
								//	$('.plasearch').css({'top' :  $('#plaSelContainer').offset().top+$('.gfm-tabs').height()})
					   		}
					
					   		lastScrollTop = st;
							});	   

            
            $(".droppableContainer, #mstf-header-outer").droppable({
            	drop: function( event, ui ) {
              	var plaId = ui.draggable.attr('plaid');
                oRemovePlayer(plaId);
                oDrawPitch();
              }
            });  
            
            loadPlayers();
        });
        
        
				$(window).bind('resize load', pageLoadAction);
				
					function pageLoadAction(){
						if( $(this).width() > 767 ) {
							$('#page').attr('id', 'leftPitch');
							$('.page, .sidebarRight').removeClass('aminatedSlide, slidLeft');
							$('.page div#scroll').removeClass('scrollableArea');
							var headerHeight = $('#mstf-header-outer').height() + 1;
					    $('.page .scrollableArea').css('top', headerHeight);
						}
						else {
							$('#leftPitch').attr('id', 'page');			
							$('.page div#scroll').addClass('scrollableArea');
							var headerHeight = $('#mstf-header-outer').height() + 1;
					    $('.offcanvas .sidebarRight, .page .scrollableArea').css('top', headerHeight);
						}
					}
        
        
		        function drawCaterpillar() {
		        	 $('#js-players img').each(function(){
		        		if($(this).attr('id').charAt(2) !== $('ul.gfm-tabs li.on').attr('id').charAt(2)) {
		        			$(this).hide();
		        		}
		        		else {
		        			$(this).show();
		        		}
		        	});
		        }
            
            function oPlayer(){
                this.t          = 'UKN';
                this.v          = 0;
                this.g          = 'G';
                this.p          = 0;
                this.pk         = 0;
                this.s          = 'RSCURENT';
                this.i          = 'N';
                this.lyshots    = 0;
                this.lymins     = 0;
                this.lyred      = 0;
                this.lyassist   = 0;
                this.lyappear   = 0;
                this.lygoals    = 0;
                this.lyyellow   = 0;
            }
                      
            function loadPlayers(){

                /* We need to load all the players ready for selection */
                $.ajaxSetup({cache:true,timeout:10000});
                $.getJSON("players.json", function(data){

                    // Looks for the error message;
                    if(data.error){
                        alert(data.error);
                        return;
                    }

                    /* sets up or updates the player information */
                    json_players    = data.players;
                    json_team       = data.team;
                    json_teamname   = data.teamname;
                    json_group      = data.group; 

                    var iGroupLen = json_group.length;
                    while(iGroupLen--) {
                        var sGroup = json_group[iGroupLen];
                        var plaGroup = json_players[iGroupLen];
                        var iPlaLen = plaGroup.length;
                        while(iPlaLen--) {
                            var currPlayer = plaGroup[iPlaLen];
                            var newPlayer = new oPlayer;

                            newPlayer.n     = currPlayer[1];
                            newPlayer.t     = json_team[currPlayer[2]];
                            newPlayer.teamcode     = currPlayer[2];
                            newPlayer.v     = (currPlayer[3]/10).toFixed(1);
                            newPlayer.pk    = (currPlayer[4]/10).toFixed(1)+'%';
                            newPlayer.p     = currPlayer[5];
                            newPlayer.avail = currPlayer[6];
                            newPlayer.news 	= currPlayer[7];   
                            if(currPlayer[7] != 1){
                                newPlayer.s = 'RSCURENT';
                            }else{
                                newPlayer.s = 'RSSUSPEN';
                            }
                            newPlayer.shortname = currPlayer[8];

                            newPlayer.g = sGroup;
                            newPlayer.i = 'Y';

                            newPlayer.lyshots   = currPlayer[8];
                            newPlayer.lymins    = currPlayer[9];
                            newPlayer.lyred     = currPlayer[10];
                            newPlayer.lyassist  = currPlayer[11];
                            newPlayer.lyappear  = currPlayer[12];
                            newPlayer.lygoals   = currPlayer[13];
                            newPlayer.lyyellow  = currPlayer[14];
                            newPlayer.league    = currPlayer[31];

                            oPlayers[currPlayer[0]] = newPlayer;

                            aTeamNames[json_team[currPlayer[2]]] = json_teamname[currPlayer[2]];
                        }
                    }

                    initialise();

                }).success(function() {
                    //debug("second success");
                }).error(function(xhr, ajaxOptions, thrownError) {
                iRetry++;
                if(iRetry<=1){
                    loadPlayers();
                    return;
                }
                }).complete(function() {
                    //debug("complete");
                    loadTeamFromCookie();
                });
            }
            

            function getCookie(cname) {
		  				  var name = cname + "=";
						    var ca = document.cookie.split(';');
						    for(var i=0; i<ca.length; i++) {
						        var c = ca[i];
						        while (c.charAt(0)==' ') c = c.substring(1);
						        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
						    }
						    return "";
						}

            $('table.sortable th').on('click', function(){
							if ($(this).html() === '') {
								return;
							}
							var clicked = $(this);
							var table = $(this).closest('table.sortable');
							var index = $(this).index() + 1;
							var oPlayers = [];
							$(table).find('tbody tr').each(function() {
								var html = $(this)[0].outerHTML;
								var compared = $(this).find('td:nth-child('+index+')').html();
								if (compared === '' || compared === undefined) {
									return;
								}														
								if (compared === '-') {
									compared = 999;
								}
								if (!isNaN(compared)) {
									compared = parseFloat(compared);
								}
								else {
									compared = compared.toLowerCase();
								}
								$(this).remove();
								oPlayers.push({id:compared,player:html});
							});
							if (clicked.find('span').hasClass('') || clicked.find('span').hasClass('down')) {
								oPlayers = sortByKey(oPlayers, "id");
								$("th span").removeClass();
								$(this).find('span').addClass('up');
							}
							else {
								oPlayers = oPlayers.reverse();
								$("th span").removeClass();
								$(this).find('span').addClass('down');
							}
							for(var plaId in oPlayers) {
								$(table).find("tbody").append(oPlayers[plaId].player);
							}
							if($(table).attr('id') === 'gt' || $(table).attr('id') === 'dt' || $(table).attr('id') === 'mt' || $(table).attr('id') === 'st' || $(table).attr('id') === 'at'){
								assignEvents($(table).attr('id'));
							}
							oPlayers = []
						});
						
						function sortByKey(array, key) {
					    return array.sort(function(a, b) {
					        var x = a[key]; var y = b[key];
					        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
					    });
						}
            
            function filterPlayers(oBj){
                var re = new RegExp($(oBj).val().toLowerCase());
                var oTable = $(oBj).parent().parent();
                oTable.find(".filterable .player-row").each(function(){
                    var oCurrObj = $(this);
                    if($(this).attr('search').match(re)){
                        $(this).show();
                    }else{
                        $(this).hide();
                    }
                });
            }            
 
            function oDrawPlayerList(plaType){
                if($('#'+plaType).length == 1){return;}

                var iNo = 1;
                var iGroup = 'G';

                if(plaType == 'gt'){
                    iNo = 1;
                    iGroup = 'G';
                }

                if(plaType == 'dt'){
                    iNo = 2;
                    iGroup = 'D';
                }

                if(plaType == 'mt'){
                    iNo = 3;
                    iGroup = 'M';
                }

                if(plaType == 'st'){
                    iNo = 4;
                    iGroup = 'S';
                }

                if(plaType == 'at'){
                    iNo = 5;
                    iGroup = 'A';
                }

                $('').appendTo('#pl'+iNo);

                var s=[];
                for(var k in oPlayers){
                    var v = oPlayers[k];

                    if(v.s != 'RSCURENT'){
                        continue;
                    }

                    if(iGroup != 'A' && v.g != iGroup){
                        continue;
                    }

                    if(v.p == undefined){v.p = '-'};
                    if(v.pk == undefined){v.pk = '-'};
                    if(v.cs == undefined){v.cs = '-'};
                    
                    
										var sAvail = getAvailability(v);
										if (sAvail === '') {
											sAvail = 'icoInfo'
										}
                    //var sRow = '<tr id="p' + k + '" class="picked player-row" ><td class="lname">' + v.n + '</td><td class="lclub">' + v.t + '</td><td class="lpric">' + v.v + '</td><td class="lpoin">'+v.p+'</td><td class="lstat"><span plaId='+k+' class="pla-inf sprite ' +sAvail + '"></span></td></tr>';
                    var sPlayer = '<div id="' + k + '" class="picked player-row" search="'+aTeamNames[v.t].toLowerCase()+v.t.toLowerCase()+v.n.toLowerCase()+v.v+'">';
                    sPlayer += '<img class="pla-img" src="/sfdt/mobile/images/players/'+k+'.jpg" onerror="this.src=\'/sfdt/mobile/images/players/blank.jpg\'">';
                    sPlayer += '<div class="lplaInf"><div class="rowLeft"><p class="lname">'+v.n+'</p>';
					  				sPlayer += '<p class="lpoin">'+v.p+'pts</p><p class="lclub"> ' + v.t + '</p></div>';
					  				sPlayer += '<div class="rowRight"><p class="lpric">&pound;' + v.v + '<span>M</span></p></div>';
					  				//sPlayer += '<div class="lstat"><span plaId='+k+' class="pla-inf sprite ' +sAvail + '"></span></div></div></div>';
					  				sPlayer += '<div class="plusMinus">+</div></div></div>';
                    
                    s.push(sPlayer);
        						}			
        						var sPlayer = '<div class="toTop"><p>Back to top ^</div>';
        						 s.push(sPlayer); 
                
                if (s.length) {
                	$('#pl'+iNo+' .players').attr('id', plaType)
									$('#pl'+iNo+' .players').html(s.join(""));
								}
								assignEvents(plaType);
                
                oDrawPitch();
                
                
            }    
                
            
            function getAvailability(oPlayer) {
            	if (oPlayer.avail === 'D') {
            		return 'icoDou';
            	}
            	else if (oPlayer.avail === 'I') {
            		return 'icoInj';
            	}
            	else if (oPlayer.avail === 'S') {
            		return 'icoSus';
            	} 
            	else if (oPlayer.avail === 'U' || oPlayer.news !=0 ) {
            		return 'icoUna';
            	} 
            	else {
            		return '';
            	}
            }
            
            
            function saveTeamToCookie() {
            	var cookie = '';
              for(var i=1;i<=iTeamSize;i++){  
                var iCurrId = $('#p'+i).attr('value');
                if(iCurrId) {
               	  cookie = cookie + "%2C" + iCurrId;                   
                } else {
                	cookie = cookie + "%2C";                 
                }
              }
              cookie = cookie + "%7C";
                       
              var date = new Date();
              date.setDate(date.getDate() + 1)
              var expires = "; expires=" + date.toGMTString();
              document.cookie = 'MFDTSelTeam' + "=" + cookie + expires + "; path=/";                    
            }

            function loadTeamFromCookie() {
            	var team    = getCookie("MFDTSelTeam");
            	if (!team) {
            		return;
            	}
            	var parts    = team.split("%7C");           	
            	var players  = parts[0].split("%2C");
            	var teamName = parts[1];
            	$('#teamname').val(decodeURIComponent(teamName).split('+').join(' '));   
            	for (var i = 0; i < players.length; i++) {
            		if (players[i]) {
            		  oSelectPlayer(players[i]);    
            		}        		
            	}
            	drawCaterpillar() 
            	oDrawPitch();
        	
            }		                     
                
            function initialise(){
            
	            /* Draw table */
	            oDrawPlayerList('gt');
	            
	            $('.plalist').on('click', '.toTop p', function(){
	            	$('#plaSelContainer').animate({ scrollTop: 0 }, "slow");
	            });

            	$('.placols>li').click(function(){

                var oTable = $(this).parent().parent().find('table');

                if($(this).hasClass("lname")){
                    oTable.sortable('sort',0);
                }else if ($(this).hasClass("lclub")){
                    oTable.sortable('sort',1);
                }else if ($(this).hasClass("lpric")){
                    oTable.sortable('sort',2);
                }else if ($(this).hasClass("lpick")){
                    oTable.sortable('sort',3);
                }else if ($(this).hasClass("lpoin")){
                    oTable.sortable('sort',3);
                }else if ($(this).hasClass("lcs")){
                    oTable.sortable('sort',4);
                }
            	});

            	/* Functions for the filter options */
	            $(".plasearch input").keypress(function(e){
	                if(e.which == 13){
	                    e.preventDefault();
	                    return false;
	                }
	            }).focus(function(){
	                $(this).animate( { "opacity" : 1 }, 250 );
	            }).blur(function(){
	                if($(this).hasClass('placeholder')){
	                    $(this).animate( { "opacity" : .5 }, 250 );
	                }
	            }).animate(
	                { "opacity" : .5 }
	                , 250
	            ).keyup(function(e){
	                clearTimeout($.data(this, 'timer'));
	                var oObj = this;
	                var wait = setTimeout(function(){filterPlayers(oObj)}, 500);
	                $(this).data('timer', wait);
	            });

	            $('ul.gfm-tabs li>a').click(function(){
	                oDrawPlayerList(aPlaTables[$(this).parent().index()]);
	            });
            
	            /* These are new team options */
	            if($('#teamname').length){
	
	                // get current number of characters
	                $('#teamname-counter').html("("+($('#teamname').attr('maxlength')-$('#teamname').val().length) + " char's)");
	
	                $('#teamname').keyup(function(){
	                    // get new length of characters
	                    $('#teamname-counter').html("("+($('#teamname').attr('maxlength')-$('#teamname').val().length) + " char's)");
	                }).blur(function(){
	                    if($(this).val().length > 0){
	                        $(this).addClass('reg-comp');
	                    }else{
	                        $(this).removeClass('reg-comp');
	                    }
	                    setSubmitState();
	                });
	
	                $('#email').blur(function(){
	                    if($(this).val().length > 0 && validateEmail($(this).val())){
	                        $(this).addClass('reg-comp');
	                    }else{
	                        $(this).removeClass('reg-comp');
	                    }
	                    setSubmitState();
	                });
	
	                $("#teamname").keyup(function(e){
	                    clearTimeout($.data(this, 'timer'));
	                    var oObj = this;
	                    var wait = setTimeout(function(){setSubmitState()}, 500);
	                    $(this).data('timer', wait);
	                }).blur(setSubmitState);
	
	                $('#termsok').click(
	                    setSubmitState()
	                )
	
	                $('#lterms a').click(function(){
	                    $.colorbox({href:'/terms-and-conditions/popup/',width:"737px",height:"570px",iframe:true,scrolling:false});
	                    return false;
	                });
	
	                $('#save').click(function(){
	                    oSave();
	                    return false;
	                });
	
	            }
	            
	          	$('#autosel, #autoSelPitch').click(function(){                   	                            
	              autoComplete();
	              saveTeamToCookie();
	              drawCaterpillar();
	              $('.slideLeft.listView').trigger('click')
	              return false;
	            });
	
	            $('#teamSaved, #platPop, #goldPop').click(function(){
	                $(this).hide();
	            });
	
	            $('#headerBanner').click(function(e){
	                hideInstructions();
	                $('#platPop').hide();
	                $('#goldPop').hide();
	                var offset = $(this).offset();
	                //alert(e.clientX - offset.left);
	                //alert(e.clientY - offset.top);
	                if((e.clientX - offset.left) < 350){
	                    $('#goldPop').show();
	                }else{
	                    $('#platPop').show();
	                }
	            });

	            $('#footerBanner').click(function(e){
	                hideInstructions();
	                $('#platPop').hide();
	                $('#goldPop').hide();
	                var offset = $(this).offset();
	                if((e.clientX - offset.left) < 600){
	                    if($('#sTStatus').text() == "Platinum"){
	                        return;
	                    }
	                    //$('#goldPop').show();
	                    $('#sTStatus').text('Gold');
	                    $('#sTCredits').text('3');
	                    $(this).attr('src',sContentUrl+'/images/team/footer/940x120-plat-upgrade.jpg')
	                }else{
	                    //$('#platPop').show();
	                    $('#sTStatus').text('Platinum');
	                    $('#sTCredits').text('7');
	                }
            	});
         
	            $('#submit').click(function(){
	                return oConfirm();
	            });
	            
	
	            $('button.filter').click(function(){
	            	clearTimeout(scrollTimeout)
	            	if(	$('.filterContent').css('display') === 'none') {
	              	$('.filterContent').show();
	              	$('button.filter').html('Apply Filters');
	              	var sHtml = '<div id="filterCheck"><label id="hidePlayers">Hide Injured / Suspended players<input type="checkbox" id="hidePla" name="hidePla"></label></div>' 
	              	sHtml += '<div class="showMe"><h3>Show me</h3>';
	              	sHtml += '<ul><li class="current">Season pts</li><li>Month pts</li><li>Week pts</li><li>Most Picked %</li><li>14/15 XI</li><li>DT PICKS</li></ul></div>';
	              	sHtml += '<div class="sortBy"><h3>Sort by</h3><ul><li>Name</li><li>Club</li><li class="current">Value</li><li>Points</li></ul></div>';
	              	$('.filterContent').html(sHtml);
	              }
	              else {
	              		$('.filterContent').hide();
	              		$('button.filter').html('Show Filters');	
	              }
						});
								
						$(document).mouseup(function (event) {
						    var container = $(".filterContent");
						    var toggle = $("button.filter");
						    if (!container.is(event.target) && container.has(event.target).length === 0 && !toggle.is(event.target) )  {
						        container.hide();
						       $('button.filter').html('Show Filters');
						    }
						});


            $('#resetteam').click(function(){

                if(!iTransfers){
                    //var agree = confirm("Are you sure you want to reset your selections?");
                    //if (!agree){
                    //    return false;
                    //}
                }else{
                    iTransfersLeft = 3;
                    resetTeam();
                }

                for(var z=1;z<=iTeamSize;z++){
                    if(iTransfers){
                        $('#p'+z).attr('value',aOriginalTeam[(z-1)]);
                    }else{
                        $('#p'+z).attr('value','');
                    }
                }
                                   
                saveTeamToCookie('MFDTSelTeam');
                $('#js-players .myPlayers').remove();
                $('.player-row .plusMinus').html('+')

                oDrawPitch();
                return false;
            });


            }

						function drawTableView(){
                var aRows=[];
            	for(var j=1;j<=iTeamSize;j++){
            		var sCurrPla = $('#p'+j).attr('value');

            		if(sCurrPla && oPlayers[sCurrPla]) {
                        var oPlayerData = oPlayers[sCurrPla];
			            			var sAvail = getAvailability(oPlayerData.avail); 
                        var playerUnavailable = '';
                        if (sAvail === 'icoUna' || oPlayerData.news === 1) {
                        	playerUnavailable = 'playerUnavailable';
                        	sAvail = 'icoUna';
                        }			       
                             			                       
                        var sRow = '<tr id="p' + sCurrPla + '" class="removePlayer '+playerUnavailable+'"><td><a href="/statistics/popup/player/'+sCurrPla+'/"class="pla-inf sprite '+sAvail+' iconInfo"></a>' + aGroup3Char[oPlayerData.g] + '</td><td class="tabName">' + oPlayerData.n + '</td><td>' + oPlayerData.t + '</td><td>' + oPlayerData.v + '</td><td>' + oPlayerData.pk + '</td><td class="points listViewRemoveIco"><div>' + oPlayerData.p + '</div><span></span</td></tr>';
                        aRows.push(sRow);
            		}

                    $('#listView table tbody').html(aRows.join(""));
                    
                    $('#listView').on('click', '#p'+sCurrPla+'.removePlayer', function(){
              				oRemovePlayer($(this).attr('id').substring(1,5));
                    	oDrawPitch();
              			});            			                    
            	}
            }            
            
            function oSelectFromList(iPlaId){
                for(var i=1;i<=iTeamSize;i++){
                    if($('#p'+i).attr('value') == iPlaId){
                        oRemovePlayer(iPlaId);
                        oDrawPitch();
                        $('#'+iPlaId+'.player-row div.plusMinus').html('+')
                        return;
                    }
                    
                }
                oSelectPlayer(iPlaId);
            }

            function oSelectPlayer(iPlaId){

                /* Hide on this action */
                $('#miff-firsttime').hide();

            	if(!iPlaId){alert('Unable to determine player ID');return;}

            	/* how many players for his player group */
                var oPlayer = oPlayers[iPlaId];
                if(oPlayer == undefined){
                    return;
                }

            	/* Count all existing players to see how many players in this position we have */
                var iPlaPosCount = 1;
                var iMaxInPosition = aMaxForm[(aGroupCat[oPlayer.g]-1)]; // add 1 to it for a sub of each type

                /* Load in existing Players */
                var aCurrPlayers = new Array;
                var aTotalPlayers = new Array;

                for(var i=1;i<=iTeamSize;i++){
                    var iCurrId = $('#p'+i).attr('value');
                    if(iCurrId){
                        aCurrPlayers.push(iCurrId);
                        aTotalPlayers.push(iCurrId);
                    }
                }

                var iTeamWatch = 1; /* Set to 1 as this is the player we are picking */
                for(var i=0;i<=(aTotalPlayers.length-1);i++){
                    var sCurrPla = aCurrPlayers[i];
                    if(sCurrPla){

                        /* Check they are not in team */
                        if(sCurrPla == iPlaId){
                            return;
                        }

                        /* Check Team retrictions */
                        if(oPlayers[sCurrPla].t == oPlayer.t){
                            iTeamWatch++;
                            if(iTeamWatch>aMaxPerTeam){
                                errorV2("You are unable to select any more players from '"+aTeamNames[oPlayer.t]+"'");
                				return;
                            }
                        }
                    }
                }

                /* Add in the new player and validate the formation */
                aCurrPlayers.push(iPlaId);
                var sFormation = getFormation(aCurrPlayers);
                if(!sFormation){
                	if(aGroupName[oPlayer.g] === 'Goalkeeper') {
                	errorV2("You can only choose one Goalkeeper!");
                }
                else {
                  errorV2("Invalid player formation, you can't pick any more "+aGroupName[oPlayer.g]+"s <br/><br/>The 11 players you select on the pitch must fit into one of the following two formations:<br/><br />4-4-2 or 4-3-3");
               	}
               	return;
                }else{
                  /* Set the player id as picked */
                	for(var i=1;i<=iTeamSize;i++){
                		if($('#p'+i).attr('value') == ""){
                			$('#p'+i).attr('value',iPlaId);
                            break;
                		}
                	}

									if ($('tr#p'+iPlaId).text() !== 'Player Added') {
									var playerRow = $('tr#p'+iPlaId).html();
									var index = $('tr#p'+iPlaId+' td').length;
									$('tr#p'+iPlaId).html('')
              		$("<td colspan="+index+" class='plaAdded'>Player Added</td>")
									    .appendTo($('tr#p'+iPlaId))
									    .timeout(1000, function(){ this.fadeOut('slow');})
									    .timeout(1500, function(){this.remove(); $('tr#p'+iPlaId).html(playerRow);})
									}
									$('#js-players').append('<img class="myPlayers" id="pl'+iPlaId+'" src="/sfdt/mobile/images/players/'+iPlaId+'.jpg" onerror="this.src=\'/sfdt/mobile/images/players/blank.jpg\'">')
                	$('#'+iPlaId+'.player-row div.plusMinus').html('-')
                	saveTeamToCookie();
                }
            }

            function oRemovePlayer(iPlaId){
            	for(var j=1;j<=iTeamSize;j++){
            		if($('#p'+j).attr('value') == iPlaId){
            			$('#p'+j).attr('value','');
            			$('#js-players img#pl'+iPlaId).remove();
                  /* move players list */
                  //$('div.player-tab').gfmtabs('openItem',(aGroupCat[oPlayers[iPlaId].g]-1));
                  oDrawPlayerList(aPlaTables[aGroupCat[oPlayers[iPlaId].g]-1]);
                  saveTeamToCookie();
            			return;
            		}
            	}
            }

            function oDrawPitch(){
            	
            	// this function puts all the players in place on the pitch
                var aCurrPlayers = new Array();
                for(var i=1;i<=iTeamSize;i++){
            		var iCurrId = $('#p'+i).attr('value');
                    if(iCurrId){aCurrPlayers.push(iCurrId)};
                }

                var sFormation = getFormation(aCurrPlayers);
                if (!sFormation){return}
                setFormation(sFormation);
                teamTidyUp(sFormation);
                
                /* Remove row highlight */
               	$('.plalist .player-row.unavail').removeClass('unavail');
                $('.plalist .player-row.picked').removeClass('picked');
                $('.plalist .player-row .lplaInf').css( "border-bottom", "1px solid #a9a9a9" );

                /* Remove also unbinds */
                $('#pitch .pla-icon').remove();

                /* Build team icons */
            	iRunningTotalValue = 0;
                var ClubWatch = new Array;
            	for(var j=1;j<=iTeamSize;j++){
            		var sCurrPla = $('#p'+j).attr('value');
            		if(sCurrPla && oPlayers[sCurrPla]) {
                	createIcon(j);

                	$('.plalist .player-row#'+sCurrPla).addClass('picked');
                	$('.plalist .player-row#'+sCurrPla+' div.plusMinus').html('-');
                	$('.plalist .player-row#'+sCurrPla).prev().find('.lplaInf').css( "border-bottom", "none" );
            			iRunningTotalValue += parseFloat(oPlayers[sCurrPla].v);
            			iRunningTotalValue.toFixed(1);

                        /* count how many times its been used */
                        if(ClubWatch[oPlayers[sCurrPla].t] == undefined){
                            ClubWatch[oPlayers[sCurrPla].t] = 0;
                        }
                        ClubWatch[oPlayers[sCurrPla].t]++;
                        if(ClubWatch[oPlayers[sCurrPla].t] >= aMaxPerTeam){
                            $('.t'+oPlayers[sCurrPla].t+':not(.picked)').addClass('unavail');
                        }
            		}
            	}
            	if (aCurrPlayers.length === 11) {
              	$('.plalist .player-row:not(.picked)').addClass('unavail');
              }

            	iRunningTotalValue = parseFloat(iRunningTotalValue);
            	iRunningTotalValue = iRunningTotalValue.toFixed(1);
            	$('#budget').html("<span class='budgetVal'>&pound;"+iRunningTotalValue+"</span>M");
                iRunningTotalValue = parseFloat(iMaxValue-iRunningTotalValue).toFixed(1);
                $('.budgetRemain').html("<span class='budgetVal'>&pound;"+iRunningTotalValue+"</span>M");

                /* Values set here to save recalculation later */
                if(iRunningTotalValue < iMostExpensive){
                     iRunningTotalValue = parseInt(iRunningTotalValue*100);
                     /* Loop players to mark as unavailable price wise */
                     $('.plalist .player-row:not(.picked) p.lpric').each(function(){
                        var Val = $(this).text().replace(/[^0-9.-]/g,'');
                        Val = parseFloat(Val).toFixed(1);
                        Val = parseInt(Val*100);
                        if(Val > iRunningTotalValue){
                            $(this).parent().addClass('unavail');
                        }
                     });
                }

                drawTableView();

                //setSubmitState();

								$('#pitch .pla-icon').show();

            }

				
            function errorV2(sMessage){
                $.colorbox({width:"300", height:"240", inline:true, scrolling:false,
	      				onComplete: function() { $('#cboxLoadedContent').html('<div class="colorboxContent"><p>'+sMessage+'</p></div>'); }
							});
            }

            function createIcon(iIconNo){
                /* Function to create the player icon on the pitch */
                var sCurrPla = $('#p'+iIconNo).attr('value');
                var oPlayerData = oPlayers[sCurrPla];

                var sKeeper = (iIconNo == 1) ? '-keep' : '';

                var aName = oPlayerData.shortname.split(", ");
                var sName = "";
                if(aName.length == 2){
                    sName = aName[0];
                }else{
                    sName = oPlayerData.shortname;
                }

								var sAvail = getAvailability(oPlayerData);
								var plaUna = '';
								if (sAvail === 'icoUna') {
	              	plaUna = 'playerUnavailable';
	              }

                var sIcon = "<div id='icon"+iIconNo+"' class='pla-icon player-sel icon"+iIconNo+" "+plaUna+"' plaId='"+sCurrPla+"'>";
                sIcon += "<img class='pla-img' src='"+sContentUrl+"/sfdt/mobile/images/players/"+sCurrPla+".jpg' onerror=\"this.src='"+sContentUrl+"/sfdt/mobile/images/players/blank.jpg'\" />";
                sIcon += "<p class='pla-name'>"+sName+"</p>";
                sIcon += "<p class='pla-team'>"+oPlayerData.t+"</p>";
                sIcon += "<p class='pla-price'>&pound;"+oPlayerData.v+"m</p>";
                sIcon += "<a href='/statistics/popup/player/"+sCurrPla+"/'</a>";
                sIcon += "<div class='pla-avail'><span class='sprite "+sAvail+"'></span></div>";
                sIcon += "<p class='pla-rem'></p>";
                sIcon += "</div>";

                $('#pitch').append(sIcon);

								$('#icon'+iIconNo).mousedown(function() {
									dragging=0;
								    $(document).mousemove(function(){
								       dragging = 1;
								    });
								});
								
								$(document).mouseup(function(){
								    $(document).unbind('mousemove');
								});
                               
                /* Set click event */
                $('#icon'+iIconNo).click(function(){
                	if (dragging == 0){
                    openIconInfo($(this).attr('plaId'));
                    return false;
                  }
                  else return false;
                });
								$('#icon'+iIconNo).draggable({
										containment: "body",
                    helper: 'clone',
                    start: function(event, ui) {			
					            $('.droppableContainer').show();
						        },
						        stop: function(event, ui) {
				            	$('.droppableContainer').hide();
				        		} 
                });	

            }
            
           	
            	
            function autoComplete(NoFilter){

                // Checks to see if already in team
                var ClubWatch = new Array;
                var iCurrBudget  = 0;

                /* Load in existing Players */
                var aCurrPlayers = new Array;
                for(var i=1;i<=iTeamSize;i++){
                    var iCurrId = $('#p'+i).attr('value');
                    if(iCurrId){
                        aCurrPlayers.push(iCurrId);
                        iCurrBudget += parseFloat(oPlayers[iCurrId].v);

                        if(ClubWatch[oPlayers[iCurrId].t] == undefined){
                            ClubWatch[oPlayers[iCurrId].t] = 0;
                        }

                        ClubWatch[oPlayers[iCurrId].t]++;
                    }
                }

                iCurrBudget.toFixed(1);

                /* Find all available possible */
                var aFormCheck = new Array(0,0,0,0);
                for(var i=0; i<=aCurrPlayers.length;i++) {
                    var iPlaId = aCurrPlayers[i];
                    if(iPlaId){
                        aFormCheck[(aGroupCat[oPlayers[iPlaId].g]-1)]++;
                    }
                }
                var aValidFormations = aFormations.slice();;
                for( i = 0 ; i < aFormations.length ; i++ ) {
                    var aPositions = new Array;
                    aPositions = aFormations[i].split('');
                    for( j = 0 ; j < aPositions.length ; j++ ){
                        if(aFormCheck[j]>aPositions[j]){
                            aValidFormations[i]='';
                        }
                    }
                }

                var aLoopForm = new Array;
                for( i = 0 ; i < aValidFormations.length ; i++ ) {
                    if(aValidFormations[i]){
                        aLoopForm.push(aValidFormations[i]);
                    }
                }

                var rand_no = Math.floor(aLoopForm.length*Math.random());
            	sNewForm = aLoopForm[rand_no];

            	for(var j=1;j<=sNewForm.length;j++){

            		var iNoToFind = sNewForm.substr((j-1),1)-aFormCheck[(j-1)];

            		var tmpPla = new Array;
            		$.each(oPlayers, function(k,v) {

                        if(aGroupCat[v.g] == j && v.s == 'RSCURENT'){
                            v.id = k;
                            tmpPla.push(v);
                        }
                    });

                    function sortValue(a, b) {
                        return b.v - a.v;
                    }

                    tmpPla.sort(sortValue);
                    if(NoFilter != 1){
                        tmpPla = tmpPla.splice(0,40);
                    }

                    var iMaxLoopage = 20;
            		for(var i=0;i<iNoToFind;i++){

            			var iFound = 1;
            			var sNewPla = "";
                        var iLoopWatch = 0;
            			while(iFound){
            				iFound = 0;

                            iLoopWatch++;
                            if(iLoopWatch > iMaxLoopage){
                                break;
                            }

            				var rand_no = Math.floor(tmpPla.length*Math.random());
            				sNewPla = tmpPla[rand_no];

                            if(!oPlayers[sNewPla.id]){
                                iFound = 1;
                                continue;
                            }

                            var NewTeam = oPlayers[sNewPla.id].t;
                            var NewTeamCount = 1;
                            var EuroCount = 0;
                            for(var z=1;z<=iTeamSize;z++){
                                var plaPin= $('#p'+z).attr('value');
            					if(!plaPin){continue;}
            					if(oPlayers[plaPin].t == NewTeam){
            					    NewTeamCount++;
            					}
								if(oPlayers[plaPin].league != 'PL'){
								    EuroCount++;
								}
            				}

                            if(NewTeamCount >= 11){
                                iFound = 1;
                                continue;
                            }

                            if(ClubWatch[oPlayers[sNewPla.id].t] >= 11){
                                iFound = 1;
                                continue;
                            }

                            if(oPlayers[sNewPla.id].v > (iMaxValue-iCurrBudget)){
                                iFound = 1;
                                continue;
                            }

                            /* Is it in the current team */
            				for(var z=1;z<=iTeamSize;z++){
            					var iMyPlayer = $('#p'+z).attr('value');

            					if(iMyPlayer == sNewPla.id){
            						iFound = 1;
                                    break;
            					}
            				}
            			}

                        if(iLoopWatch > iMaxLoopage){
                            if(NoFilter != 1){
                                autoComplete(1);
                                return;
                            }
                            alertV2('Looks like you may have spent too much and need to amend your squad');
                            break;
                        }

                        ClubWatch[oPlayers[sNewPla.id].t]++;
                        iCurrBudget += parseFloat(oPlayers[sNewPla.id].v);
                        iCurrBudget.toFixed(1);

            			oSelectPlayer(sNewPla.id);
            		}
            	}

                oDrawPitch();
            	return false;
            }            	
                            
            
            function openIconInfo(plaId){
            	var playerName = oPlayers[plaId].n;
            	removePlayer = false;
            	$.colorbox({width:"300", height:"260", inline:true, scrolling:false,
            		onClosed: function () {if(removePlayer !== false) { popupRemovePlayer(removePlayer);} }, 
	      				onComplete: function() { $('#cboxLoadedContent').html('<div class="colorboxContent"><h1 class="iconInfoName">'+playerName+'</h1><a href="/statistics/player/'+plaId+'/" class="buttonL secondBtn">View Stats</a><button onclick="$.colorbox.close();removePlayer='+plaId+';" class="buttonL secondBtn">Remove Player</button></div>'); }
							});
            }    
            
            function popupRemovePlayer(removePlayerId) {	
							if (removePlayerId !== false) {
								oRemovePlayer(removePlayerId);
								oDrawPitch();
							}
						}  
											
            
            function getFormation(aTeam){
                var aFormCheck = new Array(0,0,0,0);
                for(var i=0; i<=aTeam.length;i++) {
                	if($('span#currFormVal').length) {
                  	$('span#currFormVal').html(aFormCheck[1]+'-'+aFormCheck[2]+'-'+aFormCheck[3])
                  }
                    var iPlaId = aTeam[i];
                    if(iPlaId){
                        if(oPlayers[iPlaId]){
                            aFormCheck[(aGroupCat[oPlayers[iPlaId].g]-1)]++;

                        }else{
                            alert(iPlaId);
                        }
                    }
                }

                var aValidFormations = aFormations.slice();
                for( i = 0 ; i < aFormations.length ; i++ ) {
                    var aPositions = new Array;
                    aPositions = aFormations[i].split('');
                    for( j = 0 ; j < aPositions.length ; j++ ){
                        if(aFormCheck[j]>aPositions[j]){
                            aValidFormations[i]='';
                        }
                    }
                }

                /* Get first valid formation and say thats it */
                var sCurrFormation = '';
                var iFound = 0;
                for( i = 0 ; i < aValidFormations.length ; i++ ) {
                    if(aValidFormations[i]){
                        return aValidFormations[i];
                    }
                }

                return
            }
            
            function setFormation(sCurrFormation){
                /* Set the formation */
                $('#pitch').removeClass();
                $('#pitch').addClass("f"+sCurrFormation);
            }
            
            function teamTidyUp(sFormation) {

            	var iNumPlayersMoved;
            	Finished = 0;

                /* pull out all the data to find out what we have picked */
                var aPickedPlayers = new Array();
            	for(i=1;i<=iTeamSize;i++){
                    var sCurrPla = $('#p'+i).attr('value');
            		if(!sCurrPla){break};
                    aPickedPlayers.push(sCurrPla);
                }

                if(aPickedPlayers.length==iTeamSize){
                    /* We have a complete team so all we need to do is order it and it should be correct */
                    aPickedPlayers.sort();
                    for(i=1;i<=iTeamSize;i++){
                        var sCurrPla = $('#p'+i).attr('value',aPickedPlayers[i-1]);
                    }
                    return;
                }

            	while( Finished == 0 ) {
            		iNumPlayersMoved = 0;
            		for( i = 1 ; i <= iTeamSize ; i++ ) {
            			var sCurrPla = $('#p'+i).attr('value');
            			if(!sCurrPla){continue}

            			if(oGetGroupForPosition(i,sFormation) == oPlayers[sCurrPla].g){
            				continue;
            			}
            			newPosition = oGetEmptySlot(oPlayers[sCurrPla].g,sFormation);
            			if(newPosition == -1){   // No empty slot available, so just continue
            				continue;
            			}
                        $('#p'+newPosition).attr('value',sCurrPla);
                        $('#p'+i).attr('value','');
            			iNumPlayersMoved++;
            		}

            		if( iNumPlayersMoved == 0 ) {
            			Finished = 1;
            		}
            	}
            }
            
            
            function oGetGroupForPosition( position,sFormation) {
            	var iRunningTotal = 1;
            	for( var i = 0 ; i < sFormation.length ; i++ ) {
            		var iNumForCategory = parseInt(sFormation.substr(i,1));

            		for( var j = 0 ; j < iNumForCategory ; j++ ) {
            			if( position == j + iRunningTotal ) {
            				return(aCatGroup[i]);
            			}
            		}
            		iRunningTotal += iNumForCategory;
            	}
            }            
            
            function oGetEmptySlot(positionGroup,sFormation) {
            	iRunningTotal = 1;

            	for( var i = 0 ; i <sFormation.length ; i++ ) {
            		var iMaxForCategory = parseInt(sFormation.substr(i,1));
            		if( positionGroup != aCatGroup[i] ) {   // is this the right position
            			iRunningTotal += iMaxForCategory;
            			continue;
            		}
            		for( j = iRunningTotal ; j < iRunningTotal + iMaxForCategory ; j++ ) {
            			if(!$('#p'+j).attr('value')){return j;}
            		}
            		iRunningTotal += iMaxForCategory;
            	}
            	return -1;
            }                 

        		function assignEvents(sTableId){
        			var dragging;
            	 $(".pla-img, .rowLeft").click(function () {
            	 	if (!dragging) {
            	 	var plaId = $(this).closest('.player-row').attr('id');
                   	window.location = '/statistics/player/'+plaId+'';
                    return false;
                  }
                }); 
            	$(".pla-img, .rowLeft").mousedown(function() {
									dragging=0;
								    $(document).mousemove(function(){
								    	
								       dragging = 1;
								    });
								});
								$(document).mouseup(function(){
								    $(document).unbind('mousemove');
								    dragging = 0;
								});
								
              $('#'+sTableId+' div.plusMinus').click(function(){  
              	  if (dragging == 0){
                    oSelectFromList($(this).closest('.player-row').attr('id'));
                    oDrawPitch();
                  }
                  else return false;
              });
        		}   
        		
        		
    }
})();



(function ($) {
    var methods = {
        init: function (options) {

            var settings = $.extend({
                defaultClass: 'on',
                defaultElement: 0,
                changetab: undefined
            }, options);

            return this.each(function () {
                var $tabsObj = $(this);
                $tabsObj.data('gfmtabs-options', settings);

                var iCurrOpen = settings.defaultElement;

                $tabsObj.children('ul').find('li a').not('[target]').each(function (intIndex) {
                    $(this).bind("click", function () {
                        $tabsObj.gfmtabs('openItem', intIndex);
                        return false;
                    }).bind("mouseenter", function () {
                        //alert('over')
                    });

                    if ($(this).parent().hasClass('on')) {
                        iCurrOpen = intIndex;
                    }

                });
                $tabsObj.gfmtabs('openItem', iCurrOpen);
            });

        },
        destroy: function () {

            return this.each(function () {
                $(window).unbind('.gfmtabs');
            });

        },
        openItem: function (iIndex) {
            var $tabsObj = $(this),
                options = $tabsObj.data('gfmtabs-options');

            /* look for any that may be on */
            $tabsObj.children('ul').find('li').removeClass(options.defaultClass);
            $tabsObj.children('ul').find('li').eq(iIndex).addClass(options.defaultClass);

            $tabsObj.children('div').css('display', 'none');
            if ($tabsObj.children('div').eq(iIndex).hasClass('gfmtab-nofade')) {
                $tabsObj.children('div').eq(iIndex).show();
            } else {
                $tabsObj.children('div').eq(iIndex).fadeIn().addClass('gfmtab-nofade');
            }

            // call back if there is one
            if (typeof options.changetab == 'function') {
                options.changetab(iIndex);
            }
        }
    };

    $.fn.gfmtabs = function (method) {

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.tooltip');
        }
    };

})(jQuery);


(function($){ 
    $.fn.timeout = function(ms, callback)
    {
        var self = this;
        setTimeout(function(){ callback.call(self); }, ms);
        return this;
    }
})(jQuery);
