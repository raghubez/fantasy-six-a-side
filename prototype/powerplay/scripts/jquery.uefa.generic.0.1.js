var lang;

function checkEnv(){
	var env;
	
	if (window.location.href.indexOf('dev-') !== -1) {
		env = 'dev';
	} else if(window.location.href.indexOf('test-') !== -1){
		env = 'test';
	} else {
		env = 'live';
	}	
	return env
}
    	
jQuery(document).ready(function($) {
  
  $('#logoutHL').click(function(event){
  	var newPath = window.location.href.split('/'),
  	oldPath = $(this).attr('href'),
  	fullPath;
  	
  	newPath = newPath.slice(0,4);
  	newPath = newPath.join('/');
  	  	
  	fullPath = oldPath.split('?returnurl=')[0] + '?returnurl=' + newPath;
  	
  	$(this).attr('href', fullPath);
  	
  	$.removeCookie('uefa_login', { path: '/' });
  	
  	//event.preventDefault();
  })
  
  //alert('ready');
  function initialize() {

    if ($('.js-ajaxLoadPageContent').length) {
      $('.js-ajaxLoadPageContent').click(function(event) {  
        event.preventDefault();
        if (! $(this).is("select")) {
          var sContentUrl = $(this).data("url");
          ajaxLoadPageContent(sContentUrl);
        }
        return false;
      });

      $('.js-ajaxLoadPageContent').change(function(event) {
        event.preventDefault();
        var sContentUrl;
        if ($(this).is("select")) {
          var sContentUrl = $(this).data("url");
          ajaxLoadPageContent(sContentUrl);
        }
        return false;
      });            
    }
  }  
                           
  $('#js-lang-english').click(function () {
    $.getJSON("/championsleague/json/lang/EN/", function(data){
      if(data.error){
        return false;
      }
    }).success(function(data) {
      //alert('changed language to English');
      lang = data; 
      location.reload();
    })
  });    
                      
  $('#js-lang-french').click(function () {
    $.getJSON("/championsleague/json/lang/FR/", function(data){
      if(data.error){
        return false;
      }
    }).success(function(data) {
      //alert('changed language to French');
      lang = data; 
      location.reload();
    })
  }); 
  
  $('#js-lang-german').click(function () {
    $.getJSON("/championsleague/json/lang/DE/", function(data){
      if(data.error){
        return false;
      }
    }).success(function(data) {
      //alert('changed language to German');
      lang = data; 
      location.reload();
    })
  }); 
  
  $('#js-lang-russia').click(function () {
    $.getJSON("/championsleague/json/lang/RU/", function(data){
      if(data.error){
        return false;
      }
    }).success(function(data) {
      //alert('changed language to Russian');
      lang = data; 
      location.reload();
    })
  });   
  
  $('#js-lang-spanish').click(function () {
    $.getJSON("/championsleague/json/lang/ES/", function(data){
      if(data.error){
        return false;
      }
    }).success(function(data) {
      //alert('changed language to Spanish');
      lang = data; 
      location.reload();
    })
  });  
  
  $('#js-lang-italian').click(function () {
    $.getJSON("/championsleague/json/lang/IT/", function(data){
      if(data.error){
        return false;
      }
    }).success(function(data) {
      //alert('changed language to Italian');
      lang = data; 
      location.reload();
    })
  }); 
  
  $('#js-lang-portuguese').click(function () {
    $.getJSON("/championsleague/json/lang/PT/", function(data){
      if(data.error){
        return false;
      }
    }).success(function(data) {
      //alert('changed language to Portuguese');
      lang = data; 
      location.reload();
    })
  });                            
	
  function ajaxLoadPageContent(sContentUrl){
    var sContentUrl = window.location.protocol + "//" + window.location.host + '/json' + sContentUrl;
    //console.log("AJAX:" + sContentUrl);
    $.ajax({
      url: sContentUrl,
      cache: false,
      contentType : "application/json; charset=utf-8",
      type: 'json',
      method: 'post',
      error:  function (err) {
        //console.log('Error', err);  
      },
      success: function (data) {      
      	//console.log(data);
        // replace the content on the current page
        document.getElementById(data.CONTENT_ID).innerHTML=data.CONTENT;
        // re-bind any onclick events in the content by
        // calling initialize()
        initialize();
      }
    });
  }
  
  // load up the language mapping
  $.getJSON("/championsleague/json/lang/", function(data){
    if(data.error){
      return false;
    }
  }).success(function(data) {
     lang = data;  
  });
  
  initialize();
});



















